// require the environment to configure itself, but only once.

var once = (function () {
    var executed = false;
    return function() {
        if (!executed) {
            executed = true;
            require('dotenv').config();
        }
    };
})();

once();

export default class VoordeelwagenDatabag {
    typeVoertuig;
    nummerplaat;
    brandstof = "BENZINE";
    co2uitstoot;
    eersteInschrijvingDIV;
    beginDatumVAA;
    eindDatumVAA;
    catalogusPrijsVoorKorting;
    opties;
    werkelijkBetaaldeBTW;
    kortingConstructeur;
    eigenBijdrage = {};
    wagenMetTankKaart = {};
    jaarAankoopLeaseOfRenting;
    isValseHybride = false;
    effectieveCO2ValseHybride;
    aanvangsdatumBoekjaar;

    static copy(input) {
        let tbr = new VoordeelwagenDatabag();

        tbr.typeVoertuig = input.typeVoertuig;
        tbr.nummerplaat = input.nummerplaat;
        tbr.brandstof = input.brandstof;
        tbr.co2uitstoot = input.co2uitstoot;
        tbr.eersteInschrijvingDIV = input.eersteInschrijvingDIV;
        tbr.beginDatumVAA = input.beginDatumVAA;
        tbr.eindDatumVAA = input.eindDatumVAA;
        tbr.catalogusPrijsVoorKorting = input.catalogusPrijsVoorKorting;
        tbr.opties = input.opties;
        tbr.werkelijkBetaaldeBTW = input.werkelijkBetaaldeBTW;
        tbr.kortingConstructeur = input.kortingConstructeur;
        tbr.eigenBijdrage = input.eigenBijdrage;
        tbr.wagenMetTankKaart = input.wagenMetTankKaart;
        tbr.jaarAankoopLeaseOfRenting = input.jaarAankoopLeaseOfRenting;
        tbr.isValseHybride = input.isValseHybride;
        tbr.effectieveCO2ValseHybride = input.effectieveCO2ValseHybride;
        tbr.aanvangsdatumBoekjaar = input.aanvangsdatumBoekjaar;
        return tbr;
    }

    equals(input){
        return this.typeVoertuig === input.typeVoertuig &&
            this.nummerplaat === input.nummerplaat &&
            this.brandstof === input.brandstof &&
            this.co2uitstoot === input.co2uitstoot &&
            this.eersteInschrijvingDIV === input.eersteInschrijvingDIV &&
            this.beginDatumVAA === input.beginDatumVAA &&
            this.eindDatumVAA === input.eindDatumVAA &&
            this.catalogusPrijsVoorKorting === input.catalogusPrijsVoorKorting &&
            this.opties === input.opties &&
            this.werkelijkBetaaldeBTW === input.werkelijkBetaaldeBTW &&
            this.kortingConstructeur === input.kortingConstructeur &&
            this.eigenBijdrage === input.eigenBijdrage &&
            this.wagenMetTankKaart === input.wagenMetTankKaart &&
            this.jaarAankoopLeaseOfRenting === input.jaarAankoopLeaseOfRenting &&
            this.isValseHybride === input.isValseHybride &&
            this.effectieveCO2ValseHybride === input.effectieveCO2ValseHybride &&
            this.aanvangsdatumBoekjaar === input.aanvangsdatumBoekjaar
    }
}


import auth from '../../submodules/auth/auth';

export default [
    {
        path: '/',
        name: 'root',
    },
    {
        path: '/login',
        name: 'login',
        component: () => import('../../submodules/auth/login')
    },
    {
        path: '/send-reset',
        name: 'send-reset',
        component: () => import('../../submodules/auth/send-reset')
    },
    {
        path: '/reset-password/:resetToken',
        name: 'reset-password',
        component: () => import('../../submodules/auth/reset-password'),
        props: true
    },
    {
        path: '/update-password',
        name: 'update-password',
        component: () => import('../../submodules/auth/change-password'),
        meta: {
            middleware: auth
        },
    },
    {
        path: '/rekentools',
        name: 'calculators-index',
        component: () => import('./components/Calculators'),
        meta: {
            middleware: auth
        },
    },
    {
        path: '*',
        name: '404',
        component: () => import('../../submodules/common/resources/js/components/NotFoundComponent')
    },
];

import auth from '../../submodules/auth/auth';

export default [
    {
        name: 'taxshelter',
        path: '/taxshelter',
        display: 'tax shelter',
        component: () => import('../../submodules/tax-shelter/resources/js/components/TaxShelter'),
        meta: {
            middleware: auth
        },
    },
    {
        name: "venb",
        path: "/venb",
        display: 'venb',
        component: () => import('../../submodules/venb/resources/js/components/Venb')
    },
    {
        name: 'voordeelwagen',
        path: '/voordeelwagen',
        display: 'voordeelwagen',
        component: () => import('./components/VoordeelWagen')
    },
    {
        name: 'gramformula',
        path: '/gramformula',
        display: 'gramformule',
        component: () => import('../../submodules/gramformule/resources/js/components/GramFormula')
    }
]

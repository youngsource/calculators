require('./envloader');
const path = require('path');
const merge = require("webpack-merge");
const common = require("./webpack.dev.js");
const webpack = require("webpack");

module.exports = merge(common, {
    output: {
        publicPath: 'http://localhost:' + process.env.APP_HOT_PORT + '/dist/',
    },
    devServer: {
        hot: true,
        inline: true,
        host: '0.0.0.0',
        disableHostCheck: true,
        port: process.env.APP_HOT_PORT,
        contentBase: path.join(__dirname, 'dist'),
        historyApiFallback: true,
        headers: {
            'Access-Control-Allow-Origin' : '*'
        },
        watchOptions: {
            poll: true
        }
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
    ],
});

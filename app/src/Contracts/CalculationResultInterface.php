<?php
declare(strict_types=1);

namespace Laudis\Calculators\Contracts;

/**
 * Interface CalculationResultInterface
 * Encapsulates the result of a calculation by dividing it in logical subsections.
 *
 * @package Laudis\Calculators\Contracts
 */
interface CalculationResultInterface
{
    /** @var int default presentation mode */
    public const DEFAULT = 0x1;
    /** @var int iteratie presentation mode */
    public const EXTENDED = 0x2;
    /** @var int basic presentation mode */
    public const BASIC = 0x4;

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array;
}

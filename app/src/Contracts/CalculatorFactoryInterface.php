<?php
declare(strict_types=1);

namespace Laudis\Calculators\Contracts;

use Rakit\Validation\Validation;

/**
 * Interface CalculatorFactoryInterface
 * Builds the key components operating during the calculation strategy.
 *
 * @package Laudis\Calculators\Contracts
 */
interface CalculatorFactoryInterface
{
    /**
     * Returns the calculator.
     *
     * @param array $values
     * @return object  The calculator must have a calculate method which accepts the input from the request and returns
     *                  the result encapsulated in a CalculationResultInterface.
     */
    public function calculator(array $values): object;

    /**
     * Builds the input as a standard databag from the current server request.
     *
     * @param array $values
     * @return object
     */
    public function inputFromArray(array $values): object;

    /**
     * Derives the validation from the current input values.
     *
     * @param array $values
     * @return Validation
     */
    public function validation(array $values): Validation;
}

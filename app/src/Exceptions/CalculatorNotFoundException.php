<?php
declare(strict_types=1);

namespace Laudis\Calculators\Exceptions;

use Laudis\Common\Exceptions\ResourceNotFoundException;

/**
 * Class CalculatorNotFoundException
 * Encapsulates the exception in which a calculator cannot be found in the current state of the application.
 *
 * @package Laudis\Calculators\Exceptions
 */
final class CalculatorNotFoundException extends ResourceNotFoundException
{

}

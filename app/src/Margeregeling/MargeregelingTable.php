<?php


namespace Laudis\Calculators\Margeregeling;


class MargeregelingTable
{
    /** @var float */
    protected $kwartaal1Aankoop;

    /** @var float */
    protected $kwartaal1Verkoop;

    /** @var float */
    protected $kwartaal2Aankoop;

    /** @var float */
    protected $kwartaal2Verkoop;

    /** @var float */
    protected $kwartaal3Aankoop;

    /** @var float */
    protected $kwartaal3Verkoop;

    /** @var float */
    protected $kwartaal4Aankoop;

    /** @var float */
    protected $kwartaal4Verkoop;

    /**
     * BelgischeVerkopenEnICLInput constructor.
     * @param float $kwartaal1Aankoop
     * @param float $kwartaal1Verkoop
     * @param float $kwartaal2Aankoop
     * @param float $kwartaal2Verkoop
     * @param float $kwartaal3Aankoop
     * @param float $kwartaal3Verkoop
     * @param float $kwartaal4Aankoop
     * @param float $kwartaal4Verkoop
     */
    public function __construct(
        float $kwartaal1Aankoop,
        float $kwartaal1Verkoop,
        float $kwartaal2Aankoop,
        float $kwartaal2Verkoop,
        float $kwartaal3Aankoop,
        float $kwartaal3Verkoop,
        float $kwartaal4Aankoop,
        float $kwartaal4Verkoop)
    {
        $this->kwartaal1Aankoop = $kwartaal1Aankoop;
        $this->kwartaal1Verkoop = $kwartaal1Verkoop;
        $this->kwartaal2Aankoop = $kwartaal2Aankoop;
        $this->kwartaal2Verkoop = $kwartaal2Verkoop;
        $this->kwartaal3Aankoop = $kwartaal3Aankoop;
        $this->kwartaal3Verkoop = $kwartaal3Verkoop;
        $this->kwartaal4Aankoop = $kwartaal4Aankoop;
        $this->kwartaal4Verkoop = $kwartaal4Verkoop;
    }

    /**
     * @return float
     */
    public function getKwartaal1Aankoop(): float
    {
        return $this->kwartaal1Aankoop;
    }

    /**
     * @return float
     */
    public function getKwartaal1Verkoop(): float
    {
        return $this->kwartaal1Verkoop;
    }

    /**
     * @return float
     */
    public function getKwartaal2Aankoop(): float
    {
        return $this->kwartaal2Aankoop;
    }

    /**
     * @return float
     */
    public function getKwartaal2Verkoop(): float
    {
        return $this->kwartaal2Verkoop;
    }

    /**
     * @return float
     */
    public function getKwartaal3Aankoop(): float
    {
        return $this->kwartaal3Aankoop;
    }

    /**
     * @return float
     */
    public function getKwartaal3Verkoop(): float
    {
        return $this->kwartaal3Verkoop;
    }

    /**
     * @return float
     */
    public function getKwartaal4Aankoop(): float
    {
        return $this->kwartaal4Aankoop;
    }

    /**
     * @return float
     */
    public function getKwartaal4Verkoop(): float
    {
        return $this->kwartaal4Verkoop;
    }

    public function getTotalAankopen() : float
    {
        return array_sum([
            $this->getKwartaal1Aankoop(),
            $this->getKwartaal2Aankoop(),
            $this->getKwartaal3Aankoop(),
            $this->getKwartaal4Aankoop(),
        ]);
    }

    public function getTotalVerkoop() : float
    {
        return array_sum([
            $this->getKwartaal1Verkoop(),
            $this->getKwartaal2Verkoop(),
            $this->getKwartaal3Verkoop(),
            $this->getKwartaal4Verkoop(),
        ]);
    }

    public function output() : array
    {
        return [
            'aankopen' =>
                [
                    'kwartaal 1' => $this->getKwartaal1Aankoop(),
                    'kwartaal 2' => $this->getKwartaal2Aankoop(),
                    'kwartaal 3' => $this->getKwartaal3Aankoop(),
                    'kwartaal 4' => $this->getKwartaal4Aankoop(),
                    'totaal' => $this->getTotalAankopen()
                ],
            'verkopen' =>
                [
                    'kwartaal 1' => $this->getKwartaal1Verkoop(),
                    'kwartaal 2' => $this->getKwartaal2Verkoop(),
                    'kwartaal 3' => $this->getKwartaal3Verkoop(),
                    'kwartaal 4' => $this->getKwartaal4Verkoop(),
                    'totaal' => $this->getTotalVerkoop()
                ]
        ];
    }
}
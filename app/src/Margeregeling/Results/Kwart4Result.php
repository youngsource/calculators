<?php


namespace Laudis\Calculators\Margeregeling\Results;


use Composer\Package\Archiver\ArchivableFilesFilter;

class Kwart4Result
{
    /** @var float */
    private $begin;
    /** @var float */
    private $aankopenVHJaar;
    /** @var float */
    private $correctieExport;
    /** @var float */
    private $eindVoorraad;
    /** @var float */
    private $verkopenVHJaar;
    /** @var float */
    private $verkopenExport;
    /** @var float */
    private $reedsInAangifte;


    /**
     * Kwart4Result constructor.
     * @param float $begin
     * @param float $aankopenVHJaar
     * @param float $correctieExport
     * @param float $eindVoorraad
     * @param float $verkochteAankopen
     * @param float $verkopenVHJaar
     * @param float $verkopenExport
     * @param float $totaal
     * @param float $reedsInAangifte
     * @param float $nogOpTeNemen
     */
    public function __construct(
        float $begin,
        float $aankopenVHJaar,
        float $correctieExport,
        float $eindVoorraad,
        float $verkopenVHJaar,
        float $verkopenExport,
        float $reedsInAangifte)
    {
        $this->begin = $begin;
        $this->aankopenVHJaar = $aankopenVHJaar;
        $this->correctieExport = $correctieExport;
        $this->eindVoorraad = $eindVoorraad;
        $this->verkopenVHJaar = $verkopenVHJaar;
        $this->verkopenExport = $verkopenExport;
        $this->reedsInAangifte = $reedsInAangifte;
    }

    /**
     * @return float
     */
    public function getBegin(): float
    {
        return $this->begin;
    }

    /**
     * @return float
     */
    public function getAankopenVHJaar(): float
    {
        return $this->aankopenVHJaar;
    }

    /**
     * @return float
     */
    public function getCorrectieExport(): float
    {
        return $this->correctieExport;
    }

    /**
     * @return float
     */
    public function getEindVoorraad(): float
    {
        return $this->eindVoorraad;
    }

    /**
     * @return float
     */
    public function getVerkochteAankopen(): float
    {
        return array_sum([
            $this->getBegin(),
            $this->getAankopenVHJaar(),
            $this->getCorrectieExport(),
            $this->getEindVoorraad()
        ]);
    }

    /**
     * @return float
     */
    public function getVerkopenVHJaar(): float
    {
        return $this->verkopenVHJaar;
    }

    /**
     * @return float
     */
    public function getVerkopenExport(): float
    {
        return $this->verkopenExport;
    }

    /**
     * @return float
     */
    public function getTotaal(): float
    {
        return array_sum([
            $this->getVerkopenVHJaar(),
            -$this->getVerkochteAankopen(),
            $this->getVerkopenExport()
        ]);
    }

    /**
     * @return float
     */
    public function getReedsInAangifte(): float
    {
        return $this->reedsInAangifte;
    }

    /**
     * @return float
     */
    public function getNogOpTeNemen(): float
    {
        return array_sum([
            $this->getTotaal(),
            $this->getReedsInAangifte()
        ]);
    }

    public function output(int $jaar) : array
    {
        return
        [
            'Beginvoorraad ' . $jaar . ':'=> $this->getBegin(),
            'Aankopen van het jaar ' . $jaar . ':' => $this->getAankopenVHJaar(),
            'Correctie export:' => $this->getCorrectieExport(),
            'Eindvoorraad ' . $jaar . ':' => $this->getEindVoorraad(),
            'Verkochte aankopen ' . $jaar . ':' => $this->getVerkochteAankopen(),
            'Verkopen van het jaar ' . $jaar . ':' => $this->getVerkopenVHJaar(),
            'Verkopen export ' . $jaar . ':' => $this->getVerkopenExport(),
            'Totaal:' => $this->getTotaal(),
            'Reeds in aangifte' => $this->getReedsInAangifte(),
            'Nog op te nemen' => $this->getNogOpTeNemen()
        ];
    }
}
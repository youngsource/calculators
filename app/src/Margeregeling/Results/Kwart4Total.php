<?php


namespace Laudis\Calculators\Margeregeling\Results;


class Kwart4Total
{
    /** @var Kwart4Result */
    private $kwart4Result;
    /** @var kwart4btwAangifte */
    private $kwart4btwAangifte;

    /**
     * Kwart4Total constructor.
     * @param Kwart4Result $kwart4Result
     * @param kwart4btwAangifte $kwart4btwAangifte
     */
    public function __construct(Kwart4Result $kwart4Result, kwart4btwAangifte $kwart4btwAangifte)
    {
        $this->kwart4Result = $kwart4Result;
        $this->kwart4btwAangifte = $kwart4btwAangifte;
    }

    /**
     * @return Kwart4Result
     */
    public function getKwart4Result(): Kwart4Result
    {
        return $this->kwart4Result;
    }

    /**
     * @return kwart4btwAangifte
     */
    public function getKwart4btwAangifte(): kwart4btwAangifte
    {
        return $this->kwart4btwAangifte;
    }


    public function output(int $jaar) : array
    {
        return
        [
            'kwartaalResult' => $this->getKwart4Result()->output($jaar),
            'btwAangifte' => $this->getKwart4btwAangifte()->output()
        ];
    }

}
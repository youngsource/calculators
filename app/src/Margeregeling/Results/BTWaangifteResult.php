<?php


namespace Laudis\Calculators\Margeregeling\Results;


class BTWaangifteResult
{
    /** @var float */
    private $aankopenHandelsgoederen;
    /** @var float */
    private $export;
    /** @var float */
    private $mvHVerkopen;
    /** @var float */
    private $btwVerkopen;
    /** @var float */
    private $total;

    /**
     * BTWaangifteResult constructor.
     * @param float $aankopenHandelsgoederen
     * @param float $export
     * @param float $mvHVerkopen
     * @param float $btwVerkopen
     * @param float $total
     */
    public function __construct(
        float $aankopenHandelsgoederen,
        float $export ,
        float $mvHVerkopen ,
        float $btwVerkopen )
    {
        $this->aankopenHandelsgoederen = $aankopenHandelsgoederen;
        $this->export = $export;
        $this->mvHVerkopen = $mvHVerkopen;
        $this->btwVerkopen = $btwVerkopen;
    }

    /**
     * @return float
     */
    public function getAankopenHandelsgoederen(): float
    {
        return $this->aankopenHandelsgoederen;
    }

    /**
     * @return float
     */
    public function getExport(): float
    {
        return $this->export;
    }

    /**
     * @return float
     */
    public function getMvHVerkopen(): float
    {
        return $this->mvHVerkopen;
    }

    /**
     * @return float
     */
    public function getBtwVerkopen(): float
    {
        return $this->btwVerkopen;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return array_sum([
            $this->getMvHVerkopen(),
            $this->getBtwVerkopen()
        ]);
    }

    public function output() : array
    {
        return [
            'Aankopen handelsgoederen' => $this->getAankopenHandelsgoederen(),
            'Export' => $this->getExport(),
            'MvH verkopen' => $this->getMvHVerkopen(),
            'btw verkopen' => $this->getBtwVerkopen(),
            'total' => $this->getTotal()
        ];
    }

}
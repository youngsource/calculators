<?php


namespace Laudis\Calculators\Margeregeling\Results;


class kwart4btwAangifte extends BTWaangifteResult
{
    /** @var float */
    private $andereVrijgesteldeHandelingen;

    public function __construct(
        float $aankopenHandelsgoederen,
        float $export,
        float $andereVrijgesteldeHandelingen,
        float $mvHVerkopen,
        float $btwVerkopen)
    {
        parent::__construct($aankopenHandelsgoederen, $export, $mvHVerkopen, $btwVerkopen);
        $this->andereVrijgesteldeHandelingen = $andereVrijgesteldeHandelingen;
    }

    /**
     * @return float
     */
    public function getAndereVrijgesteldeHandelingen(): float
    {
        return $this->andereVrijgesteldeHandelingen;
    }

    /**
     * @return array
     */
    public function output(): array
    {
        return [
            'Aankopen handelsgoederen' => $this->getAankopenHandelsgoederen(),
            'Export' => $this->getExport(),
            'Andere vrijgestelde handelingen' => $this->getAndereVrijgesteldeHandelingen(),
            'MvH verkopen' => $this->getMvHVerkopen(),
            'btw verkopen' => $this->getBtwVerkopen(),
            'total' => $this->getTotal()
        ];
    }
}
<?php


namespace Laudis\Calculators\Margeregeling\Results;


class KwartaalTotalResult
{
    /** @var KwartaalResult */
    private $kwartaalResult;
    /** @var BTWaangifteResult */
    private $btwAangifte;

    /**
     * KwartaalTotalResult constructor.
     * @param KwartaalTotalResult $kwartaalResult
     * @param BTWaangifteResult $btwAangifte
     */
    public function __construct(KwartaalResult $kwartaalResult, BTWaangifteResult $btwAangifte)
    {
        $this->kwartaalResult = $kwartaalResult;
        $this->btwAangifte = $btwAangifte;
    }

    /**
     * @return BTWaangifteResult
     */
    public function getBtwAangifte(): BTWaangifteResult
    {
        return $this->btwAangifte;
    }

    /**
     * @return KwartaalTotalResult
     */
    public function getKwartaalResult(): KwartaalResult
    {
        return $this->kwartaalResult;
    }

    public function output() :array {
        return [
            'kwartaalResult' => $this->getKwartaalResult()->output(),
            'btwAangifte' => $this->getBtwAangifte()->output()
        ];
    }
}
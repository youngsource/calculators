<?php


namespace Laudis\Calculators\Margeregeling\Results;


class KwartaalResult
{
    /** @var float */
    private $begin;
    /** @var float */
    private $aankopenVHKwartaal;
    /** @var float */
    private $correctieExport;
    /** @var float */
    private $verkoopKwartaal;
    /** @var float */
    private $verkoopExport;

    /**
     * KwartaalResult constructor.
     * @param float $begin
     * @param float $aankopenVHKwartaal
     * @param float $correctieExport
     * @param float $verkoopKwartaal
     * @param float $verkoopExport
     * @param float $nogInStock
     */
    public function __construct(
        float $begin,
        float $aankopenVHKwartaal ,
        float $correctieExport,
        float $verkoopKwartaal,
        float $verkoopExport)
    {
        $this->begin = $begin;
        $this->aankopenVHKwartaal = $aankopenVHKwartaal;
        $this->correctieExport = $correctieExport;
        $this->verkoopKwartaal = $verkoopKwartaal;
        $this->verkoopExport = $verkoopExport;
    }

    /**
     * @return float
     */
    public function getBegin(): float
    {
        return $this->begin;
    }

    /**
     * @return float
     */
    public function getAankopenVHKwartaal(): float
    {
        return $this->aankopenVHKwartaal;
    }

    /**
     * @return float
     */
    public function getCorrectieExport(): float
    {
        return $this->correctieExport;
    }

    /**
     * @return float
     */
    public function getVerkoopKwartaal(): float
    {
        return $this->verkoopKwartaal;
    }

    /**
     * @return float
     */
    public function getVerkoopExport(): float
    {
        return $this->verkoopExport;
    }

    /**
     * @return float
     */
    public function getNogInStock(): float
    {
        return array_sum([
            $this->getBegin(),
            $this->getAankopenVHKwartaal(),
            $this->getCorrectieExport(),
            $this->getVerkoopKwartaal(),
            $this->getVerkoopExport(),
        ]);
    }

    public function output($beginOrEnd =  false) : array
    {
        $beginOrEnd ? $beginstring = 'beginVoorraad' :   $beginstring ='Overdracht vorig kwartaal:';
        return [
            $beginstring => $this->getBegin(),
            'aankopen van het kwartaal:' => $this->getAankopenVHKwartaal(),
            'correctie export:' => $this->getCorrectieExport(),
            'verkopen kwartaal:' => $this->getVerkoopKwartaal(),
            'verkopen export:' => $this->getVerkoopExport(),
            'Nog in stock:' => $this->getNogInStock()
        ];
    }
}
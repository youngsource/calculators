<?php


namespace Laudis\Calculators\Margeregeling;


use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\Margeregeling\Auxiliaries\MargeregelingAuxiliaries;
use Laudis\Calculators\Margeregeling\Inputs\MargeregelingInput;
use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

class MargeregelingCalculatorFactory implements CalculatorFactoryInterface
{
    /** @var MargeregelingAuxiliaries */
    private $auxiliaries;

    /**
     * VoordeelWagenCalculatorFactory constructor.
     * @param MargeregelingAuxiliaries $auxiliaries
     */
    public function __construct(MargeregelingAuxiliaries $auxiliaries)
    {
        $this->auxiliaries = $auxiliaries;
    }

    /**
     * @param array $values
     * @return object
     */
    public function inputFromArray(array $values): object
    {
        $belgischeVerkopenEnICL = new MargeregelingTable(
            to_float($values['bVerkoopEnICLKwartaal1Aankoop']) ?? 0.0,
            to_float($values['bVerkoopEnICLKwartaal1Verkoop']) ?? 0.0,
            to_float($values['bVerkoopEnICLKwartaal2Aankoop']) ?? 0.0,
            to_float($values['bVerkoopEnICLKwartaal2Verkoop']) ?? 0.0,
            to_float($values['bVerkoopEnICLKwartaal3Aankoop']) ?? 0.0,
            to_float($values['bVerkoopEnICLKwartaal3Verkoop']) ?? 0.0,
            to_float($values['bVerkoopEnICLKwartaal4Aankoop']) ?? 0.0,
            to_float($values['bVerkoopEnICLKwartaal4Verkoop']) ?? 0.0,

        );
        $export = new MargeregelingTable(
            to_float($values['exportKwartaal1Aankoop']) ?? 0.0,
            to_float($values['exportKwartaal1Verkoop']) ?? 0.0,
            to_float($values['exportKwartaal2Aankoop']) ?? 0.0,
            to_float($values['exportKwartaal2Verkoop']) ?? 0.0,
            to_float($values['exportKwartaal3Aankoop']) ?? 0.0,
            to_float($values['exportKwartaal3Verkoop']) ?? 0.0,
            to_float($values['exportKwartaal4Aankoop']) ?? 0.0,
            to_float($values['exportKwartaal4Verkoop']) ?? 0.0,
        );
        $year = $values['kalenderjaar'] ?? 2019;
        $input = new MargeregelingInput(
            $year,
            $belgischeVerkopenEnICL,
            $export,
            to_float($values['beginVoorraad']) ?? 0.0,
            to_float($values['eindVoorraad']) ?? 0.0
        );
        return $input;
    }

    /**
     * @param array $values
     * @return Validation
     */
    public function validation(array $values): Validation
    {
        $validator = new Validator([
            'required' => 'This field is required',
            'numeric' => 'This field needs to be a number'
        ]);

        return $validator->make($values,$this->margeRegelingRules());
    }

    /**
     * @return array
     */
    public function margeRegelingRules() : array {
        //TODO make rules
        return [];
    }


    /**
     * Returns the calculator.
     *
     * @param array $values
     * @return object  The calculator must have a calculate method which accepts the input from the request and returns
     *                  the result encapsulated in a CalculationResultInterface.
     */
    public function calculator(array $values): object
    {
        return new MargeregelingCalculator($this->auxiliaries);
    }
}
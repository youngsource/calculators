<?php


namespace Laudis\Calculators\Margeregeling\Inputs;


use Laudis\Calculators\Margeregeling\MargeregelingTable;

class MargeregelingInput
{
    /** @var int */
    private $kalenderjaar;
    /** @var MargeregelingTable */
    private $BelgischeVerkopenEnICL;
    /** @var MargeregelingTable */
    private $export;
    /** @var float */
    private $beginVoorraad;
    /** @var float */
    private $eindVoorraad;

    /**
     * MargeregelingInput constructor.
     * @param int $kalenderjaar
     * @param MargeregelingTable $BelgischeVerkopenEnICL
     * @param MargeregelingTable $export
     * @param float|null $beginVoorraad
     * @param float|null $eindVoorraad
     */
    public function __construct(
        int $kalenderjaar,
        MargeregelingTable $BelgischeVerkopenEnICL,
        MargeregelingTable $export,
        float $beginVoorraad,
        float $eindVoorraad
)
    {
        $this->kalenderjaar = $kalenderjaar;
        $this->BelgischeVerkopenEnICL = $BelgischeVerkopenEnICL;
        $this->export = $export;
        $this->beginVoorraad = $beginVoorraad;
        $this->eindVoorraad =  $eindVoorraad;
    }

    /**
     * @return int
     */
    public function getKalenderjaar(): int
    {
        return $this->kalenderjaar;
    }

    /**
     * @return MargeregelingTable
     */
    public function getBelgischeVerkopenEnICL(): MargeregelingTable
    {
        return $this->BelgischeVerkopenEnICL;
    }

    /**
     * @return MargeregelingTable
     */
    public function getExport(): MargeregelingTable
    {
        return $this->export;
    }

    /**
     * @return float
     */
    public function getBeginVoorraad(): float
    {
        return $this->beginVoorraad;
    }

    /**
     * @return float
     */
    public function getEindVoorraad(): float
    {
        return $this->eindVoorraad;
    }

}
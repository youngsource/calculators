<?php


namespace Laudis\Calculators\Margeregeling\Interfaces;


use Laudis\Calculators\Margeregeling\Inputs\MargeregelingInput;
use Laudis\Calculators\Venb\BasicCalculationResult;

Interface MargeregelingInterface
{
    /**
     * @param MargeregelingInput $input
     * @return BasicCalculationResult
     */
    public function calculate(MargeregelingInput $input) : BasicCalculationResult;
}
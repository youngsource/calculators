<?php

namespace Laudis\Calculators\Margeregeling;

use Laudis\Calculators\Margeregeling\Auxiliaries\MargeregelingAuxiliaries;
use Laudis\Calculators\Margeregeling\Inputs\MargeregelingInput;
use Laudis\Calculators\Margeregeling\Interfaces\MargeregelingInterface;
use Laudis\Calculators\Margeregeling\Results\BTWaangifteResult;
use Laudis\Calculators\Margeregeling\Results\kwart4btwAangifte;
use Laudis\Calculators\Margeregeling\Results\Kwart4Result;
use Laudis\Calculators\Margeregeling\Results\Kwart4Total;
use Laudis\Calculators\Margeregeling\Results\KwartaalResult;
use Laudis\Calculators\Margeregeling\Results\KwartaalTotalResult;
use Laudis\Calculators\Venb\BasicCalculationResult;

class MargeregelingCalculator implements MargeregelingInterface
{
    /** @var MargeregelingAuxiliaries */
    private $auxiliaries;

    public function __construct(MargeregelingAuxiliaries $auxiliaries)
    {
        $this->auxiliaries = $auxiliaries;
    }

    public function calculate(MargeregelingInput $input): BasicCalculationResult
    {
        $results = [];

        $results['Totalen'] =  $this->getTotals($input)->output();

        for ($i = 1 ; $i <=3 ; $i++ ){
            $results['kwartaal' . $i] =  $this->getKwartaalResult($i,$input)->output();
        }

        $results['kwartaal4'] = $this->getKwart4($input)->output($input->getKalenderjaar());

        return new BasicCalculationResult($results);
    }

    public function getTotals(MargeregelingInput $input) : MargeregelingTable
    {
        return new MargeregelingTable(
            $input->getBelgischeVerkopenEnICL()->getKwartaal1Aankoop() + $input->getExport()->getKwartaal1Aankoop(),
            $input->getBelgischeVerkopenEnICL()->getKwartaal1Verkoop() + $input->getExport()->getKwartaal1Verkoop(),
            $input->getBelgischeVerkopenEnICL()->getKwartaal2Aankoop() + $input->getExport()->getKwartaal2Aankoop(),
            $input->getBelgischeVerkopenEnICL()->getKwartaal2Verkoop() + $input->getExport()->getKwartaal2Verkoop(),
            $input->getBelgischeVerkopenEnICL()->getKwartaal3Aankoop() + $input->getExport()->getKwartaal3Aankoop(),
            $input->getBelgischeVerkopenEnICL()->getKwartaal3Verkoop() + $input->getExport()->getKwartaal3Verkoop(),
            $input->getBelgischeVerkopenEnICL()->getKwartaal4Aankoop() + $input->getExport()->getKwartaal4Aankoop(),
            $input->getBelgischeVerkopenEnICL()->getKwartaal4Verkoop() + $input->getExport()->getKwartaal4Verkoop(),
        );
    }

    public function getKwartaalResult(int $kwartaal,MargeregelingInput $input) : KwartaalTotalResult
    {
        $aankoopMethodName = 'getKwartaal' . $kwartaal . 'Aankoop';
        $verkoopMethodName = 'getKwartaal' . $kwartaal . 'Verkoop';

        $begin = -$input->getBeginVoorraad();

        switch ($kwartaal) {
            case 1:
                $begin = -$input->getBeginVoorraad();
                break;
            case 2:
                $begin = min($this->getKwartaalResult(1,$input)->getKwartaalResult()->getNogInStock(),0);
                break;
            case 3:
                $begin = min($this->getKwartaalResult(2,$input)->getKwartaalResult()->getNogInStock(),0);
                break;
        }

        $kwartaalResult = new KwartaalResult(
            $begin,
            -$this->getTotals($input)->$aankoopMethodName(),
            $input->getExport()->$aankoopMethodName(),
            $this->getTotals($input)->$verkoopMethodName(),
            $input->getExport()->$verkoopMethodName()
        );

        $mvHVerkopen = $kwartaalResult->getNogInStock() < 0 ? 0 : $kwartaalResult->getNogInStock()/1.21;

        $btwAangifteResult =  new BTWaangifteResult(
            -$kwartaalResult->getAankopenVHKwartaal(),
            -$kwartaalResult->getVerkoopExport(),
            $mvHVerkopen,
            $kwartaalResult->getNogInStock()< 0 ? 0 :  $kwartaalResult->getNogInStock() - $mvHVerkopen,
        );

        return new KwartaalTotalResult($kwartaalResult,$btwAangifteResult);
    }

    public function getKwart4(MargeregelingInput $input) : Kwart4Total
    {
        $reedsInAangifte = array_sum(
            [
                -$this->getKwartaalResult(1,$input)->getBtwAangifte()->getTotal(),
                -$this->getKwartaalResult(2,$input)->getBtwAangifte()->getTotal(),
                -$this->getKwartaalResult(3,$input)->getBtwAangifte()->getTotal(),
            ]
        );

        $kwartResult =  new Kwart4Result
        (
            $input->getBeginVoorraad(),
            $this->getTotals($input)->getTotalAankopen(),
            -$input->getExport()->getTotalAankopen(),
            -$input->getEindVoorraad(),
            $this->getTotals($input)->getTotalVerkoop(),
            $input->getExport()->getTotalVerkoop(),
            $reedsInAangifte
        );

        $mvh = $kwartResult->getNogOpTeNemen()<0 ? 0 : $kwartResult->getNogOpTeNemen()/1.21;

        $btwAangifte = new kwart4btwAangifte(
            $this->getTotals($input)->getKwartaal4Aankoop(),
            $input->getExport()->getKwartaal4Verkoop(),
            $kwartResult->getVerkochteAankopen() + $kwartResult->getNogOpTeNemen()<0 ? $kwartResult->getNogOpTeNemen() : 0,
            $mvh,
            $kwartResult->getNogOpTeNemen()<0 ? 0 : $kwartResult->getNogOpTeNemen() - $mvh
        );

        return new Kwart4Total($kwartResult,$btwAangifte);
    }

}
<?php
declare(strict_types=1);

namespace Laudis\Calculators\Calculators\Results;

use function is_array;
use function is_float;
use function is_int;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use NumberFormatter;

/**
 * Class FormattedResult
 * @package Laudis\Calculators\Calculators\Results
 */
class FormattedResult implements CalculationResultInterface
{
    /** @var CalculationResultInterface */
    private $result;
    /** @var NumberFormatter */
    private $formatter;

    /**
     * FormattedResult constructor.
     * @param CalculationResultInterface $result
     * @param NumberFormatter $formatter
     */
    public function __construct(CalculationResultInterface $result, NumberFormatter $formatter)
    {
        $this->result = $result;
        $this->formatter = $formatter;
    }

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array
    {
        $tbr = [];
        foreach ($this->result->output() as $key => $value) {
            $tbr[$key] = $this->iterate($value);
        }
        return $tbr;
    }

    /**
     * @param $current
     * @return mixed
     */
    private function iterate($current)
    {
        if (is_array($current)) {
            $tbr = [];
            foreach ($current as $key => $value) {
                $tbr[$key] = $this->iterate($value);
            }
            return $tbr;
        }
        if (is_int($current) || is_float($current)) {
            if (round($current,  2) === -1 * 0.00) {
                $current = 0.0;
            }
            return $this->formatter->format($current);
        }
        return $current;
    }
}

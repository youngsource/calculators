<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use DateTime;
use DateTimeInterface;
use Laudis\Index\ContextualIndexedValueRepository;
use Laudis\Index\IndexedValue;
use Laudis\Index\IndexType;

/**
 * Class IndexedValueRegister
 * @package Laudis\Calculators\Registers
 */
final class IndexedValueRegister
{
    /**
     * @param ContextualIndexedValueRepository $repository
     */
    public function register(ContextualIndexedValueRepository $repository): void
    {
        $repository->addIndex(
            'vermeerdering',
            $this->date(2017),
            IndexedValue::make(0.0225, 0.0225, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'vermeerdering',
            $this->date(2018),
            IndexedValue::make(0.0225, 0.0675, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'vermeerdering',
            $this->date(2024),
            IndexedValue::make(0.0225, 0.09, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'vermeerdering',
            $this->date(2025),
            IndexedValue::make(0.0225, 0.0675, IndexType::PERCENTAGE())
        );

        $repository->addIndex(
            'bonificatie_VA1',
            $this->date(2017),
            IndexedValue::make(0.03, 0.03, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA1',
            $this->date(2018),
            IndexedValue::make(0.03, 0.09, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA1',
            $this->date(2024),
            IndexedValue::make(0.03, 0.12, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA1',
            $this->date(2024),
            IndexedValue::make(0.03, 0.09, IndexType::PERCENTAGE())
        );


        $repository->addIndex(
            'bonificatie_VA2',
            $this->date(2017),
            IndexedValue::make(0.025, 0.025, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA2',
            $this->date(2018),
            IndexedValue::make(0.025, 0.075, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA2',
            $this->date(2024),
            IndexedValue::make(0.025, 0.1, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA2',
            $this->date(2024),
            IndexedValue::make(0.025, 0.075, IndexType::PERCENTAGE())
        );

        $repository->addIndex(
            'bonificatie_VA3',
            $this->date(2017),
            IndexedValue::make(0.02, 0.02, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA3',
            $this->date(2018),
            IndexedValue::make(0.02, 0.06, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA3',
            $this->date(2024),
            IndexedValue::make(0.02, 0.08, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA3',
            $this->date(2024),
            IndexedValue::make(0.02, 0.06, IndexType::PERCENTAGE())
        );

        $repository->addIndex(
            'bonificatie_VA4',
            $this->date(2017),
            IndexedValue::make(0.015, 0.015, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA4',
            $this->date(2018),
            IndexedValue::make(0.015, 0.045, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA4',
            $this->date(2024),
            IndexedValue::make(0.015, 0.06, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'bonificatie_VA4',
            $this->date(2024),
            IndexedValue::make(0.015, 0.045, IndexType::PERCENTAGE())
        );



        $repository->addIndex(
            'relatieve_grens',
            $this->date(2017),
            IndexedValue::make(0.01, 0.005, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'relatieve_grens',
            $this->date(2018),
            IndexedValue::make(0.01, 0.0, IndexType::PERCENTAGE())
        );


        $repository->addIndex(
            'absolute_grens',
            $this->date(2017),
            IndexedValue::make(40, 80, IndexType::EURO())
        );
        $repository->addIndex(
            'absolute_grens',
            $this->date(2018),
            IndexedValue::make(40, 0, IndexType::EURO())
        );



        $repository->addIndex(
            'vrijstellingspercentage',
            $this->date(2017),
            IndexedValue::make(1.5, 3.1, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'vrijstellingspercentage',
            $this->date(2018),
            IndexedValue::make(1.5, 3.56, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'vrijstellingspercentage',
            $this->date(2020),
            IndexedValue::make(1.5, 4.21, IndexType::PERCENTAGE())
        );


        $repository->addIndex(
            'maximale_vrijstelling_tax_shelter',
            $this->date(2017),
            IndexedValue::make(1.5, 750000.00, IndexType::EURO())
        );
        $repository->addIndex(
            'maximale_vrijstelling_tax_shelter',
            $this->date(2018),
            IndexedValue::make(1.5, 850000.00, IndexType::EURO())
        );
        $repository->addIndex(
            'maximale_vrijstelling_tax_shelter',
            $this->date(2020),
            IndexedValue::make(1.5, 1000000.00, IndexType::EURO())
        );


        $repository->addIndex(
            'gewoon_tarief',
            $this->date(2017),
            IndexedValue::make(0.3399, 0.3399, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'gewoon_tarief',
            $this->date(2018),
            IndexedValue::make(0.3399, 0.2958, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'gewoon_tarief',
            $this->date(2020),
            IndexedValue::make(0.3399, 0.25, IndexType::PERCENTAGE())
        );

        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2015-01-01'),
            IndexedValue::make(0.04876, 0.04876, IndexType::PERCENTAGE())
        );

        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2015-07-01'),
            IndexedValue::make(0.04876, 0.04699, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2016-01-01'),
            IndexedValue::make(0.04876, 0.04614, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2016-07-01'),
            IndexedValue::make(0.04876, 0.04485, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2017-01-01'),
            IndexedValue::make(0.04876, 0.04434, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2017-07-01'),
            IndexedValue::make(0.04876, 0.04378, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
            IndexedValue::make(0.04876, 0.04326, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2018-07-01'),
            IndexedValue::make(0.04876, 0.04312, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2019-01-01'),
            IndexedValue::make(0.04876, 0.04348, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2019-07-01'),
            IndexedValue::make(0.04876, 0.04363, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2020-01-01'),
            IndexedValue::make(0.04876, 0.04198, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2020-07-01'),
            IndexedValue::make(0.04876, 0.04301, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2021-01-01'),
            IndexedValue::make(0.04876, 0.04061, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2021-07-01'),
            IndexedValue::make(0.04876, 0.04012, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2022-01-01'),
            IndexedValue::make(0.04876, 0.04008, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2022-07-01'),
            IndexedValue::make(0.04876, 0.04620, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2023-01-01'),
            IndexedValue::make(0.04876, 0.06834, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2023-07-01'),
            IndexedValue::make(0.04876, 0.08286, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2024-01-01'),
            IndexedValue::make(0.04876, 0.08481, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2024-07-01'),
            IndexedValue::make(0.04876, 0.08163, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'euribor',
            DateTime::createFromFormat('Y-m-d', '2025-01-01'),
            IndexedValue::make(0.04876, 0.07281, IndexType::PERCENTAGE())
        );

        $repository->addIndex(
            '4kwart',
            $this->date(2017),
            IndexedValue::make(0.0225, 0.0225, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            '3kwart',
            $this->date(2017),
            IndexedValue::make(0.02, 0.02, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            '2kwart',
            $this->date(2017),
            IndexedValue::make(0.0175, 0.0175, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            '1kwart',
            $this->date(2017),
            IndexedValue::make(0.015, 0.015, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA1',
            $this->date(2017),
            IndexedValue::make(0.03, 0.03, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA2',
            $this->date(2017),
            IndexedValue::make(0.025, 0.025, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA3',
            $this->date(2017),
            IndexedValue::make(0.02, 0.02, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA4',
            $this->date(2017),
            IndexedValue::make(0.015, 0.015, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'pct',
            $this->date(2017),
            IndexedValue::make(0.005, 0.005, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'geen verhoging',
            $this->date(2017),
            IndexedValue::make(80, 80, IndexType::EURO())
        );
        $repository->addIndex(
            'CrisisBijdrage',
            $this->date(2017),
            IndexedValue::make(0.03, 0.03, IndexType::EURO())
        );

        $repository->addIndex(
            '4kwart',
            $this->date(2018),
            IndexedValue::make(0.0225, 0.0225, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            '3kwart',
            $this->date(2018),
            IndexedValue::make(0.02, 0.02, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            '2kwart',
            $this->date(2018),
            IndexedValue::make(0.0175, 0.0175, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            '1kwart',
            $this->date(2018),
            IndexedValue::make(0.015, 0.015, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA1',
            $this->date(2018),
            IndexedValue::make(0.03, 0.03, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA2',
            $this->date(2018),
            IndexedValue::make(0.025, 0.025, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA3',
            $this->date(2018),
            IndexedValue::make(0.02, 0.02, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA4',
            $this->date(2018),
            IndexedValue::make(0.015, 0.015, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'pct',
            $this->date(2018),
            IndexedValue::make(0.005, 0.005, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'geen verhoging',
            $this->date(2018),
            IndexedValue::make(80, 80, IndexType::EURO())
        );



        $repository->addIndex(
            '4kwart',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0.0675, 0.0675, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            '3kwart',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0.06, 0.06, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            '2kwart',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0.0525, 0.0525, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            '1kwart',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0.045, 0.045, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA1',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0.09, 0.09, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA2',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0.075, 0.075, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA3',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0.06, 0.06, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'VA4',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0.045, 0.045, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'pct',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0.0, 0.0, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'geen verhoging',
            DateTime::createFromFormat('Y-m-d', '2018-07-26'),
            IndexedValue::make(0, 0, IndexType::EURO())
        );
        //Voordeel wagen indexes
            //2012
        $repository->addIndex(
            'CO2Percentage',
            $this->date(2012),
            IndexedValue::make(0.055, 0.055, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_aanpassing_per_gram',
            $this->date(2012),
            IndexedValue::make(0.001, 0.001, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_minimum_percentage',
            $this->date(2012),
            IndexedValue::make(0.04, 0.04, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_maximum_percentage',
            $this->date(2012),
            IndexedValue::make(0.18, 0.18, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_referentie_benzine',
            $this->date(2012),
            IndexedValue::make(115, 115, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_referentie_diesel',
            $this->date(2012),
            IndexedValue::make(95, 95, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_benzine',
            $this->date(2012),
            IndexedValue::make(205, 205, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_diesel',
            $this->date(2012),
            IndexedValue::make(195, 195, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_mimimum_voordeel',
            $this->date(2012),
            IndexedValue::make(1200, 1200, IndexType::CONSTANT())
        );
            //2013
        $repository->addIndex(
            'CO2Percentage',
            $this->date(2013),
            IndexedValue::make(0.055, 0.055, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_aanpassing_per_gram',
            $this->date(2013),
            IndexedValue::make(0.001, 0.001, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_minimum_percentage',
            $this->date(2013),
            IndexedValue::make(0.04, 0.04, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_maximum_percentage',
            $this->date(2013),
            IndexedValue::make(0.18, 0.18, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_referentie_benzine',
            $this->date(2013),
            IndexedValue::make(116, 116, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_referentie_diesel',
            $this->date(2013),
            IndexedValue::make(95, 95, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_benzine',
            $this->date(2013),
            IndexedValue::make(205, 205, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_diesel',
            $this->date(2013),
            IndexedValue::make(195, 195, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_mimimum_voordeel',
            $this->date(2013),
            IndexedValue::make(1230, 1230, IndexType::CONSTANT())
        );
            //2014
        $repository->addIndex(
            'CO2Percentage',
            $this->date(2014),
            IndexedValue::make(0.055, 0.055, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_aanpassing_per_gram',
            $this->date(2014),
            IndexedValue::make(0.001, 0.001, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_minimum_percentage',
            $this->date(2014),
            IndexedValue::make(0.04, 0.04, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_maximum_percentage',
            $this->date(2014),
            IndexedValue::make(0.18, 0.18, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_referentie_benzine',
            $this->date(2014),
            IndexedValue::make(112, 112, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_referentie_diesel',
            $this->date(2014),
            IndexedValue::make(93, 93, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_benzine',
            $this->date(2014),
            IndexedValue::make(205, 205, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_diesel',
            $this->date(2014),
            IndexedValue::make(195, 195, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_mimimum_voordeel',
            $this->date(2014),
            IndexedValue::make(1250, 1250, IndexType::CONSTANT())
        );
            //2015
        $repository->addIndex(
            'CO2Percentage',
            $this->date(2015),
            IndexedValue::make(0.055, 0.055, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_aanpassing_per_gram',
            $this->date(2015),
            IndexedValue::make(0.001, 0.001, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_minimum_percentage',
            $this->date(2015),
            IndexedValue::make(0.04, 0.04, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_maximum_percentage',
            $this->date(2015),
            IndexedValue::make(0.18, 0.18, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_referentie_benzine',
            $this->date(2015),
            IndexedValue::make(110, 110, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_referentie_diesel',
            $this->date(2015),
            IndexedValue::make(91, 91, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_benzine',
            $this->date(2015),
            IndexedValue::make(205, 205, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_diesel',
            $this->date(2015),
            IndexedValue::make(195, 195, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_mimimum_voordeel',
            $this->date(2015),
            IndexedValue::make(1250, 1250, IndexType::CONSTANT())
        );
            //2016
        $repository->addIndex(
            'CO2Percentage',
            $this->date(2016),
            IndexedValue::make(0.055, 0.055, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_aanpassing_per_gram',
            $this->date(2016),
            IndexedValue::make(0.001, 0.001, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_minimum_percentage',
            $this->date(2016),
            IndexedValue::make(0.04, 0.04, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_maximum_percentage',
            $this->date(2016),
            IndexedValue::make(0.18, 0.18, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_referentie_benzine',
            $this->date(2016),
            IndexedValue::make(107, 107, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_referentie_diesel',
            $this->date(2016),
            IndexedValue::make(89, 89, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_benzine',
            $this->date(2016),
            IndexedValue::make(205, 205, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_diesel',
            $this->date(2016),
            IndexedValue::make(195, 195, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_mimimum_voordeel',
            $this->date(2016),
            IndexedValue::make(1260, 1260, IndexType::CONSTANT())
        );
            //2017
        $repository->addIndex(
            'CO2Percentage',
            $this->date(2017),
            IndexedValue::make(0.055, 0.055, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_aanpassing_per_gram',
            $this->date(2017),
            IndexedValue::make(0.001, 0.001, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_minimum_percentage',
            $this->date(2017),
            IndexedValue::make(0.04, 0.04, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_maximum_percentage',
            $this->date(2017),
            IndexedValue::make(0.18, 0.18, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_referentie_benzine',
            $this->date(2017),
            IndexedValue::make(105, 105, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_referentie_diesel',
            $this->date(2017),
            IndexedValue::make(87, 87, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_benzine',
            $this->date(2017),
            IndexedValue::make(205, 205, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_diesel',
            $this->date(2017),
            IndexedValue::make(195, 195, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_mimimum_voordeel',
            $this->date(2017),
            IndexedValue::make(1280, 1280, IndexType::CONSTANT())
        );
            //2018
        $repository->addIndex(
            'CO2Percentage',
            $this->date(2018),
            IndexedValue::make(0.055, 0.055, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_aanpassing_per_gram',
            $this->date(2018),
            IndexedValue::make(0.001, 0.001, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_minimum_percentage',
            $this->date(2018),
            IndexedValue::make(0.04, 0.04, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_maximum_percentage',
            $this->date(2018),
            IndexedValue::make(0.18, 0.18, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_referentie_benzine',
            $this->date(2018),
            IndexedValue::make(105, 105, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_referentie_diesel',
            $this->date(2018),
            IndexedValue::make(86, 86, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_benzine',
            $this->date(2018),
            IndexedValue::make(205, 205, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_diesel',
            $this->date(2018),
            IndexedValue::make(195, 195, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_mimimum_voordeel',
            $this->date(2018),
            IndexedValue::make(1310, 1310, IndexType::CONSTANT())
        );
            //2019
        $repository->addIndex(
            'CO2Percentage',
            $this->date(2019),
            IndexedValue::make(0.055, 0.055, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_aanpassing_per_gram',
            $this->date(2019),
            IndexedValue::make(0.001, 0.001, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_minimum_percentage',
            $this->date(2019),
            IndexedValue::make(0.04, 0.04, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_maximum_percentage',
            $this->date(2019),
            IndexedValue::make(0.18, 0.18, IndexType::PERCENTAGE())
        );
        $repository->addIndex(
            'wagen_referentie_benzine',
            $this->date(2019),
            IndexedValue::make(107, 107, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_referentie_diesel',
            $this->date(2019),
            IndexedValue::make(88, 88, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_benzine',
            $this->date(2019),
            IndexedValue::make(205, 205, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_gebrek_uitstootgegevens_diesel',
            $this->date(2019),
            IndexedValue::make(195, 195, IndexType::CONSTANT())
        );
        $repository->addIndex(
            'wagen_mimimum_voordeel',
            $this->date(2019),
            IndexedValue::make(1340, 1340, IndexType::CONSTANT())
        );
    }

    /**
     * @param int $jaar
     * @return DateTimeInterface
     */
    private function date(int $jaar): DateTimeInterface
    {
        return DateTime::createFromFormat('Y-m-d', $jaar . '-01-01');
    }
}

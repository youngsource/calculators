<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use DateTime;
use Laudis\Scale\Operators\AbsoluteOperator;
use Laudis\Scale\ContextualScaleRepository;
use Laudis\Scale\Contracts\ScaleInterface;
use Laudis\Scale\Operators\MultiplicationOperator;
use Laudis\Scale\Scale;
use Laudis\Scale\ScaleRule;

/**
 * Class ScaleRegister
 * @package Laudis\Calculators\Registers
 */
final class ScaleRegister
{
    /**
     * @param ContextualScaleRepository $repository
     */
    public function register(ContextualScaleRepository $repository): void
    {
        $repository->addScale(
            'verlaagd_tarief',
            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
            $this->verlaagdTariefAj2019()
        );

        $repository->addScale(
            'verlaagd_tarief',
            DateTime::createFromFormat('Y-m-d', '2020-01-01'),
            $this->verlaagdTariefAj2021()
        );

        $repository->addScale(
            'verlaagd_tarief',
            DateTime::createFromFormat('Y-m-d', '2013-01-01'),
            $this->verlaagdTariefVoorAj2019()
        );

        $repository->addScale(
            'venb_barema',
            DateTime::createFromFormat('Y-m-d', '2017-01-01'),
            $this->venbBarema2017()
        );

        $repository->addScale(
            'venb_barema',
            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
            $this->venbBarema2018New()
        );

        $repository->addScale(
            'voordeelWagenDieselCO2',
            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
            $this->voordeelWagenDieselCO2()
        );

        $repository->addScale(
            'voordeelWagenBenzineCO2',
            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
            $this->voordeelWagenBenzineCO2()
        );

        $repository->addScale(
            'voordeelWagenOuderDomsPercentage',
            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
            $this->voordeelWagenOuderDomsPercentage()
        );

    }

    /**
     * @return Scale
     */
    private function verlaagdTariefAj2019(): Scale
    {
        $scale = new Scale(new MultiplicationOperator);
        $scale->addScaleRule(ScaleRule::make(0, 0.2 * 1.02));
        $scale->addScaleRule(ScaleRule::make(100000, 0.29 * 1.02));
        return $scale;
    }

    /**
     * @return Scale
     */
    private function verlaagdTariefAj2021(): Scale
    {
        $scale = new Scale(new MultiplicationOperator);
        $scale->addScaleRule(ScaleRule::make(0, 0.2));
        $scale->addScaleRule(ScaleRule::make(100000, 0.25));
        return $scale;
    }

    /**
     * @return Scale
     */
    private function verlaagdTariefVoorAj2019(): Scale
    {
        $scale = new Scale(new MultiplicationOperator);
        $scale->addScaleRule(ScaleRule::make(0, 0.2425 * 1.03));
        $scale->addScaleRule(ScaleRule::make(25000, 0.31 * 1.03));
        $scale->addScaleRule(ScaleRule::make(90000, 0.345 * 1.03));
        $scale->addScaleRule(ScaleRule::make(322500, 0));
        return $scale;
    }

    /**
     * @return ScaleInterface
     */
    private function venbBarema2017(): ScaleInterface
    {
        $scale = new Scale(new MultiplicationOperator);
        $scale->addScaleRule(ScaleRule::make(0.0, 0.2425));
        $scale->addScaleRule(ScaleRule::make(25000.0, 0.31));
        $scale->addScaleRule(ScaleRule::make(90000.0, 0.345));
        $scale->addScaleRule(ScaleRule::make(322500, 0));
        return $scale;
    }

    /**
     * @return ScaleInterface
     */
    private function venbBarema2018New(): ScaleInterface
    {
        $scale = new Scale(new MultiplicationOperator);
        $scale->addScaleRule(ScaleRule::make(0.0, 0.20));
        $scale->addScaleRule(ScaleRule::make(100000.0, 0.29));
        return $scale;
    }

    /**
     * @return ScaleInterface
     */
    private function voordeelWagenDieselCO2(): ScaleInterface
    {
        $scale = new Scale(new AbsoluteOperator);
        $scale->addScaleRule(ScaleRule::make(0, 1));
        $scale->addScaleRule(ScaleRule::make(61, -0.1));
        $scale->addScaleRule(ScaleRule::make(106, -0.1));
        $scale->addScaleRule(ScaleRule::make(116, -0.05));
        $scale->addScaleRule(ScaleRule::make(146, -0.05));
        $scale->addScaleRule(ScaleRule::make(171, -0.1));
        $scale->addScaleRule(ScaleRule::make(196, -0.1));

        return $scale;
    }

    /**
     * @return ScaleInterface
     */
    private function voordeelWagenBenzineCO2(): ScaleInterface
    {
        $scale = new Scale(new AbsoluteOperator);
        $scale->addScaleRule(ScaleRule::make(0, 1));
        $scale->addScaleRule(ScaleRule::make(60, -0.1));
        $scale->addScaleRule(ScaleRule::make(105, -0.1));
        $scale->addScaleRule(ScaleRule::make(125, -0.05));
        $scale->addScaleRule(ScaleRule::make(155, -0.05));
        $scale->addScaleRule(ScaleRule::make(180, -0.1));
        $scale->addScaleRule(ScaleRule::make(205, -0.1));
        return $scale;
    }

    /**
     * @return ScaleInterface
     */
    private function voordeelWagenOuderDomsPercentage(): ScaleInterface
    {
        $scale = new Scale(new AbsoluteOperator());
        $scale->addScaleRule(ScaleRule::make(0.0, 1));
        $scale->addScaleRule(ScaleRule::make(12.0, -0.06));
        $scale->addScaleRule(ScaleRule::make(24.0, -0.06));
        $scale->addScaleRule(ScaleRule::make(36.0, -0.06));
        $scale->addScaleRule(ScaleRule::make(48.0, -0.06));
        $scale->addScaleRule(ScaleRule::make(60.0, -0.06));

        return $scale;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use Closure;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;
use Laudis\Common\Contracts\MutableContainerInterface;
use Laudis\Index\ContextualIndexedValueRepository;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Scale\ContextualScaleRepository;
use Laudis\Scale\Contracts\ContextualRepositoryScaleInterface;
use NumberFormatter;
use PDO;
use Psr\Container\ContainerInterface;
use function getenv;

/**
 * Class BasicRegister
 * @package Laudis\Calculators\Registers
 */
final class BasicRegister
{
    /**
     * @param MutableContainerInterface $container
     */
    public function register(MutableContainerInterface $container): void
    {
        $container->set('currency_formatter', Closure::fromCallable([$this, 'buildNumberFormatter']));

        $container->set(ContextualRepositoryScaleInterface::class, new ContextualScaleRepository());
        $container->set(ContextualIndexedValueRepositoryInterface::class, new ContextualIndexedValueRepository);
        $container->set(CalculatorFactoriesCollection::class, new CalculatorFactoriesCollection);
        $this->registerPdo($container);
    }

    /**
     * @param ContainerInterface $container
     * @return NumberFormatter
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function buildNumberFormatter(ContainerInterface $container): NumberFormatter
    {
        $formatter = NumberFormatter::create(
            $container->get('locale') ?: getenv('APP_LOCALE'),
            NumberFormatter::DECIMAL
        );
        $formatter->setAttribute(NumberFormatter::MAX_FRACTION_DIGITS, '2');
        $formatter->setAttribute(NumberFormatter::MIN_FRACTION_DIGITS, '2');
        $formatter->setAttribute(NumberFormatter::FRACTION_DIGITS, '2');
        return $formatter;
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerPdo(MutableContainerInterface $container): void
    {
        $container->set(PDO::class, static function () {
            $dsn = getenv('DB_DRIVER') . ':host=' . getenv('DB_HOST') . ';dbname=' . getenv('DB_NAME') . ';charset=' . getenv('DB_CHARRSET');
            return new PDO($dsn, getenv('DB_USERNAME'), getenv('DB_PASSWORD'), [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]);
        });
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use Laudis\Common\Contracts\MutableContainerInterface;
use Laudis\Common\Factories\ResponseFactory;
use Laudis\UserManagement\UserManagerRegister;

/**
 * Class DependencyRegister
 * Registry of various dependencies.
 *
 * @package Laudis\Calculators\Registers
 */
final class DependencyRegister
{
    /** @var FrameworkOverrideRegister */
    private $frameworkRegister;
    /** @var ControllerRegister */
    private $controllerRegister;
    /** @var StrategiesRegister */
    private $strategiesRegister;
    /** @var BasicRegister */
    private $basicRegister;
    /** @var UserManagerRegister */
    private $userRegister;

    /**
     * DependencyRegister constructor.
     * @param FrameworkOverrideRegister $frameworkRegister
     * @param ControllerRegister $controllerRegister
     * @param StrategiesRegister $strategiesRegister
     */
    public function __construct(
        FrameworkOverrideRegister $frameworkRegister,
        ControllerRegister $controllerRegister,
        StrategiesRegister $strategiesRegister,
        BasicRegister $basicRegister,
        UserManagerRegister $userRegister
    ) {
        $this->frameworkRegister = $frameworkRegister;
        $this->controllerRegister = $controllerRegister;
        $this->strategiesRegister = $strategiesRegister;
        $this->basicRegister = $basicRegister;
        $this->userRegister = $userRegister;
    }

    /**
     * @return DependencyRegister
     */
    public static function make(): self
    {
        return new self(
            new FrameworkOverrideRegister(new ResponseFactory),
            new ControllerRegister,
            new StrategiesRegister,
            new BasicRegister,
            new UserManagerRegister
        );
    }

    /**
     * Registers various dependencies to the container.
     *
     * @param MutableContainerInterface $container
     */
    public function register(MutableContainerInterface $container): void
    {
        $this->frameworkRegister->register($container);
        $this->controllerRegister->register($container);
        $this->strategiesRegister->register($container);
        $this->basicRegister->register($container);
        $this->userRegister->register($container);
    }
}

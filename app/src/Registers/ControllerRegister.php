<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use Closure;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Controllers\CalculationController;
use Laudis\Calculators\Controllers\CalculatorsController;
use Laudis\Calculators\Presenters\CalculatorsPresenter;
use Laudis\Calculators\Strategies\StoreMetaData;
use Laudis\Common\Contracts\MutableContainerInterface;
use Laudis\Common\Contracts\RequestToCalculationInterface;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\Common\Controllers\PreFlightCorsController;
use Laudis\IndexApi\IndexedValueController;
use Laudis\IndexApi\IndexedValueFactory;
use Laudis\UserManagement\UserManager;
use PDO;
use Psr\Container\ContainerInterface;
use Rakit\Validation\Validator;

/**
 * Class ControllerRegister
 * @package Laudis\Calculators\Registers
 */
final class ControllerRegister
{
    /**
     * @param MutableContainerInterface $container
     */
    public function register(MutableContainerInterface $container): void
    {
        $container->set(CalculationController::class, Closure::fromCallable([$this, 'buildCalculationController']));
        $container->set(PreFlightCorsController::class, Closure::fromCallable([$this, 'buildCorsController']));
        $container->set(CalculatorsController::class, Closure::fromCallable([$this, 'buildCalculatorsController']));
        $container->set(IndexedValueController::class, static function (ContainerInterface $container) {
            return new IndexedValueController(
                $container->get(ResponseWriterInterface::class),
                new IndexedValueFactory(
                    $container->get(Validator::class),
                    $container->get(PDO::class)
                )
            );
        });
    }

    /**
     * @param ContainerInterface $container
     * @return CalculatorsController
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function buildCalculatorsController(ContainerInterface $container): CalculatorsController
    {
        return new CalculatorsController(
            $container->get(ResponseWriterInterface::class),
            new CalculatorsPresenter,
            $container->get(CalculatorFactoriesCollection::class)
        );
    }

    /**
     * @param ContainerInterface $container
     * @return CalculationController
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function buildCalculationController(ContainerInterface $container): CalculationController
    {
            return new CalculationController(
                $container->get(ResponseWriterInterface::class),
                $container->get(RequestToCalculationInterface::class)
            );
    }

    /**
     * @param ContainerInterface $container
     * @return PreFlightCorsController
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function buildCorsController(ContainerInterface $container): PreFlightCorsController
    {
        return new PreFlightCorsController($container->get(ResponseWriterInterface::class));
    }
}

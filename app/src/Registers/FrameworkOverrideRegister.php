<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use Bnf\Slim3Psr15\CallableResolver;
use Closure;
use Exception;
use Laudis\Common\Contracts\MutableContainerInterface;
use Laudis\Common\Factories\ResponseFactory;
use Locale;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

/**
 * Class FrameworkOverrideRegister
 * @package Laudis\Calculators\Registers
 */
final class FrameworkOverrideRegister
{
    /** @var ResponseFactory */
    private $responseFactory;

    /**
     * FrameworkOverrideRegister constructor.
     * @param ResponseFactory $responseFactory
     */
    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }

    /**
     * @return FrameworkOverrideRegister
     */
    public static function make(): FrameworkOverrideRegister
    {
        return new self(new ResponseFactory);
    }

    /**
     * @param MutableContainerInterface $container
     */
    public function register(MutableContainerInterface $container): void
    {
        $container->set('response', Closure::fromCallable([$this, 'buildResponse']));
        $container->set('locale', Closure::fromCallable([$this, 'buildLocale']));
        $container->set('logger', Closure::fromCallable([$this, 'makeLogger']));
        $container->set('callableResolver', static function (ContainerInterface $container) {
            return new CallableResolver($container);
        });
    }

    /**
     * @return ResponseInterface
     *
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function buildResponse(): ResponseInterface
    {
        return $this->responseFactory->make();
    }

    /**
     * @param ContainerInterface $container
     * @return LoggerInterface
     * @throws Exception
     *
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function makeLogger(ContainerInterface $container): LoggerInterface
    {
        $logger = $container->get('settings')['logger'];
        $log = new Logger($logger['name']);
        $handler = new StreamHandler($logger['path'], $logger['level']);
        $handler->setFormatter(new LineFormatter(null, null, true, true));
        $log->pushHandler($handler);
        return $log;
    }

    /**
     * @param ContainerInterface $container
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function buildLocale(ContainerInterface $container): string
    {
        return Locale::acceptFromHttp($container->get('settings')['locale']);
    }
}

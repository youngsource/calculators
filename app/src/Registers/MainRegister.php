<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use Laudis\Calculators\Collections\CalculatorFactoriesCollection as CalculatorFactories;
use Laudis\Common\Contracts\AppInterface;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Scale\Contracts\ContextualRepositoryScaleInterface;
use Laudis\UserManagement\AddPossibleUserMiddleware;
use function bcscale;

/**
 * Class MainRegister
 * The main register of the application.
 *
 * @package Laudis\Calculators\Registers
 */
final class MainRegister
{
    /** @var DependencyRegister */
    private $dependencyRegister;
    /** @var RouteRegister */
    private $routeRegister;
    /** @var CalculatorRegister */
    private $calculatorRegister;
    /** @var ExceptionHandlerRegister */
    private $exceptionHandlerRegister;
    /**
     * @var ScaleRegister
     */
    private $scaleRegister;
    /**
     * @var IndexedValueRegister
     */
    private $indexedValueRegister;

    /**
     * MainRegister constructor.
     * @param DependencyRegister $dependencyRegister
     * @param RouteRegister $routeRegister
     * @param CalculatorRegister $calculatorRegister
     * @param ExceptionHandlerRegister $exceptionHandlerRegister
     * @param ScaleRegister $scaleRegister
     * @param IndexedValueRegister $indexedValueRegister
     */
    public function __construct(
        DependencyRegister $dependencyRegister,
        RouteRegister $routeRegister,
        CalculatorRegister $calculatorRegister,
        ExceptionHandlerRegister $exceptionHandlerRegister,
        ScaleRegister $scaleRegister,
        IndexedValueRegister $indexedValueRegister
    ) {
        $this->dependencyRegister = $dependencyRegister;
        $this->routeRegister = $routeRegister;
        $this->calculatorRegister = $calculatorRegister;
        $this->exceptionHandlerRegister = $exceptionHandlerRegister;
        $this->scaleRegister = $scaleRegister;
        $this->indexedValueRegister = $indexedValueRegister;
    }

    /**
     * Initiates the main register.
     *
     * @return MainRegister
     */
    public static function make(): MainRegister
    {
        return new self(
            DependencyRegister::make(),
            new RouteRegister,
            new CalculatorRegister,
            new ExceptionHandlerRegister,
            new ScaleRegister,
            new IndexedValueRegister
        );
    }

    /**
     * @param AppInterface $app
     */
    public function register(AppInterface $app): void
    {
        bcscale(100);
        $this->dependencyRegister->register($app->getContainer());
        $this->routeRegister->register($app->getRouter());
        $this->exceptionHandlerRegister->register($app->getContainer());
        $this->calculatorRegister->register($app->getContainer()[CalculatorFactories::class], $app->getContainer());
        $this->scaleRegister->register($app->getContainer()[ContextualRepositoryScaleInterface::class]);
        $this->indexedValueRegister->register($app->getContainer()[ContextualIndexedValueRepositoryInterface::class]);

        $app->add(AddPossibleUserMiddleware::class);
    }
}

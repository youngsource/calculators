<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use Closure;
use Laudis\Common\Contracts\ExceptionHandlerInterface;
use Laudis\Common\Contracts\FinalExceptionHandler;
use Laudis\Common\Contracts\MutableContainerInterface as Container;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\Common\Handlers\DebugExceptionHandler;
use Laudis\Common\Handlers\NotFoundHandler;
use Laudis\Common\Handlers\ProductionExceptionHandler;
use Laudis\Common\Handlers\ThrowableHandler;
use Laudis\Common\Handlers\ValidationExceptionHandler;
use Laudis\Common\Presenters\ValidationPresenter;
use Laudis\UserManagement\Handlers\UnauthenticatedExceptionHandler;
use Laudis\UserManagement\Handlers\UnauthorizedExceptionHandler;

/**
 * Class ExceptionHandlerRegister
 * Registers all exception handling.
 *
 * @package Laudis\Calculators\Registers
 */
final class ExceptionHandlerRegister
{
    /**
     * Registers all the exception handling to the container.
     *
     * @param Container $container
     */
    public function register(Container $container): void
    {
        $this->registerFrameworkHandlers($container);
        $container->set(FinalExceptionHandler::class, Closure::fromCallable([$this, 'finalExceptionHandler']));
        $container->set('exceptionHandlers', Closure::fromCallable([$this, 'exceptionHandlers']));
    }

    /**
     * Registers and overrides the basic error handlers from the slim framework.
     *
     * @param Container $container
     */
    private function registerFrameworkHandlers(Container $container): void
    {
        $handler = static function () use ($container) {
            return new ThrowableHandler(
                $container->get(FinalExceptionHandler::class),
                $container->get('exceptionHandlers')
            );
        };

        $container->set('errorHandler', $handler);
        $container->set('phpErrorHandler', $handler);
    }

    /**
     * Registers the final exception handler.
     *
     * @param Container $container
     * @return ExceptionHandlerInterface
     *
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function finalExceptionHandler(Container $container): ExceptionHandlerInterface
    {
        if ((bool) ($container->get('settings')['debug'] ?? true)) {
            return new DebugExceptionHandler($container->get(ResponseWriterInterface::class));
        }
        return new ProductionExceptionHandler($container->get(ResponseWriterInterface::class));
    }

    /**
     * Registers all other exception handlers.
     *
     * @param Container $container
     * @return array
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function exceptionHandlers(Container $container): array
    {
        $tbr = [
            new NotFoundHandler($container->get(ResponseWriterInterface::class)),
            new ValidationExceptionHandler($container->get(ResponseWriterInterface::class), new ValidationPresenter),
            new UnauthenticatedExceptionHandler(),
            new UnauthorizedExceptionHandler()
        ];

        return $tbr;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use Casbin\Enforcer;
use DateTime;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;
use Laudis\Calculators\Margeregeling\Auxiliaries\MargeregelingAuxiliaries;
use Laudis\Calculators\Margeregeling\MargeregelingCalculator;
use Laudis\Calculators\Margeregeling\MargeregelingCalculatorFactory;
use Laudis\Calculators\GramFormula\GramFormulaCalculatorFactory;
use Laudis\Calculators\TaxShelter\Boekingen\BoekingenFactory;
use Laudis\Calculators\TaxShelter\Raming\RamingFactory;
use Laudis\Calculators\TaxShelter\TaxShelterCalulatorFactory;
use Laudis\Calculators\TaxShelter\TaxShelterInputFactory;
use Laudis\Calculators\TaxShelter\TaxShelterOperationInputFactory;
use Laudis\Calculators\TaxShelter\ValidationFactory;
use Laudis\Calculators\Venb\VenbCalculatorFactory;
use Laudis\Calculators\VoordeelWagen\Auxiliaries\VoordeelWagenAuxiliaries;
use Laudis\Calculators\VoordeelWagen\VoordeelWagenCalculatorFactory;
use Laudis\Common\Contracts\MutableContainerInterface;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Scale\Contracts\ContextualRepositoryScaleInterface;
use Laudis\UserManagement\AuthorizationMiddleware;
use Laudis\UserManagement\UserManager;
use Psr\Container\ContainerInterface;
use Rakit\Validation\Validator;

/**
 * Class CalculatorRegister
 * Registry of all calculators.
 *
 * @package Laudis\Calculators\Registers
 */
final class CalculatorRegister
{
    /**
     * Registers all calculators to the calculator factories collection.
     *
     * @param CalculatorFactoriesCollection $collection
     * @param MutableContainerInterface $container
     */
    public function register(CalculatorFactoriesCollection $collection, MutableContainerInterface $container): void
    {
        $this->registerCalculatorFactory($container);
        $this->registerInputFactory($container);
        $this->registerTaxShelter($collection, $container);
        $this->registerTaxShelterRaming($collection, $container);

        $collection->register('gramformule', static function () {
            return new GramFormulaCalculatorFactory();
        });

        $container->set('canCalculate', static function (ContainerInterface $container) {
            return new AuthorizationMiddleware(
                $container->get(Enforcer::class),
                $container->get(UserManager::class),
                null,
                'calculate'
            );
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerCalculatorFactory(MutableContainerInterface $container): void
    {
        $container->set(TaxShelterCalulatorFactory::class, static function (ContainerInterface $container) {
            return new TaxShelterCalulatorFactory(
                new TaxShelterInputFactory(new DateTime()),
                new ValidationFactory(new Validator()),
                $container->get(ContextualIndexedValueRepositoryInterface::class),
                $container->get(ContextualRepositoryScaleInterface::class)
            );
        });

        $container->set(VoordeelWagenCalculatorFactory::class, static function (ContainerInterface $container) {
            return new VoordeelWagenCalculatorFactory(
                new VoordeelWagenAuxiliaries(
                    $container->get(ContextualRepositoryScaleInterface::class),
                    $container->get(ContextualIndexedValueRepositoryInterface::class)
                ),
                new GramFormulaCalculatorFactory()
            );
        });

        $container->set(MargeregelingCalculatorFactory::class, static function (ContainerInterface $container){
            return new MargeregelingCalculatorFactory(
                new MargeregelingAuxiliaries()
            );
        });

        $container->set(VenbCalculatorFactory::class, function (ContainerInterface $container) {
            return new VenbCalculatorFactory($container->get(ContextualRepositoryScaleInterface::class),
                $container->get(ContextualIndexedValueRepositoryInterface::class));
        });

        $container->set(TaxShelterOperationInputFactory::class, static function (ContainerInterface $container) {
            return new TaxShelterOperationInputFactory(
                $container->get(ContextualIndexedValueRepositoryInterface::class),
                $container->get(ContextualRepositoryScaleInterface::class)
            );
        });

        $collection = $container->get(CalculatorFactoriesCollection::class);

        $collection->register('tax_shelter', static function () use ($container) {
            return $container->get(TaxShelterCalulatorFactory::class);
        });

        $collection->register('voordeelwagen', static function () use ($container) {
            return $container->get(VoordeelWagenCalculatorFactory::class);
        });

        $collection->register('margeregeling', static function () use ($container){
           return $container->get(MargeregelingCalculatorFactory::class);
        });

        $collection->register('venb', static function () use ($container) {
            return $container->get(VenbCalculatorFactory::class);
        });

        $collection->register('tax_shelter_raming', static function () use ($container) {
            return new RamingFactory(
                $container->get(TaxShelterCalulatorFactory::class),
                $container->get(ContextualIndexedValueRepositoryInterface::class)
            );
        });

        $collection->register('tax_shelter_boeking', static function () use ($collection, $container) {
            /** @var RamingFactory $ramingFactory */
            $ramingFactory = $collection->get('tax_shelter_raming');
            return new BoekingenFactory(
                $ramingFactory,
                $container->get(ContextualIndexedValueRepositoryInterface::class)
            );
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerInputFactory(MutableContainerInterface $container): void
    {
        $container->set(TaxShelterOperationInputFactory::class, static function (ContainerInterface $container) {
            return new TaxShelterOperationInputFactory(
                $container->get(ContextualIndexedValueRepositoryInterface::class),
                $container->get(ContextualRepositoryScaleInterface::class)
            );
        });
    }

    /**
     * @param CalculatorFactoriesCollection $collection
     * @param MutableContainerInterface $container
     */
    private function registerTaxShelter(
        CalculatorFactoriesCollection $collection,
        MutableContainerInterface $container
    ): void {
        $collection->register('tax_shelter', static function () use ($container) {
            return $container->get(TaxShelterCalulatorFactory::class);
        });
    }

    /**
     * @param CalculatorFactoriesCollection $collection
     * @param MutableContainerInterface $container
     */
    private function registerTaxShelterRaming(
        CalculatorFactoriesCollection $collection,
        MutableContainerInterface $container
    ): void {
        $collection->register('tax_shelter_raming', static function () use ($container) {
            return new RamingFactory(
                $container->get(TaxShelterCalulatorFactory::class),
                $container->get(ContextualIndexedValueRepositoryInterface::class)
            );
        });
    }
}

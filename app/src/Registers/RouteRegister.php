<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use Laudis\Calculators\Controllers\CalculationController;
use Laudis\Calculators\Controllers\CalculatorsController;
use Laudis\Common\Contracts\RouterInterface;
use Laudis\Common\Controllers\PreFlightCorsController;
use Laudis\IndexApi\IndexedValueController;
use Laudis\UserManagement\AuthenticateMiddleware;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\Controllers\ChangePasswordWithLinkController;
use Laudis\UserManagement\Controllers\ResetPasswordLinkController;
use Laudis\UserManagement\Controllers\UpdatePasswordController;

/**
 * Class RouteRegister
 * Registers all routes of the application.
 *
 * @package Laudis\Calculators\Registers
 */
final class RouteRegister
{
    /** @var array */
    private const FALLBACK_METHODS = ['GET', 'POST', 'PUT', 'PATCH'];

    /**
     * @param RouterInterface $router
     */
    public function register(RouterInterface $router): void
    {
        $router->get('/calculators', CalculatorsController::class)->setName('calculators');

        # Fallback route because options route now triggers Method Not Allowed 405.

        $router->post('/token', AuthTokenIssueController::class)->setName('issueToken');
        $router->post('/reset-password', ResetPasswordLinkController::class)->setName('resetPasswordLink');
        $router->post('/change-password', ChangePasswordWithLinkController::class)->setName('changePasswordWithLink');
        $router->post('/update-password', UpdatePasswordController::class)->setName('updatePassword');

        $router->get('/indexed-values', IndexedValueController::class . ':index');
        $router->delete('/indexed-values', IndexedValueController::class . ':delete');
        $router->get('/indexed-values/search', IndexedValueController::class . ':search');
        $router->post('/indexed-values', IndexedValueController::class . ':create');
        $router->put('/indexed-values', IndexedValueController::class . ':update');
        $router->get('/indexed-values/{id}', IndexedValueController::class . ':show');

        $this->registerProtectedRoutes($router);


        $router->options('/{routes:.+}', PreFlightCorsController::class . ':allowOptions')->setName('preflight');
        $router->map(self::FALLBACK_METHODS, '/{routes:.+}', PreFlightCorsController::class . ':fallback')
            ->setName('preflight_fallback');
    }

    /**
     * @param RouterInterface $router
     */
    private function registerProtectedRoutes(RouterInterface $router): void
    {
        $router->group('', function () use ($router): void {
            $router->post('/calculator/{calculator}', CalculationController::class)
                ->setName('calculator')
                ->add('canCalculate');

        })->add(AuthenticateMiddleware::class);
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\Registers;

use Closure;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;
use Laudis\Calculators\Strategies\BasicRequestToCalculation;
use Laudis\Common\Contracts\MutableContainerInterface as Container;
use Laudis\Common\Contracts\RequestToCalculationInterface;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\Common\Strategies\WriteJsonToResponse;
use Psr\Container\ContainerInterface;
use Rakit\Validation\Validator;

/**
 * Class StrategiesRegister
 * @package Laudis\Calculators\Registers
 */
final class StrategiesRegister
{
    /**
     * @param Container $container
     */
    public function register(Container $container): void
    {
        $container->set(Validator::class, Closure::fromCallable([$this, 'buildValidator']));
        $container->set(ResponseWriterInterface::class, new WriteJsonToResponse);
        $container->set(RequestToCalculationInterface::class, Closure::fromCallable([
            $this,
            'buildBasicRequestToCalculation'
        ]));
    }

    /**
     * @return Validator
     *
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function buildValidator(): Validator
    {
        return new Validator([
            'required' => 'gelieve dit veld in te vullen',
            'numeric' => 'dit veld moet een getal zijn',
            'integer' => 'dit veld moet een geheel getal zijn'
        ]);
    }

    /**
     * @param ContainerInterface $container
     * @return BasicRequestToCalculation
     *
     * @SuppressWarnings(PHPMD.UnusedPrivateMethod)
     */
    private function buildBasicRequestToCalculation(ContainerInterface $container): BasicRequestToCalculation
    {
        return new BasicRequestToCalculation(
            $container->get(CalculatorFactoriesCollection::class),
            $container->get('currency_formatter')
        );
    }
}

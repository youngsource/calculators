<?php
declare(strict_types=1);

namespace Laudis\Calculators\Presenters;

use function count;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;

/**
 * Class CalculatorsPresenter
 * Presents the calculator factories collection.
 *
 * @package Laudis\Calculators\Presenters
 */
final class CalculatorsPresenter
{
    /**
     * Presents the collection as an array.
     *
     * @param CalculatorFactoriesCollection $collection
     * @return array
     */
    public function present(CalculatorFactoriesCollection $collection): array
    {
        $names = $collection->getAllCalculatorNames();
        return [
            'meta' => [
                'count' => count($names)
            ],
            'data' => $names
        ];
    }
}

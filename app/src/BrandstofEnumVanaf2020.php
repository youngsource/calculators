<?php


namespace Laudis\Calculators;


use Laudis\Calculators\VoordeelWagen\BrandstofEnumTot2020;

/**
 * Class BrandstofEnumVanaf2020
 * @package Laudis\Calculators
 *
 * @method static BrandstofEnumVanaf2020 CNG_MIN_12PK()
 * @method static BrandstofEnumVanaf2020 CNG_PLUS_12PK()
 * @method static BrandstofEnumVanaf2020 LPG()
 * @method static BrandstofEnumVanaf2020 HYBRIDE_BENZINE()
 * @method static BrandstofEnumVanaf2020 HYBRIDE_DIESEL()
 * @method static BrandstofEnumVanaf2020 HYBRIDE_CNG_MIN_12PK()
 * @method static BrandstofEnumVanaf2020 HYBRIDE_CNG_PLUS_12PK()
 * @method static BrandstofEnumVanaf2020 HYBRIDE_LPG()
 */
class BrandstofEnumVanaf2020 extends BrandstofEnumTot2020
{
    protected const CNG_MIN_12PK = 'CNG_MIN_12PK';
    protected const CNG_PLUS_12PK = 'CNG_PLUS_12PK';
    protected const LPG = 'LPG';
    protected const HYBRIDE_BENZINE = 'HYBRIDE_BENZINE';
    protected const HYBRIDE_DIESEL = 'HYBRIDE_DIESEL';
    protected const HYBRIDE_CNG_MIN_12PK = 'HYBRIDE_CNG_MIN_12PK';
    protected const HYBRIDE_CNG_PLUS_12PK = 'HYBRIDE_CNG_PLUS_12PK';
    protected const HYBRIDE_LPG = 'HYBRIDE_LPG';
}
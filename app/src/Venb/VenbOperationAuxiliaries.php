<?php

declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 11/07/19
 * Time: 15:01
 */

namespace Laudis\Calculators\Venb;

use DateTimeInterface;
use Laudis\Calculators\Venb\Filters\VenbFilter;
use Laudis\Calculators\Venb\Input\VoorafBetalingenInput;
use Laudis\Calculators\Venb\Results\BelastbaartariefResult;
use Laudis\Calculators\Venb\Results\VenbAanslagenOutput;
use Laudis\Calculators\Venb\Results\VenbVoorafbetalingenOutput;
use Laudis\Calculators\Venb\VersieAj2021New\Venb2021Input;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Scale\Contracts\ScaleInterface;
use Laudis\Scale\ScalePresenter;

use phpDocumentor\Reflection\Types\This;
use function number_format;


/**
 * Class VenbOperationAuxiliaries
 * @package Laudis\Calculators\Venb
 */
final class VenbOperationAuxiliaries
{
    /** @var VenbFilter */
    private $filter;
    /** @var ScaleInterface */
    private $barema;
    /** @var ContextualIndexedValueRepositoryInterface */
    private $indexedValues;
    /** @var DateTimeInterface */
    private $context;
    /** @var ScalePresenter */
    private $scalePresenter;

    /**
     * VenbOperationAuxiliaries constructor.
     * @param ScaleInterface $barema
     * @param ContextualIndexedValueRepositoryInterface $indexedValues
     * @param VenbFilter $filter
     * @param ScalePresenter $scalePresenter
     * @param DateTimeInterface $context
     */
    public function __construct(
        ScaleInterface $barema,
        ContextualIndexedValueRepositoryInterface $indexedValues,
        VenbFilter $filter,
        ScalePresenter $scalePresenter,
        DateTimeInterface $context
    ) {
        $this->barema = $barema;
        $this->indexedValues = $indexedValues;
        $this->filter = $filter;
        $this->scalePresenter = $scalePresenter;
        $this->context = $context;
    }

    /**
     * @param float $value
     * @return float
     */
    public function calculateScale(
        float $value
    ): float {
        return $this->barema->calculate($value);
    }

    /**
     * @param float|int $value
     * @return array
     */
    public function presentBarema($value): array
    {
        $presentation = $this->scalePresenter->present($value);
        $belasting = $presentation['explanation'];

        $current = array_first($belasting);
        $i = 0;
        while ($current['valueInRule']) {
            $output[$i]['description'] = number_format($current['start'], 2, ',', '.')
                . ' tot ' . number_format($current['end'] ?? $value, 2, ',', '.');
            $output[$i]['bedrag'] = $current['lhs'];
            $output[$i]['operation'] = '&times;';
            $output[$i]['percentage'] = number_format($current['rhs'] * 100, 2, ',', ' ') . '%';
            $output[$i]['is'] = '=';
            $output[$i]['result'] = $current['lhs'] * $current['rhs'];
            $current = next($belasting);
            ++$i;
        }
        $output[] = $presentation['output'];

        return $output;
    }

    /**
     * @param array $array
     * @return VenbAanslagenOutput
     */
    public function calculateTotalOfArray(array $array): VenbAanslagenOutput
    {
        $crisibijdrageSom = 0;

        foreach ($array as $calculation) {
            foreach ($calculation as $v) {
                if (is_array($v['belasting'])) {
                    $crisibijdrageSom += array_last($v['belasting']);
                } else {
                    $crisibijdrageSom += $v['belasting'];
                }
            }
        }
        return new VenbAanslagenOutput($array, $crisibijdrageSom);
    }

    /**
     * @param VenbGeneralInput | VenbInput2018 $input
     * @return float
     */
    public function voorafBetalingenTotaal($input): float
    {
        $total = 0;
        for ($i = 4; $i > 4 - $input->getAantalkwartalen(); $i--) {
            $method = 'getVoorafbetalingKwart' . $i;
            $total -= $input->getVoorafBetalingenInput()->$method();
        }

        return $total;
    }

    /**
     * @param string $identifier
     * @return float
     */
    public function indexedValue(string $identifier): float
    {
        return $this->indexedValues->getFromDate($this->context, $identifier)->getValue();
    }

    /**
     * @param string $number
     * @param float $amount
     * @param $percentage
     * @return TaxationRule
     */
    public function belastingCalculation(
        string $number,
        float $amount,
        $percentage
    ): TaxationRule {
        $belastingenEnCrisisbijdrage = 0;

        if ($amount > 0) {
            $belastingenEnCrisisbijdrage = $amount * $percentage;
        }
        return new TaxationRule($number, $amount, $percentage, $belastingenEnCrisisbijdrage);
    }

    /**
     * @param string $number
     * @param VenbFilter $filter
     * @param float $amount
     * @param float $percentage
     * @param bool $letterBeforeNumber
     * @return TaxationRule
     */
    public function crisisBijdrageCalculation(
        string $number,
        VenbFilter $filter,
        float $amount,
        float $percentage,
        bool $letterBeforeNumber
    ): TaxationRule {
        /// TODO -
        /// This creates a subtle bug:
        /// The flag letterBeforeNumber creates the illusion it is possible to use the filter in (O|N)(\d+) format
        /// Which it does not. We either need to get rid of that flag, or take in to account the possibility of
        /// this format in the filter.
        $belastingenEnCrisisbijdrage = 0;

        if ($filter->isCrisisbijdrage($number) && $amount > 0) {
            $belastingenEnCrisisbijdrage = $amount * $percentage;
        }
        if ($letterBeforeNumber) {
            return new TaxationRule($filter->output($number), $amount, $percentage, $belastingenEnCrisisbijdrage);
        }
        return new TaxationRule($number, $amount, $percentage, $belastingenEnCrisisbijdrage);
    }

    /**
     * @return VenbFilter
     */
    public function getFilter(): VenbFilter
    {
        return $this->filter;
    }

    public function getAmountBelastbaarInkomen(BelastbaartariefResult $belastbaartariefResult): float
    {
        if (is_array($belastbaartariefResult->getData()['belasting'])) {
            $belasting = array_last($belastbaartariefResult->getData()['belasting']);
        } else {
            $belasting = $belastbaartariefResult->getData()['belasting'];
        }
        return $belasting;
    }

    /**
     * @param venb2021Input $input
     * @return VenbVoorafbetalingenOutput
     *
     * This function overrides the natural flow of the program adjusting the percentages because of the corona pandemic
     */
    private function verhoogdeVA3VA4(VoorafBetalingenInput $input): VenbVoorafbetalingenOutput
    {
        $percentages = [0.09,0.075,0.0675,0.0525];
        $totaal = 0;

        $tmp = [];
        for ($i = 4; $i > 4 - $input->getAantalKwartalen(); $i--) {
            $method = 'getVoorafbetalingKwart' . $i;
            $tmp[$i]['number'] = 'Voorafbetaling ' . $i;
            $tmp[$i]['amount'] = $input->$method();
            $tmp[$i]['operation'] = '&times;';
            $tmp[$i]['percentage'] = number_format($percentages[$i-1] * 100, 2,',', ' '). '%';
            $tmp[$i]['is'] = '=';
            $tmp[$i]['result'] = -$input->$method() * $percentages[$i-1];

            $totaal -= $input->$method() * $percentages[$i-1];
        }
        return new VenbVoorafbetalingenOutput($tmp, $totaal);
    }


    /**
     * @param VoorafBetalingenInput $input
     * @param bool $verhoogd
     * @return VenbVoorafbetalingenOutput
     */
    public function voorafBetalingen(VoorafBetalingenInput $input, bool $verhoogd= false): VenbVoorafbetalingenOutput
    {
        if($verhoogd){
            return $this->verhoogdeVA3VA4($input);
        }

        $totaal = 0;
        $tmp = [];
        for ($i = 4; $i > 4 - $input->getAantalKwartalen(); $i--) {
            $method = 'getVoorafbetalingKwart' . $i;
            $tmp[$i]['number'] = 'Voorafbetaling ' . $i;
            $tmp[$i]['amount'] = $input->$method();

            $tmp[$i]['operation'] = '&times;';
            $tmp[$i]['percentage'] = number_format($this->indexedValue('VA' . $i) * 100, 2,',', ' '). '%';
            $tmp[$i]['is'] = '=';
            $tmp[$i]['result'] = -$input->$method() * $this->indexedValue('VA' . $i);

            $totaal -= $input->$method() * $this->indexedValue('VA' . $i);
        }
        return new VenbVoorafbetalingenOutput($tmp, $totaal);
    }
}

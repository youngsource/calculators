<?php
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 11/07/19
 * Time: 15:45
 */

namespace Laudis\Calculators\Venb;


use Laudis\Calculators\Venb\Input\VoorafBetalingenInput;

class VenbGeneralInput
{
    /**
     * @var float
     */
    private $belastbaarGewoonTarief;
    /**
     * @var bool
     */
    private $isAanspraakVerminderdTarief;
    /**
     * @var bool
     */
    private $isVenootshapHuisvesting;

    /**
     * @var bool
     */
    private $isBelastbaarTijdPerkTenVroegste01012018;
    /**
     * @var bool
     */
    private $isWijzigingAfsluitdatum26072017;
    /**
     * @var float
     */
    private $meerwaardeOpAandelen25;
    /**
     * @var float
     */
    private $belastbareTegenExitTaks125;
    /**
     * @var float
     */
    private $kapitaalenInterestsubsidiesLandboouw05;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagAlleAard50;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagAlleAard100;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagUitgekeerdeDividenden;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagLiqRes10;
    /**
     * @var float
     */
    private $geheleOfGedeeltelijkMaatschappelijkVermogen33;
    /**
     * @var float
     */
    private $geheleOfGedeeltelijkMaatschappelijkVermogen165;
    /**
     * @var float
     */
    private $voordeleAlleAardVerleend;
    /**
     * @var bool
     */
    private $isStartendeKMO;
    /**
     * @var float
     */
    private $terugbetalingTegen100;
    /**
     * @var float
     */
    private $nietTerugbetaalbareVoorheffing;
    /**
     * @var float
     */
    private $buitenlandseBelastingkrediet;
    /**
     * @var float
     */
    private $terugbetaalbareVoorheffing;
    /**
     * @var float
     */
    private $belastingkredietOndOntw;
    /** @var VoorafBetalingenInput */
    private $voorafBetalingenInput;
    /**
     * @var float
     */
    private $latenteMeerwaarden;
    /**
     * @var float
     */
    private $negatieveCorrectieGrondslagGewoonTarief;
    /**
     * @var float
     */
    private $positieveCorrectieGrondslagGewoonTarief;

    /**
     * VenbGeneralInput constructor.
     * @param float $belastbaarGewoonTarief
     * @param bool $isAanspraakVerminderdTarief
     * @param bool $isVenootshapHuisvesting
     * @param bool $isBelastbaarTijdPerkTenVroegste01012018
     * @param bool $isWijzigingAfsluitdatum26072017
     * @param float $meerwaardeOpAandelen25
     * @param float $belastbareTegenExitTaks125
     * @param float $kapitaalenInterestsubsidiesLandboouw05
     * @param float $afzonderlijkeAanslagAlleAard50
     * @param float $afzonderlijkeAanslagAlleAard100
     * @param float $afzonderlijkeAanslagBelasteReservesKredietInstellingen34
     * @param float $afzonderlijkeAanslagBelasteReservesKredietInstellingen28
     * @param float $afzonderlijkeAanslagUitgekeerdeDividenden
     * @param float $afzonderlijkeAanslagLiqRes10
     * @param float $geheleOfGedeeltelijkMaatschappelijkVermogen33
     * @param float $geheleOfGedeeltelijkMaatschappelijkVermogen165
     * @param float $voordeleAlleAardVerleend
     * @param bool $isStartendeKMO
     * @param float $terugbetalingTegen100
     * @param float $nietTerugbetaalbareVoorheffing
     * @param float $buitenlandseBelastingkrediet
     * @param float $terugbetaalbareVoorheffing
     * @param float $belastingkredietOndOntw
     * @param VoorafBetalingenInput $voorafBetalingenInput
     * @param float $latenteMeerwaarden
     * @param float $negatieveCorrectieGrondslagGewoonTarief
     * @param float $positieveCorrectieGrondslagGewoonTarief
     */
    public function __construct(
        float $belastbaarGewoonTarief,
        bool $isAanspraakVerminderdTarief,
        bool $isVenootshapHuisvesting,
        bool $isBelastbaarTijdPerkTenVroegste01012018,
        bool $isWijzigingAfsluitdatum26072017,
        float $meerwaardeOpAandelen25,
        float $belastbareTegenExitTaks125,
        float $kapitaalenInterestsubsidiesLandboouw05,
        float $afzonderlijkeAanslagAlleAard50,
        float $afzonderlijkeAanslagAlleAard100,
        float $afzonderlijkeAanslagBelasteReservesKredietInstellingen34,
        float $afzonderlijkeAanslagBelasteReservesKredietInstellingen28,
        float $afzonderlijkeAanslagUitgekeerdeDividenden,
        float $afzonderlijkeAanslagLiqRes10,
        float $geheleOfGedeeltelijkMaatschappelijkVermogen33,
        float $geheleOfGedeeltelijkMaatschappelijkVermogen165,
        float $voordeleAlleAardVerleend,
        bool $isStartendeKMO,
        float $terugbetalingTegen100,
        float $nietTerugbetaalbareVoorheffing,
        float $buitenlandseBelastingkrediet,
        float $terugbetaalbareVoorheffing,
        float $belastingkredietOndOntw,
        VoorafBetalingenInput $voorafBetalingenInput,
        float $latenteMeerwaarden,
        float $negatieveCorrectieGrondslagGewoonTarief = 0,
        float $positieveCorrectieGrondslagGewoonTarief = 0)
    {
        $this->belastbaarGewoonTarief = $belastbaarGewoonTarief;
        $this->isAanspraakVerminderdTarief = $isAanspraakVerminderdTarief;
        $this->isVenootshapHuisvesting = $isVenootshapHuisvesting;
        $this->isBelastbaarTijdPerkTenVroegste01012018 = $isBelastbaarTijdPerkTenVroegste01012018;
        $this->isWijzigingAfsluitdatum26072017 = $isWijzigingAfsluitdatum26072017;
        $this->meerwaardeOpAandelen25 = $meerwaardeOpAandelen25;
        $this->belastbareTegenExitTaks125 = $belastbareTegenExitTaks125;
        $this->kapitaalenInterestsubsidiesLandboouw05 = $kapitaalenInterestsubsidiesLandboouw05;
        $this->afzonderlijkeAanslagAlleAard50 = $afzonderlijkeAanslagAlleAard50;
        $this->afzonderlijkeAanslagAlleAard100 = $afzonderlijkeAanslagAlleAard100;
        $this->afzonderlijkeAanslagBelasteReservesKredietInstellingen34 = $afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
        $this->afzonderlijkeAanslagBelasteReservesKredietInstellingen28 = $afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
        $this->afzonderlijkeAanslagUitgekeerdeDividenden = $afzonderlijkeAanslagUitgekeerdeDividenden;
        $this->afzonderlijkeAanslagLiqRes10 = $afzonderlijkeAanslagLiqRes10;
        $this->geheleOfGedeeltelijkMaatschappelijkVermogen33 = $geheleOfGedeeltelijkMaatschappelijkVermogen33;
        $this->geheleOfGedeeltelijkMaatschappelijkVermogen165 = $geheleOfGedeeltelijkMaatschappelijkVermogen165;
        $this->voordeleAlleAardVerleend = $voordeleAlleAardVerleend;
        $this->isStartendeKMO = $isStartendeKMO;
        $this->terugbetalingTegen100 = $terugbetalingTegen100;
        $this->nietTerugbetaalbareVoorheffing = $nietTerugbetaalbareVoorheffing;
        $this->buitenlandseBelastingkrediet = $buitenlandseBelastingkrediet;
        $this->terugbetaalbareVoorheffing = $terugbetaalbareVoorheffing;
        $this->belastingkredietOndOntw = $belastingkredietOndOntw;
        $this->voorafBetalingenInput = $voorafBetalingenInput;
        $this->latenteMeerwaarden = $latenteMeerwaarden;
        $this->negatieveCorrectieGrondslagGewoonTarief = $negatieveCorrectieGrondslagGewoonTarief;
        $this->positieveCorrectieGrondslagGewoonTarief = $positieveCorrectieGrondslagGewoonTarief;
    }

    /**
     * @return float
     */
    public function getBelastbaarGewoonTarief(): float
    {
        return $this->belastbaarGewoonTarief;
    }

    /**
     * @return bool
     */
    public function isAanspraakVerminderdTarief(): bool
    {
        return $this->isAanspraakVerminderdTarief;
    }

    /**
     * @return bool
     */
    public function isVenootshapHuisvesting(): bool
    {
        return $this->isVenootshapHuisvesting;
    }

    /**
     * @return int
     */
    public function getAantalKwartalen(): int
    {
        return $this->voorafBetalingenInput->getAantalKwartalen();
    }

    /**
     * @return bool
     */
    public function isBelastbaarTijdPerkTenVroegste01012018(): bool
    {
        return $this->isBelastbaarTijdPerkTenVroegste01012018;
    }

    /**
     * @return bool
     */
    public function isWijzigingAfsluitdatum26072017(): bool
    {
        return $this->isWijzigingAfsluitdatum26072017;
    }

    /**
     * @return float
     */
    public function getMeerwaardeOpAandelen25(): float
    {
        return $this->meerwaardeOpAandelen25;
    }

    /**
     * @return float
     */
    public function getBelastbareTegenExitTaks125(): float
    {
        return $this->belastbareTegenExitTaks125;
    }

    /**
     * @return float
     */
    public function getKapitaalenInterestsubsidiesLandboouw05(): float
    {
        return $this->kapitaalenInterestsubsidiesLandboouw05;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagAlleAard50(): float
    {
        return $this->afzonderlijkeAanslagAlleAard50;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagAlleAard100(): float
    {
        return $this->afzonderlijkeAanslagAlleAard100;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagBelasteReservesKredietInstellingen34(): float
    {
        return $this->afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagBelasteReservesKredietInstellingen28(): float
    {
        return $this->afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagUitgekeerdeDividenden(): float
    {
        return $this->afzonderlijkeAanslagUitgekeerdeDividenden;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagLiqRes10(): float
    {
        return $this->afzonderlijkeAanslagLiqRes10;
    }

    /**
     * @return float
     */
    public function getGeheleOfGedeeltelijkMaatschappelijkVermogen33(): float
    {
        return $this->geheleOfGedeeltelijkMaatschappelijkVermogen33;
    }

    /**
     * @return float
     */
    public function getGeheleOfGedeeltelijkMaatschappelijkVermogen165(): float
    {
        return $this->geheleOfGedeeltelijkMaatschappelijkVermogen165;
    }

    /**
     * @return float
     */
    public function getVoordeleAlleAardVerleend(): float
    {
        return $this->voordeleAlleAardVerleend;
    }

    /**
     * @return bool
     */
    public function isStartendeKMO(): bool
    {
        return $this->isStartendeKMO;
    }

    /**
     * @return float
     */
    public function getTerugbetalingTegen100(): float
    {
        return $this->terugbetalingTegen100;
    }

    /**
     * @return float
     */
    public function getNietTerugbetaalbareVoorheffing(): float
    {
        return $this->nietTerugbetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getBuitenlandseBelastingkrediet(): float
    {
        return $this->buitenlandseBelastingkrediet;
    }

    /**
     * @return float
     */
    public function getTerugbetaalbareVoorheffing(): float
    {
        return $this->terugbetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getBelastingkredietOndOntw(): float
    {
        return $this->belastingkredietOndOntw;
    }

    /**
     * @return float
     */
    public function getLatenteMeerwaarden(): float
    {
        return $this->latenteMeerwaarden;
    }

    /**
     * @return float
     */
    public function getNegatieveCorrectieGrondslagGewoonTarief(): float
    {
        return $this->negatieveCorrectieGrondslagGewoonTarief;
    }

    /**
     * @return float
     */
    public function getPositieveCorrectieGrondslagGewoonTarief(): float
    {
        return $this->positieveCorrectieGrondslagGewoonTarief;
    }

    /**
     * @return VoorafBetalingenInput
     */
    public function getVoorafBetalingenInput(): VoorafBetalingenInput
    {
        return $this->voorafBetalingenInput;
    }
}
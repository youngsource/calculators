<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb\Filters;

use function in_array;

/**
 * Class VenbFilter
 * @package Laudis\Calculators\Venb\Filters
 */
final class VenbFilter
{
    /**
     * @var array
     */
    private $crisisbijdrageBools;
    /**
     * @var string
     */
    private $letter;

    /**
     * VenbFilter constructor.
     * @param array $crisisbijdrageBools
     * @param array $sanctieVABools
     */
    public function __construct(array $crisisbijdrageBools, string $letter)
    {
        $this->crisisbijdrageBools = $crisisbijdrageBools;
        $this->letter = $letter;
    }

    /**
     * @return VenbFilter
     */
    public static function make2018AJ(): VenbFilter
    {
        return new self([
                '1460',
                '1465',
                '1466',
                '1471',
                '1472',
                '1424',
                '1506',
                '1507',
                '1502a',
                '1502b',
                '1503',
                '1504'
            ],'O'
        );
    }

    /**
     * @return VenbFilter
     */
    public static function make2019AJ(): VenbFilter
    {
        return new self(
            [
                '1460',
                '1466',
                '1465',
                '1472',
                '1506',
                '1507',
                '1502a',
                '1502b'
            ], 'N');
    }

    /**
     * @param string $number
     * @return bool
     */
    public function isCrisisbijdrage(string $number): bool
    {
        return in_array($number, $this->crisisbijdrageBools, true);
    }

    public function output(string $number) : string
    {
        return $this->letter . ' ' . $number;
    }

}

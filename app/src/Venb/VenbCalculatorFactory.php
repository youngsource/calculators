<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb;

use DateTime;
use DateTimeInterface;
use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\Venb\Filters\VenbFilter;
use Laudis\Calculators\Venb\Input\VoorafBetalingenInput;
use Laudis\Calculators\Venb\Rules\LessThan;
use Laudis\Calculators\Venb\Rules\LessThanMinus;
use Laudis\Calculators\Venb\Rules\LessThanSum;
use Laudis\Calculators\Venb\VersieAj2018\VenbAJ2018Operations;
use Laudis\Calculators\Venb\VersieAj2019New\Venb2019NewOperations;
use Laudis\Calculators\Venb\VersieAj2019New\Venb2019InputNew;
use Laudis\Calculators\Venb\VersieAj2019Old\Venb2019OldOperations;
use Laudis\Calculators\Venb\VersieAj2019Old\Venb2019InputOld;
use Laudis\Calculators\Venb\VersieAj2020\Venb2020Input;
use Laudis\Calculators\Venb\VersieAj2020\Venb2020Operations;
use Laudis\Calculators\Venb\VersieAj2021New\Venb2021Input;
use Laudis\Calculators\Venb\VersieAj2021New\Venb2021NewOperations;
use Laudis\Calculators\Venb\VersieAj2021Old\Venb2021OldOperations;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Scale\Contracts\ContextualRepositoryScaleInterface;
use Laudis\Scale\ScalePresenter;
use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

/**
 * Class VenbCalculator
 * @package Laudis\Calculators\Venb
 */
final class VenbCalculatorFactory implements CalculatorFactoryInterface
{
    /** @var ContextualRepositoryScaleInterface */
    private $repositoryScale;
    /** @var ContextualIndexedValueRepositoryInterface */
    private $indexedValueRepository;

    /**
     * VenbCalculatorFactory constructor.
     * @param ContextualRepositoryScaleInterface $repositoryScale
     * @param ContextualIndexedValueRepositoryInterface $indexedValueRepository
     */
    public function __construct(ContextualRepositoryScaleInterface $repositoryScale, ContextualIndexedValueRepositoryInterface $indexedValueRepository)
    {
        $this->repositoryScale = $repositoryScale;
        $this->indexedValueRepository = $indexedValueRepository;
    }


    /**
     * @param array $values
     * @return object
     * @throws \Exception
     */
    public function calculator(array $values): object
    {
        if ($values['version'] === 'v2018aj') {
            return new VenbAJ2018Operations(
                new VenbOperationAuxiliaries(
                    $this->repositoryScale->getFromDate(
                        DateTime::createFromFormat('Y-m-d', '2017-01-01'),
                        'venb_barema'
                    ),
                    $this->indexedValueRepository,
                    VenbFilter::make2018AJ(),
                    $this->baremaPresenter(simple_date(2017, 1, 1)),
                    DateTime::createFromFormat('Y-m-d', '2017-01-01'))
            );
        }

        if ($values['version'] === 'v2019ajOld') {
            return  new Venb2019OldOperations(
                    new VenbOperationAuxiliaries(
                        $this->repositoryScale->getFromDate(
                            DateTime::createFromFormat('Y-m-d', '2017-01-01'),
                            'venb_barema'
                        ),
                        $this->indexedValueRepository,
                        VenbFilter::make2018AJ(),
                        $this->baremaPresenter(simple_date(2017, 1, 1)),
                        DateTime::createFromFormat('Y-m-d', '2017-01-01')
                    ),
                    new VenbOperationAuxiliaries(
                        $this->repositoryScale->getFromDate(
                            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
                            'venb_barema'
                        ),
                        $this->indexedValueRepository,
                        VenbFilter::make2019AJ(),
                        $this->baremaPresenter(simple_date(2018, 1, 1)),
                        DateTime::createFromFormat('Y-m-d', '2018-01-01'))
                );
        }
        if ($values['version'] === 'v2019ajNew') {
            return  new Venb2019NewOperations(
                new VenbOperationAuxiliaries(
                    $this->repositoryScale->getFromDate(
                        DateTime::createFromFormat('Y-m-d', '2017-01-01'),
                        'venb_barema'
                    ),
                    $this->indexedValueRepository,
                    VenbFilter::make2018AJ(),
                    $this->baremaPresenter(simple_date(2017, 1, 1)),
                    DateTime::createFromFormat('Y-m-d', '2017-01-01')
                ),
                new VenbOperationAuxiliaries(
                    $this->repositoryScale->getFromDate(
                        DateTime::createFromFormat('Y-m-d', '2018-01-01'),
                        'venb_barema'
                    ),
                    $this->indexedValueRepository,
                    VenbFilter::make2019AJ(),
                    $this->baremaPresenter(simple_date(2018, 1, 1)),
                    DateTime::createFromFormat('Y-m-d', '2018-07-26'))
            );
        }
        if ($values['version'] === 'v2020aj') {
            return  new Venb2020Operations(
                new VenbOperationAuxiliaries(
                    $this->repositoryScale->getFromDate(
                        DateTime::createFromFormat('Y-m-d', '2019-01-01'),
                        'venb_barema'
                    ),
                    $this->indexedValueRepository,
                    VenbFilter::make2018AJ(),
                    $this->baremaPresenter(simple_date(2019, 1, 1)),
                    DateTime::createFromFormat('Y-m-d', '2019-01-01')
                ),
                new VenbOperationAuxiliaries(
                    $this->repositoryScale->getFromDate(
                        DateTime::createFromFormat('Y-m-d', '2019-01-01'),
                        'venb_barema'
                    ),
                    $this->indexedValueRepository,
                    VenbFilter::make2019AJ(),
                    $this->baremaPresenter(simple_date(2019, 1, 1)),
                    DateTime::createFromFormat('Y-m-d', '2019-01-01'))
            );
        }

        if ($values['version'] === 'v2021ajOld') {
            return  new Venb2021OldOperations(
                new VenbOperationAuxiliaries(
                    $this->repositoryScale->getFromDate(
                        DateTime::createFromFormat('Y-m-d', '2019-01-01'),
                        'venb_barema'
                    ),
                    $this->indexedValueRepository,
                    VenbFilter::make2018AJ(),
                    $this->baremaPresenter(simple_date(2019, 1, 1)),
                    DateTime::createFromFormat('Y-m-d', '2019-01-01')
                ),
                new VenbOperationAuxiliaries(
                    $this->repositoryScale->getFromDate(
                        DateTime::createFromFormat('Y-m-d', '2019-01-01'),
                        'venb_barema'
                    ),
                    $this->indexedValueRepository,
                    VenbFilter::make2019AJ(),
                    $this->baremaPresenter(simple_date(2019, 1, 1)),
                    DateTime::createFromFormat('Y-m-d', '2020-01-01'))
            );
        }

        return new Venb2021NewOperations(
                new VenbOperationAuxiliaries(
                    $this->repositoryScale->getFromDate(
                        DateTime::createFromFormat('Y-m-d', '2020-01-01'),
                        'verlaagd_tarief'
                    ),
                    $this->indexedValueRepository,
                    VenbFilter::make2018AJ(),
                    $this->baremaPresenter(simple_date(2020, 1, 1),"verlaagd_tarief"),
                    DateTime::createFromFormat('Y-m-d', '2020-01-01'))
                ,
                new VenbOperationAuxiliaries(
                    $this->repositoryScale->getFromDate(
                        DateTime::createFromFormat('Y-m-d', '2020-07-26'),
                        'verlaagd_tarief'
                    ),
                    $this->indexedValueRepository,
                    VenbFilter::make2019AJ(),
                    $this->baremaPresenter(simple_date(2020, 1, 1),"verlaagd_tarief"),
                    DateTime::createFromFormat('Y-m-d', '2020-07-26'))
            );
    }

    /**
     * @param DateTimeInterface $date
     * @return ScalePresenter
     */
    public function baremaPresenter(DateTimeInterface $date, string $identifier =  'venb_barema'): ScalePresenter
    {
        $scale = $this->repositoryScale->getFromDate(
            $date,
            $identifier);

        return new ScalePresenter($scale);
    }

    /**
     * Builds the input as a standard databag from the current server request.
     *
     * @param array $values
     * @return object
     */
    public function inputFromArray(array $values): object
    {
        if ($values['version'] === 'v2018aj') {
            return $this->make2018DataBag($values);
        }

        if ($values['version'] === 'v2019ajOld') {
            return $this->make2019OldDataBag($values);
        }

        if ($values['version'] === 'v2019ajNew') {
            return $this->make2019NewDatabag($values);
        }

        if ($values['version'] === 'v2020aj') {
            return $this->make2020Databag($values);
        }

        return $this->make2021Databag($values);
    }

    /**
     * @param array $values
     * @return object
     */
    public function make2018DataBag(array $values): object
    {
        $input = new VenbInput2018(
            to_float($values['belastbaarGewoonTarief'] ?? 0.0),
            to_bool($values['isAanspraakVerminderdTarief'] ?? false),
            to_bool($values['isVenootshapHuisvesting'] ?? false),
            to_float($values['meerwaardeOpAandelen25'] ?? 0.0),
            to_float($values['belastbareTegenExitTaks165'] ?? 0.0),
            to_float($values['belastbareTegenExitTaks125'] ?? 0.0),
            to_float($values['meerwaardeOpAAndelenBelastbaartegen04'] ?? 0.0),
            to_float($values['kapitaalenInterestsubsidiesLandboouw05'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard50'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard100'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen34'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen28'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagUitgekeerdeDividenden'] ?? 0.0),
            to_float($values['fairnessTax'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBoekhoudingNaWinst'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen33'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen165'] ?? 0.0),
            to_float($values['voordeleAlleAardVerleend'] ?? 0.0),
            to_bool($values['isStartendKMO'] ?? false),
            to_float($values['terugbetalingTegen100'] ?? 0.0),
            to_float($values['nietterugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['buitenlandseBelastingkrediet'] ?? 0.0),
            to_float($values['terugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['belastingkredietOndOntw'] ?? 0.0),
            new VoorafBetalingenInput(
                to_float($values['voorafbetalingKwart1'] ?? 0.0),
                to_float($values['voorafbetalingKwart2'] ?? 0.0),
                to_float($values['voorafbetalingKwart3'] ?? 0.0),
                to_float($values['voorafbetalingKwart4'] ?? 0.0),
                to_int($values['aantalkwartalen'] ?? 4)
            ),
            to_float($values['latenteMeerwaarden'] ?? 0.0)
        );
        return $input;
    }

    /**
     * @param array $values
     * @return object
     */
    public function make2019OldDataBag(array $values): object
    {
        $input = new Venb2019InputOld(
            to_float($values['belastbaarGewoonTarief'] ?? 0.0),
            to_bool($values['isAanspraakVerminderdTarief'] ?? false),
            to_bool($values['isVenootshapHuisvesting'] ?? false),
            to_bool($values['isBelastbaarTijdPerkTenVroegste01012018'] ?? false),
            to_bool($values['isWijzigingAfsluitdatum26072017'] ?? false),
            to_float($values['meerwaardeOpAandelen25'] ?? 0.0),
            to_float($values['belastbareTegenExitTaks165'] ?? 0.0),
            to_float($values['belastbareTegenExitTaks125'] ?? 0.0),
            to_float($values['meerwaardeOpAAndelenBelastbaartegen04'] ?? 0.0),
            to_float($values['kapitaalenInterestsubsidiesLandboouw05'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard50'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard100'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen34'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen28'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagUitgekeerdeDividenden'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagLiqRes10'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen33'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen165'] ?? 0.0),
            to_float($values['voordeleAlleAardVerleend'] ?? 0.0),
            to_bool($values['isStartendKMO'] ?? false),
            to_float($values['terugbetalingTegen100'] ?? 0.0),
            to_float($values['nietterugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['buitenlandseBelastingkrediet'] ?? 0.0),
            to_float($values['terugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['belastingkredietOndOntw'] ?? 0.0),
            new VoorafBetalingenInput(
                to_float($values['voorafbetalingKwart1'] ?? 0.0),
                to_float($values['voorafbetalingKwart2'] ?? 0.0),
                to_float($values['voorafbetalingKwart3'] ?? 0.0),
                to_float($values['voorafbetalingKwart4'] ?? 0.0),
                to_int($values['aantalkwartalen'] ?? 4)
            ),
            to_float($values['latenteMeerwaarden'] ?? 0),
            to_float($values['negatieveCorrectieGrondslagGewoonTarief'] ?? 0.0),
            to_float($values['positieveCorrectieGrondslagGewoonTarief'] ?? 0.0),
            to_float($values['negatieveCorrectieMeerwaardeAandelen25'] ?? 0.0),
            to_float($values['negatieveCorrectieMeerwaardeAandelen04'] ?? 0.0),
            to_float($values['gedeelteMeerwaardeBelastbaar25'] ?? 0.0),
            to_float($values['gedeelteGrondslagExitTaks125'] ?? 0.0),
            to_float($values['gedeelteGecorrigeerdeAanslag29'] ?? 0.0),
            to_bool($values['isCorrectieVerlaagdTarief'] ?? false)
        );
        return $input;
    }

    /**
     * @param array $values
     * @return object
     */
    public function make2019NewDatabag(array $values): object
    {
        return new Venb2019InputNew(
            to_float($values['belastbaarGewoonTarief'] ?? 0.0),
            to_bool($values['isAanspraakVerminderdTarief'] ?? false),
            to_bool($values['isVenootshapHuisvesting'] ?? false),
            to_bool($values['isBelastbaarTijdPerkTenVroegste01012018'] ?? false),
            to_bool($values['isWijzigingAfsluitdatum26072017'] ?? false),
            to_float($values['meerwaardeOpAandelen25'] ?? 0.0),
            to_float($values['verwezenlijkteMeerwaarde3399'] ?? 0.0),
            to_float($values['belastbareTegenExitTaks125'] ?? 0.0),
            to_float($values['kapitaalenInterestsubsidiesLandboouw05'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard50'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard100'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen34'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen28'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagUitgekeerdeDividenden'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagLiqRes10'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen33'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen165'] ?? 0.0),
            to_float($values['voordeleAlleAardVerleend'] ?? 0.0),
            to_bool($values['isStartendKMO'] ?? false),
            to_float($values['terugbetalingTegen100'] ?? 0.0),
            to_float($values['nietterugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['buitenlandseBelastingkrediet'] ?? 0.0),
            to_float($values['terugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['belastingkredietOndOntw'] ?? 0.0),
            new VoorafBetalingenInput(
                to_float($values['voorafbetalingKwart1'] ?? 0.0),
                to_float($values['voorafbetalingKwart2'] ?? 0.0),
                to_float($values['voorafbetalingKwart3'] ?? 0.0),
                to_float($values['voorafbetalingKwart4'] ?? 0.0),
                to_int($values['aantalkwartalen'] ?? 4)
            ),
            to_float($values['latenteMeerwaarden'] ?? 0),
            to_float($values['negatieveCorrectieGrondslagGewoonTarief'] ?? 0.0),
            to_float($values['positieveCorrectieGrondslagGewoonTarief'] ?? 0.0),
            to_float($values['positieveCorrectieMeerwaardeAandelen25'] ?? 0.0),
            to_float($values['positieveCorrectieMeerwaardeAandelen04'] ?? 0.0),
            to_float($values['gedeelteMeerwaardeBelastbaar25'] ?? 0.0),
            to_float($values['gedeelteGrondslagExitTaks125'] ?? 0.0),
            to_float($values['gedeelteGecorrigeerdeAanslag33'] ?? 0.0),
            to_bool($values['isCorrectieVerlaagdTarief'] ?? false)
        );
    }

    /**
     * @param array $values
     * @return object
     */
    public function make2020Databag(array $values): object
    {
        return new Venb2020Input(
            to_float($values['belastbaarGewoonTarief'] ?? 0.0),
            to_bool($values['isAanspraakVerminderdTarief'] ?? false),
            to_bool($values['isVenootshapHuisvesting'] ?? false),
            to_bool($values['isBelastbaarTijdPerkTenVroegste01012018'] ?? false),
            to_bool($values['isWijzigingAfsluitdatum26072017'] ?? false),
            to_float($values['meerwaardeOpAandelen25'] ?? 0.0),
            to_float($values['verwezenlijkteMeerwaarde3399'] ?? 0.0),
            to_float($values['verwezenlijkteMeerwaarde2958'] ?? 0.0),
            to_float($values['belastbareTegenExitTaks125'] ?? 0.0),
            to_float($values['kapitaalenInterestsubsidiesLandboouw05'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard50'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard100'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen34'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen28'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagUitgekeerdeDividenden'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagLiqRes10'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen33'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen165'] ?? 0.0),
            to_float($values['voordeleAlleAardVerleend'] ?? 0.0),
            to_bool($values['isStartendKMO'] ?? false),
            to_float($values['terugbetalingTegen100'] ?? 0.0),
            to_float($values['nietterugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['buitenlandseBelastingkrediet'] ?? 0.0),
            to_float($values['terugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['belastingkredietOndOntw'] ?? 0.0),
            to_bool($values['isVerhoogdePercentagesVA3VA4'] ?? false),
            new VoorafBetalingenInput(
                to_float($values['voorafbetalingKwart1'] ?? 0.0),
                to_float($values['voorafbetalingKwart2'] ?? 0.0),
                to_float($values['voorafbetalingKwart3'] ?? 0.0),
                to_float($values['voorafbetalingKwart4'] ?? 0.0),
                to_int($values['aantalkwartalen'] ?? 4)
            ),
            to_float($values['latenteMeerwaarden'] ?? 0),
            to_float($values['negatieveCorrectieGrondslagGewoonTarief'] ?? 0.0),
            to_float($values['positieveCorrectieGrondslagGewoonTarief'] ?? 0.0),
            to_float($values['positieveCorrectieMeerwaardeAandelen25'] ?? 0.0),
            to_float($values['positieveCorrectieMeerwaardeAandelen04'] ?? 0.0),
            to_float($values['gedeelteMeerwaardeBelastbaar25'] ?? 0.0),
            to_float($values['gedeelteGrondslagExitTaks125'] ?? 0.0),
            to_float($values['gedeelteGecorrigeerdeAanslag33'] ?? 0.0),
            to_bool($values['isCorrectieVerlaagdTarief'] ?? false)
        );
    }

    /**
     * @param array $values
     * @return object
     */
    public function make2021Databag(array $values): object
    {
        return new Venb2021Input(
            to_float($values['belastbaarGewoonTarief'] ?? 0.0),
            to_bool($values['isAanspraakVerminderdTarief'] ?? false),
            to_bool($values['isVenootshapHuisvesting'] ?? false),
            to_bool($values['isBelastbaarTijdPerkTenVroegste01012018'] ?? false),
            to_bool($values['isWijzigingAfsluitdatum26072017'] ?? false),
            to_float($values['meerwaardeOpAandelen25'] ?? 0.0),
            to_float($values['verwezenlijkteMeerwaarde3399'] ?? 0.0),
            to_float($values['verwezenlijkteMeerwaarde2958'] ?? 0.0),
            to_float($values['belastbareTegenExitTaks125'] ?? 0.0),
            to_float($values['kapitaalenInterestsubsidiesLandboouw05'] ?? 0.0),
            to_float($values['mobilisatieReserves10'] ?? 0.0),
            to_float($values['mobilisatieReserves15'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard50'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagAlleAard100'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen34'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagBelasteReservesKredietInstellingen28'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagUitgekeerdeDividenden'] ?? 0.0),
            to_float($values['afzonderlijkeAanslagLiqRes10'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen33'] ?? 0.0),
            to_float($values['geheleOfGedeeltelijkMaatschappelijkVermogen165'] ?? 0.0),
            to_float($values['voordeleAlleAardVerleend'] ?? 0.0),
            to_bool($values['isStartendKMO'] ?? false),
            to_float($values['terugbetalingTegen100'] ?? 0.0),
            to_float($values['nietterugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['buitenlandseBelastingkrediet'] ?? 0.0),
            to_float($values['terugbetaalbareVoorheffing'] ?? 0.0),
            to_float($values['belastingkredietOndOntw'] ?? 0.0),
            to_bool($values['isVerhoogdePercentagesVA3VA4'] ?? false),
            new VoorafBetalingenInput(
                to_float($values['voorafbetalingKwart1'] ?? 0.0),
                to_float($values['voorafbetalingKwart2'] ?? 0.0),
                to_float($values['voorafbetalingKwart3'] ?? 0.0),
                to_float($values['voorafbetalingKwart4'] ?? 0.0),
                to_int($values['aantalkwartalen'] ?? 4)
            ),
            to_float($values['latenteMeerwaarden'] ?? 0),
            to_float($values['negatieveCorrectieGrondslagGewoonTarief'] ?? 0.0),
            to_float($values['positieveCorrectieGrondslagGewoonTarief'] ?? 0.0),
            to_float($values['positieveCorrectieMeerwaardeAandelen25'] ?? 0.0),
            to_float($values['positieveCorrectieMeerwaardeAandelen04'] ?? 0.0),
            to_float($values['gedeelteMeerwaardeBelastbaar25'] ?? 0.0),
            to_float($values['gedeelteGrondslagExitTaks125'] ?? 0.0),
            to_float($values['gedeelteGecorrigeerdeAanslag33'] ?? 0.0),
            to_bool($values['isCorrectieVerlaagdTarief'] ?? false)
        );
    }

    /**
     * Derives the validation from the current input values.
     *
     * @param array $values
     * @return Validation
     */
    public function validation(array $values): Validation
    {
        $validator = new Validator([
            'required' => 'This field is required',
            'numeric' => 'This field needs to be a number'
        ]);

        if ($values['version'] === 'v2018aj') {
            return $validator->make($values, $this->make2018Rules());
        }

        if ($values['version'] === 'v2019ajOld') {
            return $validator->make($values, $this->make2019OldRules());
        }
        if ($values['version'] === 'v2019ajNew') {
            return $validator->make($values, $this->make2019NewRules());
        }

        if ($values['version'] === 'v2020ajNew' || $values['version'] === 'v2020ajOld') {
            return $validator->make($values, $this->make2020Rules());
        }
        return $validator->make($values, $this->make2021Rules());
    }
    /**
     * @return array
     */
    public function make2018Rules() : array
    {
        return [
            'belastbaarGewoonTarief' => ['numeric'],
            'meerwaardeOpAandelen25' => ['numeric'],
            'belastbareTegenExitTaks165' => ['numeric'],
            'belastbareTegenExitTaks125' => ['numeric'],
            'meerwaardeOpAAndelenBelastbaartegen04' => ['numeric'],
            'kapitaalenInterestsubsidiesLandboouw05' => ['numeric'],
            'afzonderlijkeAanslagAlleAard50' => ['numeric'],
            'afzonderlijkeAanslagAlleAard100' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen34' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen28' => ['numeric'],
            'afzonderlijkeAanslagUitgekeerdeDividenden' => ['numeric'],
            'fairnessTax' => ['numeric'],
            'afzonderlijkeAanslagBoekhoudingNaWinst' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen33' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen165' => ['numeric'],
            'voordeleAlleAardVerleend' => ['numeric'],
            'terugbetalingTegen100' => ['numeric'],
            'nietterugbetaalbareVoorheffing' => ['numeric'],
            'buitenlandseBelastingkrediet' => ['numeric'],
            'terugbetaalbareVoorheffing' => ['numeric'],
            'belastingkredietOndOntw' => ['numeric'],
            'voorafbetalingKwart1' => ['numeric'],
            'voorafbetalingKwart2' => ['numeric'],
            'voorafbetalingKwart3' => ['numeric'],
            'voorafbetalingKwart4' => ['numeric'],
            'latenteMeerwaarde' => ['numeric'],
        ];
    }

    /**
     * @return array
     */
    public function make2019OldRules() : array
    {
        return [
            'belastbaarGewoonTarief' => ['numeric'],
            'meerwaardeOpAandelen25' => ['numeric'],
            'belastbareTegenExitTaks165' => ['numeric'],
            'verwezenlijkteMeerwaarde3399' => ['numeric'],
            'belastbareTegenExitTaks125' => ['numeric'],
            'meerwaardeOpAAndelenBelastbaartegen04' => ['numeric'],
            'kapitaalenInterestsubsidiesLandboouw05' => ['numeric'],
            'afzonderlijkeAanslagAlleAard50' => ['numeric'],
            'afzonderlijkeAanslagAlleAard100' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen34' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen28' => ['numeric'],
            'afzonderlijkeAanslagUitgekeerdeDividenden' => ['numeric'],
            'fairnessTax' => ['numeric'],
            'afzonderlijkeAanslagBoekhoudingNaWinst' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen33' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen165' => ['numeric'],
            'voordeleAlleAardVerleend' => ['numeric'],
            'terugbetalingTegen100' => ['numeric'],
            'nietterugbetaalbareVoorheffing' => ['numeric'],
            'buitenlandseBelastingkrediet' => ['numeric'],
            'terugbetaalbareVoorheffing' => ['numeric'],
            'belastingkredietOndOntw' => ['numeric'],
            'voorafbetalingKwart1' => ['numeric'],
            'voorafbetalingKwart2' => ['numeric'],
            'voorafbetalingKwart3' => ['numeric'],
            'voorafbetalingKwart4' => ['numeric'],
            'latenteMeerwaarde' => ['numeric'],

            'negatieveCorrectieGrondslagGewoonTarief' => ['numeric'],
            'positieveCorrectieGrondslagGewoonTarief' => ['numeric'],
            'positieveCorrectieMeerwaardeAandelen25' => ['numeric'],
            'negatieveCorrectieMeerwaardeAandelen25' => ['numeric', new LessThanMinus('meerwaardeOpAandelen25', 'gedeelteMeerwaardeBelastbaar25')
            ],
            'positieveCorrectieMeerwaardeAandelen04' => ['numeric', new LessThan('meerwaardeOpAAndelenBelastbaartegen04')
            ],
            'gedeelteMeerwaardeBelastbaar25' => ['numeric', new LessThanMinus('meerwaardeOpAandelen25', 'negatieveCorrectieMeerwaardeAandelen25')
            ],
            'gedeelteGrondslagExitTaks125' => ['numeric', new LessThan('belastbareTegenExitTaks125')
            ],
            'gedeelteGecorrigeerdeAanslag33' => ['numeric'],
            'negatieveCorrectieMeerwaardeAandelen04' => ['numeric'],
            'gedeelteGecorrigeerdeAanslag29' => ['numeric', new LessThanSum('belastbaarGewoonTarief', 'positieveCorrectieGrondslagGewoonTarief', 'negatieveCorrectieGrondslagGewoonTarief')
            ],
        ];
    }

    public function make2019NewRules() : array
    {
        return [
            'belastbaarGewoonTarief' => ['numeric'],
            'meerwaardeOpAandelen25' => ['numeric'],
            'belastbareTegenExitTaks165' => ['numeric'],
            'verwezenlijkteMeerwaarde3399' => ['numeric'],
            'belastbareTegenExitTaks125' => ['numeric'],
            'meerwaardeOpAAndelenBelastbaartegen04' => ['numeric'],
            'kapitaalenInterestsubsidiesLandboouw05' => ['numeric'],
            'afzonderlijkeAanslagAlleAard50' => ['numeric'],
            'afzonderlijkeAanslagAlleAard100' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen34' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen28' => ['numeric'],
            'afzonderlijkeAanslagUitgekeerdeDividenden' => ['numeric'],
            'fairnessTax' => ['numeric'],
            'afzonderlijkeAanslagBoekhoudingNaWinst' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen33' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen165' => ['numeric'],
            'voordeleAlleAardVerleend' => ['numeric'],
            'terugbetalingTegen100' => ['numeric'],
            'nietterugbetaalbareVoorheffing' => ['numeric'],
            'buitenlandseBelastingkrediet' => ['numeric'],
            'terugbetaalbareVoorheffing' => ['numeric'],
            'belastingkredietOndOntw' => ['numeric'],
            'voorafbetalingKwart1' => ['numeric'],
            'voorafbetalingKwart2' => ['numeric'],
            'voorafbetalingKwart3' => ['numeric'],
            'voorafbetalingKwart4' => ['numeric'],
            'latenteMeerwaarde' => ['numeric'],

            'negatieveCorrectieGrondslagGewoonTarief' => ['numeric'],
            'positieveCorrectieGrondslagGewoonTarief' => ['numeric'],
            'positieveCorrectieMeerwaardeAandelen25' => ['numeric'],
            'negatieveCorrectieMeerwaardeAandelen25' => ['numeric'],
            'positieveCorrectieMeerwaardeAandelen04' => ['numeric'],
            'gedeelteMeerwaardeBelastbaar25' => ['numeric', new LessThan('meerwaardeOpAandelen25')
            ],
            'gedeelteGrondslagExitTaks125' => ['numeric', new LessThan('belastbareTegenExitTaks125')
            ],
            'gedeelteGecorrigeerdeAanslag33' => ['numeric', new LessThanSum('belastbaarGewoonTarief', 'positieveCorrectieGrondslagGewoonTarief', 'negatieveCorrectieGrondslagGewoonTarief')
            ],
            'negatieveCorrectieMeerwaardeAandelen04' => ['numeric'],
            'gedeelteGecorrigeerdeAanslag29' => ['numeric'],
        ];
    }

    public function make2020Rules() : array
    {
        return [
            'belastbaarGewoonTarief' => ['numeric'],
            'meerwaardeOpAandelen25' => ['numeric'],
            'belastbareTegenExitTaks165' => ['numeric'],
            'verwezenlijkteMeerwaarde3399' => ['numeric'],
            'verwezenlijkteMeerwaarde2958' => ['numeric'],
            'belastbareTegenExitTaks125' => ['numeric'],
            'meerwaardeOpAAndelenBelastbaartegen04' => ['numeric'],
            'kapitaalenInterestsubsidiesLandboouw05' => ['numeric'],
            'afzonderlijkeAanslagAlleAard50' => ['numeric'],
            'afzonderlijkeAanslagAlleAard100' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen34' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen28' => ['numeric'],
            'afzonderlijkeAanslagUitgekeerdeDividenden' => ['numeric'],
            'fairnessTax' => ['numeric'],
            'afzonderlijkeAanslagBoekhoudingNaWinst' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen33' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen165' => ['numeric'],
            'voordeleAlleAardVerleend' => ['numeric'],
            'terugbetalingTegen100' => ['numeric'],
            'nietterugbetaalbareVoorheffing' => ['numeric'],
            'buitenlandseBelastingkrediet' => ['numeric'],
            'terugbetaalbareVoorheffing' => ['numeric'],
            'belastingkredietOndOntw' => ['numeric'],
            'voorafbetalingKwart1' => ['numeric'],
            'voorafbetalingKwart2' => ['numeric'],
            'voorafbetalingKwart3' => ['numeric'],
            'voorafbetalingKwart4' => ['numeric'],
            'latenteMeerwaarde' => ['numeric'],

            'negatieveCorrectieGrondslagGewoonTarief' => ['numeric'],
            'positieveCorrectieGrondslagGewoonTarief' => ['numeric'],
            'positieveCorrectieMeerwaardeAandelen25' => ['numeric'],
            'negatieveCorrectieMeerwaardeAandelen25' => ['numeric'],
            'positieveCorrectieMeerwaardeAandelen04' => ['numeric'],
            'gedeelteMeerwaardeBelastbaar25' => ['numeric', new LessThan('meerwaardeOpAandelen25')
            ],
            'gedeelteGrondslagExitTaks125' => ['numeric', new LessThan('belastbareTegenExitTaks125')
            ],
            'gedeelteGecorrigeerdeAanslag33' => ['numeric', new LessThanSum('belastbaarGewoonTarief', 'positieveCorrectieGrondslagGewoonTarief', 'negatieveCorrectieGrondslagGewoonTarief')
            ],
            'negatieveCorrectieMeerwaardeAandelen04' => ['numeric'],
            'gedeelteGecorrigeerdeAanslag29' => ['numeric'],
        ];
    }

    public function make2021Rules() : array
    {
        return [
            'belastbaarGewoonTarief' => ['numeric'],
            'meerwaardeOpAandelen25' => ['numeric'],
            'belastbareTegenExitTaks165' => ['numeric'],
            'verwezenlijkteMeerwaarde3399' => ['numeric'],
            'verwezenlijkteMeerwaarde2958' => ['numeric'],
            'belastbareTegenExitTaks125' => ['numeric'],
            'meerwaardeOpAAndelenBelastbaartegen04' => ['numeric'],
            'kapitaalenInterestsubsidiesLandboouw05' => ['numeric'],
            'afzonderlijkeAanslagAlleAard50' => ['numeric'],
            'afzonderlijkeAanslagAlleAard100' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen34' => ['numeric'],
            'afzonderlijkeAanslagBelasteReservesKredietInstellingen28' => ['numeric'],
            'afzonderlijkeAanslagUitgekeerdeDividenden' => ['numeric'],
            'fairnessTax' => ['numeric'],
            'afzonderlijkeAanslagBoekhoudingNaWinst' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen33' => ['numeric'],
            'geheleOfGedeeltelijkMaatschappelijkVermogen165' => ['numeric'],
            'voordeleAlleAardVerleend' => ['numeric'],
            'terugbetalingTegen100' => ['numeric'],
            'nietterugbetaalbareVoorheffing' => ['numeric'],
            'buitenlandseBelastingkrediet' => ['numeric'],
            'terugbetaalbareVoorheffing' => ['numeric'],
            'belastingkredietOndOntw' => ['numeric'],
            'voorafbetalingKwart1' => ['numeric'],
            'voorafbetalingKwart2' => ['numeric'],
            'voorafbetalingKwart3' => ['numeric'],
            'voorafbetalingKwart4' => ['numeric'],
            'latenteMeerwaarde' => ['numeric'],

            'negatieveCorrectieGrondslagGewoonTarief' => ['numeric'],
            'positieveCorrectieGrondslagGewoonTarief' => ['numeric'],
            'positieveCorrectieMeerwaardeAandelen25' => ['numeric'],
            'negatieveCorrectieMeerwaardeAandelen25' => ['numeric'],
            'positieveCorrectieMeerwaardeAandelen04' => ['numeric'],
            'gedeelteMeerwaardeBelastbaar25' => ['numeric', new LessThan('meerwaardeOpAandelen25')
            ],
            'gedeelteGrondslagExitTaks125' => ['numeric', new LessThan('belastbareTegenExitTaks125')
            ],
            'gedeelteGecorrigeerdeAanslag33' => ['numeric', new LessThanSum('belastbaarGewoonTarief', 'positieveCorrectieGrondslagGewoonTarief', 'negatieveCorrectieGrondslagGewoonTarief')
            ],
            'negatieveCorrectieMeerwaardeAandelen04' => ['numeric'],
            'gedeelteGecorrigeerdeAanslag29' => ['numeric'],
        ];
    }
}

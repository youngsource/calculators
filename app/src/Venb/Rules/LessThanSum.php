<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 31/07/19
 * Time: 22:46
 */

namespace Laudis\Calculators\Venb\Rules;

use Rakit\Validation\Rule;
use Rakit\Validation\Validation;

/**
 * Class LessThanSum
 * @package Laudis\Calculators\Venb\Rules
 */
final class LessThanSum extends Rule
{
    /** @var string */
    private $key1;
    /** @var string */
    private $key2;
    /** @var string */
    private $key3;

    /**
     * LessThanSum constructor.
     * @param string $key1
     * @param string $key2
     * @param string $key3
     */
    public function __construct(string $key1, string $key2, string $key3)
    {
        $this->key1 = $key1;
        $this->key2 = $key2;
        $this->key3 = $key3;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        /** @var Validation $validation */
        $validation = $this->validation;
        $value = (int) $value;
        $value1 = (int) $validation->getValue($this->key1);
        $value2 = (int) $validation->getValue($this->key2);
        $value3 = (int) $validation->getValue($this->key3);
        if ($value <= ($value1 + $value2 - $value3)) {
            return true;
        }

        $this->setMessage($value . ' is groter dan '  . ($value1 ?? 0) . ' + ' . ($value2 ?? 0) . ' - ' . ($value3 ?? 0) );
        return false;
    }
}

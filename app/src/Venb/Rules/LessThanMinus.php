<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb\Rules;

use Rakit\Validation\Rule;
use Rakit\Validation\Validation;

/**
 * Class LessThanMinus
 * @package Laudis\Calculators\Venb\Rules
 */
final class LessThanMinus extends Rule
{
    /** @var string */
    private $key1;
    /** @var string */
    private $key2;

    /**
     * LessThanMinus constructor.
     * @param string $key1
     * @param string $key2
     */
    public function __construct(string $key1, string $key2)
    {
        $this->key1 = $key1;
        $this->key2 = $key2;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        /** @var Validation $validation */
        $validation = $this->validation;
        $value = (int) $value;
        $value1 = (int) $validation->getValue($this->key1);
        $value2 = (int) $validation->getValue($this->key2);
        if ($value <= ($value1 - $value2)) {
            return true;
        }

        $this->setMessage($value . ' is groter dan ' . $value1 . ' - ' . $value2);
        return false;
    }
}




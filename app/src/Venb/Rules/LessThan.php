<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb\Rules;

use Rakit\Validation\Rule;
use Rakit\Validation\Validation;

/**
 * Class LessThan
 * @package Laudis\Calculators\Venb\Rules
 */
final class LessThan extends Rule
{
    /** @var string */
    private $otherValueKey;

    /**
     * LessThan constructor.
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->otherValueKey = $key;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        /** @var Validation $validation */
        $validation = $this->validation;
        $value = (int) $value;
        $value1 = (int) $validation->getValue($this->otherValueKey);
        if ($value <= $value1) {
            return true;
        }

        $this->setMessage($value . ' is groter dan ' . $value1);
        return false;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb;

use Laudis\Calculators\Contracts\CalculationResultInterface;

/**
 * Class BasicCalculationResult
 * @package Laudis\Calculators\Venb
 */
final class BasicCalculationResult implements CalculationResultInterface
{
    /**
     * @var array
     */
    private $values;

    /**
     * BasicCalculationResult constructor.
     * @param array $values
     */
    public function __construct(array $values)
    {
        $this->values = $values;
    }

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array
    {
        return $this->values;
    }

    /**
     * @param int $outputMode
     */
    public function setOutputMode(int $outputMode): void
    {
        // unnessecary details
    }
}

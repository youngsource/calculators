<?php


namespace Laudis\Calculators\Venb\Input;


class VoorafBetalingenInput
{
    /** @var float */
    private $voorafbetalingKwart1;
    /** @var float */
    private $voorafbetalingKwart2;
    /** @var float */
    private $voorafbetalingKwart3;
    /** @var float */
    private $voorafbetalingKwart4;
    /** @var integer */
    private $aantalKwartalen;

    /**
     * VoorafBetalingenInput constructor.
     * @param float $voorafbetalingKwart1
     * @param float $voorafbetalingKwart2
     * @param float $voorafbetalingKwart3
     * @param float $voorafbetalingKwart4
     * @param int $aantalKwartalen
     */
    public function __construct(float $voorafbetalingKwart1, float $voorafbetalingKwart2, float $voorafbetalingKwart3, float $voorafbetalingKwart4, int $aantalKwartalen)
    {
        $this->voorafbetalingKwart1 = $voorafbetalingKwart1;
        $this->voorafbetalingKwart2 = $voorafbetalingKwart2;
        $this->voorafbetalingKwart3 = $voorafbetalingKwart3;
        $this->voorafbetalingKwart4 = $voorafbetalingKwart4;
        $this->aantalKwartalen = $aantalKwartalen;
    }

    /**
     * @return float
     */
    public function getVoorafbetalingKwart1(): float
    {
        return $this->voorafbetalingKwart1;
    }

    /**
     * @return float
     */
    public function getVoorafbetalingKwart2(): float
    {
        return $this->voorafbetalingKwart2;
    }

    /**
     * @return float
     */
    public function getVoorafbetalingKwart3(): float
    {
        return $this->voorafbetalingKwart3;
    }

    /**
     * @return float
     */
    public function getVoorafbetalingKwart4(): float
    {
        return $this->voorafbetalingKwart4;
    }

    /**
     * @return int
     */
    public function getAantalKwartalen(): int
    {
        return $this->aantalKwartalen;
    }

    public function getTotaal():float
    {

    }
}
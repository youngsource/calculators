<?php


namespace Laudis\Calculators\Venb\Results;


class GeenVermeerderingIndienLagerResult
{
    /** @var float */
    private $berperkingTot;
    /** @var float */
    private $beperkingGrondslag;
    /** @var float */
    private $weerhoudenVermeerdering;

    /**
     * GeenVermeerderingIndienLagerResult constructor.
     * @param float $berperkingTot
     * @param float $beperkingGrondslag
     * @param float $weerhoudenVermeerdering
     */
    public function __construct(float $berperkingTot, float $beperkingGrondslag, float $weerhoudenVermeerdering)
    {
        $this->berperkingTot = $berperkingTot;
        $this->beperkingGrondslag = $beperkingGrondslag;
        $this->weerhoudenVermeerdering = $weerhoudenVermeerdering;
    }

    /**
     * @return float
     */
    public function getBerperkingTot(): float
    {
        return $this->berperkingTot;
    }

    /**
     * @return float
     */
    public function getBeperkingGrondslag(): float
    {
        return $this->beperkingGrondslag;
    }

    /**
     * @return float
     */
    public function getWeerhoudenVermeerdering(): float
    {
        return $this->weerhoudenVermeerdering;
    }

    public function output():array {
        return [
          'beperking_tot' => $this->getBerperkingTot(),
          'beperking_grondslag' => $this->getBeperkingGrondslag(),
          'weerhoudenVermeerdering' => $this->getWeerhoudenVermeerdering()
        ];
    }
}
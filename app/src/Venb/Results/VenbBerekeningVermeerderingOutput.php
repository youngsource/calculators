<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 10/07/19
 * Time: 18:52
 */

namespace Laudis\Calculators\Venb\Results;

/**
 * Class VenbBerekeningVermeerderingOutput
 * @package Laudis\Calculators\Venb\VersieAj2018
 */
final class VenbBerekeningVermeerderingOutput
{
    /**
     * @var float
     */
    private $latenteMeerwaarden;
    /**
     * @var float
     */
    private $nietTerugBetaalbareVoorheffing;
    /**
     * @var float
     */
    private $buitenlandsBelastingsKrediet;
    /**
     * @var float
     */
    private $terugbetaalbareVoorheffing;
    /**
     * @var float
     */
    private $belastingkredietOndOntw;
    /**
     * @var GrondSlagCalculationResult
     */
    private $grondSlagCalculationResult;
    /**
     * @var VenbVoorafbetalingenOutput
     */
    private $voorafBetalingen;
    /**
     * @var float
     */
    private $saldoVermeerdering;
    /**
     * @var float
     */
    private $beperkingTot;
    /**
     * @var float
     */
    private $beperkingTotGrondslag;
    /**
     * @var float
     */
    private $GeenVermeerderingIndien;
    /**
     * @var float
     */
    private $weerhoudenVermeerdering;

    /**
     * VenbBerekeningVermeerderingOutput constructor.
     * @param float $latenteMeerwaarden
     * @param float $nietTerugBetaalbareVoorheffing
     * @param float $buitenlandsBelastingsKrediet
     * @param float $terugbetaalbareVoorheffing
     * @param float $belastingkredietOndOntw
     * @param GrondSlagCalculationResult $grondSlagCalculationResult
     * @param VenbVoorafbetalingenOutput $voorafBetalingen
     * @param float $saldoVermeerdering
     * @param float $beperkingTot
     * @param float $beperkingTotGrondslag
     * @param float $GeenVermeerderingIndien
     * @param float $weerhoudenVermeerdering
     */
    public function __construct(
        float $latenteMeerwaarden,
        float $nietTerugBetaalbareVoorheffing,
        float $buitenlandsBelastingsKrediet,
        float $terugbetaalbareVoorheffing,
        float $belastingkredietOndOntw,
        GrondSlagCalculationResult $grondSlagCalculationResult,
        VenbVoorafbetalingenOutput $voorafBetalingen,
        float $saldoVermeerdering,
        float $beperkingTot,
        float $beperkingTotGrondslag,
        float $GeenVermeerderingIndien,
        float $weerhoudenVermeerdering
    ) {
        $this->latenteMeerwaarden = $latenteMeerwaarden;
        $this->nietTerugBetaalbareVoorheffing = $nietTerugBetaalbareVoorheffing;
        $this->buitenlandsBelastingsKrediet = $buitenlandsBelastingsKrediet;
        $this->terugbetaalbareVoorheffing = $terugbetaalbareVoorheffing;
        $this->belastingkredietOndOntw = $belastingkredietOndOntw;
        $this->grondSlagCalculationResult = $grondSlagCalculationResult;
        $this->voorafBetalingen = $voorafBetalingen;
        $this->saldoVermeerdering = $saldoVermeerdering;
        $this->beperkingTot = $beperkingTot;
        $this->beperkingTotGrondslag = $beperkingTotGrondslag;
        $this->GeenVermeerderingIndien = $GeenVermeerderingIndien;
        $this->weerhoudenVermeerdering = $weerhoudenVermeerdering;
    }

    /**
     * @return array
     */
    public function getVoorafBetalingen(): array
    {
        return $this->voorafBetalingen->getVoorafbetalingen();
    }

    /**
     * @return array
     */
    public function output(): array
    {
        return [
            'Exitheffing voor voorafbetalingen' => ['code' => '1864', 'bedrag' => $this->getlatenteMeerwaarden()],
            'Niet-terugbetaalbare voorheffing' => ['code' => '1830', 'bedrag' => $this->getNietTerugBetaalbareVoorheffing()],
            'Buitenlands belastingskrediet' => ['code' => '1834', 'bedrag' => $this->getBuitenlandsBelastingsKrediet()],
            'Terugbetaalbare voorheffingen' => ['code' => '1840', 'bedrag' => $this->getTerugbetaalbareVoorheffing()],
            'Terugbetaalbaar belastingkrediet O&O' => ['code' => '1850', 'bedrag' => $this->getbelastingkredietOndOntw()],
            'grondslagCalculation'=>['code'=>'', 'calculation' => $this->getGrondSlagCalculationResult()->output()],
            'Voorafbetalingen' => ['code' => '', 'bedrag' =>$this->voorafBetalingen->getVoorafbetalingen()],
            'Saldo vermeerdering' => ['code' => '', 'bedrag' => $this->getSaldoVermeerdering()]
        ];
    }

    /**
     * @return float
     */
    public function getLatenteMeerwaarden(): float
    {
        return $this->latenteMeerwaarden;
    }

    /**
     * @return float
     */
    public function getNietTerugBetaalbareVoorheffing(): float
    {
        return $this->nietTerugBetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getBuitenlandsBelastingsKrediet(): float
    {
        return $this->buitenlandsBelastingsKrediet;
    }

    /**
     * @return float
     */
    public function getTerugbetaalbareVoorheffing(): float
    {
        return $this->terugbetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getBelastingkredietOndOntw(): float
    {
        return $this->belastingkredietOndOntw;
    }

    /**
     * @return GrondSlagCalculationResult
     */
    public function getGrondSlagCalculationResult(): GrondSlagCalculationResult
    {
        return $this->grondSlagCalculationResult;
    }

    /**
     * @return float
     */
    public function getSaldoVermeerdering(): float
    {
        return $this->saldoVermeerdering;
    }

    /**
     * @return float
     */
    public function getWeerhoudenVermeerdering(): float
    {
        return $this->weerhoudenVermeerdering;
    }


}

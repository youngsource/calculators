<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 10/07/19
 * Time: 18:48
 */

namespace Laudis\Calculators\Venb\Results;

/**
 * Class VenbVoorafbetalingenOutput
 * @package Laudis\Calculators\Venb\VersieAj2018
 */
final class VenbVoorafbetalingenOutput
{
    /**
     * @var array
     */
    private $voorafbetalingen;
    /**
     * @var float
     */
    private $total;

    /**
     * VenbVoorafbetalingenOutput constructor.
     * @param array $voorafbetalingen
     * @param float $total
     */
    public function __construct(array $voorafbetalingen = [], float $total = 0)
    {
        $this->voorafbetalingen = $voorafbetalingen;
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getVoorafbetalingen(): array
    {
        return $this->voorafbetalingen;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }
}

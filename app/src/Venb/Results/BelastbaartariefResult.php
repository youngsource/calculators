<?php


namespace Laudis\Calculators\Venb\Results;


class BelastbaartariefResult
{
    /** @var string */
    private $name;
    /** @var array */
    private $data;

    /**
     * BelastbaartariefResult constructor.
     * @param string $name
     * @param array $data
     */
    public function __construct(string $name, array $data)
    {
        $this->name = $name;
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function output() : array
    {
        return [$this->getName() => $this->getData()];
    }



}
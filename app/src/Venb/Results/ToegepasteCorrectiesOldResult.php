<?php


namespace Laudis\Calculators\Venb\Results;


class ToegepasteCorrectiesOldResult
{
    /** @var float */
    private $grondslagGewoonTarief;
    /** @var float */
    private $negatieveCorrectie;
    /** @var float */
    private $positieveCorrectie;
    /** @var float */
    private $totaalGewoneGrondslag;
    /** @var float */
    private $gedeelteGrondslagGewoonTariefOud;
    /** @var float */
    private $geCorrigeerdeGrondslagNieuw;
    /** @var float */
    private $geCorrigeerdeGrondslagOud;
    /** @var float */
    private $meerwaardeAandelen25;
    /** @var float */
    private $negatieveCorrectieMeerwaardeAandelen25;
    /** @var float */
    private $totaleGrondslagAandelen25;
    /** @var float */
    private $gedeelteCrisisbijdrage3procent;
    /** @var float */
    private $meerwaardenAandelen25Nieuw;
    /** @var float */
    private $meerwaardenAandelen20Nieuw;
    /** @var float */
    private $meerwaardeAandelen25Oud;
    /** @var float */
    private $meerwaardenAandelen04;
    /** @var float */
    private $negatieveCorrectieMeerwaardeAandelen04;
    /** @var float */
    private $meerwaardeAandelen04Oud;
    /** @var float */
    private $belastbaarExittarief125;
    /** @var float */
    private $crisisbijdrageExitTarief2Procent;
    /** @var float */
    private $belastbaarExitTarief125Nieuw;
    /** @var float */
    private $belastbaarExitTarief125Oud;

    /**
     * ToegepasteCorrectiesOldResult constructor.
     * @param float $grondslagGewoonTarief
     * @param float $negatieveCorrectie
     * @param float $positieveCorrectie
     * @param float $totaalGewoneGrondslag
     * @param float $gedeelteGrondslagGewoonTariefOud
     * @param float $geCorrigeerdeGrondslagNieuw
     * @param float $geCorrigeerdeGrondslagOud
     * @param float $meerwaardeAandelen25
     * @param float $negatieveCorrectieMeerwaardeAandelen25
     * @param float $totaleGrondslagAandelen25
     * @param float $gedeelteCrisisbijdrage3procent
     * @param float $meerwaardenAandelen25Nieuw
     * @param float $meerwaardenAandelen20Nieuw
     * @param float $meerwaardeAandelen25Oud
     * @param float $meerwaardenAandelen04
     * @param float $negatieveCorrectieMeerwaardeAandelen04
     * @param float $meerwaardeAandelen04Oud
     * @param float $belastbaarExittarief125
     * @param float $crisisbijdrageExitTarief2Procent
     * @param float $belastbaarExitTarief125Nieuw
     * @param float $belastbaarExitTarief125Oud
     */
    public function __construct(
        float $grondslagGewoonTarief,
        float $negatieveCorrectie,
        float $positieveCorrectie,
        float $totaalGewoneGrondslag,
        float $gedeelteGrondslagGewoonTariefOud,
        float $geCorrigeerdeGrondslagNieuw,
        float $geCorrigeerdeGrondslagOud,
        float $meerwaardeAandelen25,
        float $negatieveCorrectieMeerwaardeAandelen25,
        float $totaleGrondslagAandelen25,
        float $gedeelteCrisisbijdrage3procent,
        float $meerwaardenAandelen25Nieuw,
        float $meerwaardenAandelen20Nieuw,
        float $meerwaardeAandelen25Oud,
        float $meerwaardenAandelen04,
        float $negatieveCorrectieMeerwaardeAandelen04,
        float $meerwaardeAandelen04Oud,
        float $belastbaarExittarief125,
        float $crisisbijdrageExitTarief2Procent,
        float $belastbaarExitTarief125Nieuw,
        float $belastbaarExitTarief125Oud
    ) {
        $this->grondslagGewoonTarief = $grondslagGewoonTarief;
        $this->negatieveCorrectie = $negatieveCorrectie;
        $this->positieveCorrectie = $positieveCorrectie;
        $this->totaalGewoneGrondslag = $totaalGewoneGrondslag;
        $this->gedeelteGrondslagGewoonTariefOud = $gedeelteGrondslagGewoonTariefOud;
        $this->geCorrigeerdeGrondslagNieuw = $geCorrigeerdeGrondslagNieuw;
        $this->geCorrigeerdeGrondslagOud = $geCorrigeerdeGrondslagOud;
        $this->meerwaardeAandelen25 = $meerwaardeAandelen25;
        $this->negatieveCorrectieMeerwaardeAandelen25 = $negatieveCorrectieMeerwaardeAandelen25;
        $this->totaleGrondslagAandelen25 = $totaleGrondslagAandelen25;
        $this->gedeelteCrisisbijdrage3procent = $gedeelteCrisisbijdrage3procent;
        $this->meerwaardenAandelen25Nieuw = $meerwaardenAandelen25Nieuw;
        $this->meerwaardenAandelen20Nieuw = $meerwaardenAandelen20Nieuw;
        $this->meerwaardeAandelen25Oud = $meerwaardeAandelen25Oud;
        $this->meerwaardenAandelen04 = $meerwaardenAandelen04;
        $this->negatieveCorrectieMeerwaardeAandelen04 = $negatieveCorrectieMeerwaardeAandelen04;
        $this->meerwaardeAandelen04Oud = $meerwaardeAandelen04Oud;
        $this->belastbaarExittarief125 = $belastbaarExittarief125;
        $this->crisisbijdrageExitTarief2Procent = $crisisbijdrageExitTarief2Procent;
        $this->belastbaarExitTarief125Nieuw = $belastbaarExitTarief125Nieuw;
        $this->belastbaarExitTarief125Oud = $belastbaarExitTarief125Oud;
    }

    public function output(): array
    {
        //TODO find beter way to do this/ same for new. ==> combine classes ?
        return
            [
                ['class' => str_replace(' ', '-', 'grondslag gewoon tarief'), 'name' => 'Grondslag gewoon tarief', 'code' => 'O 1460', 'amount' => $this->getGrondslagGewoonTarief()],
                ['class' => str_replace(' ', '-', 'negatieve correctie'), 'name' => 'Negatieve correctie', 'code' => '- 6201', 'amount' => '- ' . $this->numberFormatter($this->getNegatieveCorrectie())],
                ['class' => str_replace(' ', '-', 'positieve correctie'), 'name' => 'Positieve correctie', 'code' => '+ 6202', 'amount' => '+ ' . $this->numberFormatter($this->getPositieveCorrectie())],
                ['class' => str_replace(' ', '-', 'totale gewone grondslag'), 'name' => 'Totale gewone grondslag', 'code' => '', 'amount' => $this->getTotaalGewoneGrondslag()],
                ['class' => str_replace(' ', '-', 'gedeelte van de grondslag gewoon tarief-oud'), 'name' => 'Gedeelte van de grondslag gewoon tarief - oud', 'code' => '- 6211', 'amount' => '- ' . $this->numberFormatter($this->getGedeelteGrondslagGewoonTariefOud())],
                ['class' => str_replace(' ', '-', 'gecorrigeerde grondslag gewoon tarief-oud'), 'name' => 'Gecorrigeerde grondslag gewoon tarief - oud', 'code' => 'O 1460', 'amount' => $this->getGeCorrigeerdeGrondslagOud()],
                ['class' => str_replace(' ', '-', 'gecorrigeerde grondslag gewoon tarief - nieuw'), 'name' => 'Gecorrigeerde grondslag gewoon tarief - nieuw', 'code' => 'N 1460', 'amount' => $this->getGeCorrigeerdeGrondslagNieuw()],
                ['class' => str_replace(' ', '-', 'meerwaarden op aandelen tegen 25%'), 'name' => 'Meerwaarden op aandelen tegen 25%', 'code' => 'O 1465', 'amount' => $this->getMeerwaardeAandelen25()],
                ['class' => str_replace(' ', '-', 'negatieve correctie'), 'name' => 'Negatieve correctie', 'code' => '- 6203', 'amount' => '- ' . $this->numberFormatter($this->getNegatieveCorrectieMeerwaardeAandelen25())],
                ['class' => str_replace(' ', '-', 'totale grondslag meerwaarden op aandelen 25%'), 'name' => 'Totale grondslag meerwaarden op aandelen 25%', 'code' => '', 'amount' => $this->getTotaleGrondslagAandelen25()],
                ['class' => str_replace(' ', '-', 'gedeelte waarvoor de crisisbijdrage 2% bedraagt'), 'name' => 'Gedeelte waarvoor de crisisbijdrage 2% bedraagt', 'code' => '- 6213', 'amount' => '- ' . $this->numberFormatter($this->getGedeelteCrisisbijdrage3procent())],
                ['class' => str_replace(' ', '-', 'meerwaarden op aandelen tegen 25%-oud'), 'name' => 'Meerwaarden op aandelen tegen 25% - oud', 'code' => 'O 1465', 'amount' => $this->getMeerwaardeAandelen25Oud()],
                ['class' => str_replace(' ', '-', 'meerwaarden op aandelen tegen 25%-nieuw'), 'name' => 'Meerwaarden op aandelen tegen 25% - nieuw', 'code' => 'N 1466', 'amount' => $this->getMeerwaardenAandelen25Nieuw()],
                ['class' => str_replace(' ', '-', 'meerwaarden op aandelen tegen 20%-nieuw'), 'name' => 'Meerwaarden op aandelen tegen 20% - nieuw', 'code' => 'N 1466', 'amount' => $this->getMeerwaardenAandelen20Nieuw()],
                ['class' => str_replace(' ', '-', 'meerwaarden op aandelen tegen 0,4%'), 'name' => 'Meerwaarden op aandelen tegen 0,4%', 'code' => 'O 1424', 'amount' => $this->getMeerwaardenAandelen04()],
                ['class' => str_replace(' ', '-', 'negatieve correctie meerwaarden aandelen tegen 0,4%'), 'name' => 'Negatieve correctie meerwaarden aandelen tegen 0,4%', 'code' => '- 6204', 'amount' => '- ' . $this->numberFormatter($this->getNegatieveCorrectieMeerwaardeAandelen04())],
                ['class' => str_replace(' ', '-', 'meerwaarden op aandelen tegen 0,4%-oud'), 'name' => 'Meerwaarden op aandelen tegen 0,4% - oud', 'code' => 'O 1424', 'amount' => $this->getMeerwaardeAandelen04Oud()],
                ['class' => str_replace(' ', '-', 'belastbaar tegen het exit tax tarief van 12,5%'), 'name' => 'Belastbaar tegen het exit tax tarief van 12,5%', 'code' => 'N 1472', 'amount' => $this->getBelastbaarExittarief125()],
                ['class' => str_replace(' ', '-', 'gedeelte waarvoor de crisisibijdrage 2% bedraagt'), 'name' => 'Gedeelte waarvoor de crisisibijdrage 2% bedraagt', 'code' => '- 6214', 'amount' => '- ' . $this->numberFormatter($this->getCrisisbijdrageExitTarief2Procent())],
                ['class' => str_replace(' ', '-', 'belastbaar tegen het exit tax tarief van 12,5%-oud'), 'name' => 'Belastbaar tegen het exit tax tarief van 12,5% - oud', 'code' => 'O 1472', 'amount' => $this->getBelastbaarExitTarief125Oud()],
                ['class' => str_replace(' ', '-', 'belastbaar tegen het exit tax tarief van 12,5%-nieuw'), 'name' => 'Belastbaar tegen het exit tax tarief van 12,5% - nieuw', 'code' => 'N 1472', 'amount' => $this->getBelastbaarExitTarief125Nieuw()],
            ];
    }

    /**
     * @return float
     */
    public function getGrondslagGewoonTarief(): float
    {
        return $this->grondslagGewoonTarief;
    }

    public function numberFormatter(float $number): string
    {
        return number_format($number, 2, ',', '.');
    }

    /**
     * @return float
     */
    public function getNegatieveCorrectie(): float
    {
        return $this->negatieveCorrectie;
    }

    /**
     * @return float
     */
    public function getPositieveCorrectie(): float
    {
        return $this->positieveCorrectie;
    }

    /**
     * @return float
     */
    public function getTotaalGewoneGrondslag(): float
    {
        return $this->totaalGewoneGrondslag;
    }

    /**
     * @return float
     */
    public function getGedeelteGrondslagGewoonTariefOud(): float
    {
        return $this->gedeelteGrondslagGewoonTariefOud;
    }

    /**
     * @return float
     */
    public function getGeCorrigeerdeGrondslagOud(): float
    {
        return $this->geCorrigeerdeGrondslagOud;
    }

    /**
     * @return float
     */
    public function getGeCorrigeerdeGrondslagNieuw(): float
    {
        return $this->geCorrigeerdeGrondslagNieuw;
    }

    /**
     * @return float
     */
    public function getMeerwaardeAandelen25(): float
    {
        return $this->meerwaardeAandelen25;
    }

    /**
     * @return float
     */
    public function getNegatieveCorrectieMeerwaardeAandelen25(): float
    {
        return $this->negatieveCorrectieMeerwaardeAandelen25;
    }

    /**
     * @return float
     */
    public function getTotaleGrondslagAandelen25(): float
    {
        return $this->totaleGrondslagAandelen25;
    }

    /**
     * @return float
     */
    public function getGedeelteCrisisbijdrage3procent(): float
    {
        return $this->gedeelteCrisisbijdrage3procent;
    }

    /**
     * @return float
     */
    public function getMeerwaardeAandelen25Oud(): float
    {
        return $this->meerwaardeAandelen25Oud;
    }

    /**
     * @return float
     */
    public function getMeerwaardenAandelen25Nieuw(): float
    {
        return $this->meerwaardenAandelen25Nieuw;
    }

    /**
     * @return float
     */
    public function getMeerwaardenAandelen20Nieuw(): float
    {
        return $this->meerwaardenAandelen20Nieuw;
    }

    /**
     * @return float
     */
    public function getMeerwaardenAandelen04(): float
    {
        return $this->meerwaardenAandelen04;
    }

    /**
     * @return float
     */
    public function getNegatieveCorrectieMeerwaardeAandelen04(): float
    {
        return $this->negatieveCorrectieMeerwaardeAandelen04;
    }

    /**
     * @return float
     */
    public function getMeerwaardeAandelen04Oud(): float
    {
        return $this->meerwaardeAandelen04Oud;
    }

    /**
     * @return float
     */
    public function getBelastbaarExittarief125(): float
    {
        return $this->belastbaarExittarief125;
    }

    /**
     * @return float
     */
    public function getCrisisbijdrageExitTarief2Procent(): float
    {
        return $this->crisisbijdrageExitTarief2Procent;
    }

    /**
     * @return float
     */
    public function getBelastbaarExitTarief125Oud(): float
    {
        return $this->belastbaarExitTarief125Oud;
    }

    /**
     * @return float
     */
    public function getBelastbaarExitTarief125Nieuw(): float
    {
        return $this->belastbaarExitTarief125Nieuw;
    }


}
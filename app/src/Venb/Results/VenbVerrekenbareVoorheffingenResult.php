<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 10/07/19
 * Time: 18:27
 */

namespace Laudis\Calculators\Venb\Results;

/**
 * Class VenbVerrekenbareVoorheffingenResult
 * @package Laudis\Calculators\Venb\VersieAj2018
 */
final class VenbVerrekenbareVoorheffingenResult
{
    /**
     * @var float
     */
    private $nietTerugbetaalbareVoorheffing;
    /**
     * @var float
     */
    private $buitenlandseBelastingkrediet;
    /**
     * @var float
     */
    private $terugbetaalbareVoorheffing;
    /**
     * @var float
     */
    private $belastingkredietVoorOnderzoek;
    /**
     * @var float
     */
    private $voorafBetalingen;
    /**
     * @var float
     */
    private $totaalVoorVermeerdering;
    /**
     * @var float
     */
    private $vermeerdering;
    /**
     * @var float
     */
    private $totaal;

    /**
     * VenbVerrekenbareVoorheffingenResult constructor.
     * @param float $nietTerugbetaalbareVoorheffing
     * @param float $buitenlandseBelastingkrediet
     * @param float $terugbetaalbareVoorheffing
     * @param float $belastingkredietVoorOnderzoek
     * @param float $voorafBetalingen
     * @param float $totaalVoorVermeerdering
     * @param float $vermeerdering
     * @param float $totaal
     */
    public function __construct(
        float $nietTerugbetaalbareVoorheffing,
        float $buitenlandseBelastingkrediet,
        float $terugbetaalbareVoorheffing,
        float $belastingkredietVoorOnderzoek,
        float $voorafBetalingen,
        float $totaalVoorVermeerdering,
        float $vermeerdering,
        float $totaal
    ) {
        $this->nietTerugbetaalbareVoorheffing = $nietTerugbetaalbareVoorheffing;
        $this->buitenlandseBelastingkrediet = $buitenlandseBelastingkrediet;
        $this->terugbetaalbareVoorheffing = $terugbetaalbareVoorheffing;
        $this->belastingkredietVoorOnderzoek = $belastingkredietVoorOnderzoek;
        $this->voorafBetalingen = $voorafBetalingen;
        $this->totaalVoorVermeerdering = $totaalVoorVermeerdering;
        $this->vermeerdering = $vermeerdering;
        $this->totaal = $totaal;
    }

    /**
     * @return array
     */
    public function output(): array
    {
        return [
            'Niet-terugbetaalbare voorheffingen' => ['code' => '1830', 'bedrag' => $this->getNietTerugbetaalbareVoorheffing()],
            'Buitenlands belastingkrediet' => ['code' => '1834', 'bedrag' => $this->getBuitenlandseBelastingkrediet()],
            'Terugbetaalbare voorheffingen' => ['code' => '1840', 'bedrag' => $this->getTerugbetaalbareVoorheffing()],
            'Terugbetaalbaar belastingkrediet O&O' => ['code' => '1850', 'bedrag' => $this->getBelastingkredietVoorOnderzoek()],
            'Voorafbetalingen' => ['code' => '1810', 'bedrag' => $this->getVoorafBetalingen()],
            'totaalVoorVermeerdering' => $this->getTotaalVoorVermeerdering(),
            'vermeerdering' => $this->getVermeerdering(),
            'Totaal' => $this->getTotaal()
        ];
    }

    /**
     * @return float
     */
    public function getNietTerugbetaalbareVoorheffing(): float
    {
        return $this->nietTerugbetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getBuitenlandseBelastingkrediet(): float
    {
        return $this->buitenlandseBelastingkrediet;
    }

    /**
     * @return float
     */
    public function getTerugbetaalbareVoorheffing(): float
    {
        return $this->terugbetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getBelastingkredietVoorOnderzoek(): float
    {
        return $this->belastingkredietVoorOnderzoek;
    }

    /**
     * @return float
     */
    public function getVoorafBetalingen(): float
    {
        return $this->voorafBetalingen;
    }

    /**
     * @return float
     */
    public function getTotaalVoorVermeerdering(): float
    {
        return $this->totaalVoorVermeerdering;
    }

    /**
     * @return float
     */
    public function getVermeerdering(): float
    {
        return $this->vermeerdering;
    }


    /**
     * @return float
     */
    public function getTotaal(): float
    {
        return $this->totaal;
    }

}

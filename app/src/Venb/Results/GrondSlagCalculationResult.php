<?php


namespace Laudis\Calculators\Venb\Results;


class GrondSlagCalculationResult
{
    /**
     * @var float
     */
    private $grondslagVoorVermeerdering;
    /**
     * @var float
     */
    private $percentage;
    /**
     * @var float
     */
    private $vermeerdering;

    /**
     * GrondSlagCalculationResult constructor.
     * @param float $grondslagVoorVermeerdering
     * @param float $percentage
     * @param float $vermeerdering
     */
    public function __construct(float $grondslagVoorVermeerdering, float $percentage, float $vermeerdering)
    {
        $this->grondslagVoorVermeerdering = $grondslagVoorVermeerdering;
        $this->percentage = $percentage;
        $this->vermeerdering = $vermeerdering;
    }

    /**
     * @return float
     */
    public function getGrondslagVoorVermeerdering(): float
    {
        return $this->grondslagVoorVermeerdering;
    }

    /**
     * @return float
     */
    public function getPercentage(): float
    {
        return $this->percentage;
    }

    /**
     * @return float
     */
    public function getVermeerdering(): float
    {
        return $this->vermeerdering;
    }

    public function output():array
    {
        return
        [
            'grondslag' => $this->getGrondslagVoorVermeerdering(),
            'operation' => '&times;',
            'percentage' =>  number_format($this->getPercentage()*100,2,',','.')  . '%',
            'is'=>'=',
            'vermeerdering' => $this->getVermeerdering()



        ];
    }

}
<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb\Results;

/**
 * Class VenbAanslagenOutput
 * @package Laudis\Calculators\Venb\VersieAj2018
 */
final class VenbAanslagenOutput
{
    /**
     * @var array
     */
    private $vermeerderingen;
    /**
     * @var float
     */
    private $belastingTotal;


    /**
     * VenbAanslagenOutput constructor.
     * @param array $vermeerderingen
     * @param float $belastingTotal
     */
    public function __construct(array $vermeerderingen, float $belastingTotal)
    {
        $this->vermeerderingen = $vermeerderingen;
        $this->belastingTotal = $belastingTotal;
    }

    /**
     * @return array
     */
    public function getVermeerderingen(): array
    {
        return $this->vermeerderingen;
    }

    /**
     * @return array
     */
    public function output(): array
    {
        return [
            'aanslagen' => $this->getVermeerderingen(),
            'belastingEnCrisisbijdrageTotal' => $this->getBelastingTotal(),
        ];
    }

    /**
     * @return float
     */
    public function getBelastingTotal(): float
    {
        return $this->belastingTotal;
    }

}

<?php


namespace Laudis\Calculators\Venb\Results;


use Laudis\Calculators\Venb\TaxationRule;

class BelastingAndCrisisBijdrageResult
{
    /**
     * @var string
     */
    private $nameCalculation;
    /**
     * @var TaxationRule
     */
    private $belasting;
    /**
     * @var TaxationRule
     */
    private $crisisBijdrage;

    /**
     * BelastingAndCrisisBijdrageResult constructor.
     * @param string $nameCalculation
     * @param TaxationRule $belasting
     * @param TaxationRule $crisisBijdrage
     */
    public function __construct(string $nameCalculation, TaxationRule $belasting, TaxationRule $crisisBijdrage)
    {
        $this->nameCalculation = $nameCalculation;
        $this->belasting = $belasting;
        $this->crisisBijdrage = $crisisBijdrage;
    }

    /**
     * @return TaxationRule
     */
    public function getBelasting(): TaxationRule
    {
        return $this->belasting;
    }

    /**
     * @return TaxationRule
     */
    public function getCrisisBijdrage(): TaxationRule
    {
        return $this->crisisBijdrage;
    }

    /**
     * @return string
     */
    public function getNameCalculation(): string
    {
        return $this->nameCalculation;
    }



    public function output() : array
    {
        return
        [
          $this->getNameCalculation() => $this->getBelasting()->output(),
            'Crisisbijdrage' => $this->getCrisisBijdrage()->output()
        ];
    }


}
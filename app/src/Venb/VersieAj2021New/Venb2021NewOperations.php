<?php
/** @noinspection DuplicatedCode */
declare(strict_types=1);

namespace Laudis\Calculators\Venb\VersieAj2021New;

use Exception;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Venb\BasicCalculationResult;
use Laudis\Calculators\Venb\Results\BelastbaartariefResult;
use Laudis\Calculators\Venb\Results\BelastingAndCrisisBijdrageResult;
use Laudis\Calculators\Venb\Results\GeenVermeerderingIndienLagerResult;
use Laudis\Calculators\Venb\Results\GrondSlagCalculationResult;
use Laudis\Calculators\Venb\Results\ToegepasteCorrectiesNieuwResult;
use Laudis\Calculators\Venb\Results\VenbAanslagenOutput;
use Laudis\Calculators\Venb\Results\VenbBerekeningVermeerderingOutput;
use Laudis\Calculators\Venb\Results\VenbVoorafbetalingenOutput;
use Laudis\Calculators\Venb\TaxationRule;
use Laudis\Calculators\Venb\VenbGeneralOperations;
use Laudis\Calculators\Venb\VenbOperationInterface;
use Laudis\Calculators\Venb\VersieAj2020\Venb2020Input;
use function Symfony\Component\Debug\Tests\testHeader;

/**
 *
 * @package Laudis\Calculators\Calculators\Calculators
 */
final class Venb2021NewOperations extends VenbGeneralOperations implements VenbOperationInterface
{
    /**
     * @param Venb2021Input $input
     * @return CalculationResultInterface
     * @throws Exception
     */
    public function calculate($input): CalculationResultInterface
    {
        $belastbaarGewoonStelsel = $this->belastbaarGewoonStelsel($input,
            $this->gewoneAanslagen($input)->getBelastingTotal(),
            $this->outputBerekeningVermeerdering($input)->getWeerhoudenVermeerdering());
        $afzonderlijkeAanslagen = $this->afzonderlijkeAanslagen($input,0.02);

        return new BasicCalculationResult([
            'gewoneAanslagen' => $this->gewoneAanslagen($input)->getVermeerderingen(),
            'totaalGewoneAanslagen' => $this->gewoneAanslagen($input)->getBelastingTotal(),
            'weerhoudenVermeerdering' =>$this->outputBerekeningVermeerdering($input)->getWeerhoudenVermeerdering(),
            'eindTotaal' => $this->getEindTotaal($belastbaarGewoonStelsel->getTotaal(),$afzonderlijkeAanslagen->getBelastingTotal()),
            'belastbaarGewoonStelsel' => $belastbaarGewoonStelsel->output(),
            'afzonderlijkBelastbaar' => $afzonderlijkeAanslagen->getVermeerderingen(),
            'totaalDoorUTeBetalen' => $belastbaarGewoonStelsel->getTotaal()
                + $afzonderlijkeAanslagen->getBelastingTotal(),
            'berekeningVanDeVermeerdering' => $this->outputBerekeningVermeerdering($input)->output(),
            'geenVemeerdering' => $this->geenVermeerderingIndienResult($input)->output(),
            'toegepasteCorrecties' => $this->toegepasteCorrecties($input)->output()
        ]);
    }

    /**
     * @param Venb2021Input $input
     * @return ToegepasteCorrectiesNieuwResult
     *
     * set manually to 0, corrections not known at this point
     */
    private function toegepasteCorrecties(Venb2021Input $input) : ToegepasteCorrectiesNieuwResult
    {
        $belastingOud = $input->getGedeelteGecorrigeerdeAanslag33();
        $belastingNieuw = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
            + $input->getPositieveCorrectieGrondslagGewoonTarief()
            - $input->getGedeelteGecorrigeerdeAanslag33();
//
//        return new ToegepasteCorrectiesNieuwResult(
//            $input->getBelastbaarGewoonTarief(),
//            $input->getNegatieveCorrectieGrondslagGewoonTarief(),
//            $input->getPositieveCorrectieGrondslagGewoonTarief(),
//            $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief() + $input->getPositieveCorrectieGrondslagGewoonTarief(),
//            $input->getGedeelteGecorrigeerdeAanslag33(),
//            $belastingNieuw,
//            $belastingOud,
//            $input->getMeerwaardeOpAandelen25(),
//            $input->getPositieveCorrectieMeerwaardeAandelen25(),
//            $input->getMeerwaardeOpAandelen25() +  $input->getPositieveCorrectieMeerwaardeAandelen25(),
//            $input->getGedeelteMeerwaardeBelastbaar25(),
//            $this->calculateMeerwaardeOpAandelen25($input)->getAmount(),
//            $this->calculateMeerwaardeOpAandelen25Korting($input)->getAmount(),
//            $this->calculateCorrectieMeerwaardeOpAandelen25($input)->getAmount(),
//            $input->getPositieveCorrectieMeerwaardeAandelen04(),
//            $this->calculateMeerWaardeOpAandelen04($input)->getAmount(),
//            $input->getBelastbareTegenExitTaks125(),
//            $input->getGedeelteGrondslagExitTaks125(),
//            $this->calculateBelastbaarTegenExitTaks125($input)->getAmount(),
//            $this->calculateCorrectieBelastbaarTegenExitTaks125($input)->getAmount());
        return new ToegepasteCorrectiesNieuwResult(
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0,
            0);

    }

    private function gewoneAanslagen(Venb2021Input $input) : VenbAanslagenOutput
    {
        $vermeerderingen[] = [$this->calculateBelastbaarInkomen($input)->getName() =>$this->calculateBelastbaarInkomen($input)->getData() ,
            'Crisisbijdrage'=>$this->calculateBelastbaarCrisisBijdrage($input)->output()
        ];
        $vermeerderingen[] = [$this->calculateCorrectieBelastbaarInkomen($input)->getName() =>$this->calculateCorrectieBelastbaarInkomen($input)->getData() ,
            'Crisisbijdrage'=>$this->calculateCrisisbijdrageCorrectieBelastbaarInkomen($input)->output()
        ];

        $vermeerderingen = $this->addCode1466($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1466Korting($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1465Correctie($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1467($input, $vermeerderingen);
        $vermeerderingen = $this->addCodeVerwezenlijkteMeerwaarde2958($input,$vermeerderingen);
        $vermeerderingen = $this->addCode1472($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1472Correctie($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1424($input, $vermeerderingen);
        $vermeerderingen = $this->addCodeMobilisatieReserves10($input,$vermeerderingen);
        $vermeerderingen = $this->addCodeMobilisatieReserves15($input,$vermeerderingen);

        return $this->venbOperationAuxiliariesNew->calculateTotalOfArray($vermeerderingen);
    }

    /**
     * @param Venb2021Input $input
     * @return BelastbaartariefResult
     */
    //TODO maybe split this method in multiple methods
    private function calculateBelastbaarInkomen(Venb2021Input $input): BelastbaartariefResult
    {
        $amount = $input->getBelastbaarGewoonTarief();
         if ($input->isWijzigingAfsluitdatum26072017()){
             $amount = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
                 + $input->getPositieveCorrectieGrondslagGewoonTarief()
                 - $input->getGedeelteGecorrigeerdeAanslag33();
         }


        if ($input->isAanspraakVerminderdTarief() && !$input->isVenootshapHuisvesting()) {
            $belasting = $this->venbOperationAuxiliariesNew->presentBarema($amount);
            $name ='Verminderd tarief';
            $data = ['code' =>'N 1460', 'belasting' => $belasting];
        } elseif (!$input->isVenootshapHuisvesting()) {
            $belasting = $amount * 0.25;
            $name ='Gewoon tarief';
            $percentage = '25,00%';
            $data =          [
                'code' => 'N 1460',
                'bedrag' => $amount,
                'operation' => '&times',
                'tarief' => $percentage,
                'is' => '=',
                'belasting' => $belasting,
            ];
        } else
        {
            $belasting = $amount * 0.05;
            $name ='Tarief huisvesting tegen 5%';
            $percentage = '5%';
            $data =          [
                'code' => 'N 1460',
                'bedrag' => $amount,
                'operation' => '&times',
                'tarief' => $percentage,
                'is' => '=',
                'belasting' => $belasting,
            ];
        }
        return new BelastbaartariefResult($name,$data);
    }

    /**
     * @param Venb2021Input $input
     * @return TaxationRule
     *
     * NO CRISISBIJDRAGES in 2021NEW SO IT'S ALL 0
     */
    private function calculateBelastbaarCrisisBijdrage(Venb2021Input $input): TaxationRule
    {
        $amount = 0;
        /** manually set to 0 for now  */
        $belastingEnCrisisBijdrage = 0;

        if ($this->venbOperationAuxiliariesOld->getFilter()->isCrisisbijdrage('1460')) {
            $belastingEnCrisisBijdrage = $this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateBelastbaarInkomen($input))*self::ACBNew;
            $amount = $this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateBelastbaarInkomen($input));
        }
        //return new TaxationRule('N 1460', $amount,0.02,$belastingEnCrisisBijdrage);
        //Manually set to 0
        return new TaxationRule('N 1460', $amount,0.02,0);
    }

    /**
     * @param Venb2021Input $input
     * @return BelastbaartariefResult
     */
    private function calculateCorrectieBelastbaarInkomen(Venb2021Input $input): BelastbaartariefResult
    {
        $name = 'Gewoon tarief';
        $data =
            [
                'code' => 'O 1460',
                'bedrag' => 0,
                'operation' => '&times',
                'tarief' => '33,00%',
                'is' => '=',
                'belasting' => 0,
            ];

        if ($input->isWijzigingAfsluitdatum26072017())
        {
            if ($input->isVenootshapHuisvesting()) {
                return $this->tariefHuisVestingCorrectie($input);
            }
            if ($input->isCorrectieVerlaagdTarief() && !$input->isVenootshapHuisvesting() && $input->getGedeelteGecorrigeerdeAanslag33() <= 322500) {
                return $this->verminderdtariefCorrectie($input);
            }

            return $this->gewoonTariefCorrectie($input);
        }
        return new BelastbaartariefResult($name,$data);
    }

    public function tariefHuisVestingCorrectie(Venb2021Input $input) : BelastbaartariefResult
    {
        $belasting = $input->getGedeelteGecorrigeerdeAanslag33()*0.05;
        $name = 'Tarief huisvesting tegen 5%';
        $data =
            [
                'code' => 'O 1460',
                'bedrag' => $input->getGedeelteGecorrigeerdeAanslag33(),
                'operation' => '&times',
                'tarief' => '5,00%',
                'is' => '=',
                'belasting' => $belasting,
            ];

        return new BelastbaartariefResult($name,$data);
    }

    public function gewoonTariefCorrectie(Venb2021Input $input) : BelastbaartariefResult
    {
        $belasting = $input->getGedeelteGecorrigeerdeAanslag33()*0.33;
        $name = 'Gewoon tarief';
        $data =
            [
                'code' => 'O 1460',
                'bedrag' => $input->getGedeelteGecorrigeerdeAanslag33(),
                'operation' => '&times',
                'tarief' => '33,00%',
                'is' => '=',
                'belasting' => $belasting,
            ];
        return new BelastbaartariefResult($name,$data);
    }

    public function verminderdtariefCorrectie(Venb2021Input $input) : BelastbaartariefResult
    {
        $belasting = $this->venbOperationAuxiliariesOld->presentBarema($input->getGedeelteGecorrigeerdeAanslag33());
        $name = 'Verminderd tarief';
        $data = ['code' =>'O 1460', 'belasting' => $belasting];

        return new BelastbaartariefResult($name,$data);
    }

    /**
     * @param $input
     * @return TaxationRule
     */
    private function calculateCrisisbijdrageCorrectieBelastbaarInkomen(Venb2021Input $input) : TaxationRule
    {
        $belastingEnCrisisBijdrage = 0;
        $amount = 0;

        if ($this->venbOperationAuxiliariesOld->getFilter()->isCrisisbijdrage('1460')) {
            $amount = $this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateCorrectieBelastbaarInkomen($input));
            $belastingEnCrisisBijdrage = $amount * 0.03;
        }
        return new TaxationRule('O 1460', $amount,0.03,$belastingEnCrisisBijdrage);
    }

    /**
     * @param Venb2021Input $input
     * @return TaxationRule
     */
    private function calculateMeerwaardeOpAandelen25(Venb2021Input $input): TaxationRule
    {
        $amount = $input->getMeerwaardeOpAandelen25() - $this->calculateMeerwaardeOpAandelen25Korting($input)->getAmount();
        if ($input->isWijzigingAfsluitdatum26072017()){
            $amount -= $input->getGedeelteMeerwaardeBelastbaar25();
        }

        return $this->belastingCalculation('N 1466', $amount, 0.25);
    }

    private function calculateMeerwaardeOpAandelen25Korting(Venb2021Input $input)
    {
        $total = $input->getBelastbaarGewoonTarief();

        if ($input->isWijzigingAfsluitdatum26072017()){
            $total = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
                + $input->getPositieveCorrectieGrondslagGewoonTarief()
                - $input->getGedeelteGecorrigeerdeAanslag33();
        }

        $tmp = 100000 - $total;
        $amount = $input->getMeerwaardeOpAandelen25() - $input->getGedeelteMeerwaardeBelastbaar25();

        if ($amount >= $tmp){
            $amount = $tmp;
        }

        if ($total <= 100000 && $input->isAanspraakVerminderdTarief()){
            return $this->belastingCalculation('N 1466', $amount, 0.20);
        }

        return $this->belastingCalculation('N 1466', 0, 0.20);
    }

    /**
     * @param Venb2021Input $input
     * @return TaxationRule
     */
    private function calculateCorrectieMeerwaardeOpAandelen25(Venb2021Input $input): TaxationRule
    {
        if ($input->isWijzigingAfsluitdatum26072017()){
            return $this->belastingCalculation('O 1465', $input->getGedeelteMeerwaardeBelastbaar25()+$input->getPositieveCorrectieMeerwaardeAandelen25()
                , 0.25);
        }
        return $this->belastingCalculation('O 1465', 0, 0.25);
    }

    /**
     * @param Venb2021Input $input
     * @return TaxationRule
     */
    private function calculateVerwezenlijkteMeerwaarde(Venb2021Input $input): TaxationRule
    {
        return $this->belastingCalculation('N 1467', $input->getVerwezenlijkteMeerwaarde3399(), 0.3399);
    }

    /**
     * @param Venb2021Input $input
     * @return TaxationRule
     */
    private function calculateVerwezenlijkteMeerwaarde2958(Venb2021Input $input): TaxationRule
    {
        return $this->belastingCalculation('N ????', $input->getVerwezenlijkteMeerwaarde2958(), 0.2958);
    }

    /**
     * @param Venb2021Input $input
     * @return TaxationRule
     */
    private function calculateBelastbaarTegenExitTaks15(Venb2021Input $input): TaxationRule
    {
        $amount = $input->getBelastbareTegenExitTaks125();
        if ($input->isWijzigingAfsluitdatum26072017()) {
            $amount -= $input->getGedeelteGrondslagExitTaks125();
        }
        return $this->belastingCalculation('N 1472', $amount, 0.15);
    }

    /**
     * @param Venb2021Input $input
     * @return TaxationRule
     */
    private function calculateCorrectieBelastbaarTegenExitTaks125(Venb2021Input $input): TaxationRule
    {
        if ($input->isWijzigingAfsluitdatum26072017()){
            return $this->belastingCalculation('O 1472', $input->getGedeelteGrondslagExitTaks125(), 0.125);
        }
        return $this->belastingCalculation('O 1472',0, 0.125);

    }

    /**
     * @param Venb2021Input $input
     * @return TaxationRule
     */
    private function calculateCorrectieCrisisBijdrageBelastbaarTegenExitTaks125(Venb2021Input $input): TaxationRule {
        return $this->crisisBijdrageCalculation('1472', $this->getOldFilter(),
                                                $this->calculateCorrectieBelastbaarTegenExitTaks125($input)->getBelastingOfCrisisbijdrage(), 0.03, true);
    }

    /**
     * @param Venb2021Input $input
     * @return TaxationRule
     */
    private function calculateMeerWaardeOpAandelen04(Venb2021Input $input): TaxationRule
    {
        $amount = 0;
        if ($input->isWijzigingAfsluitdatum26072017()) {
            $amount = $input->getPositieveCorrectieMeerwaardeAandelen04();
        }
        return $this->belastingCalculation('O 1424', $amount, 0.004);
    }

    private function calculateMobilisatieReserves10(venb2021Input $input) : TaxationRule
    {
        return $this->belastingCalculation('N ????', $input->getMobilisatieReserves10(), 0.10);
    }

    private function calculateMobilisatieReserves15(venb2021Input $input) : TaxationRule
    {
        return $this->belastingCalculation('N ????', $input->getMobilisatieReserves15(), 0.15);
    }

    /**
     * @param Venb2021Input $input
     * @return VenbBerekeningVermeerderingOutput
     * @throws Exception
     */
    private function outputBerekeningVermeerdering(Venb2021Input $input): VenbBerekeningVermeerderingOutput
    {
        if ($input->isStartendeKMO()){
            return new VenbBerekeningVermeerderingOutput(
                0,
                0,
                0,
                0,
                0,
                new GrondSlagCalculationResult(0,0,0),
                new VenbVoorafbetalingenOutput([],0),
                0,
                0,
                0,
                0,
                0
            );
        }

        return new VenbBerekeningVermeerderingOutput(
            $this->calculateGrondslagVA($input),
            -$input->getNietterugbetaalbareVoorheffing(),
            -$input->getBuitenlandseBelastingkrediet(),
            -$input->getTerugbetaalbareVoorheffing(),
            -$input->getBelastingkredietOndOntw(),
            new GrondSlagCalculationResult(
                $this->grondslagVoorVermeerding($input),
                $this->venbOperationAuxiliariesNew->indexedValue($input->getAantalkwartalen() . 'kwart'),
                $this->calculateVermeerdering($input)
            ),
            $this->venbOperationAuxiliariesNew->voorafBetalingen($input->getVoorafBetalingenInput(), $input->isVerhoogdePercentagesVA3VA4()),
            $this->saldoVermeerdering($input,$input->isVerhoogdePercentagesVA3VA4()),
            $this->beperkingTot80(),
            $this->beperkingTotGrondVerm($input),
            $this->geenVermeerderingIndien($input),
            $this->weerhoudenVermeerdering($input)
        );
    }

    /**
     * @param Venb2021Input $input
     * @return GeenVermeerderingIndienLagerResult
     * @throws Exception
     */
    private function geenVermeerderingIndienResult(Venb2021Input $input): GeenVermeerderingIndienLagerResult
    {
        if ($input->isStartendeKMO()){
            return new GeenVermeerderingIndienLagerResult(
                0,
                0,
                0
            );
        }
        return new GeenVermeerderingIndienLagerResult(
            $this->beperkingTot80(),
            $this->beperkingTotGrondVerm($input),
            $this->weerhoudenVermeerdering($input));
    }

    private function calculateGrondslagVA(Venb2021Input $input) : float
    {
        $amountGrondslagNieuw = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
            + $input->getPositieveCorrectieGrondslagGewoonTarief()
            - $input->getGedeelteGecorrigeerdeAanslag33();

        if ($amountGrondslagNieuw +$input->getGedeelteGecorrigeerdeAanslag33() === 0.00 ){
            return 0;
        }

        return - $input->getLatenteMeerwaarden() *
            (($this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateBelastbaarInkomen($input))
            + $this->calculateBelastbaarCrisisBijdrage($input)->getBelastingOfCrisisbijdrage()
            +$this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateCorrectieBelastbaarInkomen($input))
            + $this->calculateCrisisbijdrageCorrectieBelastbaarInkomen($input)->getBelastingOfCrisisbijdrage())
                / ($amountGrondslagNieuw + $input->getGedeelteGecorrigeerdeAanslag33()));
    }


    /**
     * @param Venb2021Input $input
     * @return float
     */
    private function grondslagVoorVermeerding(Venb2021Input $input): float
    {
        return max(array_sum([
            $this->gewoneAanslagen($input)->getBelastingTotal(),
            $this->calculateGrondslagVA($input),
            -$input->getNietTerugbetaalbareVoorheffing(),
            -$input->getBuitenlandseBelastingkrediet(),
            -$input->getTerugbetaalbareVoorheffing(),
            -$input->getBelastingkredietOndOntw()
        ]), 0);
    }

    /**
     * @param Venb2021Input $input
     * @return float|int
     * @throws Exception
     */
    private function calculateVermeerdering(Venb2021Input $input)
    {
        return $this->grondslagVoorVermeerding($input) * $this->venbOperationAuxiliariesNew->indexedValue($input->getAantalKwartalen() . 'kwart');
    }


    /**
     * @param Venb2021Input $input
     * @param bool $verhoogdeVA3VA4
     * @return float
     * @throws Exception
     */
    private function saldoVermeerdering(Venb2021Input $input, bool $verhoogdeVA3VA4=false): float
    {
        return max([$this->venbOperationAuxiliariesNew->voorafBetalingen($input->getVoorafBetalingenInput(),$verhoogdeVA3VA4)->getTotal() + $this->calculateVermeerdering($input), 0]);
    }

    /**
     * @return float
     * @throws Exception
     */
    private function beperkingTot80(): float
    {
        return $this->venbOperationAuxiliariesNew->indexedValue('geen verhoging');
    }

    /**
     * @param Venb2021Input $input
     * @return float
     * @throws Exception
     */
    private function beperkingTotGrondVerm(Venb2021Input $input): float
    {
        return $this->grondslagVoorVermeerding($input) * $this->venbOperationAuxiliariesNew->indexedValue('pct');
    }

    /**
     * @param Venb2021Input $input
     * @return mixed
     * @throws Exception
     */
    private function geenVermeerderingIndien(Venb2021Input $input)
    {
        return max($this->beperkingTot80(), $this->beperkingTotGrondVerm($input));
    }

    /**
     * @param Venb2021Input $input
     * @param bool $verhoogd
     * @return float|int
     * @throws Exception
     */
    private function weerhoudenVermeerdering(Venb2021Input $input)
    {
        if ($this->saldoVermeerdering($input,$input->isVerhoogdePercentagesVA3VA4()) < $this->geenVermeerderingIndien($input)) {
            return 0;
        }
        return $this->saldoVermeerdering($input, $input->isVerhoogdePercentagesVA3VA4());
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1466(Venb2021Input $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen25 = new BelastingAndCrisisBijdrageResult('Meerwaarden op aandelen tegen 25%',
            $this->calculateMeerwaardeOpAandelen25($input),
            $this->crisisBijdrageCalculation('1466', $this->getNewFilter(),
                $this->calculateMeerwaardeOpAandelen25($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));

        $meerwaardeAandelen25->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $meerwaardeAandelen25->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1466Korting(Venb2021Input $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen25Korting = new BelastingAndCrisisBijdrageResult('Meerwaarden op aandelen tegen 20%',
            $this->calculateMeerwaardeOpAandelen25Korting($input),
            $this->crisisBijdrageCalculation('1466', $this->getNewFilter(),
                $this->calculateMeerwaardeOpAandelen25Korting($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));

        $meerwaardeAandelen25Korting->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $meerwaardeAandelen25Korting->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1465Correctie(Venb2021Input $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen25 = new BelastingAndCrisisBijdrageResult('Correctie Meerwaarden op aandelen tegen 25%',
            $this->calculateCorrectieMeerwaardeOpAandelen25($input),
            $this->crisisBijdrageCalculation('1465', $this->getOldFilter(),
                $this->calculateCorrectieMeerwaardeOpAandelen25($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));

        $meerwaardeAandelen25->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $meerwaardeAandelen25->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1467(Venb2021Input $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen3399 = new BelastingAndCrisisBijdrageResult('Meerwaarden, voorzieningen,... tegen 33,99%',
            $this->calculateVerwezenlijkteMeerwaarde($input),
            $this->crisisBijdrageCalculation('1467', $this->getNewFilter(),
                $this->calculateVerwezenlijkteMeerwaarde($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));

        $meerwaardeAandelen3399->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $meerwaardeAandelen3399->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCodeVerwezenlijkteMeerwaarde2958(Venb2021Input $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen2958 = new BelastingAndCrisisBijdrageResult('Verwezenlijkte meerwaarden, voorzieningen en tax-shelter tegen 29,58%',
            $this->calculateVerwezenlijkteMeerwaarde2958($input),
            $this->crisisBijdrageCalculation('????', $this->getNewFilter(),
                $this->calculateVerwezenlijkteMeerwaarde2958($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));
        $meerwaardeAandelen2958->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $meerwaardeAandelen2958->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1472(Venb2021Input $input, array $vermeerderingen): array
    {
        $exitTax125 = new BelastingAndCrisisBijdrageResult('Belastbaar tegen exit tax 15%',
            $this->calculateBelastbaarTegenExitTaks15($input),
            $this->crisisBijdrageCalculation('1472', $this->getNewFilter(),
                $this->calculateBelastbaarTegenExitTaks15($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));

        $exitTax125->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $exitTax125->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1472Correctie(Venb2021Input $input, array $vermeerderingen): array
    {
        $corrExitTax125 = new BelastingAndCrisisBijdrageResult('Correctie belastbaar tegen exit tax 12,5%',
            $this->calculateCorrectieBelastbaarTegenExitTaks125($input),
            $this->calculateCorrectieCrisisBijdrageBelastbaarTegenExitTaks125($input));
        $corrExitTax125->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $corrExitTax125->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1424(Venb2021Input $input, array $vermeerderingen): array
    {
        $meerwaardeOpAandelen04 = new BelastingAndCrisisBijdrageResult('Meerwaarde op aandelen 0,4%',
            $this->calculateMeerWaardeOpAandelen04($input),
            $this->crisisBijdrageCalculation('1424', $this->getOldFilter(),
                $this->calculateMeerWaardeOpAandelen04($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));

        $meerwaardeOpAandelen04->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $meerwaardeOpAandelen04->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCodeMobilisatieReserves10(Venb2021Input $input, array $vermeerderingen): array
    {
        $meerwaardeOpAandelen04 = new BelastingAndCrisisBijdrageResult('Mobilisatie van reserves tegen 10%',
            $this->calculateMobilisatieReserves10($input),
            $this->crisisBijdrageCalculation('????', $this->getOldFilter(),
                $this->calculateMobilisatieReserves10($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));

        $meerwaardeOpAandelen04->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $meerwaardeOpAandelen04->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2021Input $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCodeMobilisatieReserves15(Venb2021Input $input, array $vermeerderingen): array
    {
        $meerwaardeOpAandelen04 = new BelastingAndCrisisBijdrageResult('Mobilisatie van reserves tegen 15%',
            $this->calculateMobilisatieReserves15($input),
            $this->crisisBijdrageCalculation('????', $this->getOldFilter(),
                $this->calculateMobilisatieReserves10($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));

        $meerwaardeOpAandelen04->getCrisisBijdrage()->setBelastingOfCrisisbijdrage(0);
        $vermeerderingen[] = $meerwaardeOpAandelen04->output();
        return $vermeerderingen;
    }
}

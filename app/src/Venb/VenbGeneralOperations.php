<?php

namespace Laudis\Calculators\Venb;


use Laudis\Calculators\Venb\Filters\VenbFilter;
use Laudis\Calculators\Venb\Results\BelastingAndCrisisBijdrageResult;
use Laudis\Calculators\Venb\Results\VenbAanslagenOutput;
use Laudis\Calculators\Venb\Results\VenbVerrekenbareVoorheffingenResult;
use Laudis\Calculators\Venb\VersieAj2019New\Venb2019InputNew;
use Laudis\Calculators\Venb\VersieAj2019Old\Venb2019InputOld;
use Laudis\Calculators\Venb\VersieAj2020\Venb2020Input;
use Laudis\Calculators\Venb\VersieAj2021New\Venb2021Input;

/**
 * Class VenbGeneralOperations
 * @package Laudis\Calculators\Venb
 *
 * this class is a super class to all the version with the basic calculations in it, some methods will be overwritten by subclasses if small changes occur
 */
class VenbGeneralOperations
{
    /** @var float  */
    protected const ACBOld = 0.03;
    /** @var float  */
    protected const ACBNew = 0.02;
    /** @var VenbOperationAuxiliaries */
    protected $venbOperationAuxiliariesOld;
    /** @var VenbOperationAuxiliaries */
    protected $venbOperationAuxiliariesNew;

    /**
     * VenbGeneralOperations constructor.
     * @param VenbOperationAuxiliaries $venbOperationAuxiliariesOld
     * @param VenbOperationAuxiliaries $venbOperationAuxiliariesNew
     */
    public function __construct(VenbOperationAuxiliaries $venbOperationAuxiliariesOld, VenbOperationAuxiliaries $venbOperationAuxiliariesNew)
    {
        $this->venbOperationAuxiliariesOld = $venbOperationAuxiliariesOld;
        $this->venbOperationAuxiliariesNew = $venbOperationAuxiliariesNew;
    }

    /**
     * @return VenbFilter
     * method to prevent long names in extended classes
     */
    public function getOldFilter() : VenbFilter
    {
        return $this->venbOperationAuxiliariesOld->getFilter();
    }

    /**
     * @return VenbFilter
     * method to prevent long names in extended classes
     */
    public function getNewFilter() :  VenbFilter
    {
        return $this->venbOperationAuxiliariesNew->getFilter();
    }

    /**
     * @param string $number
     * @param $amount
     * @param $percentage
     * @return TaxationRule
     */
    public function belastingCalculation(string $number ,float $amount, float $percentage): TaxationRule
    {
        return $this->venbOperationAuxiliariesNew->belastingCalculation($number,$amount,$percentage);

    }

    /**
     * @param string $number
     * @param VenbFilter $filter
     * @param float $amount
     * @param float $percentage
     * @return TaxationRule
     */
    public function crisisBijdrageCalculation(string $number,VenbFilter $filter ,float $amount, float $percentage, bool $letterBeforeNumber = false): TaxationRule
    {
       return $this->venbOperationAuxiliariesNew->crisisBijdrageCalculation($number,$filter,$amount,$percentage,$letterBeforeNumber);
    }

    /**
     * @param Venb2019InputNew | Venb2019InputOld | Venb2020Input | Venb2021Input $input
     * @param float $belastingsTotaal
     * @param float $weerhoudenVermeerdering
     * @return VenbVerrekenbareVoorheffingenResult
     */
    public function belastbaarGewoonStelsel($input, float $belastingsTotaal, float $weerhoudenVermeerdering): VenbVerrekenbareVoorheffingenResult
    {
        /** @var VenbAanslagenOutput $vermeerderingen */
        $nietTerugbetaalbareVoorheffing = -min(
            $belastingsTotaal,
            $input->getNietTerugbetaalbareVoorheffing());

        $buitenlandsBelastingskrediet = -min(
            $belastingsTotaal
            + $nietTerugbetaalbareVoorheffing,
            $input->getBuitenlandseBelastingkrediet()
        );

        $terugbetaalbareVoorheffing = -$input->getTerugbetaalbareVoorheffing();
        $belastingskredietVoorOnderzoek = -$input->getBelastingkredietOndOntw();
        $voorafbetalingen =$this->venbOperationAuxiliariesNew->voorafBetalingenTotaal($input);

        $tussentotaal = array_sum([
            $belastingsTotaal,
            $nietTerugbetaalbareVoorheffing,
            $buitenlandsBelastingskrediet,
            $terugbetaalbareVoorheffing,
            $belastingskredietVoorOnderzoek,
            $voorafbetalingen
        ]);

        return new VenbVerrekenbareVoorheffingenResult(
            $nietTerugbetaalbareVoorheffing,
            $buitenlandsBelastingskrediet,
            $terugbetaalbareVoorheffing,
            $belastingskredietVoorOnderzoek,
            $voorafbetalingen,
            $tussentotaal,
            $weerhoudenVermeerdering,
            $tussentotaal + $weerhoudenVermeerdering);
    }

    /**
     * @param VenbGeneralInput| $input
     * @param float $ACBpercentage
     * @return VenbAanslagenOutput
     */
    protected function afzonderlijkeAanslagen(VenbGeneralInput $input, float $ACBpercentage): VenbAanslagenOutput
    {
        $kap = new BelastingAndCrisisBijdrageResult('Landbouwsubsidies tegen 5%',
            $this->calculateKapitaalenInterestsubsidiesLandboouw05($input),
            $this->crisisBijdrageCalculation('1481', $this->getOldFilter(),
                                             $this->calculateKapitaalenInterestsubsidiesLandboouw05($input)->getBelastingOfCrisisbijdrage(), $ACBpercentage));
        $vermeerderingen[] = $kap->output();

        $commissielonen50 = new BelastingAndCrisisBijdrageResult('Geheime commissielonen tegen 50%',
            $this->calculateAfzonderlijkeAanslagAlleAard50($input),
            $this->crisisBijdrageCalculation('1506', $this->getNewFilter(),
                $this->calculateAfzonderlijkeAanslagAlleAard50($input)->getBelastingOfCrisisbijdrage(),
                $ACBpercentage));
        $vermeerderingen[] = $commissielonen50->output();

        $commissielonen100 = new BelastingAndCrisisBijdrageResult('Geheime commissielonen tegen 100%',
            $this->calculateAfzonderlijkeAanslagAlleAard100($input),
            $this->crisisBijdrageCalculation('1507',$this->getNewFilter(),
                $this->calculateAfzonderlijkeAanslagAlleAard100($input)->getBelastingOfCrisisbijdrage(),
                $ACBpercentage));
        $vermeerderingen[] = $commissielonen100->output();

        $krediet34 = new BelastingAndCrisisBijdrageResult('Belaste reserves kredietinstellingen tegen 34%',
            $this->calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen34($input),
            $this->crisisBijdrageCalculation('1502a',$this->getNewFilter(),
                $this->calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen34($input)->getBelastingOfCrisisbijdrage(),
                $ACBpercentage));
        $vermeerderingen[] = $krediet34->output();

        $krediet28 = new BelastingAndCrisisBijdrageResult('Belaste reserves kredietinstellingen tegen 28%',
            $this->calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen28($input),
            $this->crisisBijdrageCalculation('1502b',$this->getNewFilter(),
                $this->calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen28($input)->getBelastingOfCrisisbijdrage(),
                $ACBpercentage));
        $vermeerderingen[] = $krediet28->output();

        $dividenden28 = new BelastingAndCrisisBijdrageResult('Uitgekeerde dividenden tegen 28%',
            $this->calculateAfzonderlijkeAanslagUitgekeerdeDividenden($input),
            $this->crisisBijdrageCalculation('1503',$this->getOldFilter(),
                $this->calculateAfzonderlijkeAanslagUitgekeerdeDividenden($input)->getBelastingOfCrisisbijdrage(),
                $ACBpercentage));
        $vermeerderingen[] = $dividenden28->output();

        $liqRes = new BelastingAndCrisisBijdrageResult('Liquidatiereserve tegen 10%',
            $this->calculateAfzonderlijkeAanslagBoekhoudingNaWinst($input),
            $this->crisisBijdrageCalculation('1508',$this->getNewFilter(),
                $this->calculateAfzonderlijkeAanslagBoekhoudingNaWinst($input)->getBelastingOfCrisisbijdrage(),
                $ACBpercentage));
        $vermeerderingen[] = $liqRes->output();

        $vermogenTegen33 = new BelastingAndCrisisBijdrageResult('Verdeling maatschappelijk vermogen tegen 33%',
            $this->calculateGeheleOfGedeeltelijkMaatschappelijkVermogen33($input),
            $this->crisisBijdrageCalculation('1511',$this->getNewFilter(),
                $this->calculateGeheleOfGedeeltelijkMaatschappelijkVermogen33($input)->getBelastingOfCrisisbijdrage(),
                $ACBpercentage));
        $vermeerderingen[] = $vermogenTegen33->output();

        $vermogenTegen165 = new BelastingAndCrisisBijdrageResult('Verdeling maatschappelijk vermogen tegen 16,5%',
            $this->calculateGeheleOfGedeeltelijkMaatschappelijkVermogen165($input),
            $this->crisisBijdrageCalculation('1512',$this->getNewFilter(),
                $this->calculateGeheleOfGedeeltelijkMaatschappelijkVermogen165($input)->getBelastingOfCrisisbijdrage(),
                $ACBpercentage));
        $vermeerderingen[] = $vermogenTegen165->output();

        $vereffining33 = new BelastingAndCrisisBijdrageResult('Voordelen alle aard bij vereffening tegen 33%',
            $this->calculateVoordeleAlleAardVerleend($input),
            $this->crisisBijdrageCalculation('1513',$this->getNewFilter(),
                $this->calculateVoordeleAlleAardVerleend($input)->getBelastingOfCrisisbijdrage(),
                $ACBpercentage));
        $vermeerderingen[] = $vereffining33->output();

        $latenteMeerwaarde = new BelastingAndCrisisBijdrageResult('Terugbetaling belastingkrediet O&O tegen 100%',
            new TaxationRule('1532',$input->getTerugbetalingTegen100(),1,$input->getTerugbetalingTegen100()),
            new TaxationRule('1532',0,0,0));
        $vermeerderingen[] = $latenteMeerwaarde->output();


        return $this->venbOperationAuxiliariesNew->calculateTotalOfArray($vermeerderingen);

    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateKapitaalenInterestsubsidiesLandboouw05(VenbGeneralInput $input): TaxationRule
    {
        return $this->belastingCalculation('1481' ,$input->getKapitaalenInterestsubsidiesLandboouw05(), 0.05);
    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagAlleAard50(VenbGeneralInput $input): TaxationRule
    {
        return $this->belastingCalculation('1506' ,$input->getAfzonderlijkeAanslagAlleAard50(), 0.5);
    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagAlleAard100(VenbGeneralInput $input): TaxationRule
    {
        return $this->belastingCalculation('1507' , $input->getAfzonderlijkeAanslagAlleAard100(), 1);
    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen34(
        VenbGeneralInput $input
    ): TaxationRule {
        return $this->belastingCalculation('1502a' ,$input->getAfzonderlijkeAanslagBelasteReservesKredietInstellingen34(), 0.34);
    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen28(
        VenbGeneralInput $input
    ): TaxationRule {
        return $this->belastingCalculation('1502b',$input->getAfzonderlijkeAanslagBelasteReservesKredietInstellingen28(), 0.28);
    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagUitgekeerdeDividenden(VenbGeneralInput $input): TaxationRule
    {
        return $this->belastingCalculation('1503',$input->getAfzonderlijkeAanslagUitgekeerdeDividenden(), 0.28);
    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagBoekhoudingNaWinst(VenbGeneralInput $input): TaxationRule
    {
        return $this->belastingCalculation('1508',$input->getAfzonderlijkeAanslagLiqRes10(), 0.1);
    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateGeheleOfGedeeltelijkMaatschappelijkVermogen33(VenbGeneralInput $input): TaxationRule
    {
        return $this->belastingCalculation('1511',$input->getGeheleOfGedeeltelijkMaatschappelijkVermogen33(), 0.33);
    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateGeheleOfGedeeltelijkMaatschappelijkVermogen165(VenbGeneralInput $input): TaxationRule
    {
        return $this->belastingCalculation('1512',$input->getGeheleOfGedeeltelijkMaatschappelijkVermogen165(), 0.165);
    }

    /**
     * @param VenbGeneralInput $input
     * @return TaxationRule
     */
    private function calculateVoordeleAlleAardVerleend(VenbGeneralInput $input): TaxationRule
    {
        return $this->belastingCalculation('1513',$input->getVoordeleAlleAardVerleend(), 0.33);
    }

    protected function getEindTotaal($belastbaarGewoonStelselTotaal,$afzonderlijkeAanslagenTotaal):float
    {
        return abs($belastbaarGewoonStelselTotaal + $afzonderlijkeAanslagenTotaal > -2.5 &&
        $belastbaarGewoonStelselTotaal + $afzonderlijkeAanslagenTotaal < 2.5
            ? 0 : $belastbaarGewoonStelselTotaal + $afzonderlijkeAanslagenTotaal);
    }

    }

<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 27/06/19
 * Time: 14:17
 */

namespace Laudis\Calculators\Venb;

use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Venb\VersieAj2019New\Venb2020NewOperations;
use Laudis\Calculators\Venb\VersieAj2019Old\Venb2019InputOld;

/**
 * Interface VenbOperationInterface
 * @package Laudis\Calculators\Venb
 */
interface VenbOperationInterface
{
    /**
     * @param VenbInput2018 | Venb2019InputOld | Venb2020NewOperations
     * @return CalculationResultInterface
     */
    public function calculate($input) : CalculationResultInterface;
}

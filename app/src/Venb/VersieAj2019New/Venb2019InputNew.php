<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb\VersieAj2019New;

use Laudis\Calculators\Venb\Input\VoorafBetalingenInput;
use Laudis\Calculators\Venb\VenbGeneralInput;

/**
 * Class VenbInput2018
 * @package Laudis\Calculators\Calculators\Input
 */
final class Venb2019InputNew extends VenbGeneralInput
{
    /**
     * @var float
     */
    private $verwezenlijkteMeerwaarde3399;

    /**
     * @var float
     */
    private $positieveCorrectieMeerwaardeAandelen25;
    /**
     * @var float
     */
    private $positieveCorrectieMeerwaardeAandelen04;
    /**
     * @var float
     */
    private $gedeelteMeerwaardeBelastbaar25;
    /**
     * @var float
     */
    private $gedeelteGrondslagExitTaks125;
    /**
     * @var float
     */
    private $gedeelteGecorrigeerdeAanslag33;
    /**
     * @var bool
     */
    private $isCorrectieVerlaagdTarief;

    /**
     * Venb2019InputNew constructor.
     * @param float $belastbaarGewoonTarief
     * @param bool $isAanspraakVerminderdTarief
     * @param bool $isVenootshapHuisvesting
     * @param bool $isBelastbaarTijdPerkTenVroegste01012018
     * @param bool $isWijzigingAfsluitdatum26072017
     * @param float $meerwaardeOpAandelen25
     * @param float $verwezenlijkteMeerwaarde3399
     * @param float $belastbareTegenExitTaks125
     * @param float $kapitaalenInterestsubsidiesLandboouw05
     * @param float $afzonderlijkeAanslagAlleAard50
     * @param float $afzonderlijkeAanslagAlleAard100
     * @param float $afzonderlijkeAanslagBelasteReservesKredietInstellingen34
     * @param float $afzonderlijkeAanslagBelasteReservesKredietInstellingen28
     * @param float $afzonderlijkeAanslagUitgekeerdeDividenden
     * @param float $afzonderlijkeAanslagLiqRes10
     * @param float $geheleOfGedeeltelijkMaatschappelijkVermogen33
     * @param float $geheleOfGedeeltelijkMaatschappelijkVermogen165
     * @param float $voordeleAlleAardVerleend
     * @param bool $isStartendeKMO
     * @param float $bedragLatenteMeerwaardeNaProportioneleVerrekening
     * @param float $nietTerugbetaalbareVoorheffing
     * @param float $buitenlandseBelastingkrediet
     * @param float $terugbetaalbareVoorheffing
     * @param float $belastingkredietVoorOnderzoek
     * @param VoorafBetalingenInput $voorafBetalingenInput
     * @param float $latenteMeerwaarden
     * @param float $negatieveCorrectieGrondslagGewoonTarief
     * @param float $positieveCorrectieGrondslagGewoonTarief
     * @param float $positieveCorrectieMeerwaardeAandelen25
     * @param float $positieveCorrectieMeerwaardeAandelen04
     * @param float $gedeelteMeerwaardeBelastbaar25
     * @param float $gedeelteGrondslagExitTaks125
     * @param float $gedeelteGecorrigeerdeAanslag33
     * @param bool $isCorrectieVerlaagdTarief
     */
    public function __construct(
        float $belastbaarGewoonTarief,
        bool $isAanspraakVerminderdTarief,
        bool $isVenootshapHuisvesting,
        bool $isBelastbaarTijdPerkTenVroegste01012018,
        bool $isWijzigingAfsluitdatum26072017,
        float $meerwaardeOpAandelen25,
        float $verwezenlijkteMeerwaarde3399,
        float $belastbareTegenExitTaks125,
        float $kapitaalenInterestsubsidiesLandboouw05,
        float $afzonderlijkeAanslagAlleAard50,
        float $afzonderlijkeAanslagAlleAard100,
        float $afzonderlijkeAanslagBelasteReservesKredietInstellingen34,
        float $afzonderlijkeAanslagBelasteReservesKredietInstellingen28,
        float $afzonderlijkeAanslagUitgekeerdeDividenden,
        float $afzonderlijkeAanslagLiqRes10,
        float $geheleOfGedeeltelijkMaatschappelijkVermogen33,
        float $geheleOfGedeeltelijkMaatschappelijkVermogen165,
        float $voordeleAlleAardVerleend,
        bool $isStartendeKMO,
        float $bedragLatenteMeerwaardeNaProportioneleVerrekening,
        float $nietTerugbetaalbareVoorheffing,
        float $buitenlandseBelastingkrediet,
        float $terugbetaalbareVoorheffing,
        float $belastingkredietVoorOnderzoek,
        VoorafBetalingenInput $voorafBetalingenInput,
        float $latenteMeerwaarden,
        float $negatieveCorrectieGrondslagGewoonTarief ,
        float $positieveCorrectieGrondslagGewoonTarief ,
        float $positieveCorrectieMeerwaardeAandelen25 ,
        float $positieveCorrectieMeerwaardeAandelen04 ,
        float $gedeelteMeerwaardeBelastbaar25 ,
        float $gedeelteGrondslagExitTaks125 ,
        float $gedeelteGecorrigeerdeAanslag33 ,
        bool $isCorrectieVerlaagdTarief
    )
    {
        parent::__construct(
            $belastbaarGewoonTarief,
            $isAanspraakVerminderdTarief,
            $isVenootshapHuisvesting,
            $isBelastbaarTijdPerkTenVroegste01012018,
            $isWijzigingAfsluitdatum26072017,
            $meerwaardeOpAandelen25,
            $belastbareTegenExitTaks125,
            $kapitaalenInterestsubsidiesLandboouw05,
            $afzonderlijkeAanslagAlleAard50,
            $afzonderlijkeAanslagAlleAard100,
            $afzonderlijkeAanslagBelasteReservesKredietInstellingen34,
            $afzonderlijkeAanslagBelasteReservesKredietInstellingen28,
            $afzonderlijkeAanslagUitgekeerdeDividenden,
            $afzonderlijkeAanslagLiqRes10,
            $geheleOfGedeeltelijkMaatschappelijkVermogen33,
            $geheleOfGedeeltelijkMaatschappelijkVermogen165,
            $voordeleAlleAardVerleend,
            $isStartendeKMO,
            $bedragLatenteMeerwaardeNaProportioneleVerrekening,
            $nietTerugbetaalbareVoorheffing,
            $buitenlandseBelastingkrediet,
            $terugbetaalbareVoorheffing,
            $belastingkredietVoorOnderzoek,
            $voorafBetalingenInput,
            $latenteMeerwaarden,
            $negatieveCorrectieGrondslagGewoonTarief,
            $positieveCorrectieGrondslagGewoonTarief
        );

        $this->verwezenlijkteMeerwaarde3399 = $verwezenlijkteMeerwaarde3399;
        $this->positieveCorrectieMeerwaardeAandelen25 = $positieveCorrectieMeerwaardeAandelen25;
        $this->positieveCorrectieMeerwaardeAandelen04 = $positieveCorrectieMeerwaardeAandelen04;
        $this->gedeelteMeerwaardeBelastbaar25 = $gedeelteMeerwaardeBelastbaar25;
        $this->gedeelteGrondslagExitTaks125 = $gedeelteGrondslagExitTaks125;
        $this->gedeelteGecorrigeerdeAanslag33 = $gedeelteGecorrigeerdeAanslag33;
        $this->isCorrectieVerlaagdTarief = $isCorrectieVerlaagdTarief;
    }

    /**
     * @return float
     */
    public function getVerwezenlijkteMeerwaarde3399(): float
    {
        return $this->verwezenlijkteMeerwaarde3399;
    }


    /**
     * @return float
     */
    public function getPositieveCorrectieMeerwaardeAandelen25(): float
    {
        return $this->positieveCorrectieMeerwaardeAandelen25;
    }

    /**
     * @return float
     */
    public function getPositieveCorrectieMeerwaardeAandelen04(): float
    {
        return $this->positieveCorrectieMeerwaardeAandelen04;
    }

    /**
     * @return float
     */
    public function getGedeelteMeerwaardeBelastbaar25(): float
    {
        return $this->gedeelteMeerwaardeBelastbaar25;
    }

    /**
     * @return float
     */
    public function getGedeelteGrondslagExitTaks125(): float
    {
        return $this->gedeelteGrondslagExitTaks125;
    }

    /**
     * @return float
     */
    public function getGedeelteGecorrigeerdeAanslag33(): float
    {
        return $this->gedeelteGecorrigeerdeAanslag33;
    }

    /**
     * @return bool
     */
    public function isCorrectieVerlaagdTarief(): bool
    {
        return $this->isCorrectieVerlaagdTarief;
    }



}


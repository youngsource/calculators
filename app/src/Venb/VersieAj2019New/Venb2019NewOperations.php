<?php

/** @noinspection DuplicatedCode */
declare(strict_types=1);
/**
 * Venb version 2019New
 */

namespace Laudis\Calculators\Venb\VersieAj2019New;

use Exception;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Venb\BasicCalculationResult;
use Laudis\Calculators\Venb\Results\BelastbaartariefResult;
use Laudis\Calculators\Venb\Results\BelastingAndCrisisBijdrageResult;
use Laudis\Calculators\Venb\Results\GeenVermeerderingIndienLagerResult;
use Laudis\Calculators\Venb\Results\GrondSlagCalculationResult;
use Laudis\Calculators\Venb\Results\ToegepasteCorrectiesNieuwResult;
use Laudis\Calculators\Venb\Results\VenbAanslagenOutput;
use Laudis\Calculators\Venb\Results\VenbBerekeningVermeerderingOutput;
use Laudis\Calculators\Venb\Results\VenbVoorafbetalingenOutput;
use Laudis\Calculators\Venb\TaxationRule;
use Laudis\Calculators\Venb\VenbGeneralOperations;
use Laudis\Calculators\Venb\VenbOperationInterface;

/**
 *
 * @package Laudis\Calculators\Calculators\Calculators
 */
final class Venb2019NewOperations extends VenbGeneralOperations implements VenbOperationInterface
{
    /**
     * @param Venb2019InputNew $input
     * @return CalculationResultInterface
     * @throws Exception
     *
     * main calculate method that constructs a calculationresult
     */
    public function calculate($input): CalculationResultInterface
    {
        /** calculate belastbaarGewoonStelsel */
        $belastbaarGewoonStelsel = $this->belastbaarGewoonStelsel(
            $input,
            $this->gewoneAanslagen($input)->getBelastingTotal(),
            $this->outputBerekeningVermeerdering($input)->getWeerhoudenVermeerdering()
        );
        /** calculate afzonderlijke aanslagen using new ACB (2%) */
        $afzonderlijkeAanslagen = $this->afzonderlijkeAanslagen($input, self::ACBNew);

        return new BasicCalculationResult(
            [
                'gewoneAanslagen' => $this->gewoneAanslagen($input)->getVermeerderingen(),
                'totaalGewoneAanslagen' => $this->gewoneAanslagen($input)->getBelastingTotal(),
                'weerhoudenVermeerdering' => $this->outputBerekeningVermeerdering($input)->getWeerhoudenVermeerdering(),
                'eindTotaal' => $this->getEindTotaal(
                    $belastbaarGewoonStelsel->getTotaal(),
                    $afzonderlijkeAanslagen->getBelastingTotal()
                ),
                'belastbaarGewoonStelsel' => $belastbaarGewoonStelsel->output(),
                'afzonderlijkBelastbaar' => $afzonderlijkeAanslagen->getVermeerderingen(),
                'totaalDoorUTeBetalen' => $belastbaarGewoonStelsel->getTotaal()
                    + $afzonderlijkeAanslagen->getBelastingTotal(),
                'berekeningVanDeVermeerdering' => $this->outputBerekeningVermeerdering($input)->output(),
                'geenVemeerdering' => $this->geenVermeerderingIndienResult($input)->output(),
                'toegepasteCorrecties' => $this->toegepasteCorrecties($input)->output()
            ]
        );
    }

    private function toegepasteCorrecties(Venb2019InputNew $input): ToegepasteCorrectiesNieuwResult
    {
        /** apply correcties */
        $belastingOud = $input->getGedeelteGecorrigeerdeAanslag33();
        $belastingNieuw = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
            + $input->getPositieveCorrectieGrondslagGewoonTarief()
            - $input->getGedeelteGecorrigeerdeAanslag33();

        return new ToegepasteCorrectiesNieuwResult(
            $input->getBelastbaarGewoonTarief(),
            $input->getNegatieveCorrectieGrondslagGewoonTarief(),
            $input->getPositieveCorrectieGrondslagGewoonTarief(),
            $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief(
            ) + $input->getPositieveCorrectieGrondslagGewoonTarief(),
            $input->getGedeelteGecorrigeerdeAanslag33(),
            $belastingNieuw,
            $belastingOud,
            $input->getMeerwaardeOpAandelen25(),
            $input->getPositieveCorrectieMeerwaardeAandelen25(),
            $input->getMeerwaardeOpAandelen25() + $input->getPositieveCorrectieMeerwaardeAandelen25(),
            $input->getGedeelteMeerwaardeBelastbaar25(),
            $this->calculateMeerwaardeOpAandelen25($input)->getAmount(),
            $this->calculateMeerwaardeOpAandelen25Korting($input)->getAmount(),
            $this->calculateCorrectieMeerwaardeOpAandelen25($input)->getAmount(),
            $input->getPositieveCorrectieMeerwaardeAandelen04(),
            $this->calculateMeerWaardeOpAandelen04($input)->getAmount(),
            $input->getBelastbareTegenExitTaks125(),
            $input->getGedeelteGrondslagExitTaks125(),
            $this->calculateBelastbaarTegenExitTaks125($input)->getAmount(),
            $this->calculateCorrectieBelastbaarTegenExitTaks125($input)->getAmount()
        );
    }

    /**
     * @param Venb2019InputNew $input
     * @return VenbAanslagenOutput
     *
     * calculate gewoneAanslagen
     */
    private function gewoneAanslagen(Venb2019InputNew $input): VenbAanslagenOutput
    {
        $vermeerderingen = [];

        $vermeerderingen = $this->addBelastbaarInkomen($input, $vermeerderingen);
        $vermeerderingen = $this->addCorrectieBelastbaarInkomen($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1466($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1466MetKorting($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1465($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1467($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1472($input, $vermeerderingen);
        $vermeerderingen = $this->addCorrectie1472($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1424($input, $vermeerderingen);

        /** return a VenbAanslagenOutput object using auxiliaries funciton calculateTotalOfArray */
        return $this->venbOperationAuxiliariesNew->calculateTotalOfArray($vermeerderingen);
    }

    /**
     * @param Venb2019InputNew $input
     * @return BelastbaartariefResult
     *
     * calculate belastbaarInkomen
     */
    private function calculateBelastbaarInkomen(Venb2019InputNew $input): BelastbaartariefResult
    {
        $amount = $input->getBelastbaarGewoonTarief();
        /** check if isWijzigingAfsluitdatum26072017 (correctie)*/
        if ($input->isWijzigingAfsluitdatum26072017()) {
            /** apply correctie */
            $amount = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
                + $input->getPositieveCorrectieGrondslagGewoonTarief()
                - $input->getGedeelteGecorrigeerdeAanslag33();
        }

        if (!$input->isVenootshapHuisvesting()) {
            if ($input->isAanspraakVerminderdTarief()) {
                /** barema nieuw tarief */
                $belasting = $this->venbOperationAuxiliariesNew->presentBarema($amount);
                $name = 'Verminderd tarief';
                $data = ['code' => 'N 1460', 'belasting' => $belasting];
            } else {
                /** 29% tarief */
                $belasting = $amount * 0.29;
                $name = 'Gewoon tarief';
                $percentage = '29,00%';
                $data = [
                    'code' => 'N 1460',
                    'bedrag' => $amount,
                    'operation' => '&times',
                    'tarief' => $percentage,
                    'is' => '=',
                    'belasting' => $belasting,
                ];
            }
        /** Tarief huisvesting */
        } else {
            $belasting = $amount * 0.05;
            $name = 'Tarief huisvesting tegen 5%';
            $percentage = '5%';
            $data = [
                'code' => 'N 1460',
                'bedrag' => $amount,
                'operation' => '&times',
                'tarief' => $percentage,
                'is' => '=',
                'belasting' => $belasting,
            ];
        }

        return new BelastbaartariefResult($name, $data);
    }

    /**
     * @param Venb2019InputNew $input
     * @return TaxationRule
     *
     * calculate the crisisbijdrage of belastbaarInkomen
     */
    private function calculateBelastbaarCrisisBijdrage(Venb2019InputNew $input): TaxationRule
    {
        $amount= 0;
        $belastingEnCrisisBijdrage = 0;
        /** check if crisisbijdrage */
        if ($this->venbOperationAuxiliariesNew->getFilter()->isCrisisbijdrage('1460')) {
            /** calculate the crisibijdrage */
            $belastingEnCrisisBijdrage = $this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen(
                    $this->calculateBelastbaarInkomen($input)) * 0.02;

            /** calculate the belasting amount */
            $amount = $this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen(
                $this->calculateBelastbaarInkomen($input)
            );
        }
        /** return the result as taxationrule */
        return new TaxationRule('N 1460', $amount, 0.02, $belastingEnCrisisBijdrage);
    }

    /**
     * @param Venb2019InputNew $input
     * @return BelastbaartariefResult
     *
     * calculate the correctie belastbaarInkomen | see 2018 documentation
     */
    private function calculateCorrectieBelastbaarInkomen(Venb2019InputNew $input): BelastbaartariefResult
    {
        $name = 'Gewoon tarief';
        $data =
            [
                'code' => 'O 1460',
                'bedrag' => 0,
                'operation' => '&times',
                'tarief' => '33,00%',
                'is' => '=',
                'belasting' => 0,
            ];

        if ($input->isWijzigingAfsluitdatum26072017()) {
            if ($input->isVenootshapHuisvesting()) {
                return $this->tariefHuisVestingCorrectie($input);
            }
            if ($input->isCorrectieVerlaagdTarief() && !$input->isVenootshapHuisvesting(
                ) && $input->getGedeelteGecorrigeerdeAanslag33() <= 322500) {
                return $this->verminderdtariefCorrectie($input);
            }

            return $this->gewoonTariefCorrectie($input);
        }
        return new BelastbaartariefResult($name, $data);
    }

    public function tariefHuisVestingCorrectie(Venb2019InputNew $input): BelastbaartariefResult
    {
        $belasting = $input->getGedeelteGecorrigeerdeAanslag33() * 0.05;
        $name = 'Tarief huisvesting tegen 5%';
        $data =
            [
                'code' => 'O 1460',
                'bedrag' => $input->getGedeelteGecorrigeerdeAanslag33(),
                'operation' => '&times',
                'tarief' => '5,00%',
                'is' => '=',
                'belasting' => $belasting,
            ];

        return new BelastbaartariefResult($name, $data);
    }

    public function gewoonTariefCorrectie(Venb2019InputNew $input): BelastbaartariefResult
    {
        $belasting = $input->getGedeelteGecorrigeerdeAanslag33() * 0.33;
        $name = 'Gewoon tarief';
        $data =
            [
                'code' => 'O 1460',
                'bedrag' => $input->getGedeelteGecorrigeerdeAanslag33(),
                'operation' => '&times',
                'tarief' => '33,00%',
                'is' => '=',
                'belasting' => $belasting,
            ];
        return new BelastbaartariefResult($name, $data);
    }

    public function verminderdtariefCorrectie(Venb2019InputNew $input): BelastbaartariefResult
    {
        $belasting = $this->venbOperationAuxiliariesOld->presentBarema($input->getGedeelteGecorrigeerdeAanslag33());
        $name = 'Verminderd tarief';
        $data = ['code' => 'O 1460', 'belasting' => $belasting];

        return new BelastbaartariefResult($name, $data);
    }

    /**
     * @param $input
     * @return TaxationRule
     *
     * calculate the crisisbijdrage of the correctie
     */
    private function calculateCrisisbijdrageCorrectieBelastbaarInkomen(Venb2019InputNew $input): TaxationRule
    {
        $belastingEnCrisisBijdrage = 0;
        $amount = 0;

        /** check if crisisbijdrage */
        if ($this->venbOperationAuxiliariesOld->getFilter()->isCrisisbijdrage('1460')) {
            $amount = $this->venbOperationAuxiliariesOld->getAmountBelastbaarInkomen(
                $this->calculateCorrectieBelastbaarInkomen($input));
            /** apply crisisbijdrage */
            $belastingEnCrisisBijdrage = $amount * self::ACBOld;
        }
        return new TaxationRule('O 1460', $amount, self::ACBOld, $belastingEnCrisisBijdrage);
    }

    /**
     * @param Venb2019InputNew $input
     * @return TaxationRule
     */
    private function calculateMeerwaardeOpAandelen25(Venb2019InputNew $input): TaxationRule
    {
        $amount = $input->getMeerwaardeOpAandelen25() - $this->calculateMeerwaardeOpAandelen25Korting(
                $input
            )->getAmount();
        if ($input->isWijzigingAfsluitdatum26072017()) {
            $amount -= $input->getGedeelteMeerwaardeBelastbaar25();
        }

        return $this->belastingCalculation('N 1466', $amount, 0.25);
    }

    /**
     * @param Venb2019InputNew $input
     * @return TaxationRule
     */
    private function calculateMeerwaardeOpAandelen25Korting(Venb2019InputNew $input)
    {
        $total = $input->getBelastbaarGewoonTarief();

        if ($input->isWijzigingAfsluitdatum26072017()) {
            $total = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
                + $input->getPositieveCorrectieGrondslagGewoonTarief()
                - $input->getGedeelteGecorrigeerdeAanslag33();
        }

        $tmp = 100000 - $total;
        $amount = $input->getMeerwaardeOpAandelen25() - $input->getGedeelteMeerwaardeBelastbaar25();

        if ($amount >= $tmp) {
            $amount = $tmp;
        }

        if ($total <= 100000 && $input->isAanspraakVerminderdTarief()) {
            return $this->belastingCalculation('N 1466', $amount, 0.20);
        }

        return $this->belastingCalculation('N 1466', 0, 0.20);
    }

    /**
     * @param Venb2019InputNew $input
     * @return TaxationRule
     */
    private function calculateCorrectieMeerwaardeOpAandelen25(Venb2019InputNew $input): TaxationRule
    {
        if ($input->isWijzigingAfsluitdatum26072017()) {
            return $this->belastingCalculation(
                'O 1465',
                $input->getGedeelteMeerwaardeBelastbaar25() + $input->getPositieveCorrectieMeerwaardeAandelen25()
                ,
                0.25
            );
        }
        return $this->belastingCalculation('O 1465', 0, 0.25);
    }

    /**
     * @param Venb2019InputNew $input
     * @return TaxationRule
     */
    private function calculateVerwezenlijkteMeerwaarde(Venb2019InputNew $input): TaxationRule
    {
        return $this->belastingCalculation('N 1467', $input->getVerwezenlijkteMeerwaarde3399(), 0.3399);
    }

    /**
     * @param Venb2019InputNew $input
     * @return TaxationRule
     */
    private function calculateBelastbaarTegenExitTaks125(Venb2019InputNew $input): TaxationRule
    {
        $amount = $input->getBelastbareTegenExitTaks125();
        if ($input->isWijzigingAfsluitdatum26072017()) {
            $amount -= $input->getGedeelteGrondslagExitTaks125();
        }
        return $this->belastingCalculation('N 1472', $amount, 0.125);
    }

    /**
     * @param Venb2019InputNew $input
     * @return TaxationRule
     */
    private function calculateCorrectieBelastbaarTegenExitTaks125(Venb2019InputNew $input): TaxationRule
    {
        if ($input->isWijzigingAfsluitdatum26072017()) {
            return $this->belastingCalculation('O 1472', $input->getGedeelteGrondslagExitTaks125(), 0.125);
        }
        return $this->belastingCalculation('O 1472', 0, 0.125);
    }

    /**
     * @param Venb2019InputNew $input
     * @return TaxationRule
     */
    private function calculateMeerWaardeOpAandelen04(Venb2019InputNew $input): TaxationRule
    {
        $amount = 0;
        if ($input->isWijzigingAfsluitdatum26072017()) {
            $amount = $input->getPositieveCorrectieMeerwaardeAandelen04();
        }
        return $this->belastingCalculation('O 1424', $amount, 0.004);
    }

    /**
     * @param Venb2019InputNew $input
     * @return VenbBerekeningVermeerderingOutput
     * @throws Exception
     */
    private function outputBerekeningVermeerdering(Venb2019InputNew $input): VenbBerekeningVermeerderingOutput
    {
        if ($input->isStartendeKMO()) {
            return new VenbBerekeningVermeerderingOutput(
                0,
                0,
                0,
                0,
                0,
                new GrondSlagCalculationResult(0, 0, 0),
                new VenbVoorafbetalingenOutput([], 0),
                0,
                0,
                0,
                0,
                0
            );
        }
        return new VenbBerekeningVermeerderingOutput(
            $this->calculateGrondslagVA($input),
            -$input->getNietterugbetaalbareVoorheffing(),
            -$input->getBuitenlandseBelastingkrediet(),
            -$input->getTerugbetaalbareVoorheffing(),
            -$input->getBelastingkredietOndOntw(),
            new GrondSlagCalculationResult(
                $this->grondslagVoorVermeerding($input),
                $this->venbOperationAuxiliariesNew->indexedValue($input->getAantalkwartalen() . 'kwart'),
                $this->calculateVermeerdering($input)
            ),
            $this->venbOperationAuxiliariesNew->voorafBetalingen($input->getVoorafBetalingenInput()),
            $this->saldoVermeerdering($input),
            $this->beperkingTot80(),
            $this->beperkingTotGrondVerm($input),
            $this->geenVermeerderingIndien($input),
            $this->weerhoudenVermeerdering($input)
        );
    }

    /**
     * @param Venb2019InputNew $input
     * @return GeenVermeerderingIndienLagerResult
     * @throws Exception
     */
    private function geenVermeerderingIndienResult(Venb2019InputNew $input): GeenVermeerderingIndienLagerResult
    {
        if ($input->isStartendeKMO()) {
            return new GeenVermeerderingIndienLagerResult(
                0,
                0,
                0
            );
        }
        return new GeenVermeerderingIndienLagerResult(
            $this->beperkingTot80(),
            $this->beperkingTotGrondVerm($input),
            $this->weerhoudenVermeerdering($input)
        );
    }

    /**
     * @param Venb2019InputNew $input
     * @return float
     *
     * calculate grondslagVA , consider the corrections
     */
    private function calculateGrondslagVA(Venb2019InputNew $input): float
    {
        $amountGrondslagNieuw = $input->getBelastbaarGewoonTarief()
            - $input->getNegatieveCorrectieGrondslagGewoonTarief()
            + $input->getPositieveCorrectieGrondslagGewoonTarief()
            - $input->getGedeelteGecorrigeerdeAanslag33();

        if ($amountGrondslagNieuw + $input->getGedeelteGecorrigeerdeAanslag33() === 0.00) {
            return 0;
        }

        return -$input->getLatenteMeerwaarden() *
            (($this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateBelastbaarInkomen($input))
                    + $this->calculateBelastbaarCrisisBijdrage($input)->getBelastingOfCrisisbijdrage()
                    + $this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen(
                        $this->calculateCorrectieBelastbaarInkomen($input)
                    )
                    + $this->calculateCrisisbijdrageCorrectieBelastbaarInkomen($input)->getBelastingOfCrisisbijdrage())
                / ($amountGrondslagNieuw + $input->getGedeelteGecorrigeerdeAanslag33()));
    }


    /**
     * @param Venb2019InputNew $input
     * @return float
     */
    private function grondslagVoorVermeerding(Venb2019InputNew $input): float
    {
        return max(
            array_sum(
                [
                    $this->gewoneAanslagen($input)->getBelastingTotal(),
                    $this->calculateGrondslagVA($input),
                    -$input->getNietTerugbetaalbareVoorheffing(),
                    -$input->getBuitenlandseBelastingkrediet(),
                    -$input->getTerugbetaalbareVoorheffing(),
                    -$input->getBelastingkredietOndOntw()
                ]
            ),
            0
        );
    }

    /**
     * @param Venb2019InputNew $input
     * @return float|int
     * @throws Exception
     */
    private function calculateVermeerdering(Venb2019InputNew $input)
    {
        return $this->grondslagVoorVermeerding($input) * $this->venbOperationAuxiliariesNew->indexedValue(
                $input->getAantalKwartalen() . 'kwart'
            );
    }

    /**
     * @param Venb2019InputNew $input
     * @return float
     * @throws Exception
     */
    private function saldoVermeerdering(Venb2019InputNew $input): float
    {
        return max(
            [
                $this->venbOperationAuxiliariesNew->voorafBetalingen($input->getVoorafBetalingenInput())->getTotal(
                ) + $this->calculateVermeerdering($input),
                0
            ]
        );
    }

    /**
     * @return float
     * @throws Exception
     */
    private function beperkingTot80(): float
    {
        return $this->venbOperationAuxiliariesNew->indexedValue('geen verhoging');
    }

    /**
     * @param Venb2019InputNew $input
     * @return float
     * @throws Exception
     */
    private function beperkingTotGrondVerm(Venb2019InputNew $input): float
    {
        return $this->grondslagVoorVermeerding($input) * $this->venbOperationAuxiliariesNew->indexedValue('pct');
    }

    /**
     * @param Venb2019InputNew $input
     * @return mixed
     * @throws Exception
     */
    private function geenVermeerderingIndien(Venb2019InputNew $input)
    {
        return max($this->beperkingTot80(), $this->beperkingTotGrondVerm($input));
    }

    /**
     * @param Venb2019InputNew $input
     * @return float|int
     * @throws Exception
     */
    private function weerhoudenVermeerdering(Venb2019InputNew $input)
    {
        if ($this->saldoVermeerdering($input) < $this->geenVermeerderingIndien($input)) {
            return 0;
        }
        return $this->saldoVermeerdering($input);
    }

    /**
     * @param Venb2019InputNew $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1465(Venb2019InputNew $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen25 = new BelastingAndCrisisBijdrageResult(
            'Correctie Meerwaarden op aandelen tegen 25%',
            $this->calculateCorrectieMeerwaardeOpAandelen25($input),
            $this->crisisBijdrageCalculation(
                '1465',
                $this->getOldFilter(),
                $this->calculateCorrectieMeerwaardeOpAandelen25($input)->getBelastingOfCrisisbijdrage(),
                self::ACBOld,
                true
            )
        );
        $vermeerderingen[] = $meerwaardeAandelen25->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputNew $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1466MetKorting(Venb2019InputNew $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen25Korting = new BelastingAndCrisisBijdrageResult(
            'Meerwaarden op aandelen tegen 20%',
            $this->calculateMeerwaardeOpAandelen25Korting($input),
            $this->crisisBijdrageCalculation(
                '1466',
                $this->getNewFilter(),
                $this->calculateMeerwaardeOpAandelen25Korting($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew,
                true
            )
        );
        $vermeerderingen[] = $meerwaardeAandelen25Korting->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputNew $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1466(Venb2019InputNew $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen25 = new BelastingAndCrisisBijdrageResult(
            'Meerwaarden op aandelen tegen 25%',
            $this->calculateMeerwaardeOpAandelen25($input),
            $this->crisisBijdrageCalculation(
                '1466',
                $this->getNewFilter(),
                $this->calculateMeerwaardeOpAandelen25($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew,
                true
            )
        );
        $vermeerderingen[] = $meerwaardeAandelen25->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputNew $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addBelastbaarInkomen(Venb2019InputNew $input, array $vermeerderingen): array
    {
        $vermeerderingen[] = [
            $this->calculateBelastbaarInkomen($input)->getName() => $this->calculateBelastbaarInkomen($input)->getData(
            ),
            'Crisisbijdrage' => $this->calculateBelastbaarCrisisBijdrage($input)->output()
        ];
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputNew $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCorrectieBelastbaarInkomen(Venb2019InputNew $input, array $vermeerderingen): array
    {
        $vermeerderingen[] = [
            $this->calculateCorrectieBelastbaarInkomen($input)->getName() => $this->calculateCorrectieBelastbaarInkomen(
                $input
            )->getData(),
            'Crisisbijdrage' => $this->calculateCrisisbijdrageCorrectieBelastbaarInkomen($input)->output()
        ];
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputNew $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1467(Venb2019InputNew $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen3399 = new BelastingAndCrisisBijdrageResult(
            'Meerwaarden, voorzieningen,... tegen 33,99%',
            $this->calculateVerwezenlijkteMeerwaarde($input),
            $this->crisisBijdrageCalculation(
                '1467',
                $this->getNewFilter(),
                $this->calculateVerwezenlijkteMeerwaarde($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew,
                true
            )
        );
        $vermeerderingen[] = $meerwaardeAandelen3399->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputNew $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1472(Venb2019InputNew $input, array $vermeerderingen): array
    {
        $exitTax125 = new BelastingAndCrisisBijdrageResult(
            'Belastbaar tegen exit tax 12,5%',
            $this->calculateBelastbaarTegenExitTaks125($input),
            $this->crisisBijdrageCalculation(
                '1472',
                $this->getNewFilter(),
                $this->calculateBelastbaarTegenExitTaks125($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew,
                true
            )
        );
        $vermeerderingen[] = $exitTax125->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputNew $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCorrectie1472(Venb2019InputNew $input, array $vermeerderingen): array
    {
        $corrExitTax125 = new BelastingAndCrisisBijdrageResult(
            'Correctie belastbaar tegen exit tax 12,5%',
            $this->calculateCorrectieBelastbaarTegenExitTaks125($input),
            $this->crisisBijdrageCalculation(
                '1472',
                $this->getOldFilter(),
                $this->calculateCorrectieBelastbaarTegenExitTaks125($input)->getBelastingOfCrisisbijdrage(),
                0.03,
                true
            )
        );
        $vermeerderingen[] = $corrExitTax125->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputNew $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1424(Venb2019InputNew $input, array $vermeerderingen): array
    {
        $meerwaardeOpAandelen04 = new BelastingAndCrisisBijdrageResult(
            'Meerwaarde op aandelen 0,4%',
            $this->calculateMeerWaardeOpAandelen04($input),
            $this->crisisBijdrageCalculation(
                '1424',
                $this->getOldFilter(),
                $this->calculateMeerWaardeOpAandelen04($input)->getBelastingOfCrisisbijdrage(),
                self::ACBOld,
                true
            )
        );
        $vermeerderingen[] = $meerwaardeOpAandelen04->output();
        return $vermeerderingen;
    }
}

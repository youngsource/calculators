<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb;

/**
 * Class VenbResult
 * @package Laudis\Calculators\Calculators\Results
 */
final class TaxationRule
{
    /**
     * @var string
     */
    private $number;
    /**
     * @var float
     */
    private $amount;
    /**
     * @var float
     */
    private $tarief;
    /**
     * @var float
     */
    private $belastingOfCrisisbijdrage;

    /**
     * TaxationRule constructor.
     * @param string $number
     * @param float $amount
     * @param float $tarief
     * @param float $belastingEnCrisisbijdrage
     */
    public function __construct(string $number, float $amount, float $tarief, float $belastingEnCrisisbijdrage)
    {
        $this->number = $number;
        $this->amount = $amount;
        $this->tarief = $tarief;
        $this->belastingOfCrisisbijdrage = $belastingEnCrisisbijdrage;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @return mixed
     */
    public function getTarief()
    {
        return $this->tarief;
    }

    /**
     * @return float
     */
    public function getBelastingOfCrisisbijdrage(): float
    {
        return $this->belastingOfCrisisbijdrage;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @param float $belastingOfCrisisbijdrage
     */
    public function setBelastingOfCrisisbijdrage(float $belastingOfCrisisbijdrage): void
    {
        $this->belastingOfCrisisbijdrage = $belastingOfCrisisbijdrage;
    }



    /**
     * @return array
     */
    public function output(): array
    {
        return [
            'code' => $this->getNumber(),
            'bedrag' => $this->getAmount(),
            'operation' => '&times;',
            'tarief' => number_format($this->getTarief()*100,2, ',', ' ') . '%',
            'is' => '=',
            'belasting' => $this->getBelastingOfCrisisbijdrage()
        ];
    }


}

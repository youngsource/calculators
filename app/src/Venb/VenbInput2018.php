<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb;

use Laudis\Calculators\Venb\Input\VoorafBetalingenInput;

/**
 * Class VenbInput2018
 * @package Laudis\Calculators\Calculators\Input
 */
final class VenbInput2018
{
    /**
     * @var float
     */
    private $belastbaarGewoonTarief;
    /**
     * @var bool
     */
    private $isAanspraakVerminderdTarief;
    /**
     * @var bool
     */
    private $isVenootshapHuisvesting;
    /**
     * @var float
     */
    private $meerwaardeOpAandelen25;
    /**
     * @var float
     */
    private $belastbareTegenExitTaks165;
    /**
     * @var float
     */
    private $belastbareTegenExitTaks125;
    /**
     * @var float
     */
    private $meerwaardeOpAAndelenBelastbaartegen04;
    /**
     * @var float
     */
    private $kapitaalenInterestsubsidiesLandboouw05;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagAlleAard50;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagAlleAard100;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagUitgekeerdeDividenden;
    /**
     * @var float
     */
    private $fairnessTax;
    /**
     * @var float
     */
    private $afzonderlijkeAanslagBoekhoudingNaWinst;
    /**
     * @var float
     */
    private $geheleOfGedeeltelijkMaatschappelijkVermogen33;
    /**
     * @var float
     */
    private $geheleOfGedeeltelijkMaatschappelijkVermogen165;
    /**
     * @var float
     */
    private $voordeleAlleAardVerleend;
    /**
     * @var bool
     */
    private $isStartendKMO;
    /**
     * @var float
     */
    private $terugbetalingTegen100;
    /**
     * @var float
     */
    private $nietterugbetaalbareVoorheffing;
    /**
     * @var float
     */
    private $buitenlandseBelastingkrediet;
    /**
     * @var float
     */
    private $terugbetaalbareVoorheffing;
    /**
     * @var float
     */
    private $belastingkredietOndOntw;
    /** @var VoorafBetalingenInput */
    private $voorafBetalingenInput;
    /**
     * @var float
     */
    private $latenteMeerwaarden;


    /**
     * VenbInput2018 constructor.
     * @param float $belastbaarGewoonTarief
     * @param bool $isAanspraakVerminderdTarief
     * @param bool $isVenootshapHuisvesting
     * @param float $meerwaardeOpAandelen25
     * @param float $belastbareTegenExitTaks165
     * @param float $belastbareTegenExitTaks125
     * @param float $meerwaardeOpAAndelenBelastbaartegen04
     * @param float $kapitaalenInterestsubsidiesLandboouw05
     * @param float $afzonderlijkeAanslagAlleAard50
     * @param float $afzonderlijkeAanslagAlleAard100
     * @param float $afzonderlijkeAanslagBelasteReservesKredietInstellingen34
     * @param float $afzonderlijkeAanslagBelasteReservesKredietInstellingen28
     * @param float $afzonderlijkeAanslagUitgekeerdeDividenden
     * @param float $fairnessTax
     * @param float $afzonderlijkeAanslagBoekhoudingNaWinst
     * @param float $geheleOfGedeeltelijkMaatschappelijkVermogen33
     * @param float $geheleOfGedeeltelijkMaatschappelijkVermogen165
     * @param float $voordeleAlleAardVerleend
     * @param bool $isStartendKMO
     * @param float $terugbetalingTegen100
     * @param float $nietterugbetaalbareVoorheffing
     * @param float $buitenlandseBelastingkrediet
     * @param float $terugbetaalbareVoorheffing
     * @param float $belastingkredietOndOntw
     * @param VoorafBetalingenInput $voorafBetalingenInput
     * @param float $latenteMeerwaarden
     */
    public function __construct(
        float $belastbaarGewoonTarief,
        bool $isAanspraakVerminderdTarief,
        bool $isVenootshapHuisvesting,
        float $meerwaardeOpAandelen25,
        float $belastbareTegenExitTaks165,
        float $belastbareTegenExitTaks125,
        float $meerwaardeOpAAndelenBelastbaartegen04,
        float $kapitaalenInterestsubsidiesLandboouw05,
        float $afzonderlijkeAanslagAlleAard50,
        float $afzonderlijkeAanslagAlleAard100,
        float $afzonderlijkeAanslagBelasteReservesKredietInstellingen34,
        float $afzonderlijkeAanslagBelasteReservesKredietInstellingen28,
        float $afzonderlijkeAanslagUitgekeerdeDividenden,
        float $fairnessTax,
        float $afzonderlijkeAanslagBoekhoudingNaWinst,
        float $geheleOfGedeeltelijkMaatschappelijkVermogen33,
        float $geheleOfGedeeltelijkMaatschappelijkVermogen165,
        float $voordeleAlleAardVerleend,
        bool $isStartendKMO,
        float $terugbetalingTegen100,
        float $nietterugbetaalbareVoorheffing,
        float $buitenlandseBelastingkrediet,
        float $terugbetaalbareVoorheffing,
        float $belastingkredietOndOntw,
        VoorafBetalingenInput $voorafBetalingenInput,
        float $latenteMeerwaarden
    ) {
        $this->belastbaarGewoonTarief = $belastbaarGewoonTarief;
        $this->isAanspraakVerminderdTarief = $isAanspraakVerminderdTarief;
        $this->isVenootshapHuisvesting = $isVenootshapHuisvesting;
        $this->meerwaardeOpAandelen25 = $meerwaardeOpAandelen25;
        $this->belastbareTegenExitTaks165 = $belastbareTegenExitTaks165;
        $this->belastbareTegenExitTaks125 = $belastbareTegenExitTaks125;
        $this->meerwaardeOpAAndelenBelastbaartegen04 = $meerwaardeOpAAndelenBelastbaartegen04;
        $this->kapitaalenInterestsubsidiesLandboouw05 = $kapitaalenInterestsubsidiesLandboouw05;
        $this->afzonderlijkeAanslagAlleAard50 = $afzonderlijkeAanslagAlleAard50;
        $this->afzonderlijkeAanslagAlleAard100 = $afzonderlijkeAanslagAlleAard100;
        $this->afzonderlijkeAanslagBelasteReservesKredietInstellingen34 = $afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
        $this->afzonderlijkeAanslagBelasteReservesKredietInstellingen28 = $afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
        $this->afzonderlijkeAanslagUitgekeerdeDividenden = $afzonderlijkeAanslagUitgekeerdeDividenden;
        $this->fairnessTax = $fairnessTax;
        $this->afzonderlijkeAanslagBoekhoudingNaWinst = $afzonderlijkeAanslagBoekhoudingNaWinst;
        $this->geheleOfGedeeltelijkMaatschappelijkVermogen33 = $geheleOfGedeeltelijkMaatschappelijkVermogen33;
        $this->geheleOfGedeeltelijkMaatschappelijkVermogen165 = $geheleOfGedeeltelijkMaatschappelijkVermogen165;
        $this->voordeleAlleAardVerleend = $voordeleAlleAardVerleend;
        $this->isStartendKMO = $isStartendKMO;
        $this->terugbetalingTegen100 = $terugbetalingTegen100;
        $this->nietterugbetaalbareVoorheffing = $nietterugbetaalbareVoorheffing;
        $this->buitenlandseBelastingkrediet = $buitenlandseBelastingkrediet;
        $this->terugbetaalbareVoorheffing = $terugbetaalbareVoorheffing;
        $this->belastingkredietOndOntw = $belastingkredietOndOntw;
        $this->voorafBetalingenInput = $voorafBetalingenInput;
        $this->latenteMeerwaarden = $latenteMeerwaarden;
    }

    //getters

    /**
     * @return bool
     */
    public function isStartendKMO(): bool
    {
        return $this->isStartendKMO;
    }

    /**
     * @return float
     */
    public function getBelastbaarGewoonTarief(): float
    {
        return $this->belastbaarGewoonTarief;
    }

    /**
     * @return bool
     */
    public function isAanspraakVerminderdTarief(): bool
    {
        return $this->isAanspraakVerminderdTarief;
    }

    /**
     * @return int
     */
    public function getAantalkwartalen(): int
    {
        return $this->voorafBetalingenInput->getAantalKwartalen();
    }

    /**
     * @return bool
     */
    public function getVenootshapHuisvesting(): bool
    {
        return $this->isVenootshapHuisvesting;
    }

    /**
     * @return float
     */
    public function getMeerwaardeOpAandelen25(): float
    {
        return $this->meerwaardeOpAandelen25;
    }

    /**
     * @return float
     */
    public function getBelastbareTegenExitTaks165(): float
    {
        return $this->belastbareTegenExitTaks165;
    }

    /**
     * @return float
     */
    public function getBelastbareTegenExitTaks125(): float
    {
        return $this->belastbareTegenExitTaks125;
    }

    /**
     * @return float
     */
    public function getMeerwaardeOpAAndelenBelastbaartegen04(): float
    {
        return $this->meerwaardeOpAAndelenBelastbaartegen04;
    }

    /**
     * @return float
     */
    public function getKapitaalenInterestsubsidiesLandboouw05(): float
    {
        return $this->kapitaalenInterestsubsidiesLandboouw05;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagAlleAard50(): float
    {
        return $this->afzonderlijkeAanslagAlleAard50;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagAlleAard100(): float
    {
        return $this->afzonderlijkeAanslagAlleAard100;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagBelasteReservesKredietInstellingen34(): float
    {
        return $this->afzonderlijkeAanslagBelasteReservesKredietInstellingen34;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagBelasteReservesKredietInstellingen28(): float
    {
        return $this->afzonderlijkeAanslagBelasteReservesKredietInstellingen28;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagUitgekeerdeDividenden(): float
    {
        return $this->afzonderlijkeAanslagUitgekeerdeDividenden;
    }

    /**
     * @return float
     */
    public function getFairnessTax(): float
    {
        return $this->fairnessTax;
    }

    /**
     * @return float
     */
    public function getAfzonderlijkeAanslagBoekhoudingNaWinst(): float
    {
        return $this->afzonderlijkeAanslagBoekhoudingNaWinst;
    }

    /**
     * @return float
     */
    public function getGeheleOfGedeeltelijkMaatschappelijkVermogen33(): float
    {
        return $this->geheleOfGedeeltelijkMaatschappelijkVermogen33;
    }

    /**
     * @return float
     */
    public function getGeheleOfGedeeltelijkMaatschappelijkVermogen165(): float
    {
        return $this->geheleOfGedeeltelijkMaatschappelijkVermogen165;
    }

    /**
     * @return float
     */
    public function getVoordeleAlleAardVerleend(): float
    {
        return $this->voordeleAlleAardVerleend;
    }

    /**
     * @return float
     */
    public function getTerugbetalingTegen100(): float
    {
        return $this->terugbetalingTegen100;
    }

    /**
     * @return float
     */
    public function getNietterugbetaalbareVoorheffing(): float
    {
        return $this->nietterugbetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getBuitenlandseBelastingkrediet(): float
    {
        return $this->buitenlandseBelastingkrediet;
    }

    /**
     * @return float
     */
    public function getTerugbetaalbareVoorheffing(): float
    {
        return $this->terugbetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getBelastingkredietOndOntw(): float
    {
        return $this->belastingkredietOndOntw;
    }

    /**
     * @return bool
     */
    public function isVenootshapHuisvesting(): bool
    {
        return $this->isVenootshapHuisvesting;
    }

    /**
     * @return VoorafBetalingenInput
     */
    public function getVoorafBetalingenInput(): VoorafBetalingenInput
    {
        return $this->voorafBetalingenInput;
    }

    /**
     * @return float
     */
    public function getLatenteMeerwaarden(): float
    {
        return $this->latenteMeerwaarden;

    }


}


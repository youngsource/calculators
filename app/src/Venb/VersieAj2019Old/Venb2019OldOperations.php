<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb\VersieAj2019Old;

use Exception;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Venb\BasicCalculationResult;
use Laudis\Calculators\Venb\Results\BelastbaartariefResult;
use Laudis\Calculators\Venb\Results\BelastingAndCrisisBijdrageResult;
use Laudis\Calculators\Venb\Results\GeenVermeerderingIndienLagerResult;
use Laudis\Calculators\Venb\Results\GrondSlagCalculationResult;
use Laudis\Calculators\Venb\Results\ToegepasteCorrectiesOldResult;
use Laudis\Calculators\Venb\Results\VenbAanslagenOutput;
use Laudis\Calculators\Venb\Results\VenbBerekeningVermeerderingOutput;
use Laudis\Calculators\Venb\Results\VenbVoorafbetalingenOutput;
use Laudis\Calculators\Venb\TaxationRule;
use Laudis\Calculators\Venb\VenbGeneralOperations;
use Laudis\Calculators\Venb\VenbGeneralInput;
use Laudis\Calculators\Venb\VenbOperationInterface;
use Laudis\Calculators\Venb\VersieAj2019New\Venb2019InputNew;

/**
 *
 * @package Laudis\Calculators\Calculators\Calculators
 */
final class Venb2019OldOperations extends VenbGeneralOperations implements VenbOperationInterface
{

    /**
     * @param Venb2019InputOld $input
     * @return CalculationResultInterface
     * @throws Exception
     *
     * main calculate method that constructs a calculationresult
     */
    public function calculate($input): CalculationResultInterface
    {
        /** calculate belastbaarGewoonStelsel */
        $belastbaarGewoonStelsel = $this->belastbaarGewoonStelsel($input,
            $this->gewoneAanslagen($input)->getBelastingTotal(),
            $this->outputBerekeningVermeerdering($input)->getWeerhoudenVermeerdering());

        /** calculate afzonderlijke aanslagen using old ACB (3%) */
        $afzonderlijkeAanslagen = $this->afzonderlijkeAanslagen($input,self::ACBOld);

        return new BasicCalculationResult([
            'gewoneAanslagen' => $this->gewoneAanslagen($input)->getVermeerderingen(),
            'totaalGewoneAanslagen' => $this->gewoneAanslagen($input)->getBelastingTotal(),
            'weerhoudenVermeerdering' =>$this->outputBerekeningVermeerdering($input)->getWeerhoudenVermeerdering(),
            'eindTotaal' => $this->getEindTotaal($belastbaarGewoonStelsel->getTotaal(),$afzonderlijkeAanslagen->getBelastingTotal()),
            'belastbaarGewoonStelsel' => $belastbaarGewoonStelsel->output(),
            'afzonderlijkBelastbaar' => $afzonderlijkeAanslagen->getVermeerderingen(),
            'totaalDoorUTeBetalen' => $belastbaarGewoonStelsel->getTotaal()
                + $afzonderlijkeAanslagen->getBelastingTotal(),
            'berekeningVanDeVermeerdering' => $this->outputBerekeningVermeerdering($input)->output(),
            'geenVemeerdering' => $this->geenVermeerderingIndienResult($input)->output(),
            'toegepasteCorrecties' => $this->toegepasteCorrecties($input)->output()
        ]);
    }

    /**
     * @param Venb2019InputOld $input
     * @return ToegepasteCorrectiesOldResult
     */
    private function toegepasteCorrecties(Venb2019InputOld $input) : ToegepasteCorrectiesOldResult
    {
        /** apply correcties*/
        $belastingOud = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
            + $input->getPositieveCorrectieGrondslagGewoonTarief()
            - $input->getGedeelteGecorrigeerdeAanslag29();

        return new ToegepasteCorrectiesOldResult(
            $input->getBelastbaarGewoonTarief(),
            $input->getNegatieveCorrectieGrondslagGewoonTarief(),
            $input->getPositieveCorrectieGrondslagGewoonTarief(),
            $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief() + $input->getPositieveCorrectieGrondslagGewoonTarief(),
            $input->getGedeelteGecorrigeerdeAanslag29(),
            $input->getGedeelteGecorrigeerdeAanslag29(),
            $belastingOud,
            $input->getMeerwaardeOpAandelen25(),
            $input->getNegatieveCorrectieMeerwaardeAandelen25(),
            $input->getMeerwaardeOpAandelen25() -  $input->getNegatieveCorrectieMeerwaardeAandelen25(),
            $input->getGedeelteMeerwaardeBelastbaar25(),
            $this->calculateCorrectieMeerwaardeOpAandelen25($input)->getAmount(),
            $this->calculateCorrectieMeerwaardeOpAandelen25korting($input)->getAmount(),
            $this->calculateMeerwaardeOpAandelen25($input)->getAmount(),
            $input->getMeerwaardeOpAAndelenBelastbaartegen04(),
            $input->getNegatieveCorrectieMeerwaardeAandelen04(),
            $this->calculateMeerWaardeOpAandelen04($input)->getAmount(),
            $input->getBelastbareTegenExitTaks125(),
            $input->getGedeelteGrondslagExitTaks125(),
            $this->calculateCorrectieBelastbaarTegenExitTaks125($input)->getAmount(),
            $this->calculateBelastbaarTegenExitTaks125($input)->getAmount());
    }

    /**
     * @param Venb2019InputOld $input
     * @return VenbAanslagenOutput
     */
    private function gewoneAanslagen(Venb2019InputOld $input) : VenbAanslagenOutput
    {
        /** calculate belastbaarinkomen , belating and crisisbijdrage */
        $vermeerderingen[] = [$this->calculateBelastbaarInkomen($input)->getName() =>$this->calculateBelastbaarInkomen($input)->getData() , 'Crisisbijdrage'=>$this->calculateBelastbaarCrisisBijdrage($input)->output()];
        /** calculate correctie belastbaarinkomen , belating and crisisbijdrage */
        $vermeerderingen[] = [$this->calculateCorrectieBelastbaarInkomen($input)->getName() =>$this->calculateCorrectieBelastbaarInkomen($input)->getData() , 'Crisisbijdrage'=>$this->calculateCrisisbijdrageCorrectieBelastbaarInkomen($input)->output()];

        $vermeerderingen = $this->addCode1465($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1466($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1466Korting($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1471($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1472($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1472Correctie($input, $vermeerderingen);
        $vermeerderingen = $this->addCode1424($input, $vermeerderingen);

        return $this->venbOperationAuxiliariesNew->calculateTotalOfArray($vermeerderingen);
    }

    /**
     * @param Venb2019InputOld $input
     * @return BelastbaartariefResult
     */
    private function calculateBelastbaarInkomen(Venb2019InputOld $input): BelastbaartariefResult
    {
        $amount = $input->getBelastbaarGewoonTarief();
        if ($input->isWijzigingAfsluitdatum26072017())
        {
            $amount = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
                + $input->getPositieveCorrectieGrondslagGewoonTarief()
                - $input->getGedeelteGecorrigeerdeAanslag29();
        }

        if($input->isVenootshapHuisvesting()){
            $belasting = $amount*0.05;
            $name = 'Tarief huisvesting tegen 5%';
            $data =
                [
                    'code' => 'O 1460',
                    'bedrag' => $amount,
                    'operation' => '&times',
                    'tarief' => '5,00%',
                    'is' => '=',
                    'belasting' => $belasting,
                ];
        } else
            if ($input->isAanspraakVerminderdTarief() && $amount <= 322500) {
                $belasting = $this->venbOperationAuxiliariesOld->presentBarema($amount);
                $name = 'Verminderd tarief';
                $data = ['code' =>'O 1460', 'belasting' => $belasting];
            } else
            {
                $belasting = $amount * 0.33;
                $name ='Gewoon tarief';
                $percentage = '33,00%';
                $data =          [
                    'code' => 'O 1460',
                    'bedrag' => $amount,
                    'operation' => '&times',
                    'tarief' => $percentage,
                    'is' => '=',
                    'belasting' => $belasting,
                ];
            }

        return new BelastbaartariefResult($name,$data);
    }


    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateBelastbaarCrisisBijdrage(Venb2019InputOld $input): TaxationRule
    {
        $belastingEnCrisisBijdrage = 0;
        $amount = 0;

        /** check if crisisbijdrage */
        if ($this->venbOperationAuxiliariesOld->getFilter()->isCrisisbijdrage('1460')) {
            $amount = $this->venbOperationAuxiliariesOld->getAmountBelastbaarInkomen($this->calculateBelastbaarInkomen($input));
            /** apply crisisbijdrage */
            $belastingEnCrisisBijdrage = $amount * self::ACBOld;
        }
        return new TaxationRule('O 1460', $amount, self::ACBOld, $belastingEnCrisisBijdrage);
    }

    /**
     * @param Venb2019InputOld $input
     * @return BelastbaartariefResult
     */
    private function calculateCorrectieBelastbaarInkomen(Venb2019InputOld $input): BelastbaartariefResult
    {
        $belasting = $input->getGedeelteGecorrigeerdeAanslag29() * 0.29;
        $name = 'Gewoon tarief';
        $data =
            [
                'code' => 'N 1460',
                'bedrag' => $input->getGedeelteGecorrigeerdeAanslag29(),
                'operation' => '&times',
                'tarief' => '29,00%',
                'is' => '=',
                'belasting' => $belasting,
            ];
        if ($input->isWijzigingAfsluitdatum26072017()) {
            if ($input->isVenootshapHuisvesting()) {
                $belasting = $input->getGedeelteGecorrigeerdeAanslag29() * 0.05;
                $name = 'Tarief huisvesting tegen 5%';
                $data =
                    [
                        'code' => 'N 1460',
                        'bedrag' => $input->getGedeelteGecorrigeerdeAanslag29(),
                        'operation' => '&times',
                        'tarief' => '5,00%',
                        'is' => '=',
                        'belasting' => $belasting,
                    ];
            } else if ($input->isCorrectieVerlaagdTarief()) {
                $belasting = $this->venbOperationAuxiliariesNew->presentBarema($input->getGedeelteGecorrigeerdeAanslag29());
                $name = 'Verminderd tarief';
                $data = ['code' => 'N 1460', 'belasting' => $belasting];
            }
        }
        return new BelastbaartariefResult($name,$data);
    }

    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateCrisisbijdrageCorrectieBelastbaarInkomen(Venb2019InputOld $input) : TaxationRule
    {
        $belastingEnCrisisBijdrage = 0;
        $amount = 0;

        if ($this->venbOperationAuxiliariesOld->getFilter()->isCrisisbijdrage('1460')) {
            $belastingEnCrisisBijdrage = $this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateCorrectieBelastbaarInkomen($input))*0.02;
            $amount = $this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateCorrectieBelastbaarInkomen($input));
        }
        return new TaxationRule('N 1460', $amount,0.02,$belastingEnCrisisBijdrage);
    }

    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateMeerwaardeOpAandelen25(Venb2019InputOld $input): TaxationRule
    {
        $amount = $input->getMeerwaardeOpAandelen25();
        if ($input->isWijzigingAfsluitdatum26072017()){
            $amount -= ($input->getGedeelteMeerwaardeBelastbaar25() + $input->getNegatieveCorrectieMeerwaardeAandelen25());
        }
        return $this->belastingCalculation('O 1465', $amount, 0.25);
    }


    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateCorrectieMeerwaardeOpAandelen25(Venb2019InputOld $input): TaxationRule
    {
        $amount = 0;
        if ($input->isWijzigingAfsluitdatum26072017()){
            $amount = $input->getGedeelteMeerwaardeBelastbaar25() - $this->calculateCorrectieMeerwaardeOpAandelen25korting($input)->getAmount();
        }
        return $this->belastingCalculation('N 1466', $amount, 0.25);
    }

    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateCorrectieMeerwaardeOpAandelen25korting(Venb2019InputOld $input): TaxationRule
    {
        $tmp = 100000 - $input->getGedeelteGecorrigeerdeAanslag29();
        $amount = $input->getGedeelteMeerwaardeBelastbaar25();

        if ($amount >= $tmp){
            $amount = $tmp;
        }

        if ($input->getGedeelteGecorrigeerdeAanslag29() <= 100000 && $input->isCorrectieVerlaagdTarief() && !$input->isVenootshapHuisvesting()
            && $input->isWijzigingAfsluitdatum26072017()){
            return $this->belastingCalculation('N 1466', $amount, 0.20);
        }

        return $this->belastingCalculation('N 1466', 0, 0.20);
    }


    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateBelastbaarTegenExitTaks165(Venb2019InputOld $input): TaxationRule
    {
        return $this->belastingCalculation('O 1471', $input->getBelastbareTegenExitTaks165(), 0.165);
    }

    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateBelastbaarTegenExitTaks125(Venb2019InputOld $input): TaxationRule
    {
        $amount = $input->getBelastbareTegenExitTaks125();
        if ($input->isWijzigingAfsluitdatum26072017()){
            $amount -= $input->getGedeelteGrondslagExitTaks125();
        }

        return $this->belastingCalculation('O 1472', $amount, 0.125);
    }

    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateCorrectieBelastbaarTegenExitTaks125(Venb2019InputOld $input): TaxationRule
    {
        if ($input->isWijzigingAfsluitdatum26072017()){
            return $this->belastingCalculation('N 1472', $input->getGedeelteGrondslagExitTaks125(), 0.125);
        }
        return $this->belastingCalculation('N 1472', 0, 0.125);
    }

    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateCorrectieCrisisBijdrageBelastbaarTegenExitTaks125(Venb2019InputOld $input): TaxationRule
    {
        return $this->crisisBijdrageCalculation(
            '1472',
            $this->getNewFilter(),
            $this->calculateCorrectieBelastbaarTegenExitTaks125($input)->getBelastingOfCrisisbijdrage(),
            0.02
        );
    }

    /**
     * @param Venb2019InputOld $input
     * @return TaxationRule
     */
    private function calculateMeerWaardeOpAandelen04(Venb2019InputOld $input): TaxationRule
    {
        $amount = $input->getMeerwaardeOpAAndelenBelastbaartegen04();
        if ($input->isWijzigingAfsluitdatum26072017()){
            $amount -= $input->getNegatieveCorrectieMeerwaardeAandelen04();
        }
        return $this->belastingCalculation('O 1424', $amount, 0.004);
    }

    /**
     * @param Venb2019InputOld $input
     * @return VenbBerekeningVermeerderingOutput
     * @throws Exception
     */
    private function outputBerekeningVermeerdering(Venb2019InputOld $input): VenbBerekeningVermeerderingOutput
    {
        if ($input->isStartendeKMO()){
            return new VenbBerekeningVermeerderingOutput(
                0,
                0,
                0,
                0,
                0,
                new GrondSlagCalculationResult(0,0,0),
                new VenbVoorafbetalingenOutput([],0),
                0,
                0,
                0,
                0,
                0
            );
        }
        return new VenbBerekeningVermeerderingOutput(
            $this->calculateGrondslagVA($input),
            -$input->getNietterugbetaalbareVoorheffing(),
            -$input->getBuitenlandseBelastingkrediet(),
            -$input->getTerugbetaalbareVoorheffing(),
            -$input->getBelastingkredietOndOntw(),
            new GrondSlagCalculationResult(
                $this->grondslagVoorVermeerding($input),
                $this->venbOperationAuxiliariesNew->indexedValue($input->getAantalkwartalen() . 'kwart'),
                $this->calculateVermeerdering($input)
            ),
            $this->venbOperationAuxiliariesNew->voorafBetalingen($input->getVoorafBetalingenInput()),
            $this->saldoVermeerdering($input),
            $this->beperkingTot80(),
            $this->beperkingTotGrondVerm($input),
            $this->geenVermeerderingIndien($input),
            $this->weerhoudenVermeerdering($input)
        );
    }

    /**
     * @param Venb2019InputOld $input
     * @return float
     */
    private function calculateGrondslagVA(Venb2019InputOld $input) : float
    {
        $amountGrondslagNieuw = $input->getBelastbaarGewoonTarief() - $input->getNegatieveCorrectieGrondslagGewoonTarief()
            + $input->getPositieveCorrectieGrondslagGewoonTarief()
            - $input->getGedeelteGecorrigeerdeAanslag29();

        if ($amountGrondslagNieuw + $input->getGedeelteGecorrigeerdeAanslag29() === 0.00){
            return 0;
        }

        return -$input->getLatenteMeerwaarden() *
            ($this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateBelastbaarInkomen($input))
                + $this->calculateBelastbaarCrisisBijdrage($input)->getBelastingOfCrisisbijdrage()
                + $this->venbOperationAuxiliariesNew->getAmountBelastbaarInkomen($this->calculateCorrectieBelastbaarInkomen($input))
                + $this->calculateCrisisbijdrageCorrectieBelastbaarInkomen($input)->getBelastingOfCrisisbijdrage())
            / ($amountGrondslagNieuw +$input->getGedeelteGecorrigeerdeAanslag29());
    }

    /**
     * @param Venb2019InputOld $input
     * @return GeenVermeerderingIndienLagerResult
     * @throws Exception
     */
    private function geenVermeerderingIndienResult(Venb2019InputOld $input): GeenVermeerderingIndienLagerResult
    {
        if ($input->isStartendeKMO()){
            return new GeenVermeerderingIndienLagerResult(
                0,
                0,
                0
            );
        }
        return new GeenVermeerderingIndienLagerResult(
            $this->beperkingTot80(),
            $this->beperkingTotGrondVerm($input),
            $this->weerhoudenVermeerdering($input));
    }

    /**
     * @param Venb2019InputOld $input
     * @return float
     */
    private function grondslagVoorVermeerding(Venb2019InputOld $input): float
    {
        return max(array_sum([
            $this->gewoneAanslagen($input)->getBelastingTotal(),
            $this->calculateGrondslagVA($input),
            -$input->getNietTerugbetaalbareVoorheffing(),
            -$input->getBuitenlandseBelastingkrediet(),
            -$input->getTerugbetaalbareVoorheffing(),
            -$input->getBelastingkredietOndOntw()
        ]), 0);
    }


    /**
     * @param Venb2019InputOld $input
     * @return float|int
     * @throws Exception
     */
    private function calculateVermeerdering(Venb2019InputOld $input)
    {
        return $this->grondslagVoorVermeerding($input) * $this->venbOperationAuxiliariesOld->indexedValue($input->getAantalKwartalen() . 'kwart');
    }

    /**
     * @param Venb2019InputOld $input
     * @return float
     * @throws Exception
     */
    private function saldoVermeerdering(Venb2019InputOld $input): float
    {
        return max([$this->venbOperationAuxiliariesOld->voorafBetalingen($input->getVoorafBetalingenInput())->getTotal() + $this->calculateVermeerdering($input), 0]);
    }

    /**
     * @return float
     * @throws Exception
     */
    private function beperkingTot80(): float
    {
        return $this->venbOperationAuxiliariesOld->indexedValue('geen verhoging');
    }

    /**
     * @param Venb2019InputOld $input
     * @return float
     * @throws Exception
     */
    private function beperkingTotGrondVerm(Venb2019InputOld $input): float
    {
        return $this->grondslagVoorVermeerding($input) * $this->venbOperationAuxiliariesOld->indexedValue('pct');
    }

    /**
     * @param Venb2019InputOld $input
     * @return mixed
     * @throws Exception
     */
    private function geenVermeerderingIndien(Venb2019InputOld $input)
    {
        return max($this->beperkingTot80(), $this->beperkingTotGrondVerm($input));
    }

    /**
     * @param Venb2019InputOld $input
     * @return float|int
     * @throws Exception
     */
    private function weerhoudenVermeerdering(Venb2019InputOld $input)
    {
        if ($this->saldoVermeerdering($input) < $this->geenVermeerderingIndien($input)) {
            return 0;
        }
        return $this->saldoVermeerdering($input);
    }

    /**
     * @param Venb2019InputOld $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1465(Venb2019InputOld $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen25 = new BelastingAndCrisisBijdrageResult('Meerwaarden op aandelen tegen 25%',
            $this->calculateMeerwaardeOpAandelen25($input),
            $this->crisisBijdrageCalculation('1465', $this->getOldFilter(),
                $this->calculateMeerwaardeOpAandelen25($input)->getBelastingOfCrisisbijdrage(),
                self::ACBOld, true));
        $vermeerderingen[] = $meerwaardeAandelen25->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputOld $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1466(Venb2019InputOld $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen25Korting = new BelastingAndCrisisBijdrageResult('Correctie Meerwaarden op aandelen tegen 25%',
            $this->calculateCorrectieMeerwaardeOpAandelen25($input),
            $this->crisisBijdrageCalculation('1466', $this->getNewFilter(),
                $this->calculateCorrectieMeerwaardeOpAandelen25($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));
        $vermeerderingen[] = $meerwaardeAandelen25Korting->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputOld $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1466Korting(Venb2019InputOld $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen25Korting = new BelastingAndCrisisBijdrageResult('Correctie meerwaarden op aandelen tegen 20%',
            $this->calculateCorrectieMeerwaardeOpAandelen25korting($input),
            $this->crisisBijdrageCalculation('1466', $this->getNewFilter(),
                $this->calculateCorrectieMeerwaardeOpAandelen25korting($input)->getBelastingOfCrisisbijdrage(),
                self::ACBNew, true));
        $vermeerderingen[] = $meerwaardeAandelen25Korting->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputOld $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1471(Venb2019InputOld $input, array $vermeerderingen): array
    {
        $exitTax = new BelastingAndCrisisBijdrageResult('Belastbaar tegen exit taks 16,5%',
            $this->calculateBelastbaarTegenExitTaks165($input),
            $this->crisisBijdrageCalculation('1471', $this->getOldFilter(),
                $this->calculateBelastbaarTegenExitTaks165($input)->getBelastingOfCrisisbijdrage(),
                self::ACBOld, true));
        $vermeerderingen[] = $exitTax->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputOld $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1472(Venb2019InputOld $input, array $vermeerderingen): array
    {
        $exitTax125 = new BelastingAndCrisisBijdrageResult('Belastbaar tegen exit tax 12,5%',
            $this->calculateBelastbaarTegenExitTaks125($input),
            $this->crisisBijdrageCalculation('1472', $this->getOldFilter(),
                $this->calculateBelastbaarTegenExitTaks125($input)->getBelastingOfCrisisbijdrage(),
                self::ACBOld, true));
        $vermeerderingen[] = $exitTax125->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputOld $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1472Correctie(Venb2019InputOld $input, array $vermeerderingen): array
    {
        $corrExitTax125 = new BelastingAndCrisisBijdrageResult('Correctie belastbaar tegen exit tax 12,5%',
            $this->calculateCorrectieBelastbaarTegenExitTaks125($input),
            $this->calculateCorrectieCrisisBijdrageBelastbaarTegenExitTaks125($input));
        $vermeerderingen[] = $corrExitTax125->output();
        return $vermeerderingen;
    }

    /**
     * @param Venb2019InputOld $input
     * @param array $vermeerderingen
     * @return array
     */
    private function addCode1424(Venb2019InputOld $input, array $vermeerderingen): array
    {
        $meerwaardeAandelen04 = new BelastingAndCrisisBijdrageResult('Meerwaarde op aandelen 0,4%',
            $this->calculateMeerWaardeOpAandelen04($input),
            $this->crisisBijdrageCalculation('1472', $this->getOldFilter(),
                $this->calculateMeerWaardeOpAandelen04($input)->getBelastingOfCrisisbijdrage(),
                self::ACBOld, true));
        $vermeerderingen[] = $meerwaardeAandelen04->output();
        return $vermeerderingen;
    }
}

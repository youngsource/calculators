<?php
declare(strict_types=1);

namespace Laudis\Calculators\Venb\VersieAj2019Old;

use Laudis\Calculators\Venb\Input\VoorafBetalingenInput;
use Laudis\Calculators\Venb\VenbGeneralInput;

/**
 * Class VenbInput2018
 * @package Laudis\Calculators\Calculators\Input
 */
final class Venb2019InputOld extends VenbGeneralInput
{

    /**
     * @var float
     */
    private $belastbareTegenExitTaks165;
    /**
     * @var float
     */
    private $meerwaardeOpAAndelenBelastbaartegen04;
    /**
     * @var float
     */
    private $negatieveCorrectieMeerwaardeAandelen25;
    /**
     * @var float
     */
    private $negatieveCorrectieMeerwaardeAandelen04;
    /**
     * @var float
     */
    private $gedeelteMeerwaardeBelastbaar25;
    /**
     * @var float
     */
    private $gedeelteGrondslagExitTaks125;
    /**
     * @var float
     */
    private $gedeelteGecorrigeerdeAanslag29;
    /**
     * @var bool
     */
    private $isCorrectieVerlaagdTarief;

    /**
     * Venb2019InputOld constructor.
     * @param float $belastbaarGewoonTarief
     * @param bool $isAanspraakVerminderdTarief
     * @param bool $isVenootshapHuisvesting
     * @param bool $isBelastbaarTijdPerkTenVroegste01012018
     * @param bool $isWijzigingAfsluitdatum26072017
     * @param float $meerwaardeOpAandelen25
     * @param float $belastbareTegenExitTaks165
     * @param float $belastbareTegenExitTaks125
     * @param float $meerwaardeOpAAndelenBelastbaartegen04
     * @param float $kapitaalenInterestsubsidiesLandboouw05
     * @param float $afzonderlijkeAanslagAlleAard50
     * @param float $afzonderlijkeAanslagAlleAard100
     * @param float $afzonderlijkeAanslagBelasteReservesKredietInstellingen34
     * @param float $afzonderlijkeAanslagBelasteReservesKredietInstellingen28
     * @param float $afzonderlijkeAanslagUitgekeerdeDividenden
     * @param float $afzonderlijkeAanslagLiqRes10
     * @param float $geheleOfGedeeltelijkMaatschappelijkVermogen33
     * @param float $geheleOfGedeeltelijkMaatschappelijkVermogen165
     * @param float $voordeleAlleAardVerleend
     * @param bool $isStartendeKMO
     * @param float $bedragLatenteMeerwaardeNaProportioneleVerrekening
     * @param float $nietTerugbetaalbareVoorheffing
     * @param float $buitenlandseBelastingkrediet
     * @param float $terugbetaalbareVoorheffing
     * @param float $belastingkredietVoorOnderzoek
     * @param VoorafBetalingenInput $voorafBetalingenInput
     * @param float $latenteMeerwaarden
     * @param float $negatieveCorrectieGrondslagGewoonTarief
     * @param float $positieveCorrectieGrondslagGewoonTarief
     * @param float $negatieveCorrectieMeerwaardeAandelen25
     * @param float $negatieveCorrectieMeerwaardeAandelen04
     * @param float $gedeelteMeerwaardeBelastbaar25
     * @param float $gedeelteGrondslagExitTaks125
     * @param float $gedeelteGecorrigeerdeAanslag29
     * @param bool $isCorrectieVerlaagdTarief
     */
    public function __construct(
        float $belastbaarGewoonTarief,
        bool $isAanspraakVerminderdTarief,
        bool $isVenootshapHuisvesting,
        bool $isBelastbaarTijdPerkTenVroegste01012018,
        bool $isWijzigingAfsluitdatum26072017,
        float $meerwaardeOpAandelen25,
        float $belastbareTegenExitTaks165,
        float $belastbareTegenExitTaks125,
        float $meerwaardeOpAAndelenBelastbaartegen04,
        float $kapitaalenInterestsubsidiesLandboouw05,
        float $afzonderlijkeAanslagAlleAard50,
        float $afzonderlijkeAanslagAlleAard100,
        float $afzonderlijkeAanslagBelasteReservesKredietInstellingen34,
        float $afzonderlijkeAanslagBelasteReservesKredietInstellingen28,
        float $afzonderlijkeAanslagUitgekeerdeDividenden,
        float $afzonderlijkeAanslagLiqRes10,
        float $geheleOfGedeeltelijkMaatschappelijkVermogen33,
        float $geheleOfGedeeltelijkMaatschappelijkVermogen165,
        float $voordeleAlleAardVerleend,
        bool $isStartendeKMO,
        float $bedragLatenteMeerwaardeNaProportioneleVerrekening,
        float $nietTerugbetaalbareVoorheffing,
        float $buitenlandseBelastingkrediet,
        float $terugbetaalbareVoorheffing,
        float $belastingkredietVoorOnderzoek,
        VoorafBetalingenInput $voorafBetalingenInput,
        float $latenteMeerwaarden,
        float $negatieveCorrectieGrondslagGewoonTarief ,
        float $positieveCorrectieGrondslagGewoonTarief ,
        float $negatieveCorrectieMeerwaardeAandelen25 ,
        float $negatieveCorrectieMeerwaardeAandelen04 ,
        float $gedeelteMeerwaardeBelastbaar25 ,
        float $gedeelteGrondslagExitTaks125 ,
        float $gedeelteGecorrigeerdeAanslag29 ,
        bool $isCorrectieVerlaagdTarief
    )
    {
        parent::__construct(
            $belastbaarGewoonTarief,
            $isAanspraakVerminderdTarief,
            $isVenootshapHuisvesting,
            $isBelastbaarTijdPerkTenVroegste01012018,
            $isWijzigingAfsluitdatum26072017,
            $meerwaardeOpAandelen25,
            $belastbareTegenExitTaks125,
            $kapitaalenInterestsubsidiesLandboouw05,
            $afzonderlijkeAanslagAlleAard50,
            $afzonderlijkeAanslagAlleAard100,
            $afzonderlijkeAanslagBelasteReservesKredietInstellingen34,
            $afzonderlijkeAanslagBelasteReservesKredietInstellingen28,
            $afzonderlijkeAanslagUitgekeerdeDividenden,
            $afzonderlijkeAanslagLiqRes10,
            $geheleOfGedeeltelijkMaatschappelijkVermogen33,
            $geheleOfGedeeltelijkMaatschappelijkVermogen165,
            $voordeleAlleAardVerleend,
            $isStartendeKMO,
            $bedragLatenteMeerwaardeNaProportioneleVerrekening,
            $nietTerugbetaalbareVoorheffing,
            $buitenlandseBelastingkrediet,
            $terugbetaalbareVoorheffing,
            $belastingkredietVoorOnderzoek,
            $voorafBetalingenInput,
            $latenteMeerwaarden,
            $negatieveCorrectieGrondslagGewoonTarief,
            $positieveCorrectieGrondslagGewoonTarief
        );
        $this->belastbareTegenExitTaks165 = $belastbareTegenExitTaks165;
        $this->meerwaardeOpAAndelenBelastbaartegen04 = $meerwaardeOpAAndelenBelastbaartegen04;
        $this->negatieveCorrectieMeerwaardeAandelen25 = $negatieveCorrectieMeerwaardeAandelen25;
        $this->negatieveCorrectieMeerwaardeAandelen04 = $negatieveCorrectieMeerwaardeAandelen04;
        $this->gedeelteMeerwaardeBelastbaar25 = $gedeelteMeerwaardeBelastbaar25;
        $this->gedeelteGrondslagExitTaks125 = $gedeelteGrondslagExitTaks125;
        $this->gedeelteGecorrigeerdeAanslag29 = $gedeelteGecorrigeerdeAanslag29;
        $this->isCorrectieVerlaagdTarief = $isCorrectieVerlaagdTarief;
    }

    /**
     * @return float
     */
    public function getBelastbareTegenExitTaks165(): float
    {
        return $this->belastbareTegenExitTaks165;
    }

    /**
     * @return float
     */
    public function getMeerwaardeOpAAndelenBelastbaartegen04(): float
    {
        return $this->meerwaardeOpAAndelenBelastbaartegen04;
    }

    /**
     * @return float
     */
    public function getNegatieveCorrectieMeerwaardeAandelen25(): float
    {
        return $this->negatieveCorrectieMeerwaardeAandelen25;
    }

    /**
     * @return float
     */
    public function getNegatieveCorrectieMeerwaardeAandelen04(): float
    {
        return $this->negatieveCorrectieMeerwaardeAandelen04;
    }

    /**
     * @return float
     */
    public function getGedeelteMeerwaardeBelastbaar25(): float
    {
        return $this->gedeelteMeerwaardeBelastbaar25;
    }

    /**
     * @return float
     */
    public function getGedeelteGrondslagExitTaks125(): float
    {
        return $this->gedeelteGrondslagExitTaks125;
    }

    /**
     * @return float
     */
    public function getGedeelteGecorrigeerdeAanslag29(): float
    {
        return $this->gedeelteGecorrigeerdeAanslag29;
    }

    /**
     * @return bool
     */
    public function isCorrectieVerlaagdTarief(): bool
    {
        return $this->isCorrectieVerlaagdTarief;
    }


}


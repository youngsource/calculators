<?php
/** @noinspection NotOptimalIfConditionsInspection */
declare(strict_types=1);
/**
 * Venb version 2018
 */

namespace Laudis\Calculators\Venb\VersieAj2018;

use Exception;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Venb\BasicCalculationResult;
use Laudis\Calculators\Venb\Results\BelastbaartariefResult;
use Laudis\Calculators\Venb\Results\BelastingAndCrisisBijdrageResult;
use Laudis\Calculators\Venb\Results\GeenVermeerderingIndienLagerResult;
use Laudis\Calculators\Venb\Results\GrondSlagCalculationResult;
use Laudis\Calculators\Venb\Results\VenbAanslagenOutput;
use Laudis\Calculators\Venb\Results\VenbVerrekenbareVoorheffingenResult;
use Laudis\Calculators\Venb\Results\VenbBerekeningVermeerderingOutput;
use Laudis\Calculators\Venb\Results\VenbVoorafbetalingenOutput;
use Laudis\Calculators\Venb\TaxationRule;
use Laudis\Calculators\Venb\VenbInput2018;
use Laudis\Calculators\Venb\VenbOperationAuxiliaries;
use Laudis\Calculators\Venb\VenbOperationInterface;
use Laudis\Calculators\Venb\VersieAj2019New\Venb2019InputNew;

/**
 *
 * @package Laudis\Calculators\Calculators\Calculators
 */
final class VenbAJ2018Operations implements VenbOperationInterface
{
    /** @var float
     *ACB const percentage for 2018
     */
    private const ACB = 0.03;
    /** @var VenbOperationAuxiliaries */
    /** helper class containing functions later used */
     private $venbOperationAuxiliaries;

    /**
     * VenbAJ2018Operations constructor.
     * @param VenbOperationAuxiliaries $venbOperationAuxiliaries
     * constructor for the class, auxiliaries passed as argument
     */
    public function __construct(
        VenbOperationAuxiliaries $venbOperationAuxiliaries
    ) {
        $this->venbOperationAuxiliaries = $venbOperationAuxiliaries;
    }

    /**
     * @param VenbInput2018 $input
     * @return CalculationResultInterface
     * @throws Exception
     *
     * Main calculate function, returns a CalculationResultInterface for the front end
     */
    public function calculate($input): CalculationResultInterface
    {
        return new BasicCalculationResult([
            'gewoneAanslagen' => $this->gewoneAanslagen($input)->getVermeerderingen(),
            'totaalGewoneAanslagen' => $this->gewoneAanslagen($input)->getBelastingTotal(),
            'weerhoudenVermeerdering' =>$this->outputBerekeningVermeerdering($input)->getWeerhoudenVermeerdering(),
            'belastbaarGewoonStelsel' => $this->belastbaarGewoonStelsel($input)->output(),
            'afzonderlijkBelastbaar' => $this->afzonderlijkeAanslagen($input)->getVermeerderingen(),
            'totaalDoorUTeBetalen' => $this->belastbaarGewoonStelsel($input)->getTotaal() + $this->afzonderlijkeAanslagen($input)->getBelastingTotal(),
            'eindTotaal' => abs(
                $this->belastbaarGewoonStelsel($input)->getTotaal() + $this->afzonderlijkeAanslagen($input)->getBelastingTotal() > -2.5 &&
                $this->belastbaarGewoonStelsel($input)->getTotaal() + $this->afzonderlijkeAanslagen($input)->getBelastingTotal() < 2.50
                    ? 0 : $this->belastbaarGewoonStelsel($input)->getTotaal() + $this->afzonderlijkeAanslagen($input)->getBelastingTotal()),
            'berekeningVanDeVermeerdering' => $this->outputBerekeningVermeerdering($input)->output(),
            'geenVemeerdering' => $this->geenVermeerderingIndienResult($input)->output(),
        ]);
    }

    /**
     * @param VenbInput2018 $input
     * @return VenbAanslagenOutput
     *
     * calculate all general outputs
     */
    private function gewoneAanslagen(VenbInput2018 $input): VenbAanslagenOutput
    {
        $vermeerderingen = [];

        /** append belastbaarInkomen and crisisBijdrage to vermeerderingen list */
        $vermeerderingen[] = [$this->calculateBelastbaarInkomen($input)->getName() =>$this->calculateBelastbaarInkomen($input)->getData() , 'Crisisbijdrage'=>$this->calculateBelastbaarCrisisBijdrage($input)->output()];

        /** calculate Exit Tax 16.5%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $exitTax165 = new BelastingAndCrisisBijdrageResult('Exit tax van 16,5%',
            $this->calculateBelastbaarTegenExitTaks165($input),
            $this->taxationRule('1471',
                $this->calculateBelastbaarTegenExitTaks165($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] =  $exitTax165->output();

        /** calculate Exit Tax 12.5%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $exitTax125 = new BelastingAndCrisisBijdrageResult('Exit tax van 12,5%',
            $this->calculateBelastbaarTegenExitTaks125($input),
            $this->taxationRule('1472', $this->calculateBelastbaarTegenExitTaks125($input)->getBelastingOfCrisisbijdrage(), self::ACB));
        $vermeerderingen[] = $exitTax125->output();

        /** calculate Meerwaarden op aandelen tegen 25%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $meerwaardeAandelen25 = new BelastingAndCrisisBijdrageResult('Meerwaarden op aandelen tegen 25%',
            $this->calculateMeerwaardeOpAandelen25($input),
            $this->taxationRule('1465',
                $this->calculateMeerwaardeOpAandelen25($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] =  $meerwaardeAandelen25->output();

        /** calculate Meerwaarden op aandelen tegen 0,4%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $meerwaardeAandelen04 = new BelastingAndCrisisBijdrageResult('Meerwaarden op aandelen tegen 0,4%',
            $this->calculateMeerWaardeOpAandelen04($input),
            $this->taxationRule('1424',
                $this->calculateMeerWaardeOpAandelen04($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $meerwaardeAandelen04->output();

        /** calculate Fairnes tax tegen 5%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $fairnesTax = new BelastingAndCrisisBijdrageResult('Fairnes tax tegen 5%',
            $this->calculateFairnessTax($input),
            $this->taxationRule('1504',
                $this->calculateFairnessTax($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $fairnesTax->output();

        /** return a VenbAanslagenOutput object using auxiliaries funciton calculateTotalOfArray */
        return $this->venbOperationAuxiliaries->calculateTotalOfArray($vermeerderingen);
    }

    /**
     * @param string $identifier
     * @return float
     * @throws Exception
     *
     * get the indexed value using a string identifier
     */
    private function indexedValue(string $identifier): float
    {
        return $this->venbOperationAuxiliaries->indexedValue($identifier);
    }

    /**
     * @param VenbInput2018 $input
     * @return BelastbaartariefResult
     */
    private function calculateBelastbaarInkomen(VenbInput2018 $input) : BelastbaartariefResult
    {
        /** check if vennootschapshuisvesting, if so return sit3 result */
        if ($input->isVenootshapHuisvesting()){
            return $this->calculateBelastbaarInkomenSit3($input);
        }
        /** if aanspraak verminderdtarief and belastbaargewoonTarief <= 322500 return sit2 result  */
        if ($input->isAanspraakVerminderdTarief() && $input->getBelastbaarGewoonTarief() <= 322500) {
            return $this->calculateBelastbaarInkomenSit2($input);
        }
        /** return sit1 */
        return $this->calculateBelastbaarInkomenSit1($input);
    }

    /**
     * @param VenbInput2018 $input
     * @return BelastbaartariefResult
     */
    private function calculateBelastbaarInkomenSit1(VenbInput2018 $input): BelastbaartariefResult
    {
        /** calculate belasting using normal percentage, 33% */
        $belastingEnCrisisBijdrage = $input->getBelastbaarGewoonTarief() * 0.33;
        $name = 'Gewoon tarief';

        /**construct array for BelastbaartariefResult */
        $data = [
            'code' =>'1460',
            'bedrag' => $input->getBelastbaarGewoonTarief(),
            'operation' => '&times;',
            'tarief'=> '33,00%',
            'is' => '=',
            'belasting' => $belastingEnCrisisBijdrage
        ];

        /** return BelastbaartariefResult using name and data(array) */
        return new BelastbaartariefResult($name,$data);
    }

    /**
     * @param VenbInput2018 $input
     * @return BelastbaartariefResult
     */
    private function calculateBelastbaarInkomenSit2(VenbInput2018 $input): BelastbaartariefResult
    {
        /** calculate belastbaargewoonInkomen using the barema in auxiliaries */
        $belastingEnCrisisBijdrage = $this->venbOperationAuxiliaries->presentBarema($input->getBelastbaarGewoonTarief());
        $name ='Verminderd tarief';

        /**construct array for BelastbaartariefResult */
        $data = ['code' =>'1460', 'belasting' => $belastingEnCrisisBijdrage];

        /** return BelastbaartariefResult using name and data(array) */
        return new BelastbaartariefResult($name,$data);
    }

    /**
     * @param VenbInput2018 $input
     * @return BelastbaartariefResult
     */
    private function calculateBelastbaarInkomenSit3(VenbInput2018 $input): BelastbaartariefResult
    {
        /** calculate belastingen, 5% */
        $belastingEnCrisisBijdrage = $input->getBelastbaarGewoonTarief() * 0.05;

        /** construct array output, used in BelastbaartariefResult */
        $name = 'Tarief huisvesting tegen 5%';
        $data = [
            'code' =>'1460',
            'bedrag' => $input->getBelastbaarGewoonTarief(),
            'operation' => '&times;',
            'tarief'=> '5,00%',
            'is' => '=',
            'belasting' => $belastingEnCrisisBijdrage];

        /** return BelastbaartariefResult with the name and data(array) */
        return new BelastbaartariefResult($name,$data);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     *
     * function to calculate the crisisbijdrage belastbaarInkomen
     */
    private function calculateBelastbaarCrisisBijdrage(VenbInput2018 $input): TaxationRule
    {
        $belastingEnCrisisBijdrage = 0;
        $amount = 0;

        /** check if crisisbijdrage */
        if ($this->venbOperationAuxiliaries->getFilter()->isCrisisbijdrage('1460')) {
            /** get the belastingstotal */
            $amount = $this->venbOperationAuxiliaries->getAmountBelastbaarInkomen(
                $this->calculateBelastbaarInkomen($input));
            /** apply crisisbijdrage */
            $belastingEnCrisisBijdrage = $amount * self::ACB;
        }
        return new TaxationRule('1460', $amount, self::ACB, $belastingEnCrisisBijdrage);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateBelastbaarTegenExitTaks165(VenbInput2018 $input): TaxationRule
    {
        /** return a taxationrule using calculation function */
        return $this->calculation(
            '1471',
            $input->getBelastbareTegenExitTaks165(),
            0.165
        );
    }

    /**
     * @param string $number
     * @param $amount
     * @param float $percentage
     * @return TaxationRule
     *
     *
     * function from auxiliaries percentage standard on ACB 3%
     */
    private function calculation(string $number, $amount,$percentage = self::ACB): TaxationRule
    {
        return $this->venbOperationAuxiliaries->belastingCalculation($number, $amount, $percentage);
    }

    /**
     * @param string $number
     * @param float $amount
     * @param float $percentage
     * @return TaxationRule
     */
    private function taxationRule(string $number, float $amount, float $percentage = 0.03): TaxationRule
    {
         return $this->venbOperationAuxiliaries->crisisBijdrageCalculation($number,$this->venbOperationAuxiliaries->getFilter(),$amount,$percentage, false);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateBelastbaarTegenExitTaks125(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation(
            '1472',
            $input->getBelastbareTegenExitTaks125(),
            0.125
        );
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateMeerwaardeOpAandelen25(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation(
            '1465',
            $input->getMeerwaardeOpAandelen25(),
            0.25
        );
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateMeerWaardeOpAandelen04(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation(
            '1424',
            $input->getMeerwaardeOpAAndelenBelastbaartegen04(),
            0.004
        );
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateFairnessTax(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation('1504', $input->getFairnessTax(), 0.05);
    }

    /**
     * @param VenbInput2018 $input
     * @return VenbVerrekenbareVoorheffingenResult
     * @throws Exception
     *
     * calculate belastbaarGewoonStelsel
     */
    private function belastbaarGewoonStelsel(VenbInput2018 $input): VenbVerrekenbareVoorheffingenResult
    {
        /** get a VenbAanslagenOutput object from the function gewoneAanslagen */
        $vermeerderingen = $this->gewoneAanslagen($input);

        /**nietTerugbetaalbareVoorheffing , negative of the lowest belastingstotal(gewoneAanslagen) or NietterugbetaalbareVoorheffing*/
        $nietTerugbetaalbareVoorheffing = -min(
            $vermeerderingen->getBelastingTotal(),
            $input->getNietterugbetaalbareVoorheffing());

        /**nietTerugbetaalbareVoorheffing , negative of the lowest belastingstotal(gewoneAanslagen) or NietterugbetaalbareVoorheffing*/
        $buitenlandsBelastingskrediet = -min(
            $vermeerderingen->getBelastingTotal()
            + $nietTerugbetaalbareVoorheffing,
            $input->getBuitenlandseBelastingkrediet());

        $terugbetaalbareVoorheffing = -$input->getTerugbetaalbareVoorheffing();
        $belastingskredietVoorOnderzoek = -$input->getBelastingkredietOndOntw();

        $voorafbetalingen = $this->venbOperationAuxiliaries->voorafBetalingenTotaal($input);

        /** tussentotaal used later in VenbVerrekenbareVoorheffingenResult*/
        $tussentotaal = array_sum([
            $vermeerderingen->getBelastingTotal(),
            $nietTerugbetaalbareVoorheffing,
            $buitenlandsBelastingskrediet,
            $terugbetaalbareVoorheffing,
            $belastingskredietVoorOnderzoek,
            $voorafbetalingen
        ]);
        /** return VenbVerrekenbareVoorheffingenResult object*/
        return new VenbVerrekenbareVoorheffingenResult(
            $nietTerugbetaalbareVoorheffing,
            $buitenlandsBelastingskrediet,
            $terugbetaalbareVoorheffing,
            $belastingskredietVoorOnderzoek,
            $voorafbetalingen,
            $tussentotaal,
            $this->outputBerekeningVermeerdering($input)->getWeerhoudenVermeerdering(),
            $tussentotaal + $this->outputBerekeningVermeerdering($input)->getWeerhoudenVermeerdering());
    }

    /**
     * @param VenbInput2018 $input
     * @return GeenVermeerderingIndienLagerResult
     * @throws Exception
     */
    private function geenVermeerderingIndienResult(VenbInput2018 $input): GeenVermeerderingIndienLagerResult
    {
        /** check if startendKMO, return GeenVermeerderingIndienLagerResult with 0's */
        if ($input->isStartendKMO()){
            return new GeenVermeerderingIndienLagerResult(
                0,
                0,
                0
            );
        }
        return new GeenVermeerderingIndienLagerResult(
            $this->beperkingTot80(),
            $this->beperkingTotGrondVerm($input),
            $this->weerhoudenVermeerdering($input));
    }

    /**
     * @param VenbInput2018 $input
     * @return VenbBerekeningVermeerderingOutput
     * @throws Exception
     */
    private function outputBerekeningVermeerdering(VenbInput2018 $input): VenbBerekeningVermeerderingOutput
    {
        /** check if startendKMO, return vermeerderingen with all 0  */
        if ($input->isStartendKMO()){
            return new VenbBerekeningVermeerderingOutput(
                0,
                0,
                0,
                0,
                0,
                new GrondSlagCalculationResult(0,0,0),
                new VenbVoorafbetalingenOutput([],0),
                0,
                0,
                0,
                0,
                0
            );
        }

        return new VenbBerekeningVermeerderingOutput(
            $this->calculateGrondslagVA($input),
            -$input->getNietterugbetaalbareVoorheffing(),
            -$input->getBuitenlandseBelastingkrediet(),
            -$input->getTerugbetaalbareVoorheffing(),
            -$input->getBelastingkredietOndOntw(),
            new GrondSlagCalculationResult(
                $this->grondslagVoorVermeerding($input),
                $this->indexedValue($input->getAantalkwartalen() . 'kwart'),
                $this->calculateVermeerdering($input)
            ),
            $this->venbOperationAuxiliaries->voorafBetalingen($input->getVoorafBetalingenInput()),
            $this->saldoVermeerdering($input),
            $this->beperkingTot80(),
            $this->beperkingTotGrondVerm($input),
            $this->geenVermeerderingIndien($input),
            $this->weerhoudenVermeerdering($input)
        );
    }

    /**
     * @param VenbInput2018 $input
     * @return float
     *
     * calculate the GrondslagVA
     */
    private function calculateGrondslagVA(VenbInput2018 $input) : float
    {
        /** if belastbaarGewoontarief = 0 return 0 */
        if ($input->getBelastbaarGewoonTarief() === 0.00){
            return 0;
        }

        /** else return the grondslagVA */
        return -$input->getLatenteMeerwaarden() *
            ($this->venbOperationAuxiliaries->getAmountBelastbaarInkomen($this->calculateBelastbaarInkomen($input))
                + $this->calculateBelastbaarCrisisBijdrage($input)->getBelastingOfCrisisbijdrage())
            / $input->getBelastbaarGewoonTarief();
    }


    /**
     * @param VenbInput2018 $input
     * @return float
     */
    private function grondslagVoorVermeerding(VenbInput2018 $input): float
    {
        return max(array_sum([
            $this->gewoneAanslagen($input)->getBelastingTotal(),
            $this->calculateGrondslagVA($input),
            -$input->getNietterugbetaalbareVoorheffing(),
            -$input->getBuitenlandseBelastingkrediet(),
            -$input->getTerugbetaalbareVoorheffing(),
            -$input->getBelastingkredietOndOntw()
        ]), 0);
    }

    /**
     * @param VenbInput2018 $input
     * @return float|int
     * @throws Exception
     */
    private function calculateVermeerdering(VenbInput2018 $input)
    {
        return $this->grondslagVoorVermeerding($input) * $this->indexedValue($input->getAantalkwartalen() . 'kwart');
    }

    /**
     * @param VenbInput2018 $input
     * @return float
     * @throws Exception
     */
    private function saldoVermeerdering(VenbInput2018 $input): float
    {
        return max([$this->venbOperationAuxiliaries->voorafBetalingen($input->getVoorafBetalingenInput())->getTotal() + $this->calculateVermeerdering($input), 0]);
    }

    /**
     * @return float
     * @throws Exception
     */
    private function beperkingTot80(): float
    {
        return $this->indexedValue('geen verhoging');
    }

    /**
     * @param VenbInput2018 $input
     * @return float
     * @throws Exception
     */
    private function beperkingTotGrondVerm(VenbInput2018 $input): float
    {
        return $this->grondslagVoorVermeerding($input) * $this->indexedValue('pct');
    }

    /**
     * @param VenbInput2018 $input
     * @return mixed
     * @throws Exception
     */
    private function geenVermeerderingIndien(VenbInput2018 $input)
    {
        return max($this->beperkingTot80(), $this->beperkingTotGrondVerm($input));
    }

    /**
     * @param VenbInput2018 $input
     * @return float|int
     * @throws Exception
     */
    private function weerhoudenVermeerdering(VenbInput2018 $input)
    {
        if ($this->saldoVermeerdering($input) < $this->geenVermeerderingIndien($input)) {
            return 0;
        }
        return $this->saldoVermeerdering($input);
    }

    /**
     * @param VenbInput2018 $input
     * @return VenbAanslagenOutput
     *
     * calculate the afzonderlijkeAanslagen
     */
    private function afzonderlijkeAanslagen(VenbInput2018 $input): VenbAanslagenOutput
    {
        /** calculate Landbouwsubsidies tegen 5%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $landbouwsubsidies = new BelastingAndCrisisBijdrageResult('Landbouwsubsidies tegen 5%',
            $this->calculateKapitaalenInterestsubsidiesLandboouw05($input),
            $this->taxationRule('1481',
                                $this->calculateKapitaalenInterestsubsidiesLandboouw05($input)->getBelastingOfCrisisbijdrage(), self::ACB));
        $vermeerderingen[] = $landbouwsubsidies->output();

        /** calculate Geheime commissielonen tegen 50%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $commissielonen50 = new BelastingAndCrisisBijdrageResult('Geheime commissielonen tegen 50%',
            $this->calculateAfzonderlijkeAanslagAlleAard50($input),
            $this->taxationRule('1506',
                $this->calculateAfzonderlijkeAanslagAlleAard50($input)->getBelastingOfCrisisbijdrage(),
                self::ACB)
        );
        $vermeerderingen[] = $commissielonen50->output();

        /** calculate Geheime commissielonen tegen 100%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $commissielonen100 = new BelastingAndCrisisBijdrageResult('Geheime commissielonen tegen 100%',
            $this->calculateAfzonderlijkeAanslagAlleAard100($input),
            $this->taxationRule('1507',
                $this->calculateAfzonderlijkeAanslagAlleAard100($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $commissielonen100->output();

        /** calculate Belaste reserves kredietinstellingen tegen 34%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $krediet34 = new BelastingAndCrisisBijdrageResult('Belaste reserves kredietinstellingen tegen 34%',
            $this->calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen34($input),
            $this->taxationRule('1502a',
                $this->calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen34($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $krediet34->output();

        /** calculate Belaste reserves kredietinstellingen tegen 28%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $krediet28 = new BelastingAndCrisisBijdrageResult('Belaste reserves kredietinstellingen tegen 28%',
            $this->calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen28($input),
            $this->taxationRule('1502b',
                $this->calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen28($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $krediet28->output();

        /** calculate Uitgekeerde dividenden tegen 28%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $dividenden28 = new BelastingAndCrisisBijdrageResult('Uitgekeerde dividenden tegen 28%',
            $this->calculateAfzonderlijkeAanslagUitgekeerdeDividenden($input),
            $this->taxationRule('1503',
                $this->calculateAfzonderlijkeAanslagUitgekeerdeDividenden($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $dividenden28->output();

        /** calculate Liquidatiereserve tegen 10%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $liqRes = new BelastingAndCrisisBijdrageResult('Liquidatiereserve tegen 10%',
            $this->calculateAfzonderlijkeAanslagBoekhoudingNaWinst($input),
            $this->taxationRule('1508',
                $this->calculateAfzonderlijkeAanslagBoekhoudingNaWinst($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $liqRes->output();

        /** calculate Verdeling maatschappelijk vermogen tegen 33%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $vermogenTegen33 = new BelastingAndCrisisBijdrageResult('Verdeling maatschappelijk vermogen tegen 33%',
            $this->calculateGeheleOfGedeeltelijkMaatschappelijkVermogen33($input),
            $this->taxationRule('1511',
                $this->calculateGeheleOfGedeeltelijkMaatschappelijkVermogen33($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $vermogenTegen33->output();

        /** calculate Verdeling maatschappelijk vermogen tegen 16,5%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $vermogenTegen165 = new BelastingAndCrisisBijdrageResult('Verdeling maatschappelijk vermogen tegen 16,5%',
            $this->calculateGeheleOfGedeeltelijkMaatschappelijkVermogen165($input),
            $this->taxationRule('1512',
                $this->calculateGeheleOfGedeeltelijkMaatschappelijkVermogen165($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $vermogenTegen165->output();

        /** calculate Voordelen alle aard bij vereffening tegen 33%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $vereffining33 = new BelastingAndCrisisBijdrageResult('Voordelen alle aard bij vereffening tegen 33%',
            $this->calculateVoordeleAlleAardVerleend($input),
            $this->taxationRule('1513',
                $this->calculateVoordeleAlleAardVerleend($input)->getBelastingOfCrisisbijdrage(),
                self::ACB));
        $vermeerderingen[] = $vereffining33->output();

        /** calculate Terugbetaling belastingkrediet O&O tegen 100%, belasting and crisisbijdrage
         *  Use the ACB mentioned in the beginning of the class (3%) for this year
         *  append the result to vermeerderingen array
         */
        $latenteMeerwaarde = new BelastingAndCrisisBijdrageResult('Terugbetaling belastingkrediet O&O tegen 100%',
            new TaxationRule('1532',$input->getTerugbetalingTegen100(),1,$input->getTerugbetalingTegen100()),
            new TaxationRule('1532',0,0,0));

        $vermeerderingen[] = $latenteMeerwaarde->output();

        /** return a VenbAanslagenOutput object using auxiliaries funciton calculateTotalOfArray */
        return $this->venbOperationAuxiliaries->calculateTotalOfArray($vermeerderingen);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateKapitaalenInterestsubsidiesLandboouw05(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation('1481', $input->getKapitaalenInterestsubsidiesLandboouw05(), 0.05);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagAlleAard50(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation('1506', $input->getAfzonderlijkeAanslagAlleAard50(), 0.5);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagAlleAard100(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation('1507', $input->getAfzonderlijkeAanslagAlleAard100(), 1);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen34(
        VenbInput2018 $input
    ): TaxationRule {
        return $this->calculation('1502a', $input->getAfzonderlijkeAanslagBelasteReservesKredietInstellingen34(), 0.34);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagBelasteReservesKredietInstellingen28(
        VenbInput2018 $input
    ): TaxationRule {
        return $this->calculation('1502b', $input->getAfzonderlijkeAanslagBelasteReservesKredietInstellingen28(), 0.28);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagUitgekeerdeDividenden(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation('1503', $input->getAfzonderlijkeAanslagUitgekeerdeDividenden(), 0.28);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateAfzonderlijkeAanslagBoekhoudingNaWinst(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation('1508', $input->getAfzonderlijkeAanslagBoekhoudingNaWinst(), 0.1);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateGeheleOfGedeeltelijkMaatschappelijkVermogen33(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation('1511', $input->getGeheleOfGedeeltelijkMaatschappelijkVermogen33(), 0.33);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateGeheleOfGedeeltelijkMaatschappelijkVermogen165(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation('1512', $input->getGeheleOfGedeeltelijkMaatschappelijkVermogen165(), 0.165);
    }

    /**
     * @param VenbInput2018 $input
     * @return TaxationRule
     */
    private function calculateVoordeleAlleAardVerleend(VenbInput2018 $input): TaxationRule
    {
        return $this->calculation('1513', $input->getVoordeleAlleAardVerleend(), 0.33);
    }
}

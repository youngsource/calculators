<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use DateTime;
use Laudis\Calculators\TaxShelter\Input\AanpassingenInMeerInput;
use Laudis\Calculators\TaxShelter\Input\ActueleTaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\BelastingenLiquidateReserveInput;
use Laudis\Calculators\TaxShelter\Input\BelastingOpDeGrondslagInput;
use Laudis\Calculators\TaxShelter\Input\BerekeningVermeerderingInput;
use Laudis\Calculators\TaxShelter\Input\GeraandeBelastingenGewoonInput;
use Laudis\Calculators\TaxShelter\Input\GrondslagVermeerderingInput;
use Laudis\Calculators\TaxShelter\Input\MaximaleInvesteringInput;
use Laudis\Calculators\TaxShelter\Input\MaximaleVrijstellingInput;
use Laudis\Calculators\TaxShelter\Input\QuarterlySet;
use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use Laudis\Calculators\TaxShelter\Input\TotaleBelastingenGewoonInput;
use Laudis\Calculators\TaxShelter\VersieAj2018\GrondslagVenootschapInput2018;
use Laudis\Calculators\TaxShelter\VersieAj2018\TaxShelterInput2018;
use Laudis\Calculators\TaxShelter\VersieAj2019\GrondslagVennootschapInput2019;
use Laudis\Calculators\TaxShelter\VersieAj2019\TaxShelterInput2019;
use Laudis\Calculators\TaxShelter\VersieAj2019\TussentotaalVenBInput;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Scale\Contracts\ContextualRepositoryScaleInterface;

/**
 * Interface TaxShelterFactoryInterface
 * @package Laudis\Calculators\Contracts\TaxShelter
 */
final class TaxShelterOperationInputFactory
{
    /** @var ContextualIndexedValueRepositoryInterface */
    private $indexedValueRepository;
    /** @var ContextualRepositoryScaleInterface */
    private $scaleRepository;

    /**
     * TaxShelterFactoryInterface constructor.
     * @param ContextualIndexedValueRepositoryInterface $repository
     * @param ContextualRepositoryScaleInterface $scaleRepository
     */
    public function __construct(
        ContextualIndexedValueRepositoryInterface $repository,
        ContextualRepositoryScaleInterface $scaleRepository
    ) {
        $this->indexedValueRepository = $repository;
        $this->scaleRepository = $scaleRepository;
    }

    /**
     * @param float $belastingvrijeReserveTaxShelterIteratie
     * @param float $taxShelterVrijstellingIteratie
     * @param float $belasteReserveTaxShelterIteratie
     * @param float $taxShelterKostIteratie
     * @return TaxShelterIteratie
     */
    public function iteratie(
        float $belastingvrijeReserveTaxShelterIteratie,
        float $taxShelterVrijstellingIteratie,
        float $belasteReserveTaxShelterIteratie,
        float $taxShelterKostIteratie
    ): TaxShelterIteratie {
        return new TaxShelterIteratie(
            $belastingvrijeReserveTaxShelterIteratie,
            $belasteReserveTaxShelterIteratie,
            $taxShelterKostIteratie,
            $taxShelterVrijstellingIteratie
        );
    }

    /**
     * @param TaxShelterInput $input
     * @return TaxShelterIteratie
     */
    public function initializeIteratie($input): TaxShelterIteratie
    {
        $aanslagjaar = $input->getAanslagjaar();
        $isMaxTaxShelter = $input->isMaxTaxShelter();
        return new TaxShelterIteratie(
            $isMaxTaxShelter ? 0 : $input->getTaxShelterInvestering() * $this->vrijstellingspercentage($aanslagjaar),
            0,
            0,
            0
        );
    }

    /**
     * @param int $aanslagjaar
     * @return float|int
     */
    private function vrijstellingspercentage(int $aanslagjaar)
    {
        return $this->indexedValueRepository->getFromDate(
            DateTime::createFromFormat('Y-m-d', ($aanslagjaar - 1) . '-01-01'),
            'vrijstellingspercentage'
        )->getValue();
    }

    /**
     * @param TaxShelterInput2018 $input
     * @param TaxShelterIteratie $iteratie
     * @return GrondslagVenootschapInput2018
     */
    public function grondslagVenootschapsbelastingInput2018(
        TaxShelterInput2018 $input,
        Input\TaxShelterIteratie $iteratie
    ): GrondslagVenootschapInput2018 {
        return new GrondslagVenootschapInput2018(
            $input->getWinstVoorBelasting(),
            $input->getTantiemes(),
            $input->getAndereMutaties(),
            $input->getAndereVerworpenUitgavenGeenAftrek(),
            $input->getVerworpenUitgavenWelAftrekVerbod(),
            $input->getAftrekBewerking(),
            $iteratie->getTaxShelterVrijstelling()
        );
    }

    /**
     * @param TaxShelterInput2019 $input
     * @param TaxShelterIteratie $iteratie
     * @param float $tussentotaal
     * @param float $korfbeperking
     * @return GrondslagVennootschapInput2019
     */
    public function grondslagVenootschapsbelastingInput2019(
        TaxShelterInput2019 $input,
        Input\TaxShelterIteratie $iteratie,
        float $tussentotaal,
        float $korfbeperking
    ): GrondslagVennootschapInput2019 {
        return new GrondslagVennootschapInput2019(
            $input->getVerworpenUitgavenWelAftrekVerbod(),
            $iteratie->getTaxShelterVrijstelling(),
            $input->getAftrekkenZonderKorfBeperking(),
            $input->getAftrekkenMetKorfBeperking(),
            $tussentotaal,
            $korfbeperking
        );
    }

    /**
     * @param TaxShelterInput $input
     * @param float $grondslagVenB
     * @return BelastingOpDeGrondslagInput
     */
    public function belastingenOpDeGrondslagInput(
        TaxShelterInput $input,
        float $grondslagVenB
    ): BelastingOpDeGrondslagInput {
        return new BelastingOpDeGrondslagInput(
            $input->isVerlaagdTarief(),
            $grondslagVenB,
            $this->scaleRepository->getFromDate($this->date($input), 'verlaagd_tarief'),
            $this->indexedValueRepository->getFromDate($this->date($input), 'gewoon_tarief')->getValue()
        );
    }

    /**
     * @param TaxShelterInput $input
     * @return bool|DateTime
     */
    private function date(TaxShelterInput $input)
    {
        return DateTime::createFromFormat('Y-m-d', $input->getAanslagjaar() - 1 . '-01-01');
    }

    /**
     * @param TaxShelterInput $input
     * @param float $gewoneVenB
     */
    public function grondslagVermeerderingInput(
        TaxShelterInput $input,
        float $gewoneVenB
    ): GrondslagVermeerderingInput {
        return new GrondslagVermeerderingInput(
            $gewoneVenB,
            $input->getTerugBetaalbareVoorheffing()
        );
    }

    /**
     * @param TaxShelterInput $input
     * @param float $grondslagVermeerdering
     * @return BerekeningVermeerderingInput
     */
    public function berekeningVermeerderingInput(
        TaxShelterInput $input,
        float $grondslagVermeerdering
    ): BerekeningVermeerderingInput {
        return new BerekeningVermeerderingInput(
            $input->isVermeedering(),
            $this->voorafbetalingen($input),
            $grondslagVermeerdering,
            $this->indexedValueRepository->getFromDate($this->date($input), 'vermeerdering')->getValue(),
            QuarterlySet::make(
                $this->indexedValueRepository->getFromDate($this->date($input), 'bonificatie_VA1')->getValue(),
                $this->indexedValueRepository->getFromDate($this->date($input), 'bonificatie_VA2')->getValue(),
                $this->indexedValueRepository->getFromDate($this->date($input), 'bonificatie_VA3')->getValue(),
                $this->indexedValueRepository->getFromDate($this->date($input), 'bonificatie_VA4')->getValue()
            ),
            $this->indexedValueRepository->getFromDate($this->date($input), 'relatieve_grens')->getValue(),
            $this->indexedValueRepository->getFromDate($this->date($input), 'absolute_grens')->getValue()
        );
    }

    /**
     * @param TaxShelterInput $input
     * @return QuarterlySet
     */
    private function voorafbetalingen(TaxShelterInput $input): QuarterlySet
    {
        return QuarterlySet::make(
            $input->getVoorafBetalingKwartaal1(),
            $input->getVoorafBetalingKwartaal2(),
            $input->getVoorafBetalingKwartaal3(),
            $input->getVoorafBetalingKwartaal4()
        );
    }

    /**
     * @param TaxShelterInput $input
     * @param float $gewoneVenb
     * @param float $vermeerderingOnvoldoendeVa
     * @return GeraandeBelastingenGewoonInput
     */
    public function geraamdeBelastingenGewoonInput(
        TaxShelterInput $input,
        float $gewoneVenb,
        float $vermeerderingOnvoldoendeVa
    ): GeraandeBelastingenGewoonInput {
        return new GeraandeBelastingenGewoonInput(
            $gewoneVenb,
            $vermeerderingOnvoldoendeVa,
            $input->getTerugBetaalbareVoorheffing(),
            $this->voorafbetalingen($input),
            $input->getRegularisatiesVorigejaren()
        );
    }

    /**
     * @param TaxShelterInput $input
     * @param float $geraamdeBelastingenGewoon
     * @return TotaleBelastingenGewoonInput
     */
    public function totaleBelastingenGewoonInput(
        TaxShelterInput $input,
        float $geraamdeBelastingenGewoon
    ): TotaleBelastingenGewoonInput {
        return new TotaleBelastingenGewoonInput(
            $geraamdeBelastingenGewoon,
            $input->getTerugBetaalbareVoorheffing(),
            $this->voorafbetalingen($input),
            $input->getRegularisatiesVorigejaren()
        );
    }

    /**
     * @param TaxShelterInput $input
     * @param TaxShelterIteratie $iteratie
     * @param float $totaleBelastingenGewoon
     * @return BelastingenLiquidateReserveInput
     */
    public function grondslagLiquidatieReserveInput(
        TaxShelterInput $input,
        Input\TaxShelterIteratie $iteratie,
        float $totaleBelastingenGewoon
    ): BelastingenLiquidateReserveInput {
        return new BelastingenLiquidateReserveInput(
            $input->isMaxLiqReserve(),
            $input->getLiqReserve(),
            $input->getWinstVoorBelasting(),
            $iteratie->getBelastingsvrijeReserve(),
            $totaleBelastingenGewoon,
            $iteratie->getTaxShelterKost()
        );
    }

    /**
     * @param float $totaleBelastingenGewoon
     * @param float $belastingenLiquidatieReserves
     * @return AanpassingenInMeerInput
     */
    public function aanpassingenInMeerInput(
        float $totaleBelastingenGewoon,
        float $belastingenLiquidatieReserves
    ): AanpassingenInMeerInput {
        return new AanpassingenInMeerInput($totaleBelastingenGewoon, $belastingenLiquidatieReserves);
    }

    /**
     * @param TaxShelterInput $input
     * @param float $maximaleVrijstelling
     * @return MaximaleInvesteringInput
     */
    public function maximaleInvesteringInput(
        TaxShelterInput $input,
        float $maximaleVrijstelling
    ): MaximaleInvesteringInput {
        return new MaximaleInvesteringInput(
            $maximaleVrijstelling,
            $this->indexedValueRepository->getFromDate($this->date($input), 'vrijstellingspercentage')->getValue()
        );
    }

    /**
     * @param TaxShelterInput $input
     * @param TaxShelterIteratie $iteratie
     * @param float $totaleBelastingenGewoon
     * @param float $belastingenLiquidatieReserves
     * @param float $aanpassingenInMeer
     * @return MaximaleVrijstellingInput
     */
    public function maximaleVrijstellingInput(
        TaxShelterInput $input,
        TaxShelterIteratie $iteratie,
        float $totaleBelastingenGewoon,
        float $belastingenLiquidatieReserves,
        float $aanpassingenInMeer
    ):MaximaleVrijstellingInput {
        return new MaximaleVrijstellingInput(
            $input->getWinstVoorBelasting(),
            $input->getDividenden(),
            $input->getTantiemes(),
            $input->getAndereMutaties(),
            $totaleBelastingenGewoon,
            $belastingenLiquidatieReserves,
            $aanpassingenInMeer,
            $iteratie->getTaxShelterKost(),
            $this->indexedValueRepository->getFromDate(
                $this->date($input),
                'maximale_vrijstelling_tax_shelter'
            )->getValue(),
        );
    }

    /**
     * @param TaxShelterInput $input
     * @param TaxShelterIteratie $iteratie
     * @param float $maximaleVrijstelling
     * @param float $maximaleInvestering
     * @return ActueleTaxShelterInput
     */
    public function actualTaxShelterInput(
        TaxShelterInput $input,
        Input\TaxShelterIteratie $iteratie,
        float $maximaleVrijstelling,
        float $maximaleInvestering
    ): ActueleTaxShelterInput {
        return new ActueleTaxShelterInput(
            $input->isMaxTaxShelter(),
            $input->getTaxShelterInvestering(),
            $this->indexedValueRepository->getFromDate($this->date($input), 'vrijstellingspercentage')->getValue(),
            $iteratie,
            $maximaleVrijstelling,
            $maximaleInvestering
        );
    }

    /**
     * @param TaxShelterInput2019 $input
     * @return TussentotaalVenBInput
     */
    public function tussentotaalVenB(TaxShelterInput2019 $input, TaxShelterIteratie $iteratie): TussentotaalVenBInput
    {
        return new TussentotaalVenBInput(
            $input->getWinstVoorBelasting(),
            $input->getTantiemes(),
            $input->getAndereMutaties(),
            $input->getAndereVerworpenUitgavenGeenAftrek(),
            $input->getVerworpenUitgavenWelAftrekVerbod(),
            $input->getAftrekkenZonderKorfBeperking(),
            $iteratie->getTaxShelterVrijstelling()
        );
    }
}

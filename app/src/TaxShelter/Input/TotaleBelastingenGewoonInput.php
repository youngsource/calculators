<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class TotaleBelastingenGewoonInput
 * @package Laudis\Calculators\Calculators\Input
 */
final class TotaleBelastingenGewoonInput
{
    /** @var float */
    private $geraamdeBelastingenGewoon;
    /** @var float */
    private $terugbetaalbareVoorheffing;
    /** @var QuarterlySet */
    private $voorafbetalingen;
    /** @var float */
    private $regularisatiesVorigeJaren;

    /**
     * TotaleBelastingenGewoonInput constructor.
     * @param float $geraamdeBelastingenGewoon
     * @param float $terugbetaalbareVoorheffing
     * @param QuarterlySet $voorafbetalingen
     * @param float $regularisatiesVorigeJaren
     */
    public function __construct(
        float $geraamdeBelastingenGewoon,
        float $terugbetaalbareVoorheffing,
        QuarterlySet $voorafbetalingen,
        float $regularisatiesVorigeJaren
    ) {
        $this->geraamdeBelastingenGewoon = $geraamdeBelastingenGewoon;
        $this->terugbetaalbareVoorheffing = $terugbetaalbareVoorheffing;
        $this->voorafbetalingen = $voorafbetalingen;
        $this->regularisatiesVorigeJaren = $regularisatiesVorigeJaren;
    }

    /**
     * @return float
     */
    public function getGeraamdeBelastingenGewoon(): float
    {
        return $this->geraamdeBelastingenGewoon;
    }

    /**
     * @return float
     */
    public function getTerugbetaalbareVoorheffing(): float
    {
        return $this->terugbetaalbareVoorheffing;
    }

    /**
     * @return QuarterlySet
     */
    public function getVoorafbetalingen(): QuarterlySet
    {
        return $this->voorafbetalingen;
    }

    /**
     * @return float
     */
    public function getRegularisatiesVorigeJaren(): float
    {
        return $this->regularisatiesVorigeJaren;
    }
}

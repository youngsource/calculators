<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class GeraandeBelastingenGewoonInput
 * @package Laudis\Calculators\Calculators\Input
 */
final class GeraandeBelastingenGewoonInput
{
    /** @var float */
    private $gewoneVenB;
    /** @var float */
    private $vermeerderingOnvoldoendeVa;
    /** @var float */
    private $terugbetaalbareVoorheffing;
    /** @var QuarterlySet */
    private $voorafbetalingen;
    /** @var float */
    private $regularisatiesVorigeJaren;

    /**
     * GeraandeBelastingenGewoonInput constructor.
     * @param float $gewoneVenB
     * @param float $vermeerderingOnvoldoendeVa
     * @param float $terugbetaalbareVoorheffing
     * @param QuarterlySet $voorafbetalingen
     * @param float $regularisatiesVorigeJaren
     */
    public function __construct(
        float $gewoneVenB,
        float $vermeerderingOnvoldoendeVa,
        float $terugbetaalbareVoorheffing,
        QuarterlySet $voorafbetalingen,
        float $regularisatiesVorigeJaren
    ) {
        $this->gewoneVenB = $gewoneVenB;
        $this->vermeerderingOnvoldoendeVa = $vermeerderingOnvoldoendeVa;
        $this->terugbetaalbareVoorheffing = $terugbetaalbareVoorheffing;
        $this->voorafbetalingen = $voorafbetalingen;
        $this->regularisatiesVorigeJaren = $regularisatiesVorigeJaren;
    }

    /**
     * @return float
     */
    public function getGewoneVenB(): float
    {
        return $this->gewoneVenB;
    }

    /**
     * @return float
     */
    public function getVermeerderingOnvoldoendeVa(): float
    {
        return $this->vermeerderingOnvoldoendeVa;
    }

    /**
     * @return float
     */
    public function getTerugbetaalbareVoorheffing(): float
    {
        return $this->terugbetaalbareVoorheffing;
    }

    /**
     * @return QuarterlySet
     */
    public function getVoorafbetalingen(): QuarterlySet
    {
        return $this->voorafbetalingen;
    }

    /**
     * @return float
     */
    public function getRegularisatiesVorigeJaren(): float
    {
        return $this->regularisatiesVorigeJaren;
    }
}

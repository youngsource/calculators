<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class GrondslagVennootschapsBelasting2019
 * @package Laudis\Calculators\TaxShelter\VersieAj2019
 */
final class KorfBeperkingInput
{
    /**
     * @var float
     */
    private $winstVoorBelasting;
    /**
     * @var float
     */
    private $taxShelterVrijstellingIteratie;
    /**
     * @var float
     */
    private $andereMutaties;
    /**
     * @var float
     */
    private $andereVUGeenAftrekVerbod;
    /**
     * @var float
     */
    private $aftrekkenZonderKorfBeperking;
    /**
     * @var float
     */
    private $tantiemes;

    /**
     * GrondslagVennootschapsBelasting2019 constructor.
     * @param float $winstVoorBelasting
     * @param float $taxShelterVrijstellingIteratie
     * @param float $getAndereMutaties
     * @param float $getAndereVUGeenAftrekVerbod
     * @param float $getAftrekkenZonderKorfBeperking
     */
    public function __construct(
        float $winstVoorBelasting,
        float $tantiemes,
        float $taxShelterVrijstellingIteratie,
        float $getAndereMutaties,
        float $getAndereVUGeenAftrekVerbod,
        float $getAftrekkenZonderKorfBeperking
    ) {
        $this->winstVoorBelasting = $winstVoorBelasting;
        $this->taxShelterVrijstellingIteratie = $taxShelterVrijstellingIteratie;
        $this->andereMutaties = $getAndereMutaties;
        $this->andereVUGeenAftrekVerbod = $getAndereVUGeenAftrekVerbod;
        $this->aftrekkenZonderKorfBeperking = $getAftrekkenZonderKorfBeperking;
        $this->tantiemes = $tantiemes;
    }

    /**
     * @return float
     */
    public function getWinstVoorBelasting(): float
    {
        return $this->winstVoorBelasting;
    }

    /**
     * @return float
     */
    public function getTaxShelterVrijstellingIteratie(): float
    {
        return $this->taxShelterVrijstellingIteratie;
    }

    /**
     * @return float
     */
    public function getAndereMutaties(): float
    {
        return $this->andereMutaties;
    }

    /**
     * @return float
     */
    public function getAndereVUGeenAftrekVerbod(): float
    {
        return $this->andereVUGeenAftrekVerbod;
    }

    /**
     * @return float
     */
    public function getAftrekkenZonderKorfBeperking(): float
    {
        return $this->aftrekkenZonderKorfBeperking;
    }

    /**
     * @return float
     */
    public function getTantiemes(): float
    {
        return $this->tantiemes;
    }
}

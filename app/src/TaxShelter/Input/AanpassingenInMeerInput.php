<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class AanpassingenInMeerInput
 * @package Laudis\Calculators\Calculators\Input
 */
final class AanpassingenInMeerInput
{
    /** @var float */
    private $totaleBelastingenGewoon;
    /** @var float */
    private $belastingLiqReserve;

    /**
     * AanpassingenInMeerInput constructor.
     * @param float $totaleBelastingenGewoon
     * @param float $belastingLiqReserve
     */
    public function __construct(float $totaleBelastingenGewoon, float $belastingLiqReserve)
    {
        $this->totaleBelastingenGewoon = $totaleBelastingenGewoon;
        $this->belastingLiqReserve = $belastingLiqReserve;
    }

    /**
     * @return float
     */
    public function getTotaleBelastingenGewoon(): float
    {
        return $this->totaleBelastingenGewoon;
    }

    /**
     * @return float
     */
    public function getBelastingLiqReserve(): float
    {
        return $this->belastingLiqReserve;
    }
}

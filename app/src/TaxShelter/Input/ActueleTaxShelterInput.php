<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class ActueleTaxShelterInput
 * @package Laudis\Calculators\Calculators\Input
 */
final class ActueleTaxShelterInput
{
    /** @var bool */
    private $isMaxTaxShelter;
    /** @var float */
    private $taxShelterInvestering;
    /** @var float */
    private $vrijstellingsPercentage;
    /** @var TaxShelterIteratie */
    private $iteratie;
    /**
     * @var float
     */
    private $maximaleVrijstelling;
    /**
     * @var float
     */
    private $maximaleInvestering;

    /**
     * ActueleTaxShelterInput constructor.
     * @param bool $isMaxTaxShelter
     * @param float $taxShelterInvestering
     * @param float $vrijstellingsPercentage
     * @param TaxShelterIteratie $iteratie
     */
    public function __construct(
        bool $isMaxTaxShelter,
        float $taxShelterInvestering,
        float $vrijstellingsPercentage,
        TaxShelterIteratie $iteratie,
        float $maximaleVrijstelling,
        float $maximaleInvestering
    ) {
        $this->isMaxTaxShelter = $isMaxTaxShelter;
        $this->taxShelterInvestering = $taxShelterInvestering;
        $this->vrijstellingsPercentage = $vrijstellingsPercentage;
        $this->iteratie = $iteratie;
        $this->maximaleVrijstelling = $maximaleVrijstelling;
        $this->maximaleInvestering = $maximaleInvestering;
    }

    /**
     * @return float
     */
    public function getMaximaleInvestering(): float
    {
        return $this->maximaleInvestering;
    }

    /**
     * @return float
     */
    public function getMaximaleVrijstelling(): float
    {
        return $this->maximaleVrijstelling;
    }

    /**
     * @return bool
     */
    public function isMaxTaxShelter(): bool
    {
        return $this->isMaxTaxShelter;
    }

    /**
     * @return float
     */
    public function getTaxShelterInvestering(): float
    {
        return $this->taxShelterInvestering;
    }

    /**
     * @return float
     */
    public function getVrijstellingsPercentage(): float
    {
        return $this->vrijstellingsPercentage;
    }

    /**
     * @return TaxShelterIteratie
     */
    public function getIteratie(): TaxShelterIteratie
    {
        return $this->iteratie;
    }
}

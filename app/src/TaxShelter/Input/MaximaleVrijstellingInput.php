<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class MaximaleVrijstellingInput
 * @package Laudis\Calculators\TaxShelter\Input
 */
final class MaximaleVrijstellingInput
{
    /**
     * @var float
     */
    private $winstVoorBelasting;
    /**
     * @var float
     */
    private $tantiemes;
    /**
     * @var float
     */
    private $andereMutaties;
    /**
     * @var float
     */
    private $totaleBelastingenGewoon;
    /**
     * @var float
     */
    private $belastingLiquidatieReserve;
    /**
     * @var float
     */
    private $aanpassingenInMeerBelastingen;
    /**
     * @var float
     */
    private $taxShelterKost;
    /**
     * @var float
     */
    private $absoluteGrens;
    /**
     * @var float
     */
    private $dividenden;

    /**
     * MaximaleVrijstellingInput constructor.
     * @param float $winstVoorBelasting
     * @param float $tantiemes
     * @param float $andereMutaties
     * @param float $totaleBelastingenGewoon
     * @param float $belastingLiquidatieReserve
     * @param float $aanpassingenInMeerBelastingen
     * @param float $taxShelterKost
     * @param float $absoluteGrens
     */
    public function __construct(
        float $winstVoorBelasting,
        float $dividenden,
        float $tantiemes,
        float $andereMutaties,
        float $totaleBelastingenGewoon,
        float $belastingLiquidatieReserve,
        float $aanpassingenInMeerBelastingen,
        float $taxShelterKost,
        float $absoluteGrens
    ) {
        $this->winstVoorBelasting = $winstVoorBelasting;
        $this->tantiemes = $tantiemes;
        $this->andereMutaties = $andereMutaties;
        $this->totaleBelastingenGewoon = $totaleBelastingenGewoon;
        $this->belastingLiquidatieReserve = $belastingLiquidatieReserve;
        $this->aanpassingenInMeerBelastingen = $aanpassingenInMeerBelastingen;
        $this->taxShelterKost = $taxShelterKost;
        $this->absoluteGrens = $absoluteGrens;
        $this->dividenden = $dividenden;
    }

    /**
     * @return float
     */
    public function getWinstVoorBelasting(): float
    {
        return $this->winstVoorBelasting;
    }

    /**
     * @return float
     */
    public function getTantiemes(): float
    {
        return $this->tantiemes;
    }

    /**
     * @return float
     */
    public function getAndereMutaties(): float
    {
        return $this->andereMutaties;
    }

    /**
     * @return float
     */
    public function getTotaleBelastingenGewoon(): float
    {
        return $this->totaleBelastingenGewoon;
    }

    /**
     * @return float
     */
    public function getBelastingLiquidatieReserve(): float
    {
        return $this->belastingLiquidatieReserve;
    }

    /**
     * @return float
     */
    public function getAanpassingenInMeerBelastingen(): float
    {
        return $this->aanpassingenInMeerBelastingen;
    }

    /**
     * @return float
     */
    public function getTaxShelterKost(): float
    {
        return $this->taxShelterKost;
    }

    /**
     * @return float
     */
    public function getAbsoluteGrens(): float
    {
        return $this->absoluteGrens;
    }

    /**
     * @return float
     */
    public function getDividenden(): float
    {
        return $this->dividenden;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class MaximaleInvesteringInput
 * @package Laudis\Calculators\TaxShelter\Input
 */
class MaximaleInvesteringInput
{
    /**
     * @var float
     */
    private $maximaleVrijstelling;
    /**
     * @var float
     */
    private $vrijstellingspercentage;

    /**
     * MaximaleInvesteringInput constructor.
     * @param float $maximaleVrijstelling
     * @param float $vrijstellingspercentage
     */
    public function __construct(float $maximaleVrijstelling, float $vrijstellingspercentage)
    {
        $this->maximaleVrijstelling = $maximaleVrijstelling;
        $this->vrijstellingspercentage = $vrijstellingspercentage;
    }

    /**
     * @return float
     */
    public function getMaximaleVrijstelling(): float
    {
        return $this->maximaleVrijstelling;
    }

    /**
     * @return float
     */
    public function getVrijstellingspercentage(): float
    {
        return $this->vrijstellingspercentage;
    }
}

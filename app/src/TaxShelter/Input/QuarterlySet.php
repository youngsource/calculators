<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class QuarterlySet
 * @package Laudis\Calculators\Calculators\Input
 */
final class QuarterlySet
{
    /** @var float */
    private $q1;
    /** @var float */
    private $q2;
    /** @var float */
    private $q3;
    /** @var float */
    private $q4;

    /**
     * QuarterlySet constructor.
     * @param float $q1
     * @param float $q2
     * @param float $q3
     * @param float $q4
     */
    public function __construct(float $q1, float $q2, float $q3, float $q4)
    {
        $this->q1 = $q1;
        $this->q2 = $q2;
        $this->q3 = $q3;
        $this->q4 = $q4;
    }

    /**
     * @return float
     */
    public function getQ1(): float
    {
        return $this->q1;
    }

    /**
     * @return float
     */
    public function getQ2(): float
    {
        return $this->q2;
    }

    /**
     * @return float
     */
    public function getQ3(): float
    {
        return $this->q3;
    }

    /**
     * @return float
     */
    public function getQ4(): float
    {
        return $this->q4;
    }

    /**
     * @return array
     */
    public function all(): array
    {
        return [
            $this->q1,
            $this->q2,
            $this->q3,
            $this->q4
        ];
    }

    /**
     * @param float $q1
     * @param float $q2
     * @param float $q3
     * @param float $q4
     * @return QuarterlySet
     */
    public static function make(float $q1, float $q2, float $q3, float $q4): QuarterlySet
    {
        return new QuarterlySet($q1, $q2, $q3, $q4);
    }
}

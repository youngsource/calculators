<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

use Laudis\Scale\Contracts\ScaleInterface;

/**
 * Class BelastingOpDeGrondslagInput
 * @package Laudis\Calculators\Calculators\Input
 */
final class BelastingOpDeGrondslagInput
{
    /** @var bool */
    private $isverlaagTarief;
    /** @var float */
    private $grondslagVenB;
    /** @var ScaleInterface */
    private $verlaagdTarief;
    /** @var float */
    private $gewoonTarief;

    /**
     * BelastingOpDeGrondslagInput constructor.
     * @param bool $isverlaagTarief
     * @param float $grondslagVenB
     * @param ScaleInterface $verlaagdTarief
     * @param float $gewoonTarief
     */
    public function __construct(
        bool $isverlaagTarief,
        float $grondslagVenB,
        ScaleInterface $verlaagdTarief,
        float $gewoonTarief
    ) {
        $this->isverlaagTarief = $isverlaagTarief;
        $this->grondslagVenB = $grondslagVenB;
        $this->verlaagdTarief = $verlaagdTarief;
        $this->gewoonTarief = $gewoonTarief;
    }

    /**
     * @return bool
     */
    public function isIsverlaagTarief(): bool
    {
        return $this->isverlaagTarief;
    }

    /**
     * @return float
     */
    public function getGrondslagVenB(): float
    {
        return $this->grondslagVenB;
    }

    /**
     * @return ScaleInterface
     */
    public function getVerlaagdTarief(): ScaleInterface
    {
        return $this->verlaagdTarief;
    }

    /**
     * @return float
     */
    public function getGewoonTarief(): float
    {
        return $this->gewoonTarief;
    }
}

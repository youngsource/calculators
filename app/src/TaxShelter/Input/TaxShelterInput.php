<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

use DateTimeInterface;

/**
 * Class TaxShelterInput
 * @package Laudis\Calculators\TaxShelter\Input
 */
abstract class TaxShelterInput
{
    /**@var bool */
    protected $maxLiqReserve;
    /**@var float */
    protected $voorafBetalingKwartaal4;
    /**@var float */
    protected $taxShelterInvestering;
    /**@var float */
    protected $andereMutaties;
    /**@var float */
    protected $liqReserve;
    /**@var float */
    protected $tantiemes;
    /**@var float */
    protected $terugBetaalbareVoorheffing;
    /**@var bool */
    protected $isVerlaagdTarief;
    /**@var float */
    protected $voorafBetalingKwartaal3;
    /**@var bool */
    protected $isVermeedering;
    /**@var float */
    protected $voorafBetalingKwartaal2;
    /**@var float */
    protected $andereVerworpenUitgavenGeenAftrek;
    /**@var float */
    protected $voorafBetalingKwartaal1;
    /**@var float */
    protected $regularisatiesVorigejaren;
    /**@var float */
    protected $verworpenUitgavenAftrekVerbod;
    /**@var float */
    protected $dividenden;
    /** @var int */
    protected $aanslagjaar;
    /**@var bool */
    protected $maxTaxShelter;
    /**@var float */
    protected $winstVoorBelasting;
    /**
     * @var DateTimeInterface
     */
    private $paymentDate;

    /**
     * Calculator constructor.
     *
     * @param int $aanslagjaar
     * @param bool $maxTaxShelter
     * @param float $taxShelterInvestering
     * @param bool $maxLiqReserve
     * @param float $liqReserve
     * @param bool $isVerlaagdTarief
     * @param bool $isVermeedering
     * @param float $winstVoorBelasting
     * @param float $dividenden
     * @param float $tantiemes
     * @param float $AndereMutaties
     * @param float $AndereVerworpenUitgavenGeenAftrek
     * @param float $AndereVerworpenUitgavenWelAftrek
     * @param float $voorafBetalingKwartaal1
     * @param float $voorafBetalingKwartaal2
     * @param float $voorafBetalingKwartaal3
     * @param float $voorafBetalingKwartaal4
     * @param float $terugBetaalbareVoorheffing
     * @param float $regularisatiesVorigejaren
     * @param DateTimeInterface $paymentDate
     */
    public function __construct(
        int $aanslagjaar,
        bool $maxTaxShelter,
        float $taxShelterInvestering,
        bool $maxLiqReserve,
        float $liqReserve,
        bool $isVerlaagdTarief,
        bool $isVermeedering,
        float $winstVoorBelasting,
        float $dividenden,
        float $tantiemes,
        float $AndereMutaties,
        float $AndereVerworpenUitgavenGeenAftrek,
        float $AndereVerworpenUitgavenWelAftrek,
        float $voorafBetalingKwartaal1,
        float $voorafBetalingKwartaal2,
        float $voorafBetalingKwartaal3,
        float $voorafBetalingKwartaal4,
        float $terugBetaalbareVoorheffing,
        float $regularisatiesVorigejaren,
        DateTimeInterface $paymentDate
    ) {
        $this->aanslagjaar = $aanslagjaar;
        $this->maxTaxShelter = $maxTaxShelter;
        $this->taxShelterInvestering = $taxShelterInvestering;
        $this->maxLiqReserve = $maxLiqReserve;
        $this->liqReserve = $liqReserve;
        $this->isVerlaagdTarief = $isVerlaagdTarief;
        $this->isVermeedering = $isVermeedering;
        $this->winstVoorBelasting = $winstVoorBelasting;
        $this->dividenden = $dividenden;
        $this->tantiemes = $tantiemes;
        $this->andereMutaties = $AndereMutaties;
        $this->andereVerworpenUitgavenGeenAftrek = $AndereVerworpenUitgavenGeenAftrek;
        $this->verworpenUitgavenAftrekVerbod = $AndereVerworpenUitgavenWelAftrek;
        $this->voorafBetalingKwartaal1 = $voorafBetalingKwartaal1;
        $this->voorafBetalingKwartaal2 = $voorafBetalingKwartaal2;
        $this->voorafBetalingKwartaal3 = $voorafBetalingKwartaal3;
        $this->voorafBetalingKwartaal4 = $voorafBetalingKwartaal4;
        $this->terugBetaalbareVoorheffing = $terugBetaalbareVoorheffing;
        $this->regularisatiesVorigejaren = $regularisatiesVorigejaren;
        $this->paymentDate = $paymentDate;
    }

    /**
     * @param float $taxShelterInvestering
     */
    public function setTaxShelterInvestering(float $taxShelterInvestering): void
    {
        $this->taxShelterInvestering = $taxShelterInvestering;
    }





    /**
     * @return DateTimeInterface
     */
    public function getPaymentDate(): DateTimeInterface
    {
        return $this->paymentDate;
    }

    /**
     * @return float
     */
    public function getVoorafBetalingKwartaal1(): float
    {
        return $this->voorafBetalingKwartaal1;
    }

    /**
     * @return float
     */
    public function getVerworpenUitgavenWelAftrekVerbod(): float
    {
        return $this->verworpenUitgavenAftrekVerbod;
    }

    /**
     * @return float
     */
    public function getTerugBetaalbareVoorheffing(): float
    {
        return $this->terugBetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getDividenden(): float
    {
        return $this->dividenden;
    }

    /**
     * @return float
     */
    public function getLiqReserve(): float
    {
        return $this->liqReserve;
    }

    /**
     * @return int
     */
    public function getAanslagjaar(): int
    {
        return $this->aanslagjaar;
    }

    /**
     * @return float
     */
    public function getAndereMutaties(): float
    {
        return $this->andereMutaties;
    }

    /**
     * @return bool
     */
    public function isVermeedering(): bool
    {
        return $this->isVermeedering;
    }

    /**
     * @return float
     */
    public function getTaxShelterInvestering(): float
    {
        return $this->taxShelterInvestering;
    }

    /**
     * @return float
     */
    public function getRegularisatiesVorigejaren(): float
    {
        return $this->regularisatiesVorigejaren;
    }

    /**
     * @return bool
     */
    public function isMaxLiqReserve(): bool
    {
        return $this->maxLiqReserve;
    }

    /**
     * @return float
     */
    public function getVoorafBetalingKwartaal2(): float
    {
        return $this->voorafBetalingKwartaal2;
    }

    /**
     * @return float
     */
    public function getVoorafBetalingKwartaal4(): float
    {
        return $this->voorafBetalingKwartaal4;
    }

    /**
     * @return float
     */
    public function getVoorafBetalingKwartaal3(): float
    {
        return $this->voorafBetalingKwartaal3;
    }

    /**
     * @return float
     */
    public function getTantiemes(): float
    {
        return $this->tantiemes;
    }

    /**
     * @return bool
     */
    public function isMaxTaxShelter(): bool
    {
        return $this->maxTaxShelter;
    }

    /**
     * @return float
     */
    public function getWinstVoorBelasting(): float
    {
        return $this->winstVoorBelasting;
    }

    /**
     * @return float
     */
    public function getAndereVerworpenUitgavenGeenAftrek(): float
    {
        return $this->andereVerworpenUitgavenGeenAftrek;
    }

    /**
     * @return bool
     */
    public function isVerlaagdTarief(): bool
    {
        return $this->isVerlaagdTarief;
    }

    /**
     * @param bool $bool
     */
    public function setMaxTaxShelter(bool $bool): void
    {
        $this->maxTaxShelter = $bool;
    }

    /**
     * @return float
     */
    abstract public function getAftrekBewerking(): float;
}

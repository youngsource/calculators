<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class BerekeningVermeerderingInput
 * @package Laudis\Calculators\Calculators\Input
 */
final class BerekeningVermeerderingInput
{
    /** @var bool */
    private $isVermeerdering;
    /** @var QuarterlySet */
    private $voorafBetalingen;
    /** @var float */
    private $grondslagVermeerdering;
    /** @var float */
    private $vermeerderingsPercentage;
    /** @var QuarterlySet */
    private $voordeelpercentages;
    /** @var float */
    private $relatieveGrens;
    /** @var float */
    private $absoluteGrens;

    /**
     * BerekeningVermeerderingInput constructor.
     * @param bool $isVermeerdering
     * @param QuarterlySet $voorafBetalingen
     * @param float $grondslagVermeerdering
     * @param float $vermeerderingsPercentage
     * @param QuarterlySet $voordeelpercentages
     * @param float $relatieveGrens
     * @param float $absoluteGrens
     */
    public function __construct(
        bool $isVermeerdering,
        QuarterlySet $voorafBetalingen,
        float $grondslagVermeerdering,
        float $vermeerderingsPercentage,
        QuarterlySet $voordeelpercentages,
        float $relatieveGrens,
        float $absoluteGrens
    ) {
        $this->isVermeerdering = $isVermeerdering;
        $this->voorafBetalingen = $voorafBetalingen;
        $this->grondslagVermeerdering = $grondslagVermeerdering;
        $this->vermeerderingsPercentage = $vermeerderingsPercentage;
        $this->voordeelpercentages = $voordeelpercentages;
        $this->relatieveGrens = $relatieveGrens;
        $this->absoluteGrens = $absoluteGrens;
    }

    /**
     * @return bool
     */
    public function isVermeerdering(): bool
    {
        return $this->isVermeerdering;
    }

    /**
     * @return QuarterlySet
     */
    public function getVoorafBetalingen(): QuarterlySet
    {
        return $this->voorafBetalingen;
    }

    /**
     * @return float
     */
    public function getGrondslagVermeerdering(): float
    {
        return $this->grondslagVermeerdering;
    }

    /**
     * @return float
     */
    public function getVermeerderingsPercentage(): float
    {
        return $this->vermeerderingsPercentage;
    }

    /**
     * @return QuarterlySet
     */
    public function getVoordeelpercentages(): QuarterlySet
    {
        return $this->voordeelpercentages;
    }

    /**
     * @return float
     */
    public function getRelatieveGrens(): float
    {
        return $this->relatieveGrens;
    }

    /**
     * @return float
     */
    public function getAbsoluteGrens(): float
    {
        return $this->absoluteGrens;
    }
}

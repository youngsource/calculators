<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class BelastingenLiquidateReserveInput
 * @package Laudis\Calculators\Calculators\Input
 */
final class BelastingenLiquidateReserveInput
{
    /** @var bool */
    private $isMaxLiqReserve;
    /** @var float */
    private $gewensteNettoLiqReserve;
    /** @var float */
    private $winstVoorBelasting;
    /** @var float */
    private $belastingsvrijeReserveTaxSHetlerIteratie;
    /** @var float */
    private $totaleBelastingenGewoon;
    /** @var float */
    private $taxShelterKostIteratie;

    /**
     * BelastingenLiquidateReserveInput constructor.
     * @param bool $isMaxLiqReserve
     * @param float $gewensteNettoLiqReserve
     * @param float $winstVoorBelasting
     * @param float $belastingsvrijeReserveTaxSHetlerIteratie
     * @param float $totaleBelastingenGewoon
     * @param float $taxShelterKostIteratie
     */
    public function __construct(
        bool $isMaxLiqReserve,
        float $gewensteNettoLiqReserve,
        float $winstVoorBelasting,
        float $belastingsvrijeReserveTaxSHetlerIteratie,
        float $totaleBelastingenGewoon,
        float $taxShelterKostIteratie
    ) {
        $this->isMaxLiqReserve = $isMaxLiqReserve;
        $this->gewensteNettoLiqReserve = $gewensteNettoLiqReserve;
        $this->winstVoorBelasting = $winstVoorBelasting;
        $this->belastingsvrijeReserveTaxSHetlerIteratie = $belastingsvrijeReserveTaxSHetlerIteratie;
        $this->totaleBelastingenGewoon = $totaleBelastingenGewoon;
        $this->taxShelterKostIteratie = $taxShelterKostIteratie;
    }

    /**
     * @return bool
     */
    public function isMaxLiqReserve(): bool
    {
        return $this->isMaxLiqReserve;
    }

    /**
     * @return float
     */
    public function getGewensteNettoLiqReserve(): float
    {
        return $this->gewensteNettoLiqReserve;
    }

    /**
     * @return float
     */
    public function getWinstVoorBelasting(): float
    {
        return $this->winstVoorBelasting;
    }

    /**
     * @return float
     */
    public function getBelastingsvrijeReserveTaxSHetlerIteratie(): float
    {
        return $this->belastingsvrijeReserveTaxSHetlerIteratie;
    }

    /**
     * @return float
     */
    public function getTotaleBelastingenGewoon(): float
    {
        return $this->totaleBelastingenGewoon;
    }

    /**
     * @return float
     */
    public function getTaxShelterKostIteratie(): float
    {
        return $this->taxShelterKostIteratie;
    }
}

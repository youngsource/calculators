<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Class GrondslagVermeerderingInput
 * @package Laudis\Calculators\Calculators\Input
 */
final class GrondslagVermeerderingInput
{
    /** @var float */
    private $gewoneVenB;
    /** @var float */
    private $terugbetaalbareVoorheffing;

    /**
     * GrondslagVermeerderingInput constructor.
     * @param float $gewoneVenB
     * @param float $terugbetaalbareVoorheffing
     */
    public function __construct(float $gewoneVenB, float $terugbetaalbareVoorheffing)
    {
        $this->gewoneVenB = $gewoneVenB;
        $this->terugbetaalbareVoorheffing = $terugbetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getGewoneVenB(): float
    {
        return $this->gewoneVenB;
    }

    /**
     * @return float
     */
    public function getTerugbetaalbareVoorheffing(): float
    {
        return $this->terugbetaalbareVoorheffing;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Input;

/**
 * Interface TaxShelterIteratieInterface
 * @package Laudis\Calculators\Contracts\TaxShelter
 */
final class TaxShelterIteratie
{
    /** @var float */
    private $belastingsvrijeReserve;
    /** @var float */
    private $belasteReserve;
    /** @var float */
    private $taxShelterKost;
    /** @var float */
    private $taxShelterVrijstelling;

    /**
     * TaxShelterIteratieInterface constructor.
     * @param float $belastingsvrijeReserve
     * @param float $belasteReserve
     * @param float $taxShelterKost
     * @param float $taxShelterVrijstelling
     */
    public function __construct(
        float $belastingsvrijeReserve,
        float $belasteReserve,
        float $taxShelterKost,
        float $taxShelterVrijstelling
    ) {
        $this->belastingsvrijeReserve = $belastingsvrijeReserve;
        $this->belasteReserve = $belasteReserve;
        $this->taxShelterKost = $taxShelterKost;
        $this->taxShelterVrijstelling = $taxShelterVrijstelling;
    }

    /**
     * @return float
     */
    public function getBelastingsvrijeReserve(): float
    {
        return $this->belastingsvrijeReserve;
    }

    /**
     * @return float
     */
    public function getBelasteReserve(): float
    {
        return $this->belasteReserve;
    }

    /**
     * @return float
     */
    public function getTaxShelterKost(): float
    {
        return $this->taxShelterKost;
    }

    /**
     * @return float
     */
    public function getTaxShelterVrijstelling(): float
    {
        return $this->taxShelterVrijstelling;
    }
}

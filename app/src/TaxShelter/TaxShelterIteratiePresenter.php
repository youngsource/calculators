<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;

/**
 * Class TaxShelterIteratiePresenter
 * @package Laudis\Calculators
 */
final class TaxShelterIteratiePresenter
{
    /**
     * @param TaxShelterIteratie $iteratie
     * @return array
     */
    public function present(TaxShelterIteratie $iteratie): array
    {
        return [
            'belasteReserve' => $iteratie->getBelasteReserve(),
            'taxShelterKost' => $iteratie->getTaxShelterKost(),
            'belastingsvrijeReserve' => $iteratie->getBelastingsvrijeReserve(),
            'taxShelterVrijstelling' => $iteratie->getTaxShelterVrijstelling()
        ];
    }
}

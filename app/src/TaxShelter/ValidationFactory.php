<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Laudis\Common\Rules\BooleanRule;
use Laudis\Common\Rules\EmptyIfRule;
use Laudis\Common\Rules\NumberBetweenRule;
use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

/**
 * Class ValidationFactory
 * @package Laudis\Calculators\TaxShelter
 */
final class ValidationFactory
{
    /**
     * @var Validator
     */
    private $validator;

    /**
     * ValidationFactory constructor.
     * @param Validator $validator
     */
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param array $values
     * @return Validation
     */
    public function make2019(array $values): Validation
    {
        return $this->validator->make($values, $this->ruleset2019());
    }

    /**
     * @return array
     */
    private function ruleset2019(): array
    {
        $ruleset = $this->ruleset2018();
        $ruleset['aftrekkenMetKorfbeperking'] = ['numeric', NumberBetweenRule::make(0.0)];
        return $ruleset;
    }

    /**
     * @return array
     */
    private function ruleset2018(): array
    {
        return [
            'aanslagjaar' => ['required', NumberBetweenRule::make(2018, 9999), 'integer'],
            'maxTaxShelter' => [new BooleanRule],
            'taxShelterInvestering' => [new EmptyIfRule('maxTaxShelter'), NumberBetweenRule::make(0.0)],
            'maxLiqReserve' => [new BooleanRule],
            'liqReserve' => [new EmptyIfRule('maxLiqReserve'), NumberBetweenRule::make(0.0)],
            'isVerlaagdTarief' => [new BooleanRule],
            'isVermeerdering' => [new BooleanRule],
            'winstVoorBelastingen' => ['numeric'],
            'dividenden' => ['numeric', NumberBetweenRule::make(0.0)],
            'tantiemes' => ['numeric', NumberBetweenRule::make(0.0)],
            'andereMutaties' => ['numeric'],
            'andereVerworpenUitgavenGeenAftrek' => ['numeric', NumberBetweenRule::make(0.0)],
            'andereVerworpenUitgavenWelAftrek' => ['numeric', NumberBetweenRule::make(0.0)],
            'aftrekBewerking' => ['numeric', NumberBetweenRule::make(0.0)],
            'voorafBetalingKwartaal1' => ['numeric', NumberBetweenRule::make(0.0)],
            'voorafBetalingKwartaal2' => ['numeric', NumberBetweenRule::make(0.0)],
            'voorafBetalingKwartaal3' => ['numeric', NumberBetweenRule::make(0.0)],
            'voorafBetalingKwartaal4' => ['numeric', NumberBetweenRule::make(0.0)],
            'terugBetaalbareVoorheffing' => ['numeric', NumberBetweenRule::make(0.0)],
            'paymentDate' => ['default:2017-01-01|after:2017-01-01'],
            'regularisatiesVorigeJaren' => ['numeric']
        ];
    }

    /**
     * @param array $values
     * @return Validation
     */
    public function make2018(array $values): Validation
    {
        return $this->validator->make($values, $this->ruleset2018());
    }
}

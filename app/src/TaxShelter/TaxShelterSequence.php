<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Laudis\Calculators\TaxShelter\Contracts\TaxShelterOperationsInterface;
use Laudis\Calculators\TaxShelter\Contracts\TaxShelterSequentieInterface;
use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use function array_last;

/**
 * Class TaxShelterSequence
 * @package Laudis\Calculators\TaxShelter
 */
abstract class TaxShelterSequence implements TaxShelterSequentieInterface
{
    /** @var TaxShelterOutput2018 */
    protected $output;
    /** @var TaxShelterOperationsInterface */
    private $operations;
    /** @var TaxShelterOperationInputFactory */
    private $factory;

    /**
     * TaxShelterSequence constructor.
     * @param TaxShelterOperationsInterface $operations
     * @param TaxShelterOperationInputFactory $factory
     */
    public function __construct(TaxShelterOperationsInterface $operations, TaxShelterOperationInputFactory $factory)
    {
        $this->operations = $operations;
        $this->factory = $factory;
    }

    /**
     * @return TaxShelterOutput2018
     */
    public function getOutput(): TaxShelterOutput2018
    {
        return $this->output;
    }

    /**
     * @param TaxShelterInput $input
     * @return TaxShelterIteratie
     */
    public function intializeSequence(TaxShelterInput $input): TaxShelterIteratie
    {
        $class = $this->getOutputClass();
        $this->output = new $class;
        $next = $this->factory->initializeIteratie($input);
        $this->output->withIteratie($next);
        return $next;
    }

    /**
     * @return string
     */
    abstract protected function getOutputClass(): string;

    /**
     * @param TaxShelterInput $input
     */
    protected function storeBelastingenOpGrondslag(TaxShelterInput $input): void
    {
        $this->output->withBelastingenOpDeGrondslag($this->operations->belastingOpDeGrondslag(
            $this->factory->belastingenOpDeGrondslagInput(
                $input,
                array_last($this->output->getGrondslagenVenBen())
            )
        ));
    }

    /**
     * @param TaxShelterInput $input
     */
    protected function storeGrondslagVermeerdering(TaxShelterInput $input): void
    {
        $this->output->withGrondslagVermeerdering($this->operations->grondslagVermeerdering(
            $this->factory->grondslagVermeerderingInput(
                $input,
                array_last($this->output->getBelastingenOpDeGrondslagen())
            )
        ));
    }

    /**
     * @param TaxShelterInput $input
     */
    protected function storeBerekeningVanDeVermeerdering(TaxShelterInput $input): void
    {
        $this->output->withBerekeningVanDeVermeerdering($this->operations->berekeningVanDeVermeerdering(
            $this->factory->berekeningVermeerderingInput(
                $input,
                array_last($this->output->getGrondslagVermeerderingen())
            )
        ));
    }

    /**
     * @param TaxShelterInput $input
     */
    protected function storeGeraamdeBelastingenGewoon(TaxShelterInput $input): void
    {
        $this->output->withGeraamdeBelastingenGewoon($this->operations->geraamdeBelastingenGewoon(
            $this->factory->geraamdeBelastingenGewoonInput(
                $input,
                array_last($this->output->getBelastingenOpDeGrondslagen()),
                array_last($this->output->getBerekeningVanDeVermeerderingen())
            )
        ));
    }

    /**
     * @param TaxShelterInput $input
     */
    protected function storeTotaleBelastingenGewoon(TaxShelterInput $input): void
    {
        $this->output->withTotaleBelastingenGewoon($this->operations->totaleBelastingenGewoon(
            $this->factory->totaleBelastingenGewoonInput(
                $input,
                array_last($this->output->getGeraamdeBelastingenGewoons())
            )
        ));
    }

    /**
     * @param TaxShelterInput $input
     */
    protected function storeGrondslagLiquidatieReserve(TaxShelterInput $input): void
    {
        $this->output->withGrondslagLiquidatieReserve($this->operations->grondslagLiquidatieReserve(
            $this->factory->grondslagLiquidatieReserveInput(
                $input,
                array_last($this->output->getIteraties()),
                array_last($this->output->getTotaleBelastingenGewoon())
            )
        ));
    }

    /**
     * @param
     */
    protected function storeLiquidatieReserve(): void
    {
        $this->output->withLiquidatieReserve($this->operations->belastingLiquidatieReserve(
            array_last($this->output->getGrondslagLiquidateReserves())
        ));
    }

    /**
     * @param
     */
    protected function storeAanpassingenInMeer(): void
    {
        $this->output->withAanpassingenInMeer($this->operations->aanpassingInMeer(
            $this->factory->aanpassingenInMeerInput(
                array_last($this->output->getTotaleBelastingenGewoon()),
                array_last($this->output->getBelastingenLiquidatieReserves())
            )
        ));
    }

    /**
     * @param
     */
    protected function storeVerworpenUitgavenEnBelastingen(): void
    {
        $this->output->withVerworpenUitgavenBelastingen($this->operations->verworpenUitgevenBelastingen(
            $this->factory->aanpassingenInMeerInput(
                array_last($this->output->getTotaleBelastingenGewoon()),
                array_last($this->output->getBelastingenLiquidatieReserves())
            )
        ));
    }

    /**
     * @param TaxShelterInput $input
     * @return TaxShelterIteratie
     */
    protected function storeIteratie($input): TaxShelterIteratie
    {
        $next = $this->operations->taxShelter(
            $this->factory->actualTaxShelterInput(
                $input,
                array_last($this->output->getIteraties()),
                array_last($this->output->getMaximaleVrijstellingen()),
                array_last($this->output->getMaximaleInvesteringen())
            )
        );

        $this->output->withIteratie($next);
        return $next;
    }

    /**
     * @return TaxShelterOperationsInterface
     */
    protected function getOperations(): TaxShelterOperationsInterface
    {
        return $this->operations;
    }

    /**
     * @return TaxShelterOperationInputFactory
     */
    protected function getFactory(): TaxShelterOperationInputFactory
    {
        return $this->factory;
    }

    /**
     * @param TaxShelterInput $input
     */
    protected function storeMaximaleVrijstelling(TaxShelterInput $input): void
    {
        $this->getOutput()->withMaximaleVrijstelling($this->getOperations()->maximaleVrijstelling(
            $this->getFactory()->maximaleVrijstellingInput(
                $input,
                array_last($this->getOutput()->getIteraties()),
                array_last($this->getOutput()->getTotaleBelastingenGewoon()),
                array_last($this->getOutput()->getBelastingenLiquidatieReserves()),
                array_last($this->getOutput()->getAanpassingenInMeeren())
            )
        ));
    }

    /**
     * @param TaxShelterInput $input
     */
    protected function storeMaximaleInvestering(TaxShelterInput $input): void
    {
        $this->getOutput()->withMaximaleInvestering($this->getOperations()->maximaleInvestering(
            $this->getFactory()->maximaleInvesteringInput(
                $input,
                array_last($this->getOutput()->getMaximaleVrijstellingen())
            )
        ));
    }
}

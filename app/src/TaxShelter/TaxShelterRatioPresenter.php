<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Laudis\Calculators\TaxShelter\Raming\TaxShelterRatios;

/**
 * Class TaxShelterRatioPresenter
 * @package Laudis\Calculators\TaxShelter
 */
final class TaxShelterRatioPresenter
{
    /**
     * @var float
     */
    private $grondslagRendement;

    public function __construct(float $grondslagRendement)
    {
        $this->grondslagRendement = $grondslagRendement;
    }

    /**
     * @param TaxShelterRatios $ratios
     * @return array
     */
    public function present(TaxShelterRatios $ratios): array
    {
        return [
            'fiscaalRendement' => $ratios->getFiscaalRendement(),
            'financieelRendement' => $ratios->getFiscaalRendement(),
            'nettoFinancieelRendement' => $ratios->getNettoFincancieelRendement(),
            'totaalRendement' => $ratios->getTotaalRendement(),
            'fiscaalRendementPercentage' => $this->filterOnGrondslag($ratios->getFiscaalRendementPercentage()),
            'financieelRendementPercentage' => $this->filterOnGrondslag($ratios->getFiscaalRendementPercentage()),
            'nettoFinancieelRendementPercentage' => $this->filterOnGrondslag($ratios->getNettoFinancieelRendementPercentage()),
            'totaalRendementPercentage' => $this->filterOnGrondslag($ratios->getTotaalRendementPercentage()),
            'overdrachtRendement' => $ratios->getOverdrachtRendement(),
            'overdrachtRendementsPercentage' => $this->percentage($ratios->getOverdrachtRendementsPercentage()),

            'euribor' => $this->percentage($ratios->getEuribor()),
            'gewoonTarief' => $this->percentage($ratios->getGewoonTarief()),
            'indexDatum' => $ratios->getIndexDate()->format('Y-m-d')
        ];
    }

    private function filterOnGrondslag(?float $value): ?string
    {
        if ($this->grondslagRendement === 0.0) {
            return null;
        }
        return $this->percentage($value);
    }

    /**
     * @param float $value
     * @return string|null
     */
    private function percentage(?float $value): ?string
    {
        if ($value === null) {
            return null;
        }
        return number_format($value * 100, 4, ',', ' ') . ' %';
    }
}

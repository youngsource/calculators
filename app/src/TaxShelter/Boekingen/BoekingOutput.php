<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Boekingen;

use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\TaxShelter\Raming\RamingOutput;

/**
 * Class BoekingOutput
 * @package Laudis\Calculators\TaxShelter\Boekingen
 */
final class BoekingOutput implements CalculationResultInterface
{
    /**
     * @var float
     */
    private $d499010;
    /**
     * @var float
     */
    private $c489010;
    /**
     * @var float
     */
    private $d689010;
    /**
     * @var float
     */
    private $c132010;
    /**
     * @var float
     */
    private $d670210;
    /**
     * @var float|null
     */
    private $d495000;
    /**
     * @var float
     */
    private $c499010;
    /**
     * @var float
     */
    private $d670200;
    /**
     * @var float
     */
    private $c450010;
    /**
     * @var float
     */
    private $d692100;
    /**
     * @var float
     */
    private $c133030;
    /**
     * @var float
     */
    private $d489010;
    /**
     * @var float
     */
    private $c550010;
    /**
     * @var float
     */
    private $d550010;
    /**
     * @var float
     */
    private $c751010;
    /**
     * @var float
     */
    private $d132010;
    /**
     * @var float
     */
    private $c789010;
    /**
     * @var RamingOutput
     */
    private $ramingOutput;
    /**
     * @var float
     */
    private $c450000;
    /**
     * @var float
     */
    private $d412000;
    /**
     * @var float
     */
    private $c670100;
    /**
     * @var float
     */
    private $d670220;
    /**
     * @var bool
     */
    private $positieveGeraamdeBelastingen;
    /**
     * @var float
     */
    private $d689010Next;
    /**
     * @var float
     */
    private $c132010Next;

    /**
     * BoekingOutput constructor.
     * @param float $d499010
     * @param float $c489010C
     * @param float $d689010
     * @param float $c132010
     * @param float $d670210
     * @param float|null $d495000
     * @param float $c499010
     * @param float $d670200
     * @param float $c450010
     * @param float $d692100
     * @param float $c133030
     * @param float $d489010
     * @param float $c550010
     * @param float $d550010
     * @param float $c751010
     * @param float $d132010
     * @param float $c789010
     */
    public function __construct(
        CalculationResultInterface $ramingOutput,

        float $d499010,
        float $c489010C,
        float $d689010,
        float $c132010,

        float $d670210,
        ?float $d495000,
        float $c499010,
        float $d670200,
        float $c450000,
        float $d412000,
        float $c670100,
        float $d670220,
        float $c450010,
        float $d692100,
        float $c133030,

        float $d489010,
        float $c550010,

        float $d550010,
        float $c751010,

        float $d132010,
        float $c789010,

        float $d689010Next,
        float $c132010Next,

        bool $positieveGeraamdeBelastingen
    ) {
        $this->d499010 = $d499010;
        $this->c489010 = $c489010C;
        $this->d689010 = $d689010;
        $this->c132010 = $c132010;
        $this->d670210 = $d670210;
        $this->d495000 = $d495000;
        $this->c499010 = $c499010;
        $this->d670200 = $d670200;
        $this->c450010 = $c450010;
        $this->d692100 = $d692100;
        $this->c133030 = $c133030;
        $this->d489010 = $d489010;
        $this->c550010 = $c550010;
        $this->d550010 = $d550010;
        $this->c751010 = $c751010;
        $this->d132010 = $d132010;
        $this->c789010 = $c789010;
        $this->ramingOutput = $ramingOutput;

        $this->c450000 = $c450000;
        $this->d412000 = $d412000;
        $this->c670100 = $c670100;
        $this->d670220 = $d670220;
        $this->positieveGeraamdeBelastingen = $positieveGeraamdeBelastingen;
        $this->d689010Next = $d689010Next;
        $this->c132010Next = $c132010Next;
    }

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array
    {
        return [
            'raming' => $this->ramingOutput->output(),
            'd499010' => $this->d499010,
            'c489010' => $this->c489010,
            'd689010' => $this->d689010,
            'c132010' => $this->c132010,
            'd670210' => $this->d670210,
            'd495000' => $this->d495000,
            'c499010' => $this->c499010,
            'd670200' => $this->d670200,
            'c450000' => $this->c450000,
            'd412000' => $this->d412000,
            'c670100' => $this->c670100,
            'd670220' => $this->d670220,
            'c450010' => $this->c450010,
            'd692100' => $this->d692100,
            'c133030' => $this->c133030,
            'd489010' => $this->d489010,
            'c550010' => $this->c550010,
            'd550010' => $this->d550010,
            'c751010' => $this->c751010,
            'd132010' => $this->d132010,
            'c789010' => $this->c789010,
            'c132010Next' => $this->c132010Next,
            'd689010Next' => $this->d689010Next,
            'investeringTaxShelterMogelijk' => $this->d495000 >= 0.0,
            'positieveGeraamdeBelastingen' => $this->positieveGeraamdeBelastingen
        ];
    }

    /**
     * @param int $outputMode
     */
    public function setOutputMode(int $outputMode): void
    {
        // TODO: Implement setOutputMode() method.
    }
}

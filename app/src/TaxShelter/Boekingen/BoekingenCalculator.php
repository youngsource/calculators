<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Boekingen;

use function abs;
use DateTime;
use Exception;
use Laudis\Calculators\TaxShelter\Adapters\TaxShelterToRaming;
use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\Raming\RamingCalculator;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;

/**
 * Class BoekingenCalculator
 * @package Laudis\Calculators\TaxShelter\Boekingen
 */
final class BoekingenCalculator
{
    /** @var RamingCalculator */
    private $ramingCalculator;
    /**
     * @var TaxShelterToRaming
     */
    private $shelterToRaming;
    /**
     * @var ContextualIndexedValueRepositoryInterface
     */
    private $repository;

    /**
     * BoekingenCalculator constructor.
     * @param RamingCalculator $ramingCalculator
     * @param TaxShelterToRaming $shelterToRaming
     * @param ContextualIndexedValueRepositoryInterface $repository
     */
    public function __construct(
        RamingCalculator $ramingCalculator,
        TaxShelterToRaming $shelterToRaming,
        ContextualIndexedValueRepositoryInterface $repository
    ) {
        $this->ramingCalculator = $ramingCalculator;
        $this->shelterToRaming = $shelterToRaming;
        $this->repository = $repository;
    }

    /**
     * @param TaxShelterInput $input
     * @return BoekingOutput
     * @throws Exception
     * @throws Exception
     * @throws Exception
     */
    public function calculate(TaxShelterInput $input): BoekingOutput
    {
        $raming = $this->ramingCalculator->calculate($input);
        $ramingInput = $this->shelterToRaming->adapt($raming->getTaxShelterOutput(), $input);

        $investering = $input->isMaxTaxShelter()
            ? $raming->getTaxShelterAggregatie()->getMaximaleTaxShelterInvestering()
            : $input->getTaxShelterInvestering();

        $vrijstellingspercentageVolgend = $this->repository->getFromDate(
            DateTime::createFromFormat('Y-m-d', $input->getAanslagjaar() . '-01-01'),
            'vrijstellingspercentage'
        )->getValue();
        $vrijstellingspercentageHuidig = $this->repository->getFromDate(
            DateTime::createFromFormat('Y-m-d', ($input->getAanslagjaar() - 1) . '-01-01'),
            'vrijstellingspercentage'
        )->getValue();

        $bijTeBoeken = $ramingInput->getBelastingvrijeReserve() - $ramingInput->getTaxShelterVrijstelling();
        $overboeking = $bijTeBoeken *
            ($vrijstellingspercentageVolgend / $vrijstellingspercentageHuidig) - $bijTeBoeken;

        return new BoekingOutput(
            $raming,

            $investering,
            $investering,
            $ramingInput->getBelastingvrijeReserve(),
            $ramingInput->getBelastingvrijeReserve(),

            $investering - max($investering - $ramingInput->getMaximaleInvestering(), 0),
            max($investering - $ramingInput->getMaximaleInvestering(), 0),
            $investering,
            abs($ramingInput->getGeraamdeBelastingenGewoon()),
            abs($ramingInput->getGeraamdeBelastingenGewoon()),
            abs($ramingInput->getGeraamdeBelastingenGewoon()),
            abs($ramingInput->getGeraamdeBelastingenGewoon()),
            $ramingInput->getBelastingenLiquidatieReserve(),
            $ramingInput->getBelastingenLiquidatieReserve(),
            $ramingInput->getGrondslagLiquidatieReserve(),
            $ramingInput->getGrondslagLiquidatieReserve(),

            $investering,
            $investering,

            $investering * $raming->getTaxShelterRatios()->getEuribor() * 18 / 12,
            $investering * $raming->getTaxShelterRatios()->getEuribor() * 18 / 12,

            $ramingInput->getBelastingvrijeReserve() + (($overboeking > 0) ?  $overboeking : 0),
            $ramingInput->getBelastingvrijeReserve() + (($overboeking > 0) ?  $overboeking : 0),

            $overboeking,
            $overboeking,

            abs(max($ramingInput->getGeraamdeBelastingenGewoon(), 0)) > 0.0
        );
    }
}

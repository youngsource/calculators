<?php
/** @noinspection InterfacesAsConstructorDependenciesInspection */
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Boekingen;

use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\TaxShelter\Adapters\TaxShelterToRaming;
use Laudis\Calculators\TaxShelter\Raming\RamingFactory;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Rakit\Validation\Validation;

/**
 * Class BoekingenFactory
 * @package Laudis\Calculators\TaxShelter\Boekingen
 */
final class BoekingenFactory implements CalculatorFactoryInterface
{
    /** @var RamingFactory */
    private $ramingFactory;
    /**
     * @var ContextualIndexedValueRepositoryInterface
     */
    private $indexedValueRepository;

    /**
     * BoekingenFactory constructor.
     * @param RamingFactory $ramingFactory
     */
    public function __construct(RamingFactory $ramingFactory, ContextualIndexedValueRepositoryInterface $repository)
    {
        $this->ramingFactory = $ramingFactory;
        $this->indexedValueRepository = $repository;
    }

    /**
     * Returns the calculator.
     *
     * @param array $values
     * @return object  The calculator must have a calculate method which accepts the input from the request and returns
     *                  the result encapsulated in a calculation result.
     */
    public function calculator(array $values): object
    {
        return new BoekingenCalculator(
            $this->ramingFactory->calculator($values),
            new TaxShelterToRaming,
            $this->indexedValueRepository
        );
    }

    /**
     * Builds the input as a standard databag from the current server request.
     *
     * @param array $values
     * @return object
     */
    public function inputFromArray(array $values): object
    {
        return $this->ramingFactory->inputFromArray($values);
    }

    /**
     * Derives the validation from the current input values.
     *
     * @param array $values
     * @return Validation
     */
    public function validation(array $values): Validation
    {
        return $this->ramingFactory->validation($values);
    }
}

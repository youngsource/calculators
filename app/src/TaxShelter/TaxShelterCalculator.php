<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Laudis\Calculators\TaxShelter\Contracts\TaxShelterIteratorInterface;
use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;

/**
 * Class TaxShelterCalculator
 * @package Laudis\Calculators\Calculators\Calculators
 */
final class TaxShelterCalculator
{
    /** @var TaxShelterIteratorInterface */
    private $iterator;

    /**
     * TaxShelterCalculator constructor.
     * @param TaxShelterIteratorInterface $iterator
     */
    public function __construct(TaxShelterIteratorInterface $iterator)
    {
        $this->iterator = $iterator;
    }

    /**
     * @param TaxShelterInput
     * @return TaxShelterOutput2018
     */
    public function calculate($input): TaxShelterOutput2018
    {
        return $this->iterator->iteratie($input);
    }
}

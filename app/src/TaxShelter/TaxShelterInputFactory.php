<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use DateTime;
use DateTimeInterface;
use Exception;
use Laudis\Calculators\TaxShelter\VersieAj2018\TaxShelterInput2018;
use Laudis\Calculators\TaxShelter\VersieAj2019\TaxShelterInput2019;
use function array_merge;

/**
 * Class TaxShelterInputFactory
 * @package Laudis\Calculators\Calculators\Factories
 */
final class TaxShelterInputFactory
{

    /**
     * @var DateTimeInterface
     */
    private $currentDate;

    public function __construct(DateTimeInterface $currentDate = null)
    {
        $this->currentDate = $currentDate ?? new DateTime();
    }

    /**
     * @param array $values
     * @return TaxShelterInput2018
     * @throws Exception
     */
    public function make2018(array $values): TaxShelterInput2018
    {
        return new TaxShelterInput2018(...$this->makeParamsInput2018($values));
    }

    /**
     * @param array $values
     * @return array
     * @throws Exception
     */
    private function makeParamsInput2018(array $values): array
    {
        $aanslagjaar = to_int($values['aanslagjaar']);
        return [
            $aanslagjaar,
            to_bool($values['maxTaxShelter'] ?? false),
            to_float($values['taxShelterInvestering'] ?? 0),
            to_bool($values['maxLiqReserve'] ?? false),
            to_float($values['liqReserve'] ?? 0),
            to_bool($values['isVerlaagdTarief'] ?? false),
            to_bool($values['isVermeerdering'] ?? false),
            to_float($values['winstVoorBelastingen'] ?? 0),
            to_float($values['dividenden'] ?? 0),
            to_float($values['tantiemes'] ?? 0),
            to_float($values['andereMutaties'] ?? 0),
            to_float($values['andereVerworpenUitgavenGeenAftrek'] ?? 0),
            to_float($values['andereVerworpenUitgavenWelAftrek'] ?? 0),
            to_float($values['aftrekBewerking'] ?? 0),
            to_float($values['voorafBetalingKwartaal1'] ?? 0),
            to_float($values['voorafBetalingKwartaal2'] ?? 0),
            to_float($values['voorafBetalingKwartaal3'] ?? 0),
            to_float($values['voorafBetalingKwartaal4'] ?? 0),
            to_float($values['terugBetaalbareVoorheffing'] ?? 0),
            to_float($values['regularisatiesVorigeJaren'] ?? 0),
            DateTime::createFromFormat('Y-m-d', $this->getPaymentDate($values, $aanslagjaar))
        ];
    }

    /**
     * @param int $aanslagjaar
     * @return string
     * @throws Exception
     */
    private function standardDate(int $aanslagjaar): string
    {
        $year = ((int)$this->currentDate->format('Y'));
        if ($year === $aanslagjaar - 1) {
            return $this->currentDate->format('Y-m-d');
        }

        return $year . '-01-01';
    }

    /**
     * @param array $values
     * @return TaxShelterInput2019
     * @throws Exception
     */
    public function make2019(array $values): TaxShelterInput2019
    {
        $params = array_merge($this->makeParamsInput2018($values), [
            to_float($values['aftrekkenMetKorfbeperking'] ?? 0)
        ]);
        return new TaxShelterInput2019(...$params);
    }

    /**
     * @param array $values
     * @param int $aanslagjaar
     * @return mixed|string
     * @throws Exception
     */
    private function getPaymentDate(array $values, int $aanslagjaar)
    {
        $date = $values['paymentDate'] ?? $this->standardDate($aanslagjaar);
        if (!preg_match('/^\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2]\d|3[0-1])$/', $date)) {
            return $this->standardDate($aanslagjaar);
        }
        return $date;
    }
}

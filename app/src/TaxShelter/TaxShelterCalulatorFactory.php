<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Exception;
use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\TaxShelter\VersieAj2018\TaxShelterOperations2018;
use Laudis\Calculators\TaxShelter\VersieAj2018\TaxShelterSequence2018;
use Laudis\Calculators\TaxShelter\VersieAj2019\TaxShelterOperations2019;
use Laudis\Calculators\TaxShelter\VersieAj2019\TaxShelterSequence2019;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Scale\Contracts\ContextualRepositoryScaleInterface;
use Rakit\Validation\Validation;

/**
 * Class TaxShelterCalulatorFactory2019
 * @package Laudis\Calculators\TaxShelter\VersieAj2019
 */
final class TaxShelterCalulatorFactory implements CalculatorFactoryInterface
{
    /**
     * @var TaxShelterInputFactory
     */
    private $inputFactory;
    /**
     * @var ValidationFactory
     */
    private $validationFactory;
    /**
     * @var ContextualIndexedValueRepositoryInterface
     */
    private $indexedValueRepository;
    /**
     * @var ContextualRepositoryScaleInterface
     */
    private $scaleRepository;

    /**
     * TaxShelterCalulatorFactory2019 constructor.
     * @param TaxShelterInputFactory $inputFactory
     * @param ValidationFactory $validationFactory
     * @param ContextualIndexedValueRepositoryInterface $indexedValueRepository
     * @param ContextualRepositoryScaleInterface $scaleRepository
     */
    public function __construct(
        TaxShelterInputFactory $inputFactory,
        ValidationFactory $validationFactory,
        ContextualIndexedValueRepositoryInterface $indexedValueRepository,
        ContextualRepositoryScaleInterface $scaleRepository
    ) {
        $this->inputFactory = $inputFactory;
        $this->validationFactory = $validationFactory;
        $this->indexedValueRepository = $indexedValueRepository;
        $this->scaleRepository = $scaleRepository;
    }

    /**
     * {@inheritDoc}
     */
    public function calculator(array $values): object
    {
        $aanslagjaar = (int)$values['aanslagjaar'];
        $inputFactory = new TaxShelterOperationInputFactory($this->indexedValueRepository, $this->scaleRepository);
        $operations = new TaxShelterOperations2018($inputFactory);
        if ($aanslagjaar < 2019) {
            return new TaxShelterCalculator(new TaxShelterIterator(new TaxShelterSequence2018(
                $operations, $inputFactory
            )));
        }
        return new TaxShelterCalculator(new TaxShelterIterator(new TaxShelterSequence2019(
            new TaxShelterOperations2019($operations), $inputFactory
        )));
    }

    /**
     * Builds the input as a standard databag from the current server request.
     *
     * @param array $values
     * @return object
     * @throws Exception
     */
    public function inputFromArray(array $values): object
    {
        $aanslagjaar = (int)$values['aanslagjaar'];
        if ($aanslagjaar < 2019) {
            return $this->inputFactory->make2018($values);
        }
        return $this->inputFactory->make2019($values);
    }

    /**
     * Derives the validation from the current input values.
     *
     * @param array $values
     * @return Validation
     */
    public function validation(array $values): Validation
    {
        $aanslagjaar = (int)$values['aanslagjaar'];
        if ($aanslagjaar < 2019) {
            return $this->validationFactory->make2018($values);
        }
        return $this->validationFactory->make2019($values);
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Adapters;

use function array_first;
use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use Laudis\Calculators\TaxShelter\Raming\RamingInput;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;
use function array_last;

/**
 * Class TaxShelterToRaming
 * @package Laudis\Calculators\TaxShelter\Adapters
 */
final class TaxShelterToRaming
{
    /**
     * @param TaxShelterOutput2018 $output
     * @param TaxShelterInput $input
     * @return RamingInput
     */
    public function adapt(TaxShelterOutput2018 $output, TaxShelterInput $input): RamingInput
    {
        /** @var TaxShelterIteratie $iteratie */
        $iteratie = array_last($output->getIteraties());
        return new RamingInput(
            $input->isMaxTaxShelter(),
            $input->getWinstVoorBelasting(),
            $input->getDividenden(),
            $input->getTantiemes(),
            $iteratie->getBelastingsvrijeReserve(),
            $iteratie->getTaxShelterVrijstelling(),
            $iteratie->getBelasteReserve(),
            $input->getAndereMutaties(),
            $input->getAndereVerworpenUitgavenGeenAftrek(),
            $input->getVerworpenUitgavenWelAftrekVerbod(),
            $input->getAftrekBewerking(),
            array_last($output->getGrondslagenVenBen()),
            array_last($output->getBelastingenOpDeGrondslagen()),
            array_last($output->getBerekeningVanDeVermeerderingen()),
            $input->getVoorafBetalingKwartaal1(),
            $input->getVoorafBetalingKwartaal2(),
            $input->getVoorafBetalingKwartaal3(),
            $input->getVoorafBetalingKwartaal4(),
            $input->getTerugBetaalbareVoorheffing(),
            array_last($output->getGeraamdeBelastingenGewoons()),
            $input->getRegularisatiesVorigejaren(),
            array_last($output->getBelastingenLiquidatieReserves()),
            $iteratie->getTaxShelterKost(),
            array_last($output->getAanpassingenInMeeren()),
            $input->getTaxShelterInvestering(),
            array_last($output->getGrondslagLiquidateReserves()),
            array_first($output->getBelastingenOpDeGrondslagen()),
            array_last($output->getMaximaleInvesteringen()),
            $input->getAanslagjaar(),
            $input->getPaymentDate()
        );
    }
}

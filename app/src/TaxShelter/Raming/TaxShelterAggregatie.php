<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Raming;

use Laudis\Calculators\Contracts\CalculationResultInterface;

/**
 * Class GegevenTaxShelterOutput
 * @package Laudis\Calculators\TaxShelter\Raming
 */
final class TaxShelterAggregatie implements CalculationResultInterface
{
    /** @var float */
    private $effectieveTaxShelterInvestering;
    /** @var float */
    private $maximaleTaxShelterVrijstelling;
    /** @var float */
    private $overinvestering;
    /** @var float */
    private $belastingZonderTaxShelterInvestering;
    /** @var float */
    private $belastingMetTaxShelterInvestering;
    /**
     * @var GegevenTaxShelterOutputPresenter
     */
    private $presenter;
    /**
     * @var float
     */
    private $maximaleTaxShelterInvestering;
    /**
     * @var float
     */
    private $resterendeInvesteringsruimte;
    /**
     * @var bool
     */
    private $isOverInvestering;

    /**
     * GegevenTaxShelterOutput constructor.
     * @param float $effectieveTaxShelterInvestering
     * @param float $maximaleTaxShelterVrijstelling
     * @param float $overinvestering
     * @param float $belastingZonderTaxShelterInvestering
     * @param float $belastingMetTaxShelterInvestering
     * @param GegevenTaxShelterOutputPresenter $presenter
     */
    public function __construct(
        float $maximaleTaxShelterInvestering,
        float $maximaleTaxShelterVrijstelling,
        float $belastingZonderTaxShelterInvestering,
        float $belastingMetTaxShelterInvestering,
        float $effectieveTaxShelterInvestering,
        float $resterendeInvesteringsruimte,
        float $overinvestering,
        bool $isOverInvestering,
        GegevenTaxShelterOutputPresenter $presenter
    ) {
        $this->effectieveTaxShelterInvestering = $effectieveTaxShelterInvestering;
        $this->maximaleTaxShelterVrijstelling = $maximaleTaxShelterVrijstelling;
        $this->overinvestering = $overinvestering;
        $this->belastingZonderTaxShelterInvestering = $belastingZonderTaxShelterInvestering;
        $this->belastingMetTaxShelterInvestering = $belastingMetTaxShelterInvestering;
        $this->presenter = $presenter;
        $this->maximaleTaxShelterInvestering = $maximaleTaxShelterInvestering;
        $this->resterendeInvesteringsruimte = $resterendeInvesteringsruimte;
        $this->isOverInvestering = $isOverInvestering;
    }

    /**
     * @return float
     */
    public function getEffectieveTaxShelterInvestering(): float
    {
        return $this->effectieveTaxShelterInvestering;
    }

    /**
     * @return float
     */
    public function getMaximaleTaxShelterVrijstelling(): float
    {
        return $this->maximaleTaxShelterVrijstelling;
    }

    /**
     * @return float
     */
    public function getOverinvestering(): float
    {
        return $this->overinvestering;
    }

    /**
     * @return float
     */
    public function getBelastingZonderTaxShelterInvestering(): float
    {
        return $this->belastingZonderTaxShelterInvestering;
    }

    /**
     * @return float
     */
    public function getBelastingMetTaxShelterInvestering(): float
    {
        return $this->belastingMetTaxShelterInvestering;
    }

    /**
     * @return bool
     */
    public function isOverInvestering(): bool
    {
        return $this->isOverInvestering;
    }

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array
    {
        return $this->presenter->present($this);
    }

    /**
     * @return float
     */
    public function getMaximaleTaxShelterInvestering(): float
    {
        return $this->maximaleTaxShelterInvestering;
    }

    /**
     * @return float
     */
    public function getResterendeInvesteringsruimte(): float
    {
        return $this->resterendeInvesteringsruimte;
    }

    /**
     * @param int $outputMode
     */
    public function setOutputMode(int $outputMode): void
    {
        // irrelevant
    }
}

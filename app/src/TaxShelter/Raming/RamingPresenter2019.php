<?php

namespace Laudis\Calculators\TaxShelter\Raming;

/**
 * Class RamingPresenter2019
 * @package Laudis\Calculators\TaxShelter\Raming
 */
final class RamingPresenter2019 extends RamingPresenter
{
    /**
     * @param RamingOutput|RamingOutput2019 $output
     * @return array
     */
    public function present(RamingOutput $output): array
    {
        return array_merge(parent::present($output), [
            'aftrekBewerkingenMetKorfbeperking' => $output->getAftrekkenMetKorfBeperking()
        ]);
    }
}

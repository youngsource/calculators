<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Raming;

use DateTimeInterface;

/**
 * Class RamingInput
 * @package Laudis\Calculators\TaxShelter\Raming
 */
final class RamingInput
{
    /** @var float */
    private $winstVoorBelastingen;
    /** @var float */
    private $dividenden;
    /**
     * @var float
     */
    private $tantiemes;
    /**
     * @var float
     */
    private $taxShelterVrijstelling;
    /**
     * @var float
     */
    private $andereMutaties;
    /**
     * @var float
     */
    private $andereVerworpenUitgavenGeenAftrek;
    /**
     * @var float
     */
    private $andereVerworpenUitgavenWelAftrek;
    /**
     * @var float
     */
    private $aftrekBewerking;
    /**
     * @var float
     */
    private $grondslagVenb;
    /**
     * @var float
     */
    private $belastingenOpDeGrondslag;
    /**
     * @var float
     */
    private $berekeningVanDeVermeerdering;
    /**
     * @var float
     */
    private $voorafbetalingQ1;
    /**
     * @var float
     */
    private $voorafbetalingQ2;
    /**
     * @var float
     */
    private $voorafbetalingQ3;
    /**
     * @var float
     */
    private $voorafbetalingQ4;
    /**
     * @var float
     */
    private $terugbetaalbareVoorheffing;
    /**
     * @var float
     */
    private $geraamdeBelastingenGewoon;
    /**
     * @var float
     */
    private $regularisatieVorigeJaren;
    /**
     * @var float
     */
    private $belastingenLiquidatieReserve;
    /**
     * @var float
     */
    private $taxShelterKost;
    /**
     * @var float
     */
    private $aanpassingenInMeer;
    /**
     * @var float
     */
    private $taxShelterInvestering;
    /**
     * @var float
     */
    private $grondslagLiquidatieReserve;
    /**
     * @var float
     */
    private $belasteReserve;
    /**
     * @var float
     */
    private $belastingvrijeReserve;
    /**
     * @var bool
     */
    private $maxTaxShelter;
    /**
     * @var float
     */
    private $belastingenOpDeGrondslagVoor;
    /** @var float */
    private $maximaleInvestering;
    /**
     * @var int
     */
    private $aanslagjaar;
    /**
     * @var DateTimeInterface
     */
    private $paymentDate;

    /**
     * RamingInput constructor.
     * @param float $winstVoorBelastingen
     * @param float $tantiemes
     * @param float $taxShelterVrijstelling
     * @param float $andereMutaties
     * @param float $andereVerworpenUitgavenGeenAftrek
     * @param float $andereVerworpenUitgavenWelAftrek
     * @param float $aftrekBewerking
     * @param float $grondslagVenb
     * @param float $belastingenOpDeGrondslag
     * @param float $berekeningVanDeVermeerdering
     * @param float $voorafbetalingQ1
     * @param float $voorafbetalingQ2
     * @param float $voorafbetalingQ3
     * @param float $voorafbetalingQ4
     * @param float $terugbetaalbareVoorheffing
     * @param float $geraamdeBelastingenGewoon
     * @param float $regularisatieVorigeJaren
     * @param float $belastingenLiquidatieReserve
     * @param float $taxShelterKost
     * @param float $aanpassingenInMeer
     * @param float $taxShelterInvestering
     * @param float $grondslagLiquidatieReserve
     */
    public function __construct(
        bool $maxTaxShelter,
        float $winstVoorBelastingen,
        float $dividenden,
        float $tantiemes,
        float $belastingvrijeReserve,
        float $taxShelterVrijstelling,
        float $belasteReserve,
        float $andereMutaties,
        float $andereVerworpenUitgavenGeenAftrek,
        float $andereVerworpenUitgavenWelAftrek,
        float $aftrekBewerking,
        float $grondslagVenb,
        float $belastingenOpDeGrondslag,
        float $berekeningVanDeVermeerdering,
        float $voorafbetalingQ1,
        float $voorafbetalingQ2,
        float $voorafbetalingQ3,
        float $voorafbetalingQ4,
        float $terugbetaalbareVoorheffing,
        float $geraamdeBelastingenGewoon,
        float $regularisatieVorigeJaren,
        float $belastingenLiquidatieReserve,
        float $taxShelterKost,
        float $aanpassingenInMeer,
        float $taxShelterInvestering,
        float $grondslagLiquidatieReserve,
        float $belastingenOpDeGrondslagVoor,
        float $maximaleInvestering,
        int $aanslagjaar,
        DateTimeInterface $paymentDate
    ) {
        $this->winstVoorBelastingen = $winstVoorBelastingen;
        $this->tantiemes = $tantiemes;
        $this->taxShelterVrijstelling = $taxShelterVrijstelling;
        $this->andereMutaties = $andereMutaties;
        $this->andereVerworpenUitgavenGeenAftrek = $andereVerworpenUitgavenGeenAftrek;
        $this->andereVerworpenUitgavenWelAftrek = $andereVerworpenUitgavenWelAftrek;
        $this->aftrekBewerking = $aftrekBewerking;
        $this->grondslagVenb = $grondslagVenb;
        $this->belastingenOpDeGrondslag = $belastingenOpDeGrondslag;
        $this->berekeningVanDeVermeerdering = $berekeningVanDeVermeerdering;
        $this->voorafbetalingQ1 = $voorafbetalingQ1;
        $this->voorafbetalingQ2 = $voorafbetalingQ2;
        $this->voorafbetalingQ3 = $voorafbetalingQ3;
        $this->voorafbetalingQ4 = $voorafbetalingQ4;
        $this->terugbetaalbareVoorheffing = $terugbetaalbareVoorheffing;
        $this->geraamdeBelastingenGewoon = $geraamdeBelastingenGewoon;
        $this->regularisatieVorigeJaren = $regularisatieVorigeJaren;
        $this->belastingenLiquidatieReserve = $belastingenLiquidatieReserve;
        $this->taxShelterKost = $taxShelterKost;
        $this->aanpassingenInMeer = $aanpassingenInMeer;
        $this->taxShelterInvestering = $taxShelterInvestering;
        $this->grondslagLiquidatieReserve = $grondslagLiquidatieReserve;
        $this->dividenden = $dividenden;
        $this->belasteReserve = $belasteReserve;
        $this->belastingvrijeReserve = $belastingvrijeReserve;
        $this->maxTaxShelter = $maxTaxShelter;
        $this->belastingenOpDeGrondslagVoor = $belastingenOpDeGrondslagVoor;
        $this->maximaleInvestering = $maximaleInvestering;
        $this->aanslagjaar = $aanslagjaar;
        $this->paymentDate = $paymentDate;
    }

    /**
     * @return DateTimeInterface
     */
    public function getPaymentDate(): DateTimeInterface
    {
        return $this->paymentDate;
    }

    /**
     * @return float
     */
    public function getWinstVoorBelastingen(): float
    {
        return $this->winstVoorBelastingen;
    }

    /**
     * @return float
     */
    public function getTantiemes(): float
    {
        return $this->tantiemes;
    }

    /**
     * @return float
     */
    public function getTaxShelterVrijstelling(): float
    {
        return $this->taxShelterVrijstelling;
    }

    /**
     * @return float
     */
    public function getAndereMutaties(): float
    {
        return $this->andereMutaties;
    }

    /**
     * @return float
     */
    public function getAndereVerworpenUitgavenGeenAftrek(): float
    {
        return $this->andereVerworpenUitgavenGeenAftrek;
    }

    /**
     * @return float
     */
    public function getAndereVerworpenUitgavenWelAftrek(): float
    {
        return $this->andereVerworpenUitgavenWelAftrek;
    }

    /**
     * @return float
     */
    public function getAftrekBewerking(): float
    {
        return $this->aftrekBewerking;
    }

    /**
     * @return float
     */
    public function getGrondslagVenb(): float
    {
        return $this->grondslagVenb;
    }

    /**
     * @return float
     */
    public function getBelastingenOpDeGrondslag(): float
    {
        return $this->belastingenOpDeGrondslag;
    }

    /**
     * @return float
     */
    public function getBerekeningVanDeVermeerdering(): float
    {
        return $this->berekeningVanDeVermeerdering;
    }

    /**
     * @return float
     */
    public function getVoorafbetalingQ1(): float
    {
        return $this->voorafbetalingQ1;
    }

    /**
     * @return float
     */
    public function getVoorafbetalingQ2(): float
    {
        return $this->voorafbetalingQ2;
    }

    /**
     * @return float
     */
    public function getVoorafbetalingQ3(): float
    {
        return $this->voorafbetalingQ3;
    }

    /**
     * @return float
     */
    public function getVoorafbetalingQ4(): float
    {
        return $this->voorafbetalingQ4;
    }

    /**
     * @return float
     */
    public function getTerugbetaalbareVoorheffing(): float
    {
        return $this->terugbetaalbareVoorheffing;
    }

    /**
     * @return float
     */
    public function getGeraamdeBelastingenGewoon(): float
    {
        return $this->geraamdeBelastingenGewoon;
    }

    /**
     * @return float
     */
    public function getRegularisatieVorigeJaren(): float
    {
        return $this->regularisatieVorigeJaren;
    }

    /**
     * @return float
     */
    public function getBelastingenLiquidatieReserve(): float
    {
        return $this->belastingenLiquidatieReserve;
    }

    /**
     * @return float
     */
    public function getTaxShelterKost(): float
    {
        return $this->taxShelterKost;
    }

    /**
     * @return float
     */
    public function getAanpassingenInMeer(): float
    {
        return $this->aanpassingenInMeer;
    }

    /**
     * @return float
     */
    public function getTaxShelterInvestering(): float
    {
        return $this->taxShelterInvestering;
    }

    /**
     * @return float
     */
    public function getGrondslagLiquidatieReserve(): float
    {
        return $this->grondslagLiquidatieReserve;
    }

    /**
     * @return float
     */
    public function getDividenden(): float
    {
        return $this->dividenden;
    }

    /**
     * @return float
     */
    public function getBelasteReserve(): float
    {
        return $this->belasteReserve;
    }

    /**
     * @return float
     */
    public function getBelastingvrijeReserve(): float
    {
        return $this->belastingvrijeReserve;
    }

    /**
     * @return bool
     */
    public function isMaxTaxShelter(): bool
    {
        return $this->maxTaxShelter;
    }

    /**
     * @return float
     */
    public function getBelastingenOpDeGrondslagVoor(): float
    {
        return $this->belastingenOpDeGrondslagVoor;
    }

    /**
     * @return float
     */
    public function getMaximaleInvestering(): float
    {
        return  $this->maximaleInvestering;
    }

    /**
     * @return int
     */
    public function getAanslagjaar(): int
    {
        return $this->aanslagjaar;
    }
}

<?php
/** @noinspection InterfacesAsConstructorDependenciesInspection */
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Raming;

use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;

/**
 * Class RamingOutput
 * @package Laudis\Calculators\TaxShelter\Raming
 */
class RamingOutput implements CalculationResultInterface
{
    /** @var ColumnOutput|null */
    private $winstVoorBelastingen;
    /** @var ColumnOutput|null */
    private $dividenden;
    /** @var ColumnOutput|null */
    private $tantiemes;
    /** @var ColumnOutput|null */
    private $belastingvrijeReserveTaxShelter;
    /** @var ColumnOutput|null */
    private $vrijstellingTaxShelter;
    /** @var ColumnOutput|null */
    private $belasteReserveTaxShelter;
    /** @var ColumnOutput|null */
    private $andereMutatiesReserves;
    /** @var ColumnOutput|null */
    private $andereVerworpenUitgavenGeenAftrekverbod;
    /** @var ColumnOutput|null */
    private $andereVerworpenUitgavenWelAftrekverbod;
    /** @var ColumnOutput|null */
    private $aftrekBewerkingen;
    /** @var ColumnOutput|null */
    private $grondslagVenB;
    /** @var ColumnOutput|null */
    private $belastingenOpDeGrondslag;
    /** @var ColumnOutput|null */
    private $berekeningVanDeVermeerdering;
    /** @var ColumnOutput|null */
    private $voorafbetalingQ1;
    /** @var ColumnOutput|null */
    private $voorafbetalingQ2;
    /** @var ColumnOutput|null */
    private $voorafbetalingQ3;
    /** @var ColumnOutput|null */
    private $voorafbetalingQ4;
    /** @var ColumnOutput|null */
    private $terugbetaalbareVoorheffingGewoon;
    /** @var ColumnOutput|null */
    private $geraamdeBelastingenGewoon;
    /** @var ColumnOutput|null */
    private $regularisatieVorigeJaren;
    /** @var ColumnOutput|null */
    private $belastingLiquidatieReserve;
    /** @var ColumnOutput|null */
    private $aanpassingInMeer;
    /** @var ColumnOutput|null */
    private $nietAftrekbareTaxShelterKost;
    /** @var ColumnOutput|null */
    private $samenvatting;
    /** @var TaxShelterAggregatie */
    private $taxShelterAggregatie;
    /** @var float|null */
    private $aangelegdeLiquidatieReserve;
    /** @var RamingPresenter */
    private $presenter;
    /**
     * @var TaxShelterRatios
     */
    private $taxShelterRatios;
    /**
     * @var TaxShelterOutput2018
     */
    private $shelterOutput;

    /**
     * RamingOutput constructor.
     * @param RamingPresenter $presenter
     * @param TaxShelterOutput2018 $shelterOutput
     */
    public function __construct(RamingPresenter $presenter, TaxShelterOutput2018 $shelterOutput)
    {
        $this->presenter = $presenter;
        $this->shelterOutput = $shelterOutput;
    }

    /**
     * @return TaxShelterOutput2018
     */
    public function getTaxShelterOutput(): TaxShelterOutput2018
    {
        return $this->shelterOutput;
    }

    /**
     * {@inheritDoc}
     */
    public function output(): array
    {
        return $this->presenter->present($this);
    }

    /**
     * @return ColumnOutput|null
     */
    public function getWinstVoorBelastingen(): ?ColumnOutput
    {
        return $this->winstVoorBelastingen;
    }

    /**
     * @param ColumnOutput|null $winstVoorBelastingen
     */
    public function setWinstVoorBelastingen(?ColumnOutput $winstVoorBelastingen): void
    {
        $this->winstVoorBelastingen = $winstVoorBelastingen;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getDividenden(): ?ColumnOutput
    {
        return $this->dividenden;
    }

    /**
     * @param ColumnOutput|null $dividenden
     */
    public function setDividenden(?ColumnOutput $dividenden): void
    {
        $this->dividenden = $dividenden;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getTantiemes(): ?ColumnOutput
    {
        return $this->tantiemes;
    }

    /**
     * @param ColumnOutput|null $tantiemes
     */
    public function setTantiemes(?ColumnOutput $tantiemes): void
    {
        $this->tantiemes = $tantiemes;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getBelastingvrijeReserveTaxShelter(): ?ColumnOutput
    {
        return $this->belastingvrijeReserveTaxShelter;
    }

    /**
     * @param ColumnOutput|null $belastingvrijeReserveTaxShelter
     */
    public function setBelastingvrijeReserveTaxShelter(?ColumnOutput $belastingvrijeReserveTaxShelter): void
    {
        $this->belastingvrijeReserveTaxShelter = $belastingvrijeReserveTaxShelter;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getVrijstellingTaxShelter(): ?ColumnOutput
    {
        return $this->vrijstellingTaxShelter;
    }

    /**
     * @param ColumnOutput|null $vrijstellingTaxShelter
     */
    public function setVrijstellingTaxShelter(?ColumnOutput $vrijstellingTaxShelter): void
    {
        $this->vrijstellingTaxShelter = $vrijstellingTaxShelter;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getBelasteReserveTaxShelter(): ?ColumnOutput
    {
        return $this->belasteReserveTaxShelter;
    }

    /**
     * @param ColumnOutput|null $belasteReserveTaxShelter
     */
    public function setBelasteReserveTaxShelter(?ColumnOutput $belasteReserveTaxShelter): void
    {
        $this->belasteReserveTaxShelter = $belasteReserveTaxShelter;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getAndereMutatiesReserves(): ?ColumnOutput
    {
        return $this->andereMutatiesReserves;
    }

    /**
     * @param ColumnOutput|null $andereMutatiesReserves
     */
    public function setAndereMutatiesReserves(?ColumnOutput $andereMutatiesReserves): void
    {
        $this->andereMutatiesReserves = $andereMutatiesReserves;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getAndereVerworpenUitgavenGeenAftrekverbod(): ?ColumnOutput
    {
        return $this->andereVerworpenUitgavenGeenAftrekverbod;
    }

    /**
     * @param ColumnOutput|null $andereVerworpenUitgavenGeenAftrekverbod
     */
    public function setAndereVerworpenUitgavenGeenAftrekverbod(?ColumnOutput $andereVerworpenUitgavenGeenAftrekverbod
    ): void {
        $this->andereVerworpenUitgavenGeenAftrekverbod = $andereVerworpenUitgavenGeenAftrekverbod;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getAndereVerworpenUitgavenWelAftrekverbod(): ?ColumnOutput
    {
        return $this->andereVerworpenUitgavenWelAftrekverbod;
    }

    /**
     * @param ColumnOutput|null $andereVerworpenUitgavenWelAftrekverbod
     */
    public function setAndereVerworpenUitgavenWelAftrekverbod(?ColumnOutput $andereVerworpenUitgavenWelAftrekverbod
    ): void {
        $this->andereVerworpenUitgavenWelAftrekverbod = $andereVerworpenUitgavenWelAftrekverbod;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getAftrekBewerkingen(): ?ColumnOutput
    {
        return $this->aftrekBewerkingen;
    }

    /**
     * @param ColumnOutput|null $aftrekBewerkingen
     */
    public function setAftrekBewerkingen(?ColumnOutput $aftrekBewerkingen): void
    {
        $this->aftrekBewerkingen = $aftrekBewerkingen;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getGrondslagVenB(): ?ColumnOutput
    {
        return $this->grondslagVenB;
    }

    /**
     * @param ColumnOutput|null $grondslagVenB
     */
    public function setGrondslagVenB(?ColumnOutput $grondslagVenB): void
    {
        $this->grondslagVenB = $grondslagVenB;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getBelastingenOpDeGrondslag(): ?ColumnOutput
    {
        return $this->belastingenOpDeGrondslag;
    }

    /**
     * @param ColumnOutput|null $belastingenOpDeGrondslag
     */
    public function setBelastingenOpDeGrondslag(?ColumnOutput $belastingenOpDeGrondslag): void
    {
        $this->belastingenOpDeGrondslag = $belastingenOpDeGrondslag;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getBerekeningVanDeVermeerdering(): ?ColumnOutput
    {
        return $this->berekeningVanDeVermeerdering;
    }

    /**
     * @param ColumnOutput|null $berekeningVanDeVermeerdering
     */
    public function setBerekeningVanDeVermeerdering(?ColumnOutput $berekeningVanDeVermeerdering): void
    {
        $this->berekeningVanDeVermeerdering = $berekeningVanDeVermeerdering;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getVoorafbetalingQ1(): ?ColumnOutput
    {
        return $this->voorafbetalingQ1;
    }

    /**
     * @param ColumnOutput|null $voorafbetalingQ1
     */
    public function setVoorafbetalingQ1(?ColumnOutput $voorafbetalingQ1): void
    {
        $this->voorafbetalingQ1 = $voorafbetalingQ1;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getVoorafbetalingQ2(): ?ColumnOutput
    {
        return $this->voorafbetalingQ2;
    }

    /**
     * @param ColumnOutput|null $voorafbetalingQ2
     */
    public function setVoorafbetalingQ2(?ColumnOutput $voorafbetalingQ2): void
    {
        $this->voorafbetalingQ2 = $voorafbetalingQ2;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getVoorafbetalingQ3(): ?ColumnOutput
    {
        return $this->voorafbetalingQ3;
    }

    /**
     * @param ColumnOutput|null $voorafbetalingQ3
     */
    public function setVoorafbetalingQ3(?ColumnOutput $voorafbetalingQ3): void
    {
        $this->voorafbetalingQ3 = $voorafbetalingQ3;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getVoorafbetalingQ4(): ?ColumnOutput
    {
        return $this->voorafbetalingQ4;
    }

    /**
     * @param ColumnOutput|null $voorafbetalingQ4
     */
    public function setVoorafbetalingQ4(?ColumnOutput $voorafbetalingQ4): void
    {
        $this->voorafbetalingQ4 = $voorafbetalingQ4;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getTerugbetaalbareVoorheffingGewoon(): ?ColumnOutput
    {
        return $this->terugbetaalbareVoorheffingGewoon;
    }

    /**
     * @param ColumnOutput|null $terugbetaalbareVoorheffingGewoon
     */
    public function setTerugbetaalbareVoorheffingGewoon(?ColumnOutput $terugbetaalbareVoorheffingGewoon): void
    {
        $this->terugbetaalbareVoorheffingGewoon = $terugbetaalbareVoorheffingGewoon;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getGeraamdeBelastingenGewoon(): ?ColumnOutput
    {
        return $this->geraamdeBelastingenGewoon;
    }

    /**
     * @param ColumnOutput|null $geraamdeBelastingenGewoon
     */
    public function setGeraamdeBelastingenGewoon(?ColumnOutput $geraamdeBelastingenGewoon): void
    {
        $this->geraamdeBelastingenGewoon = $geraamdeBelastingenGewoon;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getRegularisatieVorigeJaren(): ?ColumnOutput
    {
        return $this->regularisatieVorigeJaren;
    }

    /**
     * @param ColumnOutput|null $regularisatieVorigeJaren
     */
    public function setRegularisatieVorigeJaren(?ColumnOutput $regularisatieVorigeJaren): void
    {
        $this->regularisatieVorigeJaren = $regularisatieVorigeJaren;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getBelastingLiquidatieReserve(): ?ColumnOutput
    {
        return $this->belastingLiquidatieReserve;
    }

    /**
     * @param ColumnOutput|null $belastingLiquidatieReserve
     */
    public function setBelastingLiquidatieReserve(?ColumnOutput $belastingLiquidatieReserve): void
    {
        $this->belastingLiquidatieReserve = $belastingLiquidatieReserve;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getAanpassingInMeer(): ?ColumnOutput
    {
        return $this->aanpassingInMeer;
    }

    /**
     * @param ColumnOutput|null $aanpassingInMeer
     */
    public function setAanpassingInMeer(?ColumnOutput $aanpassingInMeer): void
    {
        $this->aanpassingInMeer = $aanpassingInMeer;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getNietAftrekbareTaxShelterKost(): ?ColumnOutput
    {
        return $this->nietAftrekbareTaxShelterKost;
    }

    /**
     * @param ColumnOutput|null $nietAftrekbareTaxShelterKost
     */
    public function setNietAftrekbareTaxShelterKost(?ColumnOutput $nietAftrekbareTaxShelterKost): void
    {
        $this->nietAftrekbareTaxShelterKost = $nietAftrekbareTaxShelterKost;
    }

    /**
     * @return ColumnOutput|null
     */
    public function getSamenvatting(): ?ColumnOutput
    {
        return $this->samenvatting;
    }

    /**
     * @param ColumnOutput|null $samenvatting
     */
    public function setSamenvatting(?ColumnOutput $samenvatting): void
    {
        $this->samenvatting = $samenvatting;
    }

    /**
     * @return TaxShelterAggregatie
     */
    public function getTaxShelterAggregatie(): TaxShelterAggregatie
    {
        return $this->taxShelterAggregatie;
    }

    /**
     * @param TaxShelterAggregatie $taxShelterAggregatie
     */
    public function setTaxShelterAggregatie(TaxShelterAggregatie $taxShelterAggregatie): void
    {
        $this->taxShelterAggregatie = $taxShelterAggregatie;
    }

    /**
     * @return float|null
     */
    public function getAangelegdeLiquidatieReserve(): ?float
    {
        return $this->aangelegdeLiquidatieReserve;
    }

    /**
     * @param float|null $aangelegdeLiquidatieReserve
     */
    public function setAangelegdeLiquidatieReserve(?float $aangelegdeLiquidatieReserve): void
    {
        $this->aangelegdeLiquidatieReserve = $aangelegdeLiquidatieReserve;
    }

    /**
     * @param TaxShelterRatios $param
     */
    public function setTaxShelterRatios(TaxShelterRatios $param): void
    {
        $this->taxShelterRatios = $param;
    }

    /**
     * @return TaxShelterRatios
     */
    public function getTaxShelterRatios(): TaxShelterRatios
    {
        return $this->taxShelterRatios;
    }

    /**
     * @param int $outputMode
     */
    public function setOutputMode(int $outputMode): void
    {
        // irrelevant
    }
}

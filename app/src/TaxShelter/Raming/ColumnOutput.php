<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Raming;

use Laudis\Calculators\Contracts\CalculationResultInterface;

/**
 * Class ColumnInput
 * @package Laudis\Calculators\TaxShelter\Raming
 */
final class ColumnOutput implements CalculationResultInterface
{
    /** @var float|null */
    private $aanslagBerekeingInBelastingen;
    /** @var float|null */
    private $aangroeiVanDeReserves;
    /** @var float|null */
    private $winstVerliesBoekjaar;

    /**
     * ColumnInput constructor.
     * @param float $aanslagBerekeingInBelastingen
     * @param float $aangroeiVanDeReserves
     * @param float $winstVerliesBoekjaar
     */
    public function __construct(
        ?float $aanslagBerekeingInBelastingen,
        ?float $aangroeiVanDeReserves,
        ?float $winstVerliesBoekjaar
    ) {
        $this->aanslagBerekeingInBelastingen = $aanslagBerekeingInBelastingen;
        $this->aangroeiVanDeReserves = $aangroeiVanDeReserves;
        $this->winstVerliesBoekjaar = $winstVerliesBoekjaar;
    }

    /**
     * @param float|null $aanslagBerekeingInBelastingen
     * @param float|null $aangroeiVanDeReserves
     * @param float|null $winstVerliesBoekjaar
     * @return ColumnOutput
     */
    public static function make(
        ?float $aanslagBerekeingInBelastingen,
        ?float $aangroeiVanDeReserves,
        ?float $winstVerliesBoekjaar
    ): ColumnOutput {
        return new ColumnOutput($aanslagBerekeingInBelastingen, $aangroeiVanDeReserves, $winstVerliesBoekjaar);
    }

    /**
     * @return float|null
     */
    public function getAanslagBerekeingInBelastingen(): ?float
    {
        return $this->aanslagBerekeingInBelastingen;
    }

    /**
     * @return float|null
     */
    public function getAangroeiVanDeReserves(): ?float
    {
        return $this->aangroeiVanDeReserves;
    }

    /**
     * @return float|null
     */
    public function getWinstVerliesBoekjaar(): ?float
    {
        return $this->winstVerliesBoekjaar;
    }

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array
    {
        return [
            'aanslagBerekeningBelastingen' => $this->aanslagBerekeingInBelastingen,
            'aangroeiVanDeReserves' => $this->aangroeiVanDeReserves,
            'winstVerliesBoekjaar' => $this->winstVerliesBoekjaar
        ];
    }

    /**
     * @param int $outputMode
     */
    public function setOutputMode(int $outputMode): void
    {
        // irrelevant
    }
}

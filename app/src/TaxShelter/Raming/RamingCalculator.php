<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Raming;

use DateTime;
use Exception;
use Laudis\Calculators\TaxShelter\Adapters\TaxShelterToRaming;
use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\TaxShelterCalculator;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;
use Laudis\Calculators\TaxShelter\TaxShelterRatioPresenter;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use function min;
use function simple_date;

/**
 * Class RamingCalculator
 * @package Laudis\Calculators\TaxShelter\Raming
 */
class RamingCalculator
{
    /** @var TaxShelterCalculator */
    private $taxShelterCalculator;
    /** @var TaxShelterToRaming */
    private $adapter;
    /** @var ContextualIndexedValueRepositoryInterface */
    private $indexedValues;

    /**
     * RamingCalculator constructor.
     * @param TaxShelterCalculator $taxShelterCalculator
     * @param TaxShelterToRaming $adapter
     * @param ContextualIndexedValueRepositoryInterface $indexedValues
     */
    public function __construct(
        TaxShelterCalculator $taxShelterCalculator,
        TaxShelterToRaming $adapter,
        ContextualIndexedValueRepositoryInterface $indexedValues
    ) {
        $this->taxShelterCalculator = $taxShelterCalculator;
        $this->adapter = $adapter;
        $this->indexedValues = $indexedValues;
    }

    /**
     * @param TaxShelterInput $taxShelterInput
     * @return RamingOutput
     * @throws Exception
     */
    public function calculate(TaxShelterInput $taxShelterInput): RamingOutput
    {
        $taxShelterResult = $this->taxShelterCalculator->calculate($taxShelterInput);

        $optimalResult = $this->recalculateOptimalResultIfNeeded($taxShelterInput, $taxShelterResult);

        $input = $this->adapter->adapt($optimalResult, $taxShelterInput);
        $maxTsInput = $this->calculateMaxTaxShelterInput($taxShelterInput, $input);

        $output = $this->makeRamingOutput($optimalResult);

        $this->boekhoudkundig($input, $output);
        $this->fiscaalResultaat($input, $output);
        $this->nietAftrekbareBelastingen($input, $output);
        $this->samenvattingOverzicht($input, $output);
        $this->aggregatie($input, $output, $maxTsInput);

        return $output;
    }

    private function calculateOptimalTaxShelterInvestment(TaxShelterInput $input, TaxShelterOutput2018 $output): float
    {
        $grondslag = array_last($output->getGrondslagenVenBen());
        $maximaleVrijstelling = array_last($output->getMaximaleVrijstellingen());
        $vrijstellingsPct = $this->indexedValues->getFromDate(
            DateTime::createFromFormat('Y-m-d', $input->getAanslagjaar() - 1 . '-01-01'),
            'vrijstellingspercentage'
        )->getValue();

        return min(
            max(($grondslag + $maximaleVrijstelling - 100000) / $vrijstellingsPct, 0),
            array_last($output->getMaximaleInvesteringen())
        );
    }

    /**
     * @param TaxShelterInput $taxShelterInput
     * @param RamingInput $input
     * @return RamingInput
     */
    private function calculateMaxTaxShelterInput(TaxShelterInput $taxShelterInput, RamingInput $input): RamingInput
    {
        if ($taxShelterInput->isMaxTaxShelter() || $this->isOverinvestering($input)) {
            return $input;
        }
        // This must be an under investment
        return $this->alsoCalculateMaxTsInput($taxShelterInput);
    }

    /**
     * @param RamingInput $input
     * @return bool
     */
    private function isOverinvestering(RamingInput $input): bool
    {
        return $input->getMaximaleInvestering() < $input->getTaxShelterInvestering();
    }

    /**
     * @param TaxShelterInput $taxShelterInput
     * @return RamingInput
     */
    private function alsoCalculateMaxTsInput(TaxShelterInput $taxShelterInput): RamingInput
    {
        $taxShelterInput->setMaxTaxShelter(true);
        $maxTsOutput = $this->taxShelterCalculator->calculate($taxShelterInput);
        $maxTsInput = $this->adapter->adapt($maxTsOutput, $taxShelterInput);
        $taxShelterInput->setMaxTaxShelter(false);
        return $maxTsInput;
    }

    /**
     * @param TaxShelterOutput2018 $taxShelterResult
     * @return RamingOutput
     */
    protected function makeRamingOutput(TaxShelterOutput2018 $taxShelterResult): RamingOutput
    {
        return new RamingOutput(new RamingPresenter($taxShelterResult), $taxShelterResult);
    }

    /**
     * @param RamingInput $input
     * @param RamingOutput $output
     */
    private function boekhoudkundig(RamingInput $input, RamingOutput $output): void
    {
        $val = $input->getWinstVoorBelastingen();
        $output->setWinstVoorBelastingen(ColumnOutput::make($val, $val, $val));
        $val = $input->getDividenden();
        $output->setDividenden(ColumnOutput::make(null, -1 * $val, null));
        $val = $input->getTantiemes();
        $output->setTantiemes(ColumnOutput::make(-1 * $val, -1 * $val, null));
        $val = $input->getBelastingvrijeReserve();
        $output->setBelastingvrijeReserveTaxShelter(ColumnOutput::make(null, -1 * $val, -1 * $val));
    }

    /**
     * @param RamingInput $input
     * @param RamingOutput $output
     */
    private function fiscaalResultaat(RamingInput $input, RamingOutput $output): void
    {
        $val = $input->getTaxShelterVrijstelling();
        $output->setVrijstellingTaxShelter(ColumnOutput::make(-1 * $val, null, null));
        $val = $input->getBelasteReserve();
        $output->setBelasteReserveTaxShelter(ColumnOutput::make(null, $val, null));
        $val = $input->getAndereMutaties();
        $output->setAndereMutatiesReserves(ColumnOutput::make($val, $val, null));
        $val = $input->getAndereVerworpenUitgavenGeenAftrek();
        $output->setAndereVerworpenUitgavenGeenAftrekverbod(ColumnOutput::make($val, null, null));
        $val = $input->getAndereVerworpenUitgavenWelAftrek();
        $output->setAndereVerworpenUitgavenWelAftrekverbod(ColumnOutput::make($val, null, null));
        $val = $input->getAftrekBewerking();
        $output->setAftrekBewerkingen(ColumnOutput::make(-1 * $val, null, null));
        $val = $input->getGrondslagVenb();
        $output->setGrondslagVenB(ColumnOutput::make($val, null, null));
        $val = $input->getBelastingenOpDeGrondslag();
        $output->setBelastingenOpDeGrondslag(ColumnOutput::make($val, null, null));
        $val = $input->getBerekeningVanDeVermeerdering();
        $output->setBerekeningVanDeVermeerdering(ColumnOutput::make($val, null, null));
    }

    /**
     * @param RamingInput $input
     * @param RamingOutput $output
     */
    private function nietAftrekbareBelastingen(RamingInput $input, RamingOutput $output): void
    {
        $val = $input->getVoorafbetalingQ1();
        $output->setVoorafbetalingQ1(ColumnOutput::make(-1 * $val, null, -1 * $val));
        $val = $input->getVoorafbetalingQ2();
        $output->setVoorafbetalingQ2(ColumnOutput::make(-1 * $val, null, -1 * $val));
        $val = $input->getVoorafbetalingQ3();
        $output->setVoorafbetalingQ3(ColumnOutput::make(-1 * $val, null, -1 * $val));
        $val = $input->getVoorafbetalingQ4();
        $output->setVoorafbetalingQ4(ColumnOutput::make(-1 * $val, null, -1 * $val));
        $val = $input->getTerugbetaalbareVoorheffing();
        $output->setTerugbetaalbareVoorheffingGewoon(ColumnOutput::make(-1 * $val, null, -1 * $val));
        $val = $input->getGeraamdeBelastingenGewoon();
        $output->setGeraamdeBelastingenGewoon(ColumnOutput::make(-1 * $val, null, -1 * $val));
        $val = $input->getRegularisatieVorigeJaren();
        $output->setRegularisatieVorigeJaren(ColumnOutput::make(-1 * $val, null, -1 * $val));
        $val = $input->getBelastingenLiquidatieReserve();
        $output->setBelastingLiquidatieReserve(ColumnOutput::make(-1 * $val, -1 * $val, -1 * $val));
        $val = $input->getAanpassingenInMeer();
        $output->setAanpassingInMeer(ColumnOutput::make(null, -1 * $val, null));
        $val = $input->getTaxShelterKost();
        $output->setNietAftrekbareTaxShelterKost(ColumnOutput::make(-1 * $val, -1 * $val, null));
    }

    /**
     * @param RamingInput $input
     * @param RamingOutput $output
     */
    private function samenvattingOverzicht(RamingInput $input, RamingOutput $output): void
    {
        $output->setSamenvatting(ColumnOutput::make(
            null,
            $this->aangroeiReserves($input),
            $this->winstVerliesVanHetBoekjaar($input)
        ));
    }

    /**
     * @param RamingInput $input
     * @return float
     */
    private function aangroeiReserves(RamingInput $input): float
    {
        return $input->getWinstVoorBelastingen()
            - $input->getDividenden()
            - $input->getTantiemes()
            - $input->getBelastingvrijeReserve()
            + $input->getBelasteReserve()
            + $input->getAndereMutaties()
            - $input->getBelastingenLiquidatieReserve()
            - $input->getAanpassingenInMeer()
            - $input->getTaxShelterKost();
    }

    /**
     * @param RamingInput $input
     * @return float
     */
    private function winstVerliesVanHetBoekjaar(RamingInput $input): float
    {
        return $input->getWinstVoorBelastingen()
            - $input->getBelastingvrijeReserve()
            - $input->getVoorafbetalingQ1()
            - $input->getVoorafbetalingQ2()
            - $input->getVoorafbetalingQ3()
            - $input->getVoorafbetalingQ4()
            - $input->getTerugbetaalbareVoorheffing()
            - $input->getGeraamdeBelastingenGewoon()
            - $input->getRegularisatieVorigeJaren()
            - $input->getBelastingenLiquidatieReserve()
            - $input->getTaxShelterKost();
    }

    /**
     * @param RamingInput $input
     * @param RamingOutput $output
     * @param RamingInput $maxTsInput
     * @throws Exception
     */
    private function aggregatie(RamingInput $input, RamingOutput $output, RamingInput $maxTsInput): void
    {
        $this->taxShelterAggregatie($input, $output, $maxTsInput);
        $this->gegevenRatios($input, $output);
        $output->setAangelegdeLiquidatieReserve($input->getGrondslagLiquidatieReserve());
    }

    /**
     * @param RamingInput $input
     * @param RamingOutput $output
     * @param RamingInput $maxTsInput
     */
    private function taxShelterAggregatie(RamingInput $input, RamingOutput $output, RamingInput $maxTsInput): void
    {
        $output->setTaxShelterAggregatie(new TaxShelterAggregatie(
            $maxTsInput->getTaxShelterKost(),
            $input->getTaxShelterVrijstelling(),
            $input->getBelastingenOpDeGrondslagVoor(),
            $input->getBelastingenOpDeGrondslag(),
            $input->getTaxShelterInvestering(),
            $maxTsInput->getTaxShelterKost() - $input->getTaxShelterInvestering(),
            $input->getTaxShelterInvestering() - $maxTsInput->getTaxShelterKost(),
            round($input->getTaxShelterInvestering()) > round($maxTsInput->getTaxShelterKost()),
            new GegevenTaxShelterOutputPresenter()
        ));
    }

    /**
     * @param RamingInput $input
     * @param RamingOutput $output
     * @throws Exception
     */
    private function gegevenRatios(RamingInput $input, RamingOutput $output): void
    {
        /** @var TaxShelterAggregatie $tsOutput */
        $tsOutput = $output->getTaxShelterAggregatie();

        $fiscaalRendement = $tsOutput->getBelastingZonderTaxShelterInvestering()
            - $tsOutput->getBelastingMetTaxShelterInvestering();
        $date = simple_date($input->getAanslagjaar() - 1, 1, 1);
        $euribor = $this->indexedValues->getFromDate($input->getPaymentDate(), 'euribor')->getValue();
        $gewoonTarief = $this->indexedValues->getFromDate($date, 'gewoon_tarief')->getValue();
        $grondslagRendement = $this->grondslagRendement($input);
        $fiscaalRendementPct = $this->calculateFiscaalRendementPct($input, $grondslagRendement, $fiscaalRendement);
        $financieelRendementPct = $euribor * 18 / 12;
        $nettoFinancieelRendementPct = (1 - $gewoonTarief) * $financieelRendementPct;
        $nettoFinancieelRendement = $nettoFinancieelRendementPct * $this->grondslagRendement($input);
        $financieelRendement = $financieelRendementPct * $this->grondslagRendement($input);
        $fiscaalRendement -= $this->isOverinvestering($input) ? $input->getMaximaleInvestering() :
            $this->grondslagRendement($input);
        $vrijstelingPct = $this->indexedValues->getFromDate($date, 'vrijstellingspercentage')->getValue();
        $overdrachtRendement = $this->isOverinvestering($input) ?
            $this->overinvestering($input) * ($vrijstelingPct * $gewoonTarief - 1) :
            null;
        $overdrachtRendementsPercentage = $overdrachtRendement === null ? null : ($vrijstelingPct * $gewoonTarief - 1);

        $totaalRendement = $nettoFinancieelRendement + $fiscaalRendement + $overdrachtRendement;
        $output->setTaxShelterRatios(new TaxShelterRatios(
            new TaxShelterRatioPresenter($grondslagRendement),
            $fiscaalRendement,
            $nettoFinancieelRendement,
            $financieelRendement,
            $financieelRendementPct,
            $nettoFinancieelRendementPct,
            $fiscaalRendementPct,
            $euribor,
            $gewoonTarief,
            $totaalRendement,
            $grondslagRendement === 0.0 ? null : $totaalRendement / $grondslagRendement,
            // We took wrong output
            $totaalRendement,
            $grondslagRendement === 0.0 ? null : $totaalRendement / $grondslagRendement,
            $overdrachtRendement,
            $overdrachtRendementsPercentage,
            $date
        ));
    }

    /**
     * @param RamingInput $input
     * @return float
     */
    private function grondslagRendement(RamingInput $input): float
    {
        return $input->isMaxTaxShelter() ? $input->getTaxShelterKost() : $input->getTaxShelterInvestering();
    }

    /**
     * @param RamingInput $input
     * @param float $grondslagRendement
     * @param float $fiscaalRendement
     * @return float|int|null
     */
    private function calculateFiscaalRendementPct(
        RamingInput $input,
        float $grondslagRendement,
        float $fiscaalRendement
    ) {
        if ($grondslagRendement === 0.0) {
            return null;
        }
        if ($this->isOverinvestering($input)) {
            return $fiscaalRendement / min($grondslagRendement, $input->getMaximaleInvestering()) - 1;
        }
        return $fiscaalRendement / $grondslagRendement - 1;
    }

    /**
     * @param RamingInput $input
     * @return float
     */
    private function overinvestering(RamingInput $input): float
    {
        return $input->getTaxShelterInvestering() - $input->getMaximaleInvestering();
    }

    /**
     * @param TaxShelterInput $taxShelterInput
     * @param TaxShelterOutput2018 $taxShelterResult
     * @return TaxShelterOutput2018
     */
    private function recalculateOptimalResultIfNeeded(
        TaxShelterInput $taxShelterInput,
        TaxShelterOutput2018 $taxShelterResult
    ): TaxShelterOutput2018 {
        if ($taxShelterInput->isVerlaagdTarief() && $taxShelterInput->isMaxTaxShelter()) {
            $taxShelterInput->setTaxShelterInvestering(
                $this->calculateOptimalTaxShelterInvestment($taxShelterInput, $taxShelterResult)
            );
            $taxShelterInput->setMaxTaxShelter(false);
            $optimalResult = $this->taxShelterCalculator->calculate($taxShelterInput);
        } else {
            $optimalResult = $taxShelterResult;
        }
        return $optimalResult;
    }
}

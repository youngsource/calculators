<?php

namespace Laudis\Calculators\TaxShelter\Raming;

use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;
use Laudis\Calculators\TaxShelter\VersieAj2019\TaxShelterInput2019;

/**
 * Class RamingCalculator2019
 * @package Laudis\Calculators\TaxShelter\Raming
 */
final class RamingCalculator2019 extends RamingCalculator
{
    /**
     * @var TaxShelterInput|TaxShelterInput2019
     */
    private $input;

    /**
     * @param TaxShelterInput|TaxShelterInput2019 $taxShelterInput
     * @return RamingOutput
     * @throws \Exception
     */
    public function calculate(TaxShelterInput $taxShelterInput): RamingOutput
    {
        $this->input = $taxShelterInput;
        return parent::calculate($taxShelterInput);
    }

    /**
     * @param TaxShelterOutput2018|TaxShelterInput2019 $taxShelterResult
     * @return RamingOutput
     */
    protected function makeRamingOutput(TaxShelterOutput2018 $taxShelterResult): RamingOutput
    {
        return new RamingOutput2019(
            new RamingPresenter2019($taxShelterResult),
            $taxShelterResult,
            $this->input
        );
    }
}

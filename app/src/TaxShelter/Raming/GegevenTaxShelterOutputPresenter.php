<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Raming;

/**
 * Class GegevenTaxShelterOutputPresenter
 * @package Laudis\Calculators\TaxShelter\Raming
 */
final class GegevenTaxShelterOutputPresenter
{
    /**
     * @param TaxShelterAggregatie $shelterOutput
     * @return array
     */
    public function present(TaxShelterAggregatie $shelterOutput): array
    {
        return [
            'maximaleTaxShelterInvestering' => $shelterOutput->getMaximaleTaxShelterInvestering(),
            'maximaleTaxShelterVrijstelling' => $shelterOutput->getMaximaleTaxShelterVrijstelling(),
            'belastingZonderTaxShelterInvestering' => $shelterOutput->getBelastingZonderTaxShelterInvestering(),
            'belastingMetTaxShelterInvestering' => $shelterOutput->getBelastingMetTaxShelterInvestering(),
            'effectieveTaxShelterInvestering' => $shelterOutput->getEffectieveTaxShelterInvestering(),
            'resterendeInvesteringsRuimte' => $shelterOutput->getResterendeInvesteringsruimte(),
            'overinvestering' => $shelterOutput->getOverinvestering(),
            'isOverinvestering' => $shelterOutput->isOverInvestering()
        ];
    }
}

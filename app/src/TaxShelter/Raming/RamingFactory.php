<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Raming;

use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\TaxShelter\Adapters\TaxShelterToRaming;
use Laudis\Calculators\TaxShelter\TaxShelterCalculator;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use LogicException;
use Rakit\Validation\Validation;

/**
 * Class RamingtoolFactory
 * @package Laudis\Calculators\TaxShelter\Raming
 */
final class RamingFactory implements CalculatorFactoryInterface
{
    /** @var CalculatorFactoryInterface */
    private $tsFactory;
    /**
     * @var ContextualIndexedValueRepositoryInterface
     */
    private $valueRepository;

    /**
     * RamingFactory constructor.
     * @param CalculatorFactoryInterface $calculator
     */
    public function __construct(
        CalculatorFactoryInterface $calculator,
        ContextualIndexedValueRepositoryInterface $valueRepository
    ) {
        $this->tsFactory = $calculator;
        $this->valueRepository = $valueRepository;
    }

    /**
     * @param array $values
     * @return RamingCalculator
     */
    public function calculator(array $values): object
    {
        $taxShelterCalculator = $this->tsFactory->calculator($values);
        if (!$taxShelterCalculator instanceof TaxShelterCalculator) {
            throw new LogicException('The provided calculator must be a tax shelter one');
        }
        if ($values['aanslagjaar'] >= 2019) {
            return new RamingCalculator2019(
                $taxShelterCalculator,
                new TaxShelterToRaming,
                $this->valueRepository
            );
        }

        return new RamingCalculator(
            $taxShelterCalculator,
            new TaxShelterToRaming,
            $this->valueRepository
        );
    }

    /**
     * Builds the input as a standard databag from the current server request.
     *
     * @param array $values
     * @return object
     */
    public function inputFromArray(array $values): object
    {
        return $this->tsFactory->inputFromArray($values);
    }

    /**
     * Derives the validation from the current input values.
     *
     * @param array $values
     * @return Validation
     */
    public function validation(array $values): Validation
    {
        return $this->tsFactory->validation($values);
    }
}

<?php

namespace Laudis\Calculators\TaxShelter\Raming;

use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;
use Laudis\Calculators\TaxShelter\VersieAj2019\TaxShelterInput2019;

final class RamingOutput2019 extends RamingOutput
{
    /**
     * @var TaxShelterInput2019
     */
    private $input;

    /**
     * RamingOutput2019 constructor.
     * @param RamingPresenter2019 $presenter
     * @param TaxShelterOutput2018 $shelterOutput
     * @param TaxShelterInput2019 $input
     */
    public function __construct(
        RamingPresenter2019 $presenter,
        TaxShelterOutput2018 $shelterOutput,
        TaxShelterInput2019 $input
    ) {
        parent::__construct($presenter, $shelterOutput);
        $this->input = $input;
    }

    public function getAftrekkenMetKorfBeperking(): float
    {
        return $this->input->getAftrekkenMetKorfBeperking();
    }
}

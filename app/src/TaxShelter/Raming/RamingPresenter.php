<?php
/** @noinspection InterfacesAsConstructorDependenciesInspection */
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Raming;

use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;

/**
 * Class RamingPresenter
 * @package Laudis\Calculators\TaxShelter\Raming
 */
class RamingPresenter
{
    /**
     * @var TaxShelterOutput2018
     */
    private $shelterOutput;

    /**
     * RamingPresenter constructor.
     * @param TaxShelterOutput2018 $shelterOutput
     */
    public function __construct(TaxShelterOutput2018 $shelterOutput)
    {
        $this->shelterOutput = $shelterOutput;
    }

    /**
     * @param RamingOutput $output
     * @return array
     */
    public function present(RamingOutput $output): array
    {
        $aggregatie = $output->getTaxShelterAggregatie()->output();
        $ratios = $output->getTaxShelterRatios()->output();

        /** @noinspection NullPointerExceptionInspection */
        return [
            'taxShelterOutput' => $this->shelterOutput->output(),
            'winstVoorBelastingen' => $output->getWinstVoorBelastingen()->output(),
            'dividenden' => $output->getDividenden()->output(),
            'tantiemes' => $output->getTantiemes()->output(),
            'belastingvrijeReserveTaxShelter' => $output->getBelastingvrijeReserveTaxShelter()->output(),
            'vrijstellingTaxShelter' => $output->getVrijstellingTaxShelter()->output(),
            'belasteReserveTaxShelter' => $output->getBelasteReserveTaxShelter()->output(),
            'andereMutatiesReserves' => $output->getAndereMutatiesReserves()->output(),
            'andereVerworpenUitgavenGeenAftrekverbod' =>
                $output->getAndereVerworpenUitgavenGeenAftrekverbod()->output(),
            'andereVerworpenUitgavenWelAftrekverbod' =>
                $output->getAndereVerworpenUitgavenWelAftrekverbod()->output(),
            'aftrekBewerkingen' => $output->getAftrekBewerkingen()->output(),
            'grondslagVenB' => $output->getGrondslagVenB()->output(),
            'belastingenOpDeGrondslag' => $output->getBelastingenOpDeGrondslag()->output(),
            'berekeningVanDeVermeerdering' => $output->getBerekeningVanDeVermeerdering()->output(),
            'voorafbetalingQ1' => $output->getVoorafbetalingQ1()->output(),
            'voorafbetalingQ2' => $output->getVoorafbetalingQ2()->output(),
            'voorafbetalingQ3' => $output->getVoorafbetalingQ3()->output(),
            'voorafbetalingQ4' => $output->getVoorafbetalingQ4()->output(),
            'terugbetaalbareVoorheffingGewoon' => $output->getTerugbetaalbareVoorheffingGewoon()->output(),
            'geraamdeBelastingenGewoon' => $output->getGeraamdeBelastingenGewoon()->output(),
            'regularisatieVorigeJaren' => $output->getRegularisatieVorigeJaren()->output(),
            'belastingLiquidatieReserve' => $output->getBelastingLiquidatieReserve()->output(),
            'aanpassingInMeer' => $output->getAanpassingInMeer()->output(),
            'nietAftrekbareTaxShelterKost' => $output->getNietAftrekbareTaxShelterKost()->output(),
            'samenvatting' => $output->getSamenvatting()->output(),
            'aggregatie' => $aggregatie,
            'ratios' => $ratios,
            'aangelegdeLiquidatieReserve' => $output->getAangelegdeLiquidatieReserve()
        ];
    }
}

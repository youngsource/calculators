<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Raming;

use DateTimeInterface;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\TaxShelter\TaxShelterRatioPresenter;

/**
 * Class TaxShelterRatios
 * @package Laudis\Calculators\TaxShelter\Raming
 */
final class TaxShelterRatios implements CalculationResultInterface
{
    /**
     * @var float|null
     */
    private $fiscaalRendement;
    /**
     * @var float
     */
    private $nettoFincancieelRendement;
    /**
     * @var float
     */
    private $financieelRendement;
    /**
     * @var float
     */
    private $financieelRendementPercentage;
    /**
     * @var float
     */
    private $nettoFinancieelRendementPercentage;
    /**
     * @var float|null
     */
    private $fiscaalRendementPercentage;
    /**
     * @var float
     */
    private $euribor;
    /**
     * @var float
     */
    private $gewoonTarief;
    /**
     * @var float|null
     */
    private $totaalRendement;
    /**
     * @var float|null
     */
    private $totaalRendementPercentage;
    /**
     * @var DateTimeInterface
     */
    private $indexDate;
    /**
     * @var float|null
     */
    private $nettoTotaalRendement;
    /**
     * @var float|null
     */
    private $nettoTotaalRendementPercentage;
    /**
     * @var TaxShelterRatioPresenter
     */
    private $presenter;
    /**
     * @var float|null
     */
    private $overdrachtRendement;
    /**
     * @var float|null
     */
    private $overdrachtRendementsPercentage;

    /**
     * TaxShelterRatios constructor.
     *
     * @param TaxShelterRatioPresenter $presenter
     * @param float $fiscaalRendement
     * @param float $nettoFincancieelRendement
     * @param float $financieelRendement
     * @param float $financieelRendementPercentage
     * @param float $nettoFinancieelRendementPercentage
     * @param float $fiscaalRendementPercentage
     * @param float $euribor
     * @param float $gewoonTarief
     * @param float|null $totaalRendement
     * @param float|null $totaalRendementPercentage
     * @param float|null $nettoTotaalRendement
     * @param float|null $nettoTotaalRendementPercentage
     * @param float|null $overdrachtRendement
     * @param float|null $overdrachtRendementsPercentage
     * @param DateTimeInterface $indexDate
     */
    public function __construct(
        TaxShelterRatioPresenter $presenter,
        ?float $fiscaalRendement,
        float $nettoFincancieelRendement,
        float $financieelRendement,
        float $financieelRendementPercentage,
        float $nettoFinancieelRendementPercentage,
        ?float $fiscaalRendementPercentage,
        float $euribor,
        float $gewoonTarief,
        ?float $totaalRendement,
        ?float $totaalRendementPercentage,
        ?float $nettoTotaalRendement,
        ?float $nettoTotaalRendementPercentage,
        ?float $overdrachtRendement,
        ?float $overdrachtRendementsPercentage,
        DateTimeInterface $indexDate
    ) {
        $this->fiscaalRendement = $fiscaalRendement;
        $this->nettoFincancieelRendement = $nettoFincancieelRendement;
        $this->financieelRendement = $financieelRendement;
        $this->financieelRendementPercentage = $financieelRendementPercentage;
        $this->nettoFinancieelRendementPercentage = $nettoFinancieelRendementPercentage;
        $this->fiscaalRendementPercentage = $fiscaalRendementPercentage;
        $this->euribor = $euribor;
        $this->gewoonTarief = $gewoonTarief;
        $this->totaalRendement = $totaalRendement;
        $this->totaalRendementPercentage = $totaalRendementPercentage;
        $this->indexDate = $indexDate;
        $this->nettoTotaalRendement = $nettoTotaalRendement;
        $this->nettoTotaalRendementPercentage = $nettoTotaalRendementPercentage;
        $this->presenter = $presenter;
        $this->overdrachtRendement = $overdrachtRendement;
        $this->overdrachtRendementsPercentage = $overdrachtRendementsPercentage;
    }

    /**
     * @return float|null
     */
    public function getOverdrachtRendementsPercentage(): ?float
    {
        return $this->overdrachtRendementsPercentage;
    }

    /**
     * @return float|null
     */
    public function getOverdrachtRendement(): ?float
    {
        return $this->overdrachtRendement;
    }

    /**
     * @return float|null
     */
    public function getFiscaalRendement(): ?float
    {
        return $this->fiscaalRendement;
    }

    /**
     * @return float
     */
    public function getNettoFincancieelRendement(): float
    {
        return $this->nettoFincancieelRendement;
    }

    /**
     * @return float
     */
    public function getFinancieelRendement(): float
    {
        return $this->financieelRendement;
    }

    /**
     * @return float
     */
    public function getFinancieelRendementPercentage(): float
    {
        return $this->financieelRendementPercentage;
    }

    /**
     * @return float
     */
    public function getNettoFinancieelRendementPercentage(): float
    {
        return $this->nettoFinancieelRendementPercentage;
    }

    /**
     * @return float|null
     */
    public function getFiscaalRendementPercentage(): ?float
    {
        return $this->fiscaalRendementPercentage;
    }

    /**
     * @return float
     */
    public function getEuribor(): float
    {
        return $this->euribor;
    }

    /**
     * @return float
     */
    public function getGewoonTarief(): float
    {
        return $this->gewoonTarief;
    }

    /**
     * @return float|null
     */
    public function getTotaalRendement(): ?float
    {
        return $this->totaalRendement;
    }

    /**
     * @return float|null
     */
    public function getTotaalRendementPercentage(): ?float
    {
        return $this->totaalRendementPercentage;
    }

    /**
     * @return DateTimeInterface
     */
    public function getIndexDate(): DateTimeInterface
    {
        return $this->indexDate;
    }

    /**
     * @return float|null
     */
    public function getNettoTotaalRendement(): ?float
    {
        return $this->nettoTotaalRendement;
    }

    /**
     * @return float|null
     */
    public function getNettoTotaalRendementPercentage(): ?float
    {
        return $this->nettoTotaalRendementPercentage;
    }

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array
    {
        return $this->presenter->present($this);
    }

    /**
     * @param int $outputMode
     */
    public function setOutputMode(int $outputMode): void
    {
        // TODO: Implement setOutputMode() method.
    }
}

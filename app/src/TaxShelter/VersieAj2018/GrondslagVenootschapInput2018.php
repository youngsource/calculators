<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2018;

/**
 * Class GrondslagVenootschapInput
 * @package Laudis\Calculators\Calculators\Input
 */
class GrondslagVenootschapInput2018
{
    /** @var float */
    private $winstVoorBelasting;
    /** @var float */
    private $tantiemes;
    /** @var float */
    private $andereMutaties;
    /** @var float */
    private $andereVUGeenAftrekVerbod;
    /** @var float */
    private $andereVUWelAftrekVerbod;
    /** @var float */
    private $aftrekBewerking;
    /** @var float */
    private $taxShelterVrijstellingIteratie;

    /**
     * GrondslagVenootschapInput constructor.
     * @param float $winstVoorBelasting
     * @param float $tantiemes
     * @param float $andereMutaties
     * @param float $andereVUGeenAftrekVerbod
     * @param float $andereVUWelAftrekVerbod
     * @param float $aftrekBewerking
     * @param float $taxShelterVrijstellingIteratie
     */
    public function __construct(
        float $winstVoorBelasting,
        float $tantiemes,
        float $andereMutaties,
        float $andereVUGeenAftrekVerbod,
        float $andereVUWelAftrekVerbod,
        float $aftrekBewerking,
        float $taxShelterVrijstellingIteratie
    ) {
        $this->winstVoorBelasting = $winstVoorBelasting;
        $this->tantiemes = $tantiemes;
        $this->andereMutaties = $andereMutaties;
        $this->andereVUGeenAftrekVerbod = $andereVUGeenAftrekVerbod;
        $this->andereVUWelAftrekVerbod = $andereVUWelAftrekVerbod;
        $this->aftrekBewerking = $aftrekBewerking;
        $this->taxShelterVrijstellingIteratie = $taxShelterVrijstellingIteratie;
    }

    /**
     * @return float
     */
    public function getWinstVoorBelasting(): float
    {
        return $this->winstVoorBelasting;
    }

    /**
     * @return float
     */
    public function getTantiemes(): float
    {
        return $this->tantiemes;
    }

    /**
     * @return float
     */
    public function getAndereMutaties(): float
    {
        return $this->andereMutaties;
    }

    /**
     * @return float
     */
    public function getAndereVUGeenAftrekVerbod(): float
    {
        return $this->andereVUGeenAftrekVerbod;
    }

    /**
     * @return float
     */
    public function getAndereVUWelAftrekVerbod(): float
    {
        return $this->andereVUWelAftrekVerbod;
    }

    /**
     * @return float
     */
    public function getAftrekBewerking(): float
    {
        return $this->aftrekBewerking;
    }

    /**
     * @return float
     */
    public function getTaxShelterVrijstellingIteratie(): float
    {
        return $this->taxShelterVrijstellingIteratie;
    }
}

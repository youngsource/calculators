<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2018;

use DateTimeInterface;
use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;

/**
 * Class TaxShelterInput
 * @package Laudis\Calculators\Calculators\Input
 */
final class TaxShelterInput2018 extends TaxShelterInput
{
    /**@var float */
    private $aftrekBewerking;

    /**
     * Calculator constructor.
     *
     * @param int $aanslagjaar
     * @param bool $maxTaxShelter
     * @param float $taxShelterInvestering
     * @param bool $maxLiqReserve
     * @param float $liqReserve
     * @param bool $isVerlaagdTarief
     * @param bool $isVermeedering
     * @param float $winstVoorBelasting
     * @param float $dividenden
     * @param float $tantiemes
     * @param float $AndereMutaties
     * @param float $AndereVerworpenUitgavenGeenAftrek
     * @param float $AndereVerworpenUitgavenWelAftrek
     * @param float $aftrekBewerking
     * @param float $voorafBetalingKwartaal1
     * @param float $voorafBetalingKwartaal2
     * @param float $voorafBetalingKwartaal3
     * @param float $voorafBetalingKwartaal4
     * @param float $terugBetaalbareVoorheffing
     * @param float $regularisatiesVorigejaren
     */
    public function __construct(
        int $aanslagjaar,
        bool $maxTaxShelter,
        float $taxShelterInvestering,
        bool $maxLiqReserve,
        float $liqReserve,
        bool $isVerlaagdTarief,
        bool $isVermeedering,
        float $winstVoorBelasting,
        float $dividenden,
        float $tantiemes,
        float $AndereMutaties,
        float $AndereVerworpenUitgavenGeenAftrek,
        float $AndereVerworpenUitgavenWelAftrek,
        float $aftrekBewerking,
        float $voorafBetalingKwartaal1,
        float $voorafBetalingKwartaal2,
        float $voorafBetalingKwartaal3,
        float $voorafBetalingKwartaal4,
        float $terugBetaalbareVoorheffing,
        float $regularisatiesVorigejaren,
        DateTimeInterface $paymentDate
    ) {
        parent::__construct(
            $aanslagjaar,
            $maxTaxShelter,
            $taxShelterInvestering,
            $maxLiqReserve,
            $liqReserve,
            $isVerlaagdTarief,
            $isVermeedering,
            $winstVoorBelasting,
            $dividenden,
            $tantiemes,
            $AndereMutaties,
            $AndereVerworpenUitgavenGeenAftrek,
            $AndereVerworpenUitgavenWelAftrek,
            $voorafBetalingKwartaal1,
            $voorafBetalingKwartaal2,
            $voorafBetalingKwartaal3,
            $voorafBetalingKwartaal4,
            $terugBetaalbareVoorheffing,
            $regularisatiesVorigejaren,
            $paymentDate
        );
        $this->aftrekBewerking = $aftrekBewerking;
    }

    /**
     * @return float
     */
    public function getAftrekBewerking(): float
    {
        return $this->aftrekBewerking;
    }
}

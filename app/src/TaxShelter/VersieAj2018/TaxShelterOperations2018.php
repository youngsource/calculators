<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2018;

use Laudis\Calculators\TaxShelter\Contracts\TaxShelterOperationsInterface;
use Laudis\Calculators\TaxShelter\Input\AanpassingenInMeerInput;
use Laudis\Calculators\TaxShelter\Input\ActueleTaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\BelastingenLiquidateReserveInput;
use Laudis\Calculators\TaxShelter\Input\BelastingOpDeGrondslagInput;
use Laudis\Calculators\TaxShelter\Input\BerekeningVermeerderingInput;
use Laudis\Calculators\TaxShelter\Input\GeraandeBelastingenGewoonInput;
use Laudis\Calculators\TaxShelter\Input\GrondslagVermeerderingInput;
use Laudis\Calculators\TaxShelter\Input\MaximaleInvesteringInput;
use Laudis\Calculators\TaxShelter\Input\MaximaleVrijstellingInput;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use Laudis\Calculators\TaxShelter\Input\TotaleBelastingenGewoonInput;
use function abs;
use function array_map;
use function array_sum;
use Laudis\Calculators\TaxShelter\TaxShelterOperationInputFactory;
use Laudis\Calculators\TaxShelter\VersieAj2019\GrondslagVennootschapInput2019;
use function max;
use function min;

/**
 * Class TaxShelterOperations
 * @package Laudis\Calculators\TaxShelter
 */
final class TaxShelterOperations2018 implements TaxShelterOperationsInterface
{
    /** @var TaxShelterOperationInputFactory */
    private $factory;

    /**
     * TaxShelterOperations constructor.
     * @param TaxShelterOperationInputFactory $factory
     */
    public function __construct(TaxShelterOperationInputFactory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * @param GrondslagVenootschapInput2018|GrondslagVennootschapInput2019 $input
     * @return float
     */
    public function grondslagVennootschapsbelasting($input): float
    {
        $tussentotaal = $input->getWinstVoorBelasting()
            - $input->getTantiemes()
            - $input->getTaxShelterVrijstellingIteratie()
            + $input->getAndereMutaties()
            + $input->getAndereVUGeenAftrekVerbod()
            + $input->getAndereVUWelAftrekVerbod()
            - $input->getAftrekBewerking();

        return max($tussentotaal, $input->getAndereVUWelAftrekVerbod(), 0);
    }

    /**
     * {@inheritDoc}
     */
    public function belastingOpDeGrondslag(BelastingOpDeGrondslagInput $input): float
    {
        if ($input->isIsverlaagTarief() && $input->getGrondslagVenB() <= 322500) {
            return $input->getVerlaagdTarief()->calculate($input->getGrondslagVenB());
        }
        return $input->getGewoonTarief() * $input->getGrondslagVenB();
    }

    /**
     * {@inheritDoc}
     */
    public function grondslagVermeerdering(GrondslagVermeerderingInput $input): float
    {
        return max($input->getGewoneVenB() - $input->getTerugbetaalbareVoorheffing(), 0);
    }

    /**
     * {@inheritDoc}
     */
    public function berekeningVanDeVermeerdering(BerekeningVermeerderingInput $input): float
    {

        $vermeerderingBedrag = $input->getGrondslagVermeerdering() * $input->getVermeerderingsPercentage();

        $voordeelBedragTotaal = array_sum(array_map(static function (float $voorafbetaling, float $voordeelPct) {
            return $voorafbetaling * $voordeelPct;
        }, $input->getVoorafBetalingen()->all(), $input->getVoordeelpercentages()->all()));

        $resterendeVermeerdering = max($vermeerderingBedrag - $voordeelBedragTotaal, 0);

        if ($resterendeVermeerdering < $input->getGrondslagVermeerdering() * $input->getRelatieveGrens()
            || $resterendeVermeerdering < $input->getAbsoluteGrens()
            || !$input->isVermeerdering()) {
            $resterendeVermeerdering = 0;
        }
        return $resterendeVermeerdering;
    }

    /**
     * {@inheritDoc}
     */
    public function geraamdeBelastingenGewoon(GeraandeBelastingenGewoonInput $input): float
    {
        return $input->getGewoneVenB()
            + $input->getVermeerderingOnvoldoendeVa()
            - $input->getTerugbetaalbareVoorheffing()
            - array_sum($input->getVoorafbetalingen()->all());
    }

    /**
     * {@inheritDoc}
     */
    public function totaleBelastingenGewoon(TotaleBelastingenGewoonInput $input): float
    {
        return $input->getGeraamdeBelastingenGewoon()
            + $input->getTerugbetaalbareVoorheffing()
            + array_sum($input->getVoorafbetalingen()->all())
            + $input->getRegularisatiesVorigeJaren();
    }

    /**
     * @param BelastingenLiquidateReserveInput $input
     * @return float
     */
    public function grondslagLiquidatieReserve(BelastingenLiquidateReserveInput $input): float
    {
        $maxBrutGrondslagLiqRes = max(
            $input->getWinstVoorBelasting()
            - $input->getBelastingsvrijeReserveTaxSHetlerIteratie()
            - $input->getTotaleBelastingenGewoon()
            - $input->getTaxShelterKostIteratie(),
            0
        );

        if ($input->isMaxLiqReserve()) {
            return $maxBrutGrondslagLiqRes / 1.1;
        }

        return min($input->getGewensteNettoLiqReserve(), $maxBrutGrondslagLiqRes / 1.1);
    }

    /**
     * {@inheritDoc}
     */
    public function belastingLiquidatieReserve(float $grondslagLiqReserve): float
    {
        return $grondslagLiqReserve * 0.1;
    }

    /**
     * {@inheritDoc}
     */
    public function aanpassingInMeer(AanpassingenInMeerInput $input): float
    {
        $totaleBelastingen = $input->getTotaleBelastingenGewoon() + $input->getBelastingLiqReserve();
        return abs(min($totaleBelastingen, 0));
    }

    /**
     * @param AanpassingenInMeerInput $input
     * @return float
     */
    public function verworpenUitgevenBelastingen(AanpassingenInMeerInput $input): float
    {
        return max($input->getTotaleBelastingenGewoon(), 0);
    }

    /**
     * @param MaximaleVrijstellingInput $input
     * @return float
     */
    public function maximaleVrijstelling(MaximaleVrijstellingInput $input): float
    {
        $mutatieReserves = $input->getWinstVoorBelasting()
            - $input->getDividenden()
            - $input->getTantiemes()
            + $input->getAndereMutaties()
            - $input->getTotaleBelastingenGewoon()
            - $input->getBelastingLiquidatieReserve()
            - $input->getAanpassingenInMeerBelastingen()
            - $input->getTaxShelterKost();

        return min(
            max($mutatieReserves, 0) * 0.5,
            $input->getAbsoluteGrens()
        );
    }

    /**
     * @param MaximaleInvesteringInput $input
     * @return float
     */
    public function maximaleInvestering(MaximaleInvesteringInput $input): float
    {
        return $input->getMaximaleVrijstelling() / $input->getVrijstellingspercentage();
    }

    /**
     * {@inheritDoc}
     */
    public function taxShelter(ActueleTaxShelterInput $input): TaxShelterIteratie
    {
        if ($input->isMaxTaxShelter()) {
            return $this->factory->iteratie(
                $input->getMaximaleVrijstelling(),
                $input->getMaximaleVrijstelling(),
                0.0,
                $input->getMaximaleInvestering()
            );
        }

        $iteratie = $input->getIteratie();
        return $this->factory->iteratie(
            $input->getTaxShelterInvestering() * $input->getVrijstellingsPercentage(),
            min($iteratie->getBelastingsvrijeReserve(), $input->getMaximaleVrijstelling()),
            max($iteratie->getBelastingsvrijeReserve() - $iteratie->getTaxShelterVrijstelling(), 0),
            $iteratie->getTaxShelterVrijstelling() / $input->getVrijstellingsPercentage()
        );
    }
}

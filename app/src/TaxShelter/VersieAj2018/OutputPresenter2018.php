<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2018;

use Laudis\Calculators\TaxShelter\OutputPresenter;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;

/**
 * Class OutputPresenter
 * @package Laudis\Calculators
 */
final class OutputPresenter2018 extends OutputPresenter
{
    /**
     * @param TaxShelterOutput2018 $output
     * @return array
     */
    public function present(TaxShelterOutput2018 $output): array
    {
        return [
            'grondslagVenb' => $this->display($output->getGrondslagenVenBen()),
            'belastingenOpDeGrondslag' => $this->display($output->getBelastingenOpDeGrondslagen()),
            'grondslagVermeerdering' => $this->display($output->getGrondslagVermeerderingen()),
            'berekeningVanDeVermeerdering' => $this->display($output->getBerekeningVanDeVermeerderingen()),
            'geraamdeBelastingenGewoon' => $this->display($output->getGeraamdeBelastingenGewoons()),
            'totaleBelastingenGewoon' => $this->display($output->getTotaleBelastingenGewoon()),
            'grondslagLiquidatieReserve' => $this->display($output->getGrondslagLiquidateReserves()),
            'belastingenLiquidatieReserve' => $this->display($output->getBelastingenLiquidatieReserves()),
            'verworpenUitgavenBelastingen' => $this->display($output->getVerworpenUitgavenBelastingen()),
            'aanpassingenInMeer' => $this->display($output->getAanpassingenInMeeren()),
            'maximaleVrijstelling' => $this->display($output->getMaximaleVrijstellingen()),
            'maximaleInvestering' => $this->display($output->getMaximaleInvesteringen()),
            'iteraties' => $this->iteraties($output)
        ];
    }
}

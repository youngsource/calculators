<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2018;

use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;
use Laudis\Calculators\TaxShelter\TaxShelterSequence;
use LogicException;
use function array_last;

/**
 * Class TaxShelterSequence
 * @package Laudis\Calculators\TaxShelter
 */
final class TaxShelterSequence2018 extends TaxShelterSequence
{
    /**
     * @param $input
     * @return TaxShelterIteratie
     */
    public function run(TaxShelterInput $input): TaxShelterIteratie
    {
        if (!$input instanceof TaxShelterInput2018) {
            throw new LogicException('Input must be of version 2018.');
        }

        $this->storeGrondslagVenB($input);
        $this->storeBelastingenOpGrondslag($input);
        $this->storeGrondslagVermeerdering($input);
        $this->storeBerekeningVanDeVermeerdering($input);
        $this->storeGeraamdeBelastingenGewoon($input);
        $this->storeTotaleBelastingenGewoon($input);
        $this->storeGrondslagLiquidatieReserve($input);
        $this->storeLiquidatieReserve();
        $this->storeAanpassingenInMeer();
        $this->storeVerworpenUitgavenEnBelastingen();
        $this->storeMaximaleVrijstelling($input);
        $this->storeMaximaleInvestering($input);
        return $this->storeIteratie($input);
    }

    /**
     * @param TaxShelterInput2018 $input
     */
    private function storeGrondslagVenB(TaxShelterInput2018 $input): void
    {
        $this->output->withGrondslagVenB($this->getOperations()->grondslagVennootschapsbelasting(
            $this->getFactory()->grondslagVenootschapsbelastingInput2018($input,
                array_last($this->output->getIteraties()))
        ));
    }

    /**
     * @return string
     */
    protected function getOutputClass(): string
    {
        return TaxShelterOutput2018::class;
    }
}

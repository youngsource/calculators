<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Laudis\Calculators\TaxShelter\Contracts\TaxShelterIteratorInterface;
use Laudis\Calculators\TaxShelter\Contracts\TaxShelterSequentieInterface;
use function round;

/**
 * Class TaxShelterIteratie
 * @package Laudis\Calculators\TaxShelter
 */
class TaxShelterIterator implements TaxShelterIteratorInterface
{
    /** @var TaxShelterSequentieInterface */
    private $sequence;

    /**
     * TaxShelterIteratie constructor.
     * @param TaxShelterSequentieInterface $sequentie
     */
    public function __construct(TaxShelterSequentieInterface $sequentie)
    {
        $this->sequence = $sequentie;
    }

    /**
     * {@inheritDoc}
     */
    public function iteratie($input): TaxShelterOutput2018
    {
        $next = $this->sequence->intializeSequence($input);
        do {
            $prev = $next;
            $next = $this->sequence->run($input);
        } while (round($prev->getTaxShelterVrijstelling() - $next->getTaxShelterVrijstelling(), 2) !== 0.00);
        // Make sure to clean all previous iteration results in case of a brief iteration
        $this->sequence->run($input);

        return $this->sequence->getOutput();
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Closure;
use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\TaxShelter\Contracts\TaxShelterOperationsInterface;
use Rakit\Validation\Rule;
use Rakit\Validation\Validation;

/**
 * Class VerlaagdTariefRule
 * @package Laudis\Calculators\TaxShelter
 */
final class VerlaagdTariefRule extends Rule
{
    /**
     * @var TaxShelterOperationsInterface
     */
    private $operations;
    /**
     * @var CalculatorFactoryInterface
     */
    private $calulatorFactory;

    /**
     * VerlaagdTariefRule constructor.
     * @param TaxShelterOperationsInterface $operations
     */
    public function __construct(TaxShelterOperationsInterface $operations, CalculatorFactoryInterface $calulatorFactory)
    {
        $this->operations = $operations;
        $this->calulatorFactory = $calulatorFactory;
        $this->setMessage('De grondslag van de vennootschapsbelasting moet lager dan 322500 euro bedragen om recht 
                            te hebben op een verlaagd tarief.');
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        $inputs = $this->hackIntoProtectedVariable();
        /** @var mixed $input */
        $input = $this->calulatorFactory->inputFromArray($inputs());
        if ($value === true) {
            return $this->operations->grondslagVennootschapsbelasting($input) < 322500;
        }
        return true;
    }

    /**
     * @return Closure
     */
    private function hackIntoProtectedVariable(): Closure
    {
        $inputs = Closure::bind(function () {
            /** @noinspection PhpUndefinedFieldInspection */
            return $this->inputs;
        }, $this->validation, Validation::class);
        return $inputs;
    }
}

<?php
/** @noinspection InterfacesAsConstructorDependenciesInspection */
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\TaxShelter\VersieAj2018\OutputPresenter2018;

/**
 * Class TaxShelterResult
 * @package Laudis\Calculators
 */
final class TaxShelterResult implements CalculationResultInterface
{
    /** @var TaxShelterOutput2018 */
    private $output;
    /** @var OutputPresenter2018 */
    private $presenter;

    /**
     * TaxShelterResult constructor.
     * @param TaxShelterOutput2018 $output
     * @param OutputPresenter $presenter
     */
    public function __construct(TaxShelterOutput2018 $output, OutputPresenter $presenter)
    {
        $this->output = $output;
        $this->presenter = $presenter;
    }

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array
    {
        return $this->presenter->present($this->output);
    }

    /**
     * @param int $outputMode
     */
    public function setOutputMode(int $outputMode): void
    {
        $this->presenter->setOutputMode($outputMode);
    }
}

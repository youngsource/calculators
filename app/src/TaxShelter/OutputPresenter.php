<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Laudis\Calculators\Contracts\CalculationResultInterface;
use function array_last;
use function array_map;

/**
 * Class OutputPresenter
 * @package Laudis\Calculators\TaxShelter
 */
abstract class OutputPresenter
{
    /** @var TaxShelterIteratiePresenter */
    private $iteratiePresenter;
    /** @var int */
    private $mode = CalculationResultInterface::DEFAULT;

    /**
     * OutputPresenter constructor.
     * @param TaxShelterIteratiePresenter $iteratiePresenter
     */
    public function __construct(TaxShelterIteratiePresenter $iteratiePresenter)
    {
        $this->iteratiePresenter = $iteratiePresenter;
    }

    /**
     * @param int $outputMode
     */
    public function setOutputMode(int $outputMode): void
    {
        $this->mode = $outputMode;
    }

    /**
     * @param array $array
     * @return mixed
     */
    protected function display(array $array)
    {
        if ($this->mode & CalculationResultInterface::DEFAULT || $this->mode & CalculationResultInterface::EXTENDED) {
            return [
                'iteratie' => $array,
                'waarde' => array_last($array)
            ];
        }
        return array_last($array);
    }

    /**
     * @param TaxShelterOutput2018 $output
     * @return array
     */
    protected function iteraties(TaxShelterOutput2018 $output): array
    {
        if ($this->mode & CalculationResultInterface::DEFAULT || $this->mode & CalculationResultInterface::EXTENDED) {
            return array_map(function (Input\TaxShelterIteratie $iteratie) {
                return $this->iteratiePresenter->present($iteratie);
            }, $output->getIteraties());
        }
        return $this->iteratiePresenter->present(array_last($output->getIteraties()));
    }
}

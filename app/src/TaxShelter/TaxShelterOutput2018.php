<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter;

use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use Laudis\Calculators\TaxShelter\VersieAj2018\OutputPresenter2018;

/**
 * Interface TaxShelterOutputBuilderInterface
 * @package Laudis\Calculators\Contracts\TaxShelter
 */
class TaxShelterOutput2018 implements CalculationResultInterface
{
    /** @var float[] */
    private $geraamdeBelastingenGewoon = [];
    /** @var float[] */
    private $berekeningVanDeVermeerdering = [];
    /** @var float[] */
    private $verworpenUitgavenBelastingen = [];
    /** @var float [] */
    private $grondslagLiquidatiesReserves = [];
    /** @var float[] */
    private $grondslagVenB = [];
    /** @var float[] */
    private $belastingenOpDeGrondslag = [];
    /** @var TaxShelterIterator[] */
    private $iteraties = [];
    /** @var float[] */
    private $aanpassingenInMeer = [];
    /** @var float[] */
    private $totaleBelastingenGewoon = [];
    /** @var float[] */
    private $liquidatieReserves = [];
    /** @var float[] */
    private $grondslagVermeerdering = [];
    /** @var float[] */
    private $maximaleVrijstellingen = [];
    /** @var float[] */
    private $maximaleInvesteringen = [];
    /**
     * @var int
     */
    private $outputMode = CalculationResultInterface::DEFAULT;

    /**
     * @return float[]
     */
    public function getBerekeningVanDeVermeerderingen(): array
    {
        return $this->berekeningVanDeVermeerdering;
    }

    /**
     * @param float $value
     */
    public function withLiquidatieReserve(float $value): void
    {
        $this->liquidatieReserves[] = $value;
    }

    /**
     * @return float[]
     */
    public function getTotaleBelastingenGewoon(): array
    {
        return $this->totaleBelastingenGewoon;
    }

    /**
     * @return float[]
     */
    public function getVerworpenUitgavenBelastingen(): array
    {
        return $this->verworpenUitgavenBelastingen;
    }

    /**
     * @param TaxShelterIteratie $next
     */
    public function withIteratie(TaxShelterIteratie $next): void
    {
        $this->iteraties[] = $next;
    }

    /**
     * @return float[]
     */
    public function getGrondslagenVenBen(): array
    {
        return $this->grondslagVenB;
    }

    /**
     * @param float $berekeningVanDeVermeerdering
     */
    public function withBerekeningVanDeVermeerdering(float $berekeningVanDeVermeerdering): void
    {
        $this->berekeningVanDeVermeerdering[] = $berekeningVanDeVermeerdering;
    }

    /**
     * @return float[]
     */
    public function getBelastingenLiquidatieReserves(): array
    {
        return $this->liquidatieReserves;
    }

    /**
     * @param float $value
     */
    public function withTotaleBelastingenGewoon(float $value): void
    {
        $this->totaleBelastingenGewoon[] = $value;
    }

    /**
     * @param float $value
     */
    public function withVerworpenUitgavenBelastingen(float $value): void
    {
        $this->verworpenUitgavenBelastingen[] = $value;
    }

    /**
     * @return float[]
     */
    public function getAanpassingenInMeeren(): array
    {
        return $this->aanpassingenInMeer;
    }

    /**
     * @param float $belastingOpDeGrondslag
     * @return void
     */
    public function withBelastingenOpDeGrondslag(float $belastingOpDeGrondslag): void
    {
        $this->belastingenOpDeGrondslag[] = $belastingOpDeGrondslag;
    }

    /**
     * @param float $belastingLiquidatieReserve
     */
    public function withGrondslagLiquidatieReserve(float $belastingLiquidatieReserve): void
    {
        $this->grondslagLiquidatiesReserves[] = $belastingLiquidatieReserve;
    }

    /**
     * @param float $aanpassingInMeer
     */
    public function withAanpassingenInMeer(float $aanpassingInMeer): void
    {
        $this->aanpassingenInMeer[] = $aanpassingInMeer;
    }

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array
    {
        $result = new TaxShelterResult($this, new OutputPresenter2018(new TaxShelterIteratiePresenter));
        return $result->output();
    }

    /**
     * @param float $geraamdeBelastingenGewoon
     * @return void
     */
    public function withGeraamdeBelastingenGewoon(float $geraamdeBelastingenGewoon): void
    {
        $this->geraamdeBelastingenGewoon[] = $geraamdeBelastingenGewoon;
    }

    /**
     * @return float[]
     */
    public function getGeraamdeBelastingenGewoons(): array
    {
        return $this->geraamdeBelastingenGewoon;
    }

    /**
     * @return float[]
     */
    public function getGrondslagLiquidateReserves(): array
    {
        return $this->grondslagLiquidatiesReserves;
    }

    /**
     * @param float $grondslagVermeerdering
     */
    public function withGrondslagVermeerdering(float $grondslagVermeerdering): void
    {
        $this->grondslagVermeerdering[] = $grondslagVermeerdering;
    }

    /**
     * @return TaxShelterIteratie[]
     */
    public function getIteraties(): array
    {
        return $this->iteraties;
    }

    /**
     * @return float[]
     */
    public function getBelastingenOpDeGrondslagen(): array
    {
        return $this->belastingenOpDeGrondslag;
    }

    /**
     * @param float $grondslagVennootschapsbelasting
     */
    public function withGrondslagVenB(float $grondslagVennootschapsbelasting): void
    {
        $this->grondslagVenB[] = $grondslagVennootschapsbelasting;
    }

    /**
     * @return float[]
     */
    public function getGrondslagVermeerderingen(): array
    {
        return $this->grondslagVermeerdering;
    }

    /**
     * @return float[]
     */
    public function getMaximaleVrijstellingen(): array
    {
        return $this->maximaleVrijstellingen;
    }

    /**
     * @return float[]
     */
    public function getMaximaleInvesteringen(): array
    {
        return $this->maximaleInvesteringen;
    }

    /**
     * @param float $value
     */
    public function withMaximaleVrijstelling(float $value): void
    {
        $this->maximaleVrijstellingen[] = $value;
    }

    /**
     * @param float $value
     */
    public function withMaximaleInvestering(float $value): void
    {
        $this->maximaleInvesteringen[] = $value;
    }

    /**
     * @param int $mode
     */
    public function setOutputMode(int $mode): void
    {
        $this->outputMode = $mode;
    }

    /**
     * @return int
     */
    protected function getOutputMode(): int
    {
        return $this->outputMode;
    }
}

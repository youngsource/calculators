<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Contracts;

use Laudis\Calculators\TaxShelter\Input\AanpassingenInMeerInput;
use Laudis\Calculators\TaxShelter\Input\ActueleTaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\BelastingenLiquidateReserveInput;
use Laudis\Calculators\TaxShelter\Input\BelastingOpDeGrondslagInput;
use Laudis\Calculators\TaxShelter\Input\BerekeningVermeerderingInput;
use Laudis\Calculators\TaxShelter\Input\GeraandeBelastingenGewoonInput;
use Laudis\Calculators\TaxShelter\Input\GrondslagVermeerderingInput;
use Laudis\Calculators\TaxShelter\Input\MaximaleInvesteringInput;
use Laudis\Calculators\TaxShelter\Input\MaximaleVrijstellingInput;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use Laudis\Calculators\TaxShelter\Input\TotaleBelastingenGewoonInput;
use Laudis\Calculators\TaxShelter\VersieAj2018\GrondslagVenootschapInput2018;
use Laudis\Calculators\TaxShelter\VersieAj2019\GrondslagVennootschapInput2019;

/**
 * Interface TaxShelterCalculator
 * @package Laudis\Calculators\Contracts\TaxShelter
 */
interface TaxShelterOperationsInterface
{
    /**
     * De berekeningsgrondslag van de VenB gaan bepalen, rekening houdend met het aftrekverbod en de tax-shelter vrijstelling.
     *
     * <p> Winst voor belastingen (PN)</p>
     * <p>
     * + Dividenden (P)</p>
     *
     * <p>- Vrijgestelde reserves (tax-shelter: bij start = 0, daarna uit
     * iteratief proces) (P)</p>
     * <p>
     * + Andere mutaties van de reserves (PN)</p>
     * <p>
     * + Verworpen uitgaven zonder aftrekverbod (P)</p>
     * <p>
     * + Verworpen uitgaven met aftrekverbod (p)</p>
     * <p>
     * - Aftrekbewerkingen (P)
     * </p>
     * <p style="border-top: 1px solid black;">
     * wtussentotaal (PN)</p>
     * <p>
     * grondslagVenB (P) = max (wtussentotaal, verworpen uitgaven met
     * aftrekverbod, 0)</p>
     * <p>Bij de eerste
     * berekening heeft de operatie VrijstellingTaxShelterOperation nog niet
     * gelopen, en is er nog geen vrijstelling tax-shelter. In dat geval
     * moet er worden van uitgegaan dat de tax shelter niet wordt toegepast
     * en de vrijstelling gelijk is aan nul.</p>
     * <p>
     *
     * @param GrondslagVennootschapInput2019|GrondslagVenootschapInput2018 $input
     * @return float
     */
    public function grondslagVennootschapsbelasting($input): float;

    /**
     * Berekening van de belasting op de grondslag, rekening houdend met het al dan niet toepassen van het verlaagd tarief en de crisisbijdrage.
     *
     *  <p> <i><b>STAP 1: VERLAAGD TARIEF OF NIET</b></i></p>
     * <p>Eerst wordt bepaald of het verlaagd tarief al dan niet kan worden
     * toegepast. Hierbij wordt rekening gehouden met de hervorming VenB vanaf AJ
     * 2019 (voor boekjaren die aanvangen te vroegste vanaf 1.1.AJ-1).</p>
     * <ul>
     * <li>
     * <p> Betreft het een aanslagjaar 2018 zal het verlaagd tarief worden
     * toegepast als in de input wordt aangegeven dat de vennootschap in
     * aanmerking komt voor de toepassing ervan. Er volgt dan een bijkomende
     * check of de grondslag kleiner is dan 322.500 euro. Indien dit het
     * geval is zal het verlaagd tarief worden toegepast. Is dit niet het
     * geval dan zal het gewoon tarief toepassing vinden.</p>
     * </li>
     * <li>
     * <p> Betreft het een aanslagjaar 2019 of later moet geen bijkomende check
     * gebeuren. Het verlaagd tarief zal worden toegepast als dit zo in de
     * input wordt aangegeven, zoniet zal het gewone tarief worden toegepast.</p>
     * </li>
     * </ul>
     * <p><i><b>STAP 2: TOEPASSEN VAN DE CORRECTE BELASTINGSCHAAL</b></i></p>
     * <p>Als gevolg van de hervorming van de vennootschapsbelasting worden we
     * geconfronteerd met verschillende tarieven en baremaâ€™s naargelang het
     * aanslagjaar. We gaan er hierbij eenvoudigheidshalve van uit dat het het
     * boekjaar telkens aanvangt na 1.1.AJ-1 en dat er geen wijzigingen van het
     * boekjaar hebben plaatsgevonden.</p>
     *
     * <p>Na toepassing van de correcte belastingschaal bekomen we de
     * vennootschapsbelasting.</p>
     *
     * @param BelastingOpDeGrondslagInput $input
     * @return float
     */
    public function belastingOpDeGrondslag(BelastingOpDeGrondslagInput $input): float;

    /**
     * De berekeningsgrondslag voor de vermeerdering bij onvoldoende voorafbetalingen bepalen.
     *
     * <p> De berekeningsgrondslag voor de vermeerdering bij
     * onvoldoende voorafbetalingen omvat de gewone vennootschapsbelasting,
     * verminderd met de terugbetaalbare voorheffingen. De berekeningsgrondslag
     * kan niet negatief zijn.</p>
     *
     * @param GrondslagVermeerderingInput $input
     * @return float
     */
    public function grondslagVermeerdering(GrondslagVermeerderingInput $input): float;

    /**
     * De berekening van de effectief weerhouden vermeerdering wegens onvoldoende voorafbetalingen, rekening houdend met de voor dat aanslagjaar toepasselijke vermeerderingspercentage en voorafbetalingspercentages om de vermeerdering te vermijden (als de vermeerdering effectief van toepassing is).
     *
     * <p> <u>Is de vermeerdering van toepassing of is het een jonge kleine
     * vennootschap?</u></p>
     * <p>Kleine vennootschappen (art. 15 Â§Â§ 1 tot 6 W. Venn.) worden gedurende
     * de eerste drie boekjaren vanaf hun oprichting niet geconfronteerd met een
     * belastingvermeerdering als zij voor die boekjaren niet of onvoldoende
     * voorafbetalen (art. 218, Â§ 2 WIB 1992). Daarom vraagt de administratie in
     * het vak VOORAFBETALINGEN of de aangifte betrekking heeft op Ã©Ã©n van de
     * eerste drie boekjaren vanaf de oprichting van de vennootschap die een
     * kleine vennootschap is in de zin van art. 15 Â§Â§ 1 tot 6 W.Venn. Als dit
     * zo is, vult men JA in. Als dit niet zo is vult men NEEN in de papieren
     * versie in of vinkt men niets aan in de BIZTAX-versie. In de rekentool is
     * dit de boolean isVermeerdering.</p>
     * <p><u>Berekeningsgrondslag voor de vermeerdering</u></p>
     * <p> De berekeningsgrondslag voor de vermeerdering wordt vermenigvuldigd met
     * het globaal vermeerderingspercentage (2,25% voor aanslagjaar 2018 of 6,75%
     * vanaf aanslagjaar 2019 verbonden aan boekjaar dat ten vroegste is gestart
     * vanaf 01.01.AJ-1).</p>
     * <p> <u>Waardering van de voordelen verbonden aan de gedane voorafbetalingen</u></p>
     * <p>Vervolgens worden de voordelen van de (eventuele) vier voorafbetalingen
     * gewaardeerd tegen de voor dat voorjaar geldende voordeelpercentages.</p>
     * <p><u>Verschil tussen de vermeerdering en de voordelen gekoppeld aan de
     * voorafbetalingen</u></p>
     * <p>Daarna wordt het verschil berekend tussen de globale vermeerdering en
     * alle voordelen gekoppeld aan voorafbetalingen. Het positieve verschil
     * geeft het bedrag van de uiteindelijk vermeerdering die de vennootschap
     * moet betalen.</p>
     * <p><u>Toetsing aan de absolute en relatieve grens (enkel voor AJ 2018 (en
     * vroeger)</u></p>
     * <p> Dit positieve verschil voor AJ 2018 (en vroeger) wordt getoetst aan de
     * relatieve grens van 0,5% (minimisregel) en een absolute grens van â‚¬ 80.
     * Indien dit positieve verschil lager is, dan is te weerhouden vermeerdering
     * = 0.</p>
     *
     * @param BerekeningVermeerderingInput $input
     * @return float
     */
    public function berekeningVanDeVermeerdering(BerekeningVermeerderingInput $input): float;

    /**
     * De berekening van de bij te boeken of terug te krijgen gewone belastingen (exclusief liquidatiereserve)
     *
     * <p> Als de berekende totale gewone vennootschapsbelasting (inclusief
     * de vermeerdering wegens onvoldoende voorafbetalingen) meer bedraagt dat de
     * verrekenbare voorheffingen en terugbetalingen zal de rekening 6702 worden
     * gedebiteerd en de rekening 450 worden gecrediteerd.</p>
     * <p> Als de berekende totale gewone vennootschapsbelasting (inclusief de
     * vermeerdering wegens onvoldoende voorafbetalingen) minder bedraagt dat de
     * verrekenbare voorheffingen en terugbetalingen zal ten belope van het
     * verschil een vordering op de belastingen worden geboekt op het debet van
     * rekening 412 en zal de rekening 6701 worden gecrediteerd.</p>
     * <p> Als de berekende totale gewone vennootschapsbelasting (inclusief de
     * vermeerdering wegens onvoldoende voorafbetalingen) exact gelijk is aan de
     * verrekenbare voorheffingen en terugbetalingen zal niets worden geboekt.</p>
     * <p> gewoneVenB + vermeerderingOnvoldoendeVA - terugBetaalbareVoorheffing -
     * voorafBetalingKwartaal1 - voorafBetalingKwartaal2 -
     * voorafBetalingKwartaal3 - <u> voorafBetalingKwartaal4</u>
     * gewoneGeraamdeBelastingen</p>
     * <p> De (gewone) geraamde belasting kan dus zowel positief als negatief zijn.</p>
     *
     * @param GeraandeBelastingenGewoonInput $input
     * @return float
     */
    public function geraamdeBelastingenGewoon(GeraandeBelastingenGewoonInput $input): float;

    /**
     * De berekening van de bij te boeken of terug te krijgen gewone belastingen (exclusief liquidatiereserve)
     *
     * <p> Als de berekende totale gewone vennootschapsbelasting (inclusief
     * de vermeerdering wegens onvoldoende voorafbetalingen) meer bedraagt dat de
     * verrekenbare voorheffingen en terugbetalingen zal de rekening 6702 worden
     * gedebiteerd en de rekening 450 worden gecrediteerd.</p>
     * <p> Als de berekende totale gewone vennootschapsbelasting (inclusief de
     * vermeerdering wegens onvoldoende voorafbetalingen) minder bedraagt dat de
     * verrekenbare voorheffingen en terugbetalingen zal ten belope van het
     * verschil een vordering op de belastingen worden geboekt op het debet van
     * rekening 412 en zal de rekening 6701 worden gecrediteerd.</p>
     * <p> In deze operatie wordt ook de totale (gewone) belasting berekend. Dit is
     * de belasting (exclusief de belasting op de liquidatiereserve).</p>
     * <p> gewoneGeraamdeBelastingen</p>
     * <p>+ terugBetaalbareVoorheffing + voorafBetalingKwartaal1 +
     * voorafBetalingKwartaal2 + voorafBetalingKwartaal3 +
     * voorafBetalingKwartaal4 + <u> regulariesatieVorigeJaren </u>
     * totaleBelastingenGewoon</p>
     *
     *
     * @param TotaleBelastingenGewoonInput $input
     * @return float
     */
    public function totaleBelastingenGewoon(TotaleBelastingenGewoonInput $input): float;

    /**
     * Het bepalen van de belasting op de liquidatie reserve rekening houdend met de keuze (maximale liquidatiereserve of opgegeven bruto bedrag).
     *
     * <p>De maximale bruto grondslag voor de aanleg van de liquidatiesrereserve is
     * de code 9905 van de resultatenrekening.</p>
     * <table cellspacing="0" cellpadding="7">
     * <tbody>
     * <tr valign="top">
     * <td>
     * <p> <b>Winst (Verlies) van het boekjaar vÃ³Ã³r belasting (+/-)</b></p>
     * </td>
     * <td>
     * <p> <b>9903</b></p>
     * </td>
     * <td>
     * <p> <b>(1)</b></p>
     * </td>
     * </tr>
     * </tbody>
     * <tbody>
     * <tr valign="top">
     * <td>
     * <p> <b>Onttrekking aan de uitgestelde belastingen</b></p>
     * </td>
     * <td>
     * <p> <b>780</b></p>
     * </td>
     * <td>
     * <p> <b>(2)</b></p>
     * </td>
     * </tr>
     * <tr valign="top">
     * <td>
     * <p> <b>Overboeking naar de uitgestelde belastingen</b></p>
     * </td>
     * <td>
     * <p> <b>680</b></p>
     * </td>
     * <td>
     * <p> <b>(2)</b></p>
     * </td>
     * </tr>
     * </tbody>
     * <tbody>
     * <tr valign="top">
     * <td>
     * <p> <b>Belastingen op het resultaat (+/-)</b></p>
     * </td>
     * <td> <br>
     * </td>
     * <td> <br>
     * </td>
     * </tr>
     * <tr valign="top">
     * <td>
     * <p> Belastingen</p>
     * </td>
     * <td>
     * <p> 670/3</p>
     * </td>
     * <td> <br>
     * </td>
     * </tr>
     * <tr valign="top">
     * <td>
     * <p> Regularisaties van belastingen </p>
     * </td>
     * <td>
     * <p> 77</p>
     * </td>
     * <td> <br>
     * </td>
     * </tr>
     * </tbody>
     * <tbody>
     * <tr valign="top">
     * <td>
     * <p> <b>Winst (Verlies) van het boekjaar (+/-)</b></p>
     * </td>
     * <td>
     * <p> <b>9904</b></p>
     * </td>
     * <td> <br>
     * </td>
     * </tr>
     * </tbody>
     * <tbody>
     * <tr valign="top">
     * <td>
     * <p> <b>Onttrekking aan de belastingvrije reserve</b></p>
     * </td>
     * <td>
     * <p> <b>789</b></p>
     * </td>
     * <td>
     * <p> <b>(2)</b></p>
     * </td>
     * </tr>
     * <tr valign="top">
     * <td>
     * <p> <b>Overboeking naar de belastingvrije reserve</b></p>
     * </td>
     * <td>
     * <p> <b>689</b></p>
     * </td>
     * <td>
     * <p> <b>(2)</b></p>
     * </td>
     * </tr>
     * </tbody>
     * <tbody>
     * <tr valign="top">
     * <td>
     * <p> <b>Te bestemmen winst (verlies) van het boekjaar (+/-)</b></p>
     * </td>
     * <td>
     * <p> <b>9905</b></p>
     * </td>
     * <td> <br>
     * </td>
     * </tr>
     * </tbody>
     * </table>
     * <p> Verduidelijking: in het inputveld winstVoorBelasting nemen we naast de
     * Winst (Verlies) van het boekjaar vÃ³Ã³r belastingen (1)ook de correcties
     * uitgestelde belastingen en belastingvrije reserves (2) op, andere de
     * tax-shelter opbouw. Met op dat de aanleg van de belastingvrije reserve
     * m.b.t. de tax-shelter investering wordt in de berekening zelf toegevoegd.</p>
     * <p> <u>Bepalen van de maximale aan te leggen bruto liquidatiereserve</u></p>
     * <p> winst voor belastingen (incl. mutaties uitg. bel. en belvrije res, excl.
     * tax shelter opbouw) - belastingvrije reserve tax shelter opbouw (uit
     * iteratie, bij start = 0) - totale belastingen gewoon (excl. liquidatie
     * reserve en tax shelter kost) - tax shelterkost (uit iteratie, bij start =
     * 0)</p>
     * <p> max. (bruto) grondslag liquidatie reserve (indien negatief, opgetrokken
     * tot 0)</p>
     * <p>De max. grondslag liquidatie reserve mag niet negatief zijn. Als de
     * grondslag negatief is wordt deze opgetrokken tot 0.</p>
     * <p> <u>Berekening van de liquidatiebelasting volgens de keuze bij input</u></p>
     * <p> Als de belastingplichtige gekozen heeft voor de aanleg van de maximale
     * liquidatiereserve dan wordt de belasting van 10% berekend op de maximale
     * netto liquidatiereserve (= max. grondslag liquidatie reserve / 1,1).</p>
     * <p> Als de belastingplichtige een gewenste netto liquidatiereserve heeft
     * opgegeven, wordt deze eerst beperkt tot de maximale netto
     * liquidatiereserve. De belasting op de liquidatiereserve wordt bekomen door
     * dit (eventueel beperkte) nettobedrag te vermenigvuldigen met 10%.</p>
     *
     * @param BelastingenLiquidateReserveInput $input
     * @return float
     */
    public function grondslagLiquidatieReserve(BelastingenLiquidateReserveInput $input): float;

    /**
     * @param float $input
     * @return float
     */
    public function belastingLiquidatieReserve(float $input): float;

    /**
     * Berekening van de totale verworpen uitgave belastingen en de eventuele aanpassing in meer van de begintoestand van de reserves.
     *
     * <p> Het merendeel van de inkomstenbelastingen is niet aftrekbaar als
     * beroepskosten in de vennootschapsÂ­belasting (art. 198, 1Â° en 3Â° WIB
     * 1992). In het jaar van tenlasteneming worden zij opgenomen in de fiscale
     * winst via de verworpen uitgaven.</p>
     * <p> Als een niet-aftrekbare belasting in een later boekjaar terugbetaald of
     * geregulariseerd wordt, maakt zij via de boeking als opbrengst opnieuw deel
     * uit van het fiscaal resultaat.</p>
     * <p> Om zoâ€™n dubbele belasting te vermijden, mogen de terugbetaalde
     * belastingen in de eerste plaats worden afgetrokken van niet-aftrekbare
     * belastingen van het boekjaar. Voor zover het terugbetaalde of
     * geregulariseerde bedrag hoger is dan de niet-aftrekbare belastingen van
     * het boekjaar, wordt de begintoestand van de reserves verhoogd.</p>
     * <p> <b>Stap 1:</b> de berekening van de totale belasting, inclusief
     * belasting op liquidatiereserve (exclusief de tax-shelter kost)</p>
     * <p> <b>Stap 2:</b></p>
     * <p> Als de hiervoor berekende totale belasting niet negatief is (positief of
     * 0), weerhouden we dit positieve bedrag als verworpenUitgaveBelastingen.</p>
     * <p> Als de hiervoor berekenende totale belasting negatief is, weerhouden we
     * als aanpassingInMeerBelastingen de absolute waarde ervan.</p>
     *
     * @param AanpassingenInMeerInput $input
     * @return float
     */
    public function aanpassingInMeer(AanpassingenInMeerInput $input): float;

    /**
     * @param AanpassingenInMeerInput $input
     * @return float
     */
    public function verworpenUitgevenBelastingen(AanpassingenInMeerInput $input): float;

    /**
     * Berekening van de taxsheltervrijstelling, vrijgestelde- en
     * belastetaxshelter reserve en de taxshelterkost, rekening houden met de
     * keuze van de belastingplichtige (maximale taxshelter of opgegeven
     * geÃ¯nvesteerd bedrag).
     *
     * <p> De vrijstelling mag per jaar niet meer bedragen dan 50% van haar
     * belastbare gereserveerde winst van het boekjaar na aanpassingen in meer of
     * min van de begintoestand van de reserves, maar vastgesteld vÃ³Ã³r de
     * aanleg van de vrijgestelde reserve.</p>
     * <p> In de praktijk gaat het om 50% van de aangroei van de belastbare
     * gereserveerde winst van het boekjaar (code 1080 PN in vak Reserves
     * Belastbare gereserveerde winst van het aangifteformulier 275.1), na
     * correcte raming van de vennootschapsbelasting, verhoogd met de aangelegde
     * vrijgestelde reserve.</p>
     * <p> <b>Stap 1: de berekening van de aangroei van de reserves (vÃ³Ã³r aanleg
     * van de tax-shelter vrijstelling)</b></p>
     * <p> + Winst voor belastingen</p>
     * <p> - Dividenden</p>
     * <p> - TantiÃ¨mes</p>
     * <p> + Andere mutaties van de reserves</p>
     * <p> - Totale belastingen (gewoon)</p>
     * <p> - Belasting op de liquidatiesreserve</p>
     * <p> - Aanpassing in meer van de begintoestand â€“ belastingen</p>
     * <p> -<u> Tax Shelter kost (bij start = 0, daarna iteratie)</u></p>
     * <p> Totale mutatie van de reserves (kan positief of negatief zijn)</p>
     * <p> <b>Stap 2: de berekening van de maximale tax shelter vrijstelling</b></p>
     * <p> Maximale vrijstelling = Positieve mutatie van de reserves: max(totale
     * mutatie, 0) * 50% te beperkten tot 750Â&nbsp;000 (tot AJ 2018),
     * 850Â&nbsp;000 (AJ 2019 en AJ 2020) of 1Â&nbsp;000Â&nbsp;000 (vanaf AJ
     * 2021).</p>
     * <p> <b>Stap 3: de keuze van de investeerder</b></p>
     * <p>In de rekentool heeft de investeerder de keuze om te kiezen voor</p>
     * <ul>
     * <li>
     * <p>ofwel de maximaal vrijstelbare taxshelter</p>
     * </li>
     * <li>
     * <p> ofwel een opgegeven investeringsbedrag</p>
     * </li>
     * </ul>
     * <p>Als de investeerder <b>kiest voor de maximale tax-shelterinvestering</b>
     * dan</p>
     * <ul>
     * <li>
     * <p>is de belastingvrije reserve tax shelter gelijk aan de in stap 2
     * berekende maximale vrijstelling</p>
     * </li>
     * <li>
     * <p>is er gÃ©Ã©n belaste tax-shelter reserve (belResTaxShelter_Iter = 0)</p>
     * </li>
     * <li>
     * <p> is de taxshelterinvestering gelijk aan de taxShelterKost_Iter
     * (berekening: Maximale vrijstelling / (310% - tot AJ 2018, 356% - AJ
     * 2019 â€“ AJ 2020, 421% vanaf AJ 2021)</p>
     * </li>
     * </ul>
     * <p> Als de investeerder kiest voor een opgegeven tax-shelterinvestering dan</p>
     * <ul>
     * <li>
     * <p> is de boekhoudkundige belastingvrije reserve gelijk aan de
     * investering Ã— (310% - tot AJ 2018, 356% - AJ 2019 â€“ AJ 2020, 421%
     * vanaf AJ 2021)</p>
     * </li>
     * <li>
     * <p> is de belaste tax-shelter reserve gelijk aan het positieve verschil
     * tussen de belastingvrije reserve (1<sup>e</sup> streepje) en de
     * maximale vrijstelling uit stap 2</p>
     * </li>
     * <li>
     * <p> is de tax-shelterkost gelijk aan de (boekhoudkundige belastingvrije
     * reserve â€“ de belaste tax-shelter reserve) / (310% - tot AJ 2018,
     * 356% - AJ 2019 â€“ AJ 2020, 421% vanaf AJ 2021)</p>
     * </li>
     * </ul>
     *
     * @param ActueleTaxShelterInput $input
     * @return TaxShelterIteratie
     */
    public function taxShelter(ActueleTaxShelterInput $input): TaxShelterIteratie;

    /**
     * @param MaximaleVrijstellingInput $input
     * @return float
     */
    public function maximaleVrijstelling(MaximaleVrijstellingInput $input): float;

    /**
     * @param MaximaleInvesteringInput $input
     * @return float
     */
    public function maximaleInvestering(MaximaleInvesteringInput $input): float;
}

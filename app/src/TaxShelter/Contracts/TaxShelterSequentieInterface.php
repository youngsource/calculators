<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Contracts;

use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;

/**
 * Interface TaxShelterSequentieInterface
 * @package Laudis\Calculators\TaxShelter\Contracts
 */
interface TaxShelterSequentieInterface
{
    /**
     * @param TaxShelterInput $input
     * @return TaxShelterIteratie
     */
    public function intializeSequence(TaxShelterInput $input): TaxShelterIteratie;

    /**
     * @return TaxShelterOutput2018
     */
    public function getOutput(): TaxShelterOutput2018;

    /**
     * @param TaxShelterInput $input
     * @return TaxShelterIteratie
     */
    public function run(TaxShelterInput $input): TaxShelterIteratie;
}

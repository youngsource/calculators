<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Contracts;

use Laudis\Calculators\TaxShelter\VersieAj2019\TussentotaalVenBInput;

/**
 * Interface TaxShelterOperations2019Interface
 * @package Laudis\Calculators\TaxShelter\VersieAj2019
 */
interface TaxShelterOperations2019Interface extends TaxShelterOperationsInterface
{
    /**
     * @param float $tussentotaal
     * @return float
     */
    public function korfBeperking(float $tussentotaal, int $aanslagjaar): float;

    /**
     * @param TussentotaalVenBInput $input
     * @return float
     */
    public function tussenTotaalGrondslagVennootschapsbelasting(TussentotaalVenBInput $input): float;
}

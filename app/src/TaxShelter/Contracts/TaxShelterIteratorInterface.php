<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\Contracts;

use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;

/**
 * Interface TaxShelterIteratorInterface
 * @package Laudis\Calculators\TaxShelter\Contracts
 */
interface TaxShelterIteratorInterface
{
    /**
     * @param TaxShelterInput
     * @return TaxShelterOutput2018
     */
    public function iteratie($input): TaxShelterOutput2018;
}

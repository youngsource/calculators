<?php
/** @noinspection SenselessProxyMethodInspection */
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2019;

use Laudis\Calculators\TaxShelter\Contracts\TaxShelterOperations2019Interface;
use Laudis\Calculators\TaxShelter\Contracts\TaxShelterOperationsInterface;
use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;
use Laudis\Calculators\TaxShelter\TaxShelterSequence;
use LogicException;
use function array_last;

/**
 * Class TaxShelterSequence2019
 * @package Laudis\Calculators\TaxShelter\VersieAj2019
 */
final class TaxShelterSequence2019 extends TaxShelterSequence
{
    /**
     * @param TaxShelterInput $input
     * @return TaxShelterIteratie
     */
    public function run(TaxShelterInput $input): TaxShelterIteratie
    {
        if (!$input instanceof TaxShelterInput2019) {
            throw new LogicException('Input must be of version 2019.');
        }

        $this->storeTussentotaalGrondslagVenB($input);
        $this->storeKorfBeperking($input->getAanslagjaar());
        $this->storeGrondslagVenB($input);
        $this->storeBelastingenOpGrondslag($input);
        $this->storeGrondslagVermeerdering($input);
        $this->storeBerekeningVanDeVermeerdering($input);
        $this->storeGeraamdeBelastingenGewoon($input);
        $this->storeTotaleBelastingenGewoon($input);
        $this->storeGrondslagLiquidatieReserve($input);
        $this->storeLiquidatieReserve();
        $this->storeAanpassingenInMeer();
        $this->storeVerworpenUitgavenEnBelastingen();
        $this->storeMaximaleVrijstelling($input);
        $this->storeMaximaleInvestering($input);
        return $this->storeIteratie($input);
    }

    /**
     * @param TaxShelterInput2019 $input
     */
    private function storeTussentotaalGrondslagVenB(TaxShelterInput2019 $input): void
    {
        $this->getOutput()->withTussentotaalGrondslagVennotschapsbelasting(
            $this->getOperations()->tussenTotaalGrondslagVennootschapsbelasting(
                $this->getFactory()->tussentotaalVenB(
                    $input,
                    array_last($this->getOutput()->getIteraties())
                )
            )
        );
    }

    /**
     * @return TaxShelterOutput2019
     */
    public function getOutput(): TaxShelterOutput2018
    {
        return parent::getOutput();
    }

    /**
     * @return TaxShelterOperations2019Interface
     */
    protected function getOperations(): TaxShelterOperationsInterface
    {
        return parent::getOperations();
    }

    private function storeKorfBeperking(int $aanslagjaar): void
    {
        $this->getOutput()->withKorfBeperking(
            $this->getOperations()->korfBeperking(
                array_last($this->getOutput()->getTussentotaalGrondslagVennootschapsbelasting()),
                $aanslagjaar
            )
        );
    }

    /**
     * @param TaxShelterInput2019 $input
     */
    private function storeGrondslagVenB(TaxShelterInput2019 $input): void
    {
        $this->getOutput()->withGrondslagVenB($this->getOperations()->grondslagVennootschapsbelasting(
            $this->getFactory()->grondslagVenootschapsbelastingInput2019(
                $input,
                array_last($this->getOutput()->getIteraties()),
                array_last($this->getOutput()->getTussentotaalGrondslagVennootschapsbelasting()),
                array_last($this->getOutput()->getKorfBeperkingen())
            )
        ));
    }

    /**
     * @return string
     */
    protected function getOutputClass(): string
    {
        return TaxShelterOutput2019::class;
    }

}

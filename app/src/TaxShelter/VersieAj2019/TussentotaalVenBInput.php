<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2019;

/**
 * Class TussentotaalVenBInput
 * @package Laudis\Calculators\TaxShelter\VersieAj2019
 */
final class TussentotaalVenBInput
{
    /** @var float */
    private $winstVoorBelasting;
    /** @var float */
    private $tantiemes;
    /** @var float */
    private $andereMutaties;
    /** @var float */
    private $andereVUGeenAftrekVerbod;
    /** @var float */
    private $andereVUWelAftrekVerbod;
    /** @var float */
    private $aftrekkenZonderKorfBeperking;
    /** @var float */
    private $taxShelterVrijstellingIteratie;

    /**
     * GrondslagVenootschapInput constructor.
     * @param float $winstVoorBelasting
     * @param float $tantiemes
     * @param float $andereMutaties
     * @param float $andereVUGeenAftrekVerbod
     * @param float $andereVUWelAftrekVerbod
     * @param float $aftrekkenZonderKorfBeperking
     * @param float $taxShelterVrijstellingIteratie
     */
    public function __construct(
        float $winstVoorBelasting,
        float $tantiemes,
        float $andereMutaties,
        float $andereVUGeenAftrekVerbod,
        float $andereVUWelAftrekVerbod,
        float $aftrekkenZonderKorfBeperking,
        float $taxShelterVrijstellingIteratie
    ) {
        $this->winstVoorBelasting = $winstVoorBelasting;
        $this->tantiemes = $tantiemes;
        $this->andereMutaties = $andereMutaties;
        $this->andereVUGeenAftrekVerbod = $andereVUGeenAftrekVerbod;
        $this->andereVUWelAftrekVerbod = $andereVUWelAftrekVerbod;
        $this->aftrekkenZonderKorfBeperking = $aftrekkenZonderKorfBeperking;
        $this->taxShelterVrijstellingIteratie = $taxShelterVrijstellingIteratie;
    }

    /**
     * @return float
     */
    public function getWinstVoorBelasting(): float
    {
        return $this->winstVoorBelasting;
    }

    /**
     * @return float
     */
    public function getTantiemes(): float
    {
        return $this->tantiemes;
    }

    /**
     * @return float
     */
    public function getAndereMutaties(): float
    {
        return $this->andereMutaties;
    }

    /**
     * @return float
     */
    public function getAndereVUGeenAftrekVerbod(): float
    {
        return $this->andereVUGeenAftrekVerbod;
    }

    /**
     * @return float
     */
    public function getAndereVUWelAftrekVerbod(): float
    {
        return $this->andereVUWelAftrekVerbod;
    }

    /**
     * @return float
     */
    public function getAftrekkenZonderKorfBeperking(): float
    {
        return $this->aftrekkenZonderKorfBeperking;
    }

    /**
     * @return float
     */
    public function getTaxShelterVrijstellingIteratie(): float
    {
        return $this->taxShelterVrijstellingIteratie;
    }
}

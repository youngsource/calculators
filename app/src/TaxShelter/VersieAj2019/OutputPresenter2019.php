<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2019;

use Laudis\Calculators\TaxShelter\OutputPresenter;

/**
 * Class OutputPresenter2019
 * @package Laudis\Calculators\TaxShelter\VersieAj2019
 */
final class OutputPresenter2019 extends OutputPresenter
{
    /**
     * @param TaxShelterOutput2019 $output
     * @return array
     */
    public function present(TaxShelterOutput2019 $output): array
    {
        return [
            'tussentotaalGrondslagVenB' => $this->display($output->getTussentotaalGrondslagVennootschapsbelasting()),
            'korfBeperkingen' => $this->display($output->getKorfBeperkingen()),
            'grondslagVenb' => $this->display($output->getGrondslagenVenBen()),
            'belastingenOpDeGrondslag' => $this->display($output->getBelastingenOpDeGrondslagen()),
            'grondslagVermeerdering' => $this->display($output->getGrondslagVermeerderingen()),
            'berekeningVanDeVermeerdering' => $this->display($output->getBerekeningVanDeVermeerderingen()),
            'geraamdeBelastingenGewoon' => $this->display($output->getGeraamdeBelastingenGewoons()),
            'totaleBelastingenGewoon' => $this->display($output->getTotaleBelastingenGewoon()),
            'grondslagLiquidatieReserve' => $this->display($output->getGrondslagLiquidateReserves()),
            'belastingenLiquidatieReserve' => $this->display($output->getBelastingenLiquidatieReserves()),
            'verworpenUitgavenBelastingen' => $this->display($output->getVerworpenUitgavenBelastingen()),
            'aanpassingenInMeer' => $this->display($output->getAanpassingenInMeeren()),
            'maximaleVrijstelling' => $this->display($output->getMaximaleVrijstellingen()),
            'maximaleInvestering' => $this->display($output->getMaximaleInvesteringen()),
            'iteraties' => $this->iteraties($output)
        ];
    }
}

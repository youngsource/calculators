<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2019;

use DateTimeInterface;
use Laudis\Calculators\TaxShelter\Input\TaxShelterInput;

/**
 * Class TaxShelterInput2019
 * @package Laudis\Calculators\TaxShelter\VersieAj2019
 */
final class TaxShelterInput2019 extends TaxShelterInput
{
    /** @var float */
    private $aftrekkenZonderKorfBeperking;
    /** @var float */
    private $aftrekkenMetKorfBeperking;

    /**
     * Calculator constructor.
     *
     * @param int $aanslagjaar
     * @param bool $maxTaxShelter
     * @param float $taxShelterInvestering
     * @param bool $maxLiqReserve
     * @param float $liqReserve
     * @param bool $isVerlaagdTarief
     * @param bool $isVermeedering
     * @param float $winstVoorBelasting
     * @param float $dividenden
     * @param float $tantiemes
     * @param float $AndereMutaties
     * @param float $AndereVerworpenUitgavenGeenAftrek
     * @param float $AndereVerworpenUitgavenWelAftrek
     * @param float $aftrekkenZonderKorfBeperking
     * @param float $voorafBetalingKwartaal1
     * @param float $voorafBetalingKwartaal2
     * @param float $voorafBetalingKwartaal3
     * @param float $voorafBetalingKwartaal4
     * @param float $terugBetaalbareVoorheffing
     * @param float $regularisatiesVorigejaren
     * @param float $aftrekkenMetKorfBeperking
     */
    public function __construct(
        int $aanslagjaar,
        bool $maxTaxShelter,
        float $taxShelterInvestering,
        bool $maxLiqReserve,
        float $liqReserve,
        bool $isVerlaagdTarief,
        bool $isVermeedering,
        float $winstVoorBelasting,
        float $dividenden,
        float $tantiemes,
        float $AndereMutaties,
        float $AndereVerworpenUitgavenGeenAftrek,
        float $AndereVerworpenUitgavenWelAftrek,
        float $aftrekkenZonderKorfBeperking,
        float $voorafBetalingKwartaal1,
        float $voorafBetalingKwartaal2,
        float $voorafBetalingKwartaal3,
        float $voorafBetalingKwartaal4,
        float $terugBetaalbareVoorheffing,
        float $regularisatiesVorigejaren,
        DateTimeInterface $paymentDate,
        float $aftrekkenMetKorfBeperking
    ) {
        parent::__construct(
            $aanslagjaar,
            $maxTaxShelter,
            $taxShelterInvestering,
            $maxLiqReserve,
            $liqReserve,
            $isVerlaagdTarief,
            $isVermeedering,
            $winstVoorBelasting,
            $dividenden,
            $tantiemes,
            $AndereMutaties,
            $AndereVerworpenUitgavenGeenAftrek,
            $AndereVerworpenUitgavenWelAftrek,
            $voorafBetalingKwartaal1,
            $voorafBetalingKwartaal2,
            $voorafBetalingKwartaal3,
            $voorafBetalingKwartaal4,
            $terugBetaalbareVoorheffing,
            $regularisatiesVorigejaren,
            $paymentDate
        );
        $this->aftrekkenZonderKorfBeperking = $aftrekkenZonderKorfBeperking;
        $this->aftrekkenMetKorfBeperking = $aftrekkenMetKorfBeperking;
    }

    /**
     * @return float
     */
    public function getAftrekkenZonderKorfBeperking(): float
    {
        return $this->aftrekkenZonderKorfBeperking;
    }

    /**
     * @return float
     */
    public function getAftrekkenMetKorfBeperking(): float
    {
        return $this->aftrekkenMetKorfBeperking;
    }

    /**
     * @return float
     */
    public function getAftrekBewerking(): float
    {
        return $this->getAftrekkenZonderKorfBeperking();
    }

}

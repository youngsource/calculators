<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2019;

use Laudis\Calculators\TaxShelter\Contracts\TaxShelterOperations2019Interface;
use Laudis\Calculators\TaxShelter\Contracts\TaxShelterOperationsInterface;
use Laudis\Calculators\TaxShelter\Input\AanpassingenInMeerInput;
use Laudis\Calculators\TaxShelter\Input\ActueleTaxShelterInput;
use Laudis\Calculators\TaxShelter\Input\BelastingenLiquidateReserveInput;
use Laudis\Calculators\TaxShelter\Input\BelastingOpDeGrondslagInput;
use Laudis\Calculators\TaxShelter\Input\BerekeningVermeerderingInput;
use Laudis\Calculators\TaxShelter\Input\GeraandeBelastingenGewoonInput;
use Laudis\Calculators\TaxShelter\Input\GrondslagVermeerderingInput;
use Laudis\Calculators\TaxShelter\Input\MaximaleInvesteringInput;
use Laudis\Calculators\TaxShelter\Input\MaximaleVrijstellingInput;
use Laudis\Calculators\TaxShelter\Input\TaxShelterIteratie;
use Laudis\Calculators\TaxShelter\Input\TotaleBelastingenGewoonInput;

/**
 * Class TaxShelterCalculatorVanaf2019
 * @package Laudis\Calculators\TaxShelter\VersieAj2019
 */
final class TaxShelterOperations2019 implements TaxShelterOperations2019Interface
{
    /** @var TaxShelterOperationsInterface */
    private $operations;

    /**
     * TaxShelterCalculatorVanaf2019 constructor.
     * @param TaxShelterOperationsInterface $operations
     */
    public function __construct(TaxShelterOperationsInterface $operations)
    {
        $this->operations = $operations;
    }

    /**
     * {@inheritDoc}
     * @param GrondslagVennootschapInput2019 $input
     */
    public function grondslagVennootschapsbelasting($input): float
    {
        return max(
                $input->getTussentotaal() - min($input->getAftrekkenMetKorfBeperking(), $input->getKorfBeperking()),
                0
            ) + $input->getAndereVUWelAftrekVerbod();
    }

    /**
     * @param TussentotaalVenBInput $input
     * @return float
     */
    public function tussenTotaalGrondslagVennootschapsbelasting(TussentotaalVenBInput $input): float
    {
        return $input->getWinstVoorBelasting()
            - $input->getTantiemes()
            - $input->getTaxShelterVrijstellingIteratie()
            + $input->getAndereMutaties()
            + $input->getAndereVUGeenAftrekVerbod()
            - $input->getAftrekkenZonderKorfBeperking();
    }

    /**
     * {@inheritDoc}
     */
    public function belastingOpDeGrondslag(BelastingOpDeGrondslagInput $input): float
    {
        if ($input->isIsverlaagTarief()) {
            return $input->getVerlaagdTarief()->calculate($input->getGrondslagVenB());
        }
        return $input->getGewoonTarief() * $input->getGrondslagVenB();
    }

    /**
     * {@inheritDoc}
     */
    public function grondslagVermeerdering(GrondslagVermeerderingInput $input): float
    {
        return $this->operations->grondslagVermeerdering($input);
    }

    /**
     * {@inheritDoc}
     */
    public function berekeningVanDeVermeerdering(BerekeningVermeerderingInput $input): float
    {
        return $this->operations->berekeningVanDeVermeerdering($input);
    }

    /**
     * {@inheritDoc}
     */
    public function geraamdeBelastingenGewoon(GeraandeBelastingenGewoonInput $input): float
    {
        return $this->operations->geraamdeBelastingenGewoon($input);
    }

    /**
     * {@inheritDoc}
     */
    public function totaleBelastingenGewoon(TotaleBelastingenGewoonInput $input): float
    {
        return $this->operations->totaleBelastingenGewoon($input);
    }

    /**
     * {@inheritDoc}
     */
    public function grondslagLiquidatieReserve(BelastingenLiquidateReserveInput $input): float
    {
        return $this->operations->grondslagLiquidatieReserve($input);
    }

    /**
     * {@inheritDoc}
     */
    public function belastingLiquidatieReserve(float $input): float
    {
        return $this->operations->belastingLiquidatieReserve($input);
    }

    /**
     * {@inheritDoc}
     */
    public function aanpassingInMeer(AanpassingenInMeerInput $input): float
    {
        return $this->operations->aanpassingInMeer($input);
    }

    /**
     * {@inheritDoc}
     */
    public function verworpenUitgevenBelastingen(AanpassingenInMeerInput $input): float
    {
        return $this->operations->verworpenUitgevenBelastingen($input);
    }

    /**
     * {@inheritDoc}
     */
    public function taxShelter(ActueleTaxShelterInput $input): TaxShelterIteratie
    {
        return $this->operations->taxShelter($input);
    }

    /**
     * {@inheritDoc}
     */
    public function korfBeperking(float $tussentotaal, int $aanslagjaar): float
    {
        $percentage = 0.7;

        if ($aanslagjaar >= 2024) {
            $percentage = 0.4;
        }

        return 1000000 + max($tussentotaal - 1000000, 0) * $percentage;
    }

    /**
     * @param MaximaleVrijstellingInput $input
     * @return float
     */
    public function maximaleVrijstelling(MaximaleVrijstellingInput $input): float
    {
        return $this->operations->maximaleVrijstelling($input);
    }

    /**
     * @param MaximaleInvesteringInput $input
     * @return float
     */
    public function maximaleInvestering(MaximaleInvesteringInput $input): float
    {
        return $this->operations->maximaleInvestering($input);
    }
}

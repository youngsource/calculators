<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2019;

use Laudis\Calculators\TaxShelter\TaxShelterIteratiePresenter;
use Laudis\Calculators\TaxShelter\TaxShelterOutput2018;
use Laudis\Calculators\TaxShelter\TaxShelterResult;

/**
 * Class TaxShelterOutput2019
 * @package Laudis\Calculators\TaxShelter\VersieAj2019
 */
final class TaxShelterOutput2019 extends TaxShelterOutput2018
{
    /** @var float[] */
    private $korfBeperkingen = [];
    /** @var float[] */
    private $tussentotaalGrondslagVennootschapsbelasting = [];

    /**
     * @return float[]
     */
    public function getKorfBeperkingen(): array
    {
        return $this->korfBeperkingen;
    }

    /**
     * @param float $value
     */
    public function withKorfBeperking(float $value): void
    {
        $this->korfBeperkingen[] = $value;
    }

    /**
     * @return float[]
     */
    public function getTussentotaalGrondslagVennootschapsbelasting(): array
    {
        return $this->tussentotaalGrondslagVennootschapsbelasting;
    }

    /**
     * @param float $value
     */
    public function withTussentotaalGrondslagVennotschapsbelasting(float $value): void
    {
        $this->tussentotaalGrondslagVennootschapsbelasting[] = $value;
    }/** @noinspection PhpMissingParentCallCommonInspection */

    /**
     * Presents the output of the calculation as an array.
     *
     * @return array<string,int|float|string|bool>
     */
    public function output(): array
    {
        $result = new TaxShelterResult(
            $this,
            new OutputPresenter2019(
                new TaxShelterIteratiePresenter
            )
        );
        return $result->output();
    }
}

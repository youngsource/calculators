<?php
declare(strict_types=1);

namespace Laudis\Calculators\TaxShelter\VersieAj2019;

/**
 * Class GrondslagVenootschapInput
 * @package Laudis\Calculators\Calculators\Input
 */
class GrondslagVennootschapInput2019
{
    /** @var float */
    private $andereVUWelAftrekVerbod;
    /** @var float */
    private $taxShelterVrijstellingIteratie;
    /**
     * @var float
     */
    private $aftrekkenZonderKorfBeperking;
    /**
     * @var float
     */
    private $aftrekkenMetKorfBeperking;
    /**
     * @var float
     */
    private $tussentotaal;
    /**
     * @var float
     */
    private $korfBeperking;

    /**
     * GrondslagVenootschapInput constructor.
     * @param float $andereVUWelAftrekVerbod
     * @param float $taxShelterVrijstellingIteratie
     * @param float $aftrekkenZonderKorfBeperking
     * @param float $aftrekkenMetKorfBeperking
     * @param float $tussentotaal
     * @param float $korfBeperking
     */
    public function __construct(
        float $andereVUWelAftrekVerbod,
        float $taxShelterVrijstellingIteratie,
        float $aftrekkenZonderKorfBeperking,
        float $aftrekkenMetKorfBeperking,
        float $tussentotaal,
        float $korfBeperking
    ) {
        $this->andereVUWelAftrekVerbod = $andereVUWelAftrekVerbod;
        $this->taxShelterVrijstellingIteratie = $taxShelterVrijstellingIteratie;
        $this->aftrekkenZonderKorfBeperking = $aftrekkenZonderKorfBeperking;
        $this->aftrekkenMetKorfBeperking = $aftrekkenMetKorfBeperking;
        $this->tussentotaal = $tussentotaal;
        $this->korfBeperking = $korfBeperking;
    }

    /**
     * @return float
     */
    public function getAndereVUWelAftrekVerbod(): float
    {
        return $this->andereVUWelAftrekVerbod;
    }

    /**
     * @return float
     */
    public function getTaxShelterVrijstellingIteratie(): float
    {
        return $this->taxShelterVrijstellingIteratie;
    }

    /**
     * @return float
     */
    public function getAftrekkenZonderKorfBeperking(): float
    {
        return $this->aftrekkenZonderKorfBeperking;
    }

    /**
     * @return float
     */
    public function getAftrekkenMetKorfBeperking(): float
    {
        return $this->aftrekkenMetKorfBeperking;
    }

    /**
     * @return float
     */
    public function getTussentotaal(): float
    {
        return $this->tussentotaal;
    }

    /**
     * @return float
     */
    public function getKorfBeperking(): float
    {
        return $this->korfBeperking;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\Collections;

use ArrayIterator;
use Iterator;
use IteratorAggregate;
use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\Exceptions\CalculatorNotFoundException;
use function lazy_get;

/**
 * Class CalculatorFactoriesCollection.
 * Encapsulates a collection of Calculator factories for easy and type safe usage.
 *
 * @package Laudis\Calculators\Collections
 */
final class CalculatorFactoriesCollection implements IteratorAggregate
{
    /** @var array<string, CalculatorFactoryInterface|callable():CalculatorFactoryInterface> */
    private $calculators = [];

    /**
     * @return Iterator
     */
    public function getIterator(): Iterator
    {
        return new ArrayIterator($this->calculators);
    }

    /**
     * Gets the calculator by its registered name.
     *
     * @param string $calculatorName
     * @return CalculatorFactoryInterface
     * @throws CalculatorNotFoundException
     */
    public function get(string $calculatorName): CalculatorFactoryInterface
    {
        if (!$this->hasCalculator($calculatorName)) {
            throw new CalculatorNotFoundException("Calculator with name: $calculatorName not found");
        }
        return lazy_get($this->calculators[$calculatorName]);
    }

    /**
     * Checks to see if the collection has the calculator registered under the given name.
     *
     * @param string $calculatorName
     * @return bool
     */
    public function hasCalculator(string $calculatorName): bool
    {
        return isset($this->calculators[$calculatorName]);
    }

    /**
     * Registers the calculator with the given name.
     *
     * @param string $calculatorName
     * @param callable():CalculatorFactoryInterface|CalculatorFactoryInterface  $value
     */
    public function register(string $calculatorName, $value): void
    {
        $this->calculators[$calculatorName] = $value;
    }

    /**
     * Returns all the names of the calculators in the collection.
     *
     * @return array<int, string>
     */
    public function getAllCalculatorNames(): array
    {
        return array_keys($this->calculators);
    }
}

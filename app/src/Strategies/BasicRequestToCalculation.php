<?php
declare(strict_types=1);

namespace Laudis\Calculators\Strategies;

use BadMethodCallException;
use Laudis\Calculators\Calculators\Results\FormattedResult;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\Exceptions\CalculatorNotFoundException;
use Laudis\Common\Contracts\RequestToCalculationInterface;
use Laudis\Common\Exceptions\ValidationException;
use NumberFormatter;
use Psr\Http\Message\ServerRequestInterface;
use function count;
use function get_class;
use function is_numeric;
use function is_string;
use function method_exists;
use function str_replace;

/**
 * Class BasicRequestToCalculation
 * Basic and essential behaviour to transform the request to a calculation.
 *
 * @package Laudis\Calculators\Strategies
 */
final class BasicRequestToCalculation implements RequestToCalculationInterface
{
    /** @var CalculatorFactoriesCollection */
    private $collection;
    /** @var NumberFormatter */
    private $formatter;

    /**
     * BasicRequestToCalculation constructor.
     * @param CalculatorFactoriesCollection $collection
     * @param NumberFormatter $formatter
     */
    public function __construct(CalculatorFactoriesCollection $collection, NumberFormatter $formatter)
    {
        $this->collection = $collection;
        $this->formatter = $formatter;
    }

    /**
     * Combines the key components defined in CalculatorFactoryInterface to operate with each other.
     *
     * @param ServerRequestInterface $request
     * @param string $calculator
     *
     * @return CalculationResultInterface
     *
     * @throws ValidationException
     * @throws CalculatorNotFoundException
     * @throws BadMethodCallException
     */
    public function run(ServerRequestInterface $request, string $calculator): CalculationResultInterface
    {
        $factory = $this->collection->get($calculator);
        $parsed = $this->parseRequest($request);
        $this->guardValidation($parsed, $factory);
        $calculatorInstance = $factory->calculator($parsed);
        $this->guardDuckTyping($calculatorInstance);
        return new FormattedResult(
            $calculatorInstance->calculate($factory->inputFromArray($parsed)),
            $this->formatter
        );
    }

    /**
     * Guards the strategy against invalid validation.
     *
     * @param array $values
     * @param CalculatorFactoryInterface $factory
     */
    private function guardValidation(array $values, CalculatorFactoryInterface $factory): void
    {
        $validation = $factory->validation($values);
        $validation->validate();
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
    }

    /**
     * Guards against invalid object not having the calculate method.
     *
     * @param object $calculatorInstance
     */
    private function guardDuckTyping(object $calculatorInstance): void
    {
        if (!method_exists($calculatorInstance, 'calculate')) {
            $class = get_class($calculatorInstance);
            throw new BadMethodCallException("The instance of calculator $class has no calculate method.");
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @return array
     */
    private function parseRequest(ServerRequestInterface $request): array
    {
        $parsed = array_map(static function ($value) {
            if (is_string($value)) {
                $explosion = explode(',', trim($value));
                if (count($explosion) === 2 && is_numeric(str_replace(['', '.'], ['', ''], $explosion[0])) && is_numeric($explosion[1])) {
                    return str_replace([' ','.', ','], ['', '', '.'], trim($value));
                }
                return $value;
            }
            return $value;
        }, $request->getParsedBody());
        return $parsed;
    }
}

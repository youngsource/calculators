<?php

namespace Laudis\Calculators\Strategies;

use Facebook\WebDriver\Exception\NullPointerException;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Venb\BasicCalculationResult;
use Laudis\Common\Contracts\RequestToCalculationInterface;
use Laudis\UserManagement\UserManager;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Throwable;
use function GuzzleHttp\Psr7\str;

final class StoreMetaData implements RequestToCalculationInterface
{
    /**
     * @var RequestToCalculationInterface
     */
    private $requestToCalculation;
    /**
     * @var UserManager
     */
    private $userManager;
    /**
     * @var \PDO
     */
    private $pdo;

    public function __construct(
        BasicRequestToCalculation $requestToCalculation,
        UserManager $userManager,
        \PDO $pdo
    ) {
        $this->requestToCalculation = $requestToCalculation;
        $this->userManager = $userManager;
        $this->pdo = $pdo;
    }

    /**
     * @param Request $request
     * @param string $calculator
     * @return CalculationResultInterface
     * @throws Throwable
     */
    public function run(ServerRequestInterface $request, string $calculator) : CalculationResultInterface
    {
//        $loggedInUser  =  $this->userManager->getLoggedInUser();
//        $userID = 0;
//        if($loggedInUser){
//            $userID = $loggedInUser->getId();
//        }
//
//        //Input as a json file
//        $input = json_encode($request->getParsedBody(),JSON_THROW_ON_ERROR);
//        //Datetime before calculation
//        $calculationTimeStamp =  date('Y-m-d H:i:s');
//        //response as a json file
//        $error = false;
//        $errorMessage = "";
        try {
            $tbr = $this->requestToCalculation->run($request, $calculator);
            //$userID = $loggedInUser->getId() ?? 0 ;
        } catch (Throwable $throwable) {
            $error = true;
            $errorMessage = $throwable->getMessage();
            throw $throwable;
        }
//        //date after calculation
//        $calculedTimeStamp =  date('Y-m-d H:i:s');
//        $calculator_version = 0.1;
//        // Store output data here
//        $output =  json_encode($tbr->output(), JSON_THROW_ON_ERROR);
//
//        $sql = "INSERT INTO stored_meta_data (userID,calculator,calculator_version,error,error_message,input,calculation_timestamp,output,calculated_timestamp) VALUES (?,?,?,?,?,?,?,?,?)";
//        $stmt = $this->pdo->prepare($sql);
//        $stmt->execute([$userID,$calculator,$calculator_version,$error,$errorMessage,gettype($input),$calculationTimeStamp,gettype($output),$calculedTimeStamp]);

        return $tbr;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $params
     * @return array
     * @throws Throwable
     */
    public function __invoke(Request $request, Response $response, array $params)
    {
        return $this->run($request, $params['calculator'])->output();
    }
}

<?php


namespace Laudis\Calculators\GramFormula;


use Laudis\Calculators\BrandstofEnumVanaf2020;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Venb\BasicCalculationResult;

/**
 * Class GramForulaCalculator
 * @package Laudis\Calculators\GramFormula
 */
final class GramFormulaCalculator
{
    /** @var array */
    private $factors;

    public function __construct()
    {
        $this->factors = [
            BrandstofEnumVanaf2020::BENZINE()->getValue() => 0.95,
            BrandstofEnumVanaf2020::CNG_MIN_12PK()->getValue() => 0.9,
            BrandstofEnumVanaf2020::CNG_PLUS_12PK()->getValue() => 0.95,
            BrandstofEnumVanaf2020::DIESEL()->getValue() => 1.0,
            BrandstofEnumVanaf2020::LPG()->getValue() => 0.95,
            BrandstofEnumVanaf2020::HYBRIDE_BENZINE()->getValue() => 0.95,
            BrandstofEnumVanaf2020::HYBRIDE_DIESEL()->getValue() => 0.95,
            BrandstofEnumVanaf2020::HYBRIDE_CNG_MIN_12PK()->getValue() => 0.9,
            BrandstofEnumVanaf2020::HYBRIDE_CNG_PLUS_12PK()->getValue() => 0.95,
            BrandstofEnumVanaf2020::HYBRIDE_LPG()->getValue() => 0.95,
            BrandstofEnumVanaf2020::ELEKTRISCH()->getValue() => 0.95
        ];
    }

    public function calculate(GramFormulaInput $input): CalculationResultInterface
    {
        $actualCo2 = $this->calculateActualCo2($input);

        $modifier = $this->getModifier($input);
        $formulaResult = (120 - 0.5 * $actualCo2 * $modifier) / 100;
        $minimumValue = ($actualCo2 >= 200.0) ? 0.4 : 0.5;
        $maximumValue = 1.0;

        $result = max(min($formulaResult, $maximumValue), $minimumValue);
        $resultPercentage = $result * 100;

        return new BasicCalculationResult(
            compact(
                'actualCo2',
                'result',
                'resultPercentage',
                'modifier',
                'formulaResult',
                'minimumValue',
                'maximumValue'
            )
        );
    }

    private function getModifier(GramFormulaInput $input): float
    {
        return $this->factors[$input->getBrandstof()->getValue()];
    }

    /**
     * @param GramFormulaInput $input
     * @return float
     */
    private function calculateActualCo2(GramFormulaInput $input): float
    {
        $actualCo2 = $input->getCo2Uitstoot();
        if ($input->isFakeHybrid()) {
            if ($input->knownCo2FakeHybrid()) {
                $actualCo2 = $input->getCo2FakeHybrid();
            } else {
                $actualCo2 = $input->getCo2Uitstoot() * 2.5;
            }
        }
        return $actualCo2;
    }
}
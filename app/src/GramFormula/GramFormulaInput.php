<?php


namespace Laudis\Calculators\GramFormula;


use Laudis\Calculators\BrandstofEnumVanaf2020;

final class GramFormulaInput
{
    /**
     * @var BrandstofEnumVanaf2020
     */
    private $brandstofType;
    /**
     * @var float
     */
    private $co2Uitstoot;
    /**
     * @var bool
     */
    private $fakeHybrid;
    /**
     * @var float
     */
    private $co2FakeHybrid;
    /**
     * @var bool
     */
    private $knownCo2FakeHybrid;

    /**
     * GramFormulaInput constructor.
     * @param BrandstofEnumVanaf2020 $brandstofType
     * @param float $co2Uitstoot
     * @param bool $isFakeHybrid
     * @param float $co2FakeHybrid
     * @param bool $knownCo2FakeHybrid
     */
    public function __construct(BrandstofEnumVanaf2020 $brandstofType, float $co2Uitstoot, bool $isFakeHybrid, float $co2FakeHybrid, bool $knownCo2FakeHybrid)
    {
        $this->brandstofType = $brandstofType;
        $this->co2Uitstoot = $co2Uitstoot;
        $this->fakeHybrid = $isFakeHybrid;
        $this->co2FakeHybrid = $co2FakeHybrid;
        $this->knownCo2FakeHybrid = $knownCo2FakeHybrid;
    }

    /**
     * @return bool
     */
    public function knownCo2FakeHybrid(): bool
    {
        return $this->knownCo2FakeHybrid;
    }

    /**
     * @return BrandstofEnumVanaf2020
     */
    public function getBrandstof(): BrandstofEnumVanaf2020
    {
        return $this->brandstofType;
    }

    /**
     * @return float
     */
    public function getCo2Uitstoot(): float
    {
        return $this->co2Uitstoot;
    }

    /**
     * @return bool
     */
    public function isFakeHybrid(): bool
    {
        return $this->fakeHybrid;
    }

    /**
     * @return float
     */
    public function getCo2FakeHybrid(): float
    {
        return $this->co2FakeHybrid;
    }
}
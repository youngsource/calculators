<?php


namespace Laudis\Calculators\GramFormula;


use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

final class GramFormulaCalculatorFactory implements CalculatorFactoryInterface
{
    /**
     * Returns the calculator.
     *
     * @param array $values
     * @return GramFormulaCalculator The calculator must have a calculate method which accepts the input from the request and returns
     *                  the result encapsulated in a CalculationResultInterface.
     */
    public function calculator(array $values): GramFormulaCalculator
    {
        return new GramFormulaCalculator();
    }

    /**
     * Builds the input as a standard databag from the current server request.
     *
     * @param array $values
     * @return object
     */
    public function inputFromArray(array $values): object
    {
        return new GramFormulaInput(
            BrandstofEnum::resolve($values['brandstofType']),
            $values['co2Uitstoot'] ?? 0.0,
            $values['isFakeHybrid'] ?? false,
            $values['co2FakeHybrid'] ?? 0.0,
            $values['knownCo2FakeHybrid'] ?? false,
        );
    }

    /**
     * Derives the validation from the current input values.
     *
     * @param array $values
     * @return Validation
     */
    public function validation(array $values): Validation
    {
        return (new Validator())->make($values, []);
    }
}
<?php


namespace Laudis\Calculators\VoordeelWagen\Results;


class YearResult
{
    /** @var BetweenDatesResult */
    private $dates;
    /** @var OuderdomResult */
    private $ouderdom;
    /** @var CO2InfoResult */
    private $co2InfoResult;
    /** @var StagesResult */
    private $stages;
    /** @var VoordelenAlleAardResult */
    private $voordelenAlleAard;
    /** @var VenootschapInfoResult */
    private $vennootschapsInfo;

    /**
     * YearResult constructor.
     * @param BetweenDatesResult $dates
     * @param OuderdomResult $ouderdom
     * @param CO2InfoResult $co2InfoResult
     * @param StagesResult $stages
     * @param VoordelenAlleAardResult $voordelenAlleAard
     * @param VenootschapInfoResult $vennootschapsInfo
     */
    public function __construct(BetweenDatesResult $dates, OuderdomResult $ouderdom, CO2InfoResult $co2InfoResult, StagesResult $stages, VoordelenAlleAardResult $voordelenAlleAard, VenootschapInfoResult $vennootschapsInfo)
    {
        $this->dates = $dates;
        $this->ouderdom = $ouderdom;
        $this->co2InfoResult = $co2InfoResult;
        $this->stages = $stages;
        $this->voordelenAlleAard = $voordelenAlleAard;
        $this->vennootschapsInfo = $vennootschapsInfo;
    }

    /**
     * @return BetweenDatesResult
     */
    public function getDates(): BetweenDatesResult
    {
        return $this->dates;
    }

    /**
     * @return OuderdomResult
     */
    public function getOuderdom(): OuderdomResult
    {
        return $this->ouderdom;
    }

    /**
     * @return CO2InfoResult
     */
    public function getCo2InfoResult(): CO2InfoResult
    {
        return $this->co2InfoResult;
    }

    /**
     * @return StagesResult
     */
    public function getStages(): StagesResult
    {
        return $this->stages;
    }

    /**
     * @return VoordelenAlleAardResult
     */
    public function getVoordelenAlleAard(): VoordelenAlleAardResult
    {
        return $this->voordelenAlleAard;
    }

    /**
     * @return VenootschapInfoResult
     */
    public function getVennootschapsInfo(): VenootschapInfoResult
    {
        return $this->vennootschapsInfo;
    }

    public function output():array
    {
        return
        [
            'dates' => $this->getDates()->output(),
            'ouderdom' => $this->getOuderdom()->output(),
            'co2InfoResult' => $this->getCo2InfoResult()->output(),
            'stages' => $this->getStages()->output(),
            'voordeelAlleAard' => $this->getVoordelenAlleAard()->output(),
            'vennootschapsInfo' => $this->getVennootschapsInfo()->output()
        ];
    }
}
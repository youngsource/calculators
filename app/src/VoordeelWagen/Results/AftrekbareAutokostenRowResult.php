<?php


namespace Laudis\Calculators\VoordeelWagen\Results;


use function number_format;

class AftrekbareAutokostenRowResult
{
    /**
     * @var string
     */
    private $autokostenString;
    /**
     * @var float
     */
    private $brandstofPercentage;
    /**
     * @var float
     */
    private $anderePercentage;

    /**
     * AftrekbareAutokostenRowResult constructor.
     * @param string $autokostenString
     * @param float $brandstofPercentage
     * @param float $anderePercentage
     */
    public function __construct(string $autokostenString, float $brandstofPercentage, float $anderePercentage)
    {
        $this->autokostenString = $autokostenString;
        $this->brandstofPercentage = $brandstofPercentage;
        $this->anderePercentage = $anderePercentage;
    }

    /**
     * @return string
     */
    public function getAutokostenString(): string
    {
        return $this->autokostenString;
    }

    /**
     * @return float
     */
    public function getBrandstofPercentage(): float
    {
        return $this->brandstofPercentage;
    }

    /**
     * @return float
     */
    public function getAnderePercentage(): float
    {
        return $this->anderePercentage;
    }

    public function output(): array
    {
        return [
            'autokostenString' => $this->getAutokostenString(),
            'brandstofPercentage' => number_format($this->getBrandstofPercentage(), 2, ',', '.') . '%',
            'anderePercentage' => number_format($this->getAnderePercentage(), 2, ',', '.') . '%'
        ];
    }
}
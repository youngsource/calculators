<?php
declare(strict_types=1);

namespace Laudis\Calculators\VoordeelWagen\Results;


use DateTime;
use DateTimeInterface;
use Laudis\Calculators\VoordeelWagen\Interfaces\ArrayOutputInterface;

class BetweenDatesResult implements ArrayOutputInterface
{
    /** @var DateTimeInterface */
    private $fromDate;
    /** @var DateTimeInterface */
    private $toDate;

    /**
     * betweenDatesResult constructor.
     * @param DateTimeInterface $fromDate
     * @param DateTimeInterface $toDate
     */
    public function __construct(DateTimeInterface $fromDate, DateTimeInterface $toDate)
    {
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
    }

    /**
     * @return DateTime
     */
    public function getFromDate(): DateTimeInterface
    {
        return $this->fromDate;
    }

    /**
     * @return DateTime
     */
    public function getToDate(): DateTimeInterface
    {
        return $this->toDate;
    }

    public function output() : array
    {
        return [
            'from' => $this->getFromDate()->format('Y-m-d'),
            'to' => $this->getToDate()->format('Y-m-d')
        ];
    }


}

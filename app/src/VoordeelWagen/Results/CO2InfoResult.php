<?php

namespace Laudis\Calculators\VoordeelWagen\Results;

use Laudis\Calculators\VoordeelWagen\Interfaces\ArrayOutputInterface;

class CO2InfoResult implements ArrayOutputInterface
{
    /** @var float */
    private $referentie;
    /** @var float */
    private $percentage;

    /**
     * CO2InfoResult constructor.
     * @param float $referentie
     * @param float $percentage
     */
    public function __construct(float $referentie, float $percentage)
    {
        $this->referentie = $referentie;
        $this->percentage = $percentage;
    }

    public function output(): array
    {
        return [
            'referentie' => '' . round($this->getReferentie(),2),
            'pecentage' => '' . round($this->getPercentage(),4)
        ];
    }

    /**
     * @return float
     */
    public function getReferentie(): float
    {
        return $this->referentie;
    }

    /**
     * @return float
     */
    public function getPercentage(): string
    {
        return $this->percentage;
    }
}

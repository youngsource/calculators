<?php


namespace Laudis\Calculators\VoordeelWagen\Results;


use DateTime;
use DateTimeInterface;
use Laudis\Calculators\VoordeelWagen\Interfaces\ArrayOutputInterface;

class OuderdomStageResult implements ArrayOutputInterface
{
    /** @var DateTimeInterface */
    private $fromDate;
    /** @var DateTimeInterface */
    private $toDate;
    /** @var integer */
    private $daysBetween;
    /** @var float */
    private $percentage;

    /**
     * OuderdomStageResult constructor.
     * @param DateTimeInterface|null $fromDate
     * @param DateTimeInterface|null $toDate
     * @param int|null $daysBetween
     * @param float|null $percentage
     */
    public function __construct(
        DateTimeInterface $fromDate = null,
        DateTimeInterface $toDate = null,
        int $daysBetween = null,
        float $percentage = null)
    {
        $this->fromDate = $fromDate;
        $this->toDate = $toDate;
        $this->daysBetween = $daysBetween;
        $this->percentage = $percentage;
    }

    /**
     * @return DateTime
     */
    public function getFromDate(): ?DateTimeInterface
    {
        return $this->fromDate;
    }

    /**
     * @return DateTimeInterface
     */
    public function getToDate(): DateTimeInterface
    {
        return $this->toDate;
    }

    /**
     * @return int
     */
    public function getDaysBetween(): int
    {
        return $this->daysBetween;
    }

    /**
     * @return float
     */
    public function getPercentage(): float
    {
        return $this->percentage;
    }

    public function output() : array
    {
        return
            [
                'ouderdom' => 'van ' .  $this->getFromDate()->format('Y-m-d') . ' tot ' . $this->getToDate()->format('Y-m-d'),
                'dagen' => '' .$this->getDaysBetween(),
                'percentage' => '' . number_format(round($this->getPercentage(),4),4,',','.')
            ];
    }
}

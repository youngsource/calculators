<?php

namespace Laudis\Calculators\VoordeelWagen\Results;

use Laudis\Calculators\VoordeelWagen\Interfaces\ArrayOutputInterface;

class VenootschapInfoResult implements ArrayOutputInterface
{
    /** @var float */
    private $grondslag;
    /** @var float */
    private $tarief;
    /** @var float */
    private $info1206;

    /**
     * VenootschapInfoResult constructor.
     * @param float $grondslag
     * @param float $tarief
     * @param float $info1206
     */
    public function __construct(float $grondslag, float $tarief, float $info1206)
    {
        $this->grondslag = $grondslag;
        $this->tarief = $tarief;
        $this->info1206 = $info1206;
    }

    public function output(): array
    {
        return [
            'info' => [
            'grondslag' => '' . round($this->getGrondslag(),2),
            'tarief' => '' . number_format(round($this->getTarief()*100,2),2,',','.') . '%',
            'c1206' => '' . round($this->getInfo1206(),2)
                ]
        ];
    }

    /**
     * @return float
     */
    public function getGrondslag(): float
    {
        return $this->grondslag;
    }

    /**
     * @return float
     */
    public function getTarief(): float
    {
        return $this->tarief;
    }

    /**
     * @return float
     */
    public function getInfo1206(): float
    {
        return $this->info1206;
    }

}

<?php


namespace Laudis\Calculators\VoordeelWagen\Results;


use Casbin\RbacApi;

class AftrekbareAutokostenResult
{
    /**
     * @var AftrekbareAutokostenRowResult
     */
    private $venTotEnMetRow;

    private $venVanafRow;

    private $eenmansZaakTotEnMetRow;

    private $eenmanszaakIn2019En2020Row;

    private $eenmansZaakVanafRow;

    /**
     * AftrekbareAutokostenResult constructor.
     * @param AftrekbareAutokostenRowResult $venTotEnMetRow
     * @param $venVanafRow
     * @param $eenmansZaakTotEnMetRow
     * @param $eenmanszaakIn2019En2020Row
     * @param $eenmansZaakVanafRow
     */
    public function __construct(
        AftrekbareAutokostenRowResult $venTotEnMetRow,
        $venVanafRow,
        $eenmansZaakTotEnMetRow,
        $eenmanszaakIn2019En2020Row,
        $eenmansZaakVanafRow
    ) {
        $this->venTotEnMetRow = $venTotEnMetRow;
        $this->venVanafRow = $venVanafRow;
        $this->eenmansZaakTotEnMetRow = $eenmansZaakTotEnMetRow;
        $this->eenmanszaakIn2019En2020Row = $eenmanszaakIn2019En2020Row;
        $this->eenmansZaakVanafRow = $eenmansZaakVanafRow;
    }

    /**
     * @return AftrekbareAutokostenRowResult
     */
    public function getVenTotEnMetRow(): AftrekbareAutokostenRowResult
    {
        return $this->venTotEnMetRow;
    }

    /**
     * @return mixed
     */
    public function getVenVanafRow()
    {
        return $this->venVanafRow;
    }

    /**
     * @return mixed
     */
    public function getEenmansZaakTotEnMetRow()
    {
        return $this->eenmansZaakTotEnMetRow;
    }

    /**
     * @return mixed
     */
    public function getEenmanszaakIn2019En2020Row()
    {
        return $this->eenmanszaakIn2019En2020Row;
    }

    /**
     * @return mixed
     */
    public function getEenmansZaakVanafRow()
    {
        return $this->eenmansZaakVanafRow;
    }

    public function output() : array {
        $tbr = [];
        $tbr[] = $this->getVenTotEnMetRow()->output();
        $tbr[] = $this->getVenVanafRow()->output();
        $tbr[] = $this->getEenmansZaakTotEnMetRow()->output();
        $tbr[] = $this->getEenmanszaakIn2019En2020Row()->output();
        $tbr[] = $this->getEenmansZaakVanafRow()->output();

        return $tbr;
    }
}
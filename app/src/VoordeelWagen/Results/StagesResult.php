<?php

namespace Laudis\Calculators\VoordeelWagen\Results;

use Laudis\Calculators\VoordeelWagen\Interfaces\ArrayOutputInterface;

class StagesResult implements ArrayOutputInterface
{
    /** @var string */
    private $calculationStringStage1;
    /** @var float */
    private $resultStage1;
    /** @var string */
    private $calculationStringStage2;
    /** @var float */
    private $resultStage2;

    /**
     * StagesResult constructor.
     * @param string $calculationStringStage1
     * @param float $resultStage1
     * @param string $calculationStringStage2
     * @param float $resultStage2
     */
    public function __construct(
        string $calculationStringStage1,
        float $resultStage1,
        string $calculationStringStage2,
        float $resultStage2
    ) {
        $this->calculationStringStage1 = $calculationStringStage1;
        $this->resultStage1 = $resultStage1;
        $this->calculationStringStage2 = $calculationStringStage2;
        $this->resultStage2 = $resultStage2;
    }

    public function getTotal(): float
    {
        return $this->getResultStage1() + $this->getResultStage2();
    }

    /**
     * @return float
     */
    public function getResultStage1(): float
    {
        return $this->resultStage1;
    }

    /**
     * @return float
     */
    public function getResultStage2(): float
    {
        return $this->resultStage2;
    }

    public function output(): array
    {
        return [
            'stage1' => [
            'calculationStringStage1' => $this->getCalculationStringStage1(),
            'resultStage1' => '' . round($this->getResultStage1(),2),
                ],
            'stage2' => [
            'calculationStringStage2' => $this->getCalculationStringStage2(),
            'resultStage2' => '' . round($this->getResultStage2(),2),
                ]
        ];
    }

    /**
     * @return string
     */
    public function getCalculationStringStage1(): string
    {
        return $this->calculationStringStage1;
    }

    /**
     * @return string
     */
    public function getCalculationStringStage2(): string
    {
        return $this->calculationStringStage2;
    }

}

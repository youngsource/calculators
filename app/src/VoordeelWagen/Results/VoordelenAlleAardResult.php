<?php


namespace Laudis\Calculators\VoordeelWagen\Results;


use Laudis\Calculators\VoordeelWagen\Interfaces\ArrayOutputInterface;

class VoordelenAlleAardResult
{
    /** @var ArrayOutputInterface */
    private $stagesResult;
    /** @var string */
    private $minAmountString;
    /** @var float */
    private $minAmount;
    /** @var float */
    private $forfaitairVoordeel;
    /** @var float */
    private $eigenBijdrage;
    /** @var float */
    private $voordelenAlleAard;

    /**
     * VoordelenAlleAardResult constructor.
     * @param ArrayOutputInterface $stagesResult
     * @param string $minAmountString
     * @param float $minAmount
     * @param float $forfaitairVoordeel
     * @param float $eigenBijdrage
     * @param float $voordelenAlleAard
     */
    public function __construct(ArrayOutputInterface $stagesResult, string $minAmountString, float $minAmount, float $forfaitairVoordeel,
                                float $eigenBijdrage, float $voordelenAlleAard, float $grondslag1206)
    {
        $this->stagesResult = $stagesResult;
        $this->minAmountString = $minAmountString;
        $this->minAmount = $minAmount;
        $this->forfaitairVoordeel = $forfaitairVoordeel;
        $this->eigenBijdrage = $eigenBijdrage;
        $this->voordelenAlleAard = $voordelenAlleAard;
        $this->grondslag1206 = $grondslag1206;
    }

    /**
     * @return StagesResult
     */
    public function getStagesResult(): ArrayOutputInterface
    {
        return $this->stagesResult;
    }

    /**
     * @return string
     */
    public function getminAmountString(): string
    {
        return $this->minAmountString;
    }

    /**
     * @return float
     */
    public function getMinAmount(): float
    {
        return $this->minAmount;
    }

    /**
     * @return float
     */
    public function getForfaitairVoordeel(): float
    {
        return $this->forfaitairVoordeel;
    }
    /**
     * @return float
     */
    public function getEigenBijdrage(): float
    {
        return $this->eigenBijdrage;
    }

    /**
     * @return float
     */
    public function getVoordelenAlleAard(): float
    {
        return $this->voordelenAlleAard;
    }

    /**
     * @return float
     */
    public function getGrondslag1206(): float
    {
        return $this->grondslag1206;
    }

    public function output() :array
    {
        return
        [
            'stages' => $this->getStagesResult()->output(),
            'minAmountInfo' => [
            'minAmountString' => $this->getminAmountString(),
            'minAmount' => '' . round($this->getMinAmount(),2),
                ],
            'forfaitairVoordeel' => '' . round($this->getForfaitairVoordeel(),2),
            'eigenBijdrage' =>'' .  round($this->getEigenBijdrage(),2),
            'voordelenAlleAard' => '' . round($this->getVoordelenAlleAard(),2),
            'grondslag1206' => '' . round($this->getGrondslag1206(),2)

        ];
    }
}

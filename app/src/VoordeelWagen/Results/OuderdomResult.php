<?php


namespace Laudis\Calculators\VoordeelWagen\Results;


use Laudis\Calculators\VoordeelWagen\Interfaces\ArrayOutputInterface;

class OuderdomResult implements ArrayOutputInterface
{
    /** @var OuderdomStageResult */
    private $stage1;
    /** @var OuderdomStageResult */
    private $stage2;

    /**
     * OuderdomResult constructor.
     * @param OuderdomStageResult $stage1
     * @param OuderdomStageResult $stage2
     */
    public function __construct(OuderdomStageResult $stage1, OuderdomStageResult $stage2 = null)
    {
        $this->stage1 = $stage1;
        $this->stage2 = $stage2;
    }

    /**
     * @return OuderdomStageResult
     */
    public function getStage1(): OuderdomStageResult
    {
        return $this->stage1;
    }

    /**
     * @return OuderdomStageResult
     */
    public function getStage2(): ?OuderdomStageResult
    {
        return $this->stage2;
    }

    public function output(): array
    {
        if ($this->stage2 === null){
            return [
                'stage1' => $this->getStage1()->output(),
                'stage2' => ''
            ];
        }
        return [
            'stage1' => $this->getStage1()->output(),
            'stage2' => $this->getStage2()->output()
        ];
    }
}

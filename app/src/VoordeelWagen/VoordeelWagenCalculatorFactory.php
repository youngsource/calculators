<?php
declare(strict_types=1);

namespace Laudis\Calculators\VoordeelWagen;

use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\VoordeelWagen\Auxiliaries\VoordeelWagenAuxiliaries;
use Laudis\Calculators\VoordeelWagen\Input\VoordeelWagenInput;
use Laudis\Calculators\VoordeelWagen\Rules\MinimumYear;
use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

/**
 * Class VoordeelWagenCalculatorFactory
 * @package Laudis\Calculators\VoordeelWagen
 */
class VoordeelWagenCalculatorFactory implements CalculatorFactoryInterface
{
    /** @var VoordeelWagenAuxiliaries */
    private $auxiliaries;

    /**
     * VoordeelWagenCalculatorFactory constructor.
     * @param VoordeelWagenAuxiliaries $auxiliaries
     */
    public function __construct(VoordeelWagenAuxiliaries $auxiliaries)
    {
        $this->auxiliaries = $auxiliaries;
    }

    /**
     * Returns the calculator.
     *
     * @param array $values
     * @return object  The calculator must have a calculate method which accepts the input from the request and returns
     *                  the result encapsulated in a calculation result.
     */
    public function calculator(array $values): object
    {
        return new VoordeelWagenCalculator($this->auxiliaries);
    }

    /**
     * @param array $values
     * @return object
     */
    public function inputFromArray(array $values): object
    {
        $format = 'Y-m-d';
        $eigenBijdrage = [];
        $tankKaart = [];
        $year = date('Y');
        for ($i = 2010; $i <= $year; $i++) {
            //TODO put this in sub arrays in font end
            $eigenBijdrage[$i] = to_float($values['eigenBijdrage' . $i]) ?? 0.0;
            $tankKaart[$i] = to_bool($values['wagenMetTankKaart' . $i]) ?? true;
        }

        $input = new VoordeelWagenInput(
            $values['typeVoertuig'] ?? '',
            $values['nummerplaat'] ?? '',
            BrandstofEnum::resolve($values['brandstof'] ?? 'BENZINE'),
            to_float($values['co2uitstoot']) ?: 0,
            date_create_from_format($format, $values['eersteInschrijvingDIV']),
            date_create_from_format($format, $values['beginDatumVAA']),
            date_create_from_format($format, $values['eindDatumVAA']),
            to_float($values['catalogusPrijsVoorKorting'] ?? 0),
            to_float($values['opties'] ?? 0),
            to_float($values['werkelijkBetaaldeBTW'] ?? 0),
            to_float($values['kortingConstructeur'] ?? 0),
            $eigenBijdrage,
            $tankKaart
        );
        return $input;
    }

    /**
     * Derives the validation from the current input values.
     *
     * @param array $values
     * @return Validation
     */
    public function validation(array $values): Validation
    {
        $validator = new Validator([
            'required' => 'This field is required',
            'numeric' => 'This field needs to be a number'
        ]);

        return $validator->make($values, [
            'brandstof' => ['required'],
            'co2uitstoot' => ['required', 'numeric'],
            'eersteInschrijvingDIV' => ['date'],
            'beginDatumVAA' => [
                'date',
                new MinimumYear(2010),
            ],
            'eindDatumVAA' => [
                'date',
            ],
            'catalogusPrijsVoorKorting' => ['numeric', 'required'],
            'opties' => ['numeric'],
            'werkelijkBetaaldeBTW' => ['numeric'],
            'kortingConstructeur' => ['numeric'],
            'eigenBijdrageBeginJaar' => ['numeric'],
            'eigenBijdrageEindJaar' => ['numeric']
        ]);
    }
}

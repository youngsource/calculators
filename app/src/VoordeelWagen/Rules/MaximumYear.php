<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 05/08/19
 * Time: 17:26
 */

namespace Laudis\Calculators\VoordeelWagen\Rules;


use DateTime;
use Rakit\Validation\Rule;

/**
 * Class MaximumYear
 * @package Laudis\Calculators\VoordeelWagen\Rules
 */
final class MaximumYear extends Rule
{
    /** @var float */
    private $year;

    /**
     * MaximumYear constructor.
     * @param float $year
     */
    public function __construct(
        float $year
    ) {
        $this->year = $year;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if ($value === null) {
            return true;
        }

        $thisDate = DateTime::createFromFormat('Y-m-d', $value);
        $minimumYearDate = DateTime::createFromFormat('Y-m-d', $this->year . '-01-01-');

        $this->setMessage('Date must be maximum of the year : ' . $this->year);

        return $thisDate->getTimestamp() > $minimumYearDate->getTimestamp();
    }
}

<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 05/08/19
 * Time: 16:54
 */

namespace Laudis\Calculators\VoordeelWagen\Rules;


use DateTime;
use Rakit\Validation\Rule;
use Rakit\Validation\Validation;

/**
 * Class DaysBetween
 * @package Laudis\Calculators\VoordeelWagen\Rules
 */
final class DaysBetween extends Rule
{
    /**
     * @var string
     */
    private $otherDateKey;
    /**
     * @var float
     */
    private $daysBetween;

    /**
     * DaysBetween constructor.
     * @param string $otherDateKey
     * @param float $daysBetween
     */
    public function __construct(
        string $otherDateKey,
        float $daysBetween
    ) {
        $this->otherDateKey = $otherDateKey;
        $this->daysBetween = $daysBetween;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        /** @var Validation $validation */
        $validation = $this->validation;
        if ($value === null) {
            return true;
        }

        $otherDate = DateTime::createFromFormat('Y-m-d', $validation->getValue($this->otherDateKey));
        $thisDate = DateTime::createFromFormat('Y-m-d', $value);

        $this->setMessage('Date must be max ' . $this->daysBetween . ' days of eachother');

        return $thisDate->diff($otherDate, true)->d > $this->daysBetween;
    }
}

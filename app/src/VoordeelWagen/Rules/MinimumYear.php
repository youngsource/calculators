<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 05/08/19
 * Time: 17:26
 */

namespace Laudis\Calculators\VoordeelWagen\Rules;


use DateTime;
use Rakit\Validation\Rule;

/**
 * Class MinimumYear
 * @package Laudis\Calculators\VoordeelWagen\Rules
 */
final class MinimumYear extends Rule
{
    /** @var float */
    private $year;

    /**
     * MinimumYear constructor.
     * @param float $year
     */
    public function __construct(float $year) {
        $this->year = $year;
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if ($value === null) {
            return true;
        }

        $thisDate = DateTime::createFromFormat('Y-m-d', $value);
        $minimumYearDate = DateTime::createFromFormat('Y-m-d', $this->year . '-01-01');

        $this->setMessage('Date must be minimum of the year : ' . $this->year);
        // cant do get timestamp on bool ?
        return $thisDate->getTimestamp() < $minimumYearDate->getTimestamp();
    }
}

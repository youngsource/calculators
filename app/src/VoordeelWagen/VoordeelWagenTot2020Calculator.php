<?php

declare(strict_types=1);

namespace Laudis\Calculators\VoordeelWagen;

use DateTime;
use DateTimeInterface;
use Exception;
use Laudis\Calculators\Venb\BasicCalculationResult;
use Laudis\Calculators\VoordeelWagen\Auxiliaries\VoordeelWagenAuxiliaries;
use Laudis\Calculators\VoordeelWagen\Input\VoordeelWagenInputTot2020;
use Laudis\Calculators\VoordeelWagen\Interfaces\VoordeelWagenOperationsInterface;
use Laudis\Calculators\VoordeelWagen\Results\BetweenDatesResult;
use Laudis\Calculators\VoordeelWagen\Results\CO2InfoResult;
use Laudis\Calculators\VoordeelWagen\Results\OuderdomResult;
use Laudis\Calculators\VoordeelWagen\Results\OuderdomStageResult;
use Laudis\Calculators\VoordeelWagen\Results\StagesResult;
use Laudis\Calculators\VoordeelWagen\Results\VenootschapInfoResult;
use Laudis\Calculators\VoordeelWagen\Results\VoordelenAlleAardResult;
use Laudis\Calculators\VoordeelWagen\Results\YearResult;
use function simple_date;

/**
 * Class VoordeelWagenCalculator
 * @package Laudis\Calculators\VoordeelWagen
 */
class VoordeelWagenTot2020Calculator implements VoordeelWagenOperationsInterface
{
    /** @var VoordeelWagenAuxiliaries */
    protected $auxiliaries;

    /**
     * VoordeelWagenCalculator constructor.
     * @param VoordeelWagenAuxiliaries $auxiliaries
     */
    public function __construct(VoordeelWagenAuxiliaries $auxiliaries)
    {
        $this->auxiliaries = $auxiliaries;
    }

    /**
     * @param VoordeelWagenInputTot2020 $input
     * @return BasicCalculationResult
     * @throws Exception
     */
    public function calculate(VoordeelWagenInputTot2020 $input): BasicCalculationResult
    {
        $begin = $input->getBeginDatumVAA();
        $end = $input->getEindDatumVAA();
        $beginYear = (int)$begin->format('Y');
        $endYear = (int)$end->format('Y');

        $results = [];
        $totaleVoordelenAlleAard = 0;
        $verworpenUitgave1206 = 0;
        for ($year = $beginYear; $year <= $endYear; ++$year) {
            $form = $this->calcFormPerYear(
                $year === $beginYear ? $begin : simple_date($year, 1, 1),
                $year === $endYear ? $end : simple_date($year, 12, 31),
                $input
            );
            $results[$year] = $form->output();
            $totaleVoordelenAlleAard += $form->getVoordelenAlleAard()->getVoordelenAlleAard();
            $verworpenUitgave1206 += $form->getVennootschapsInfo()->getInfo1206();
        }
        $results['totaleVoordelenAlleAard'] = '' . round($totaleVoordelenAlleAard,2);
        $results['verworpenUitgave1206'] = '' . round($verworpenUitgave1206,2);
        $results['aftrekbbaarDeelAutokosten'] =  '' . round($this->getPercentage($input->getBrandstof(), $this->calculateActualCO2Uitstoot($input)), 2);
        return new BasicCalculationResult($results);
    }

    protected function calculateActualCO2Uitstoot(VoordeelWagenInputTot2020 $input): float
    {
        return $input->getco2uitstoot();
    }

    protected function getPercentage(BrandstofEnumTot2020 $brandstof, float $co2Uitstoot) : float
    {
        if ($brandstof === BrandstofEnumTot2020::BENZINE())
        {
            return $this->auxiliaries->getBenzinePercentage($co2Uitstoot);
        }
        if ($brandstof === BrandstofEnumTot2020::DIESEL())
        {
            return $this->auxiliaries->getDieselPercentage($co2Uitstoot);
        }
        return 1.2;
    }

    /**
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @param VoordeelWagenInputTot2020 $input
     * @return YearResult
     * @throws Exception
     */
    protected function calcFormPerYear(
        DateTimeInterface $from,
        DateTimeInterface $to,
        VoordeelWagenInputTot2020 $input
    ): YearResult {
        $year = (int)$from->format('Y');

        $dates = new BetweenDatesResult($from, $to);
        $ouderdom = $this->calcOuderdomInfo($from, $to, $input);
        $co2InfoResult = $this->calcCO2Info($year, $input);
        $stages = $this->stagesResult($input, $ouderdom, $year, $co2InfoResult);
        $voordelenAlleAard =  $this->calcVoordelenAlleAard($stages, $year, $ouderdom, $input);
        $vennootschapsInfo = $this->calcVennootschapInfo($year,$input,$voordelenAlleAard);
        return new YearResult($dates,$ouderdom,$co2InfoResult,$stages,$voordelenAlleAard,$vennootschapsInfo);
    }

    /**
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @param VoordeelWagenInputTot2020 $input
     * @return OuderdomResult
     * @throws Exception
     * returns a result of the stages , 1 stage if there is no turnover
     */
    protected function calcOuderdomInfo(
        DateTimeInterface $from,
        DateTimeInterface $to,
        VoordeelWagenInputTot2020 $input
    ): OuderdomResult
    {
        // Er is géén turnover tijdens de $from - $to periode als ofwel
        // 1) $Ouderdomspercentage op datum $from = $ouderdomspercentage op datum $to
        // 2) $turnoverdate valt op of vóór op de $from datum
        // 3) $turnoverDate valt na de $to datum

        $turnoverDate = $this->calcTurnover($from, $to, $input);
        $dayBeforeTurnoverDate = $this->calcTurnover($from, $to, $input);
        date_sub($dayBeforeTurnoverDate,date_interval_create_from_date_string('1 day'));

        if ($this->calcOuderdomsPercentage($from, $input) === $this->calcOuderdomsPercentage($to, $input) ||
            $turnoverDate <= $from ||
            $turnoverDate > $to){
             $turnoverDate = null;
            }

        if (!$turnoverDate) {
            $results = new OuderdomResult(
                new OuderdomStageResult(
                    $from,
                    $to,
                    $this->calcVerschilInDagen($from, $to),
                    $this->calcOuderdomsPercentage($from, $input))
            );
        } else {
            $results = new OuderdomResult(
                new OuderdomStageResult(
                    $from,
                    $dayBeforeTurnoverDate,
                    $this->calcVerschilInDagen($from, $dayBeforeTurnoverDate),
                    $this->calcOuderdomsPercentage($from, $input)),
                new OuderdomStageResult(
                    $turnoverDate,
                    $to,
                    $this->calcVerschilInDagen($turnoverDate, $to),
                    $this->calcOuderdomsPercentage($to, $input))
            );
        }
        return $results;
    }
    // turnoverDate is de eerste dag waarop een nieuw ouderdomspercentage van toepassing kan zijn /

    /**
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @param VoordeelWagenInputTot2020 $input
     * @return DateTimeInterface|null
     * @throws Exception
     *
     *
     *
     */
    protected function calcTurnover(
        DateTimeInterface $from,
        DateTimeInterface $to,
        VoordeelWagenInputTot2020 $input
    ): ?DateTimeInterface {

        // turnoverDate - verjaardag, maar telkend de eerste dag van de maand */

        $huidig_Jaar = (int)$from->format('Y');
        $inschrijvings_Maand = (int)$input->getEersteInschrijvingDIV()->format('m');
        $inschrijvingsMaandEerste_Dag = 1;

        $turnoverDate = DateTime::createFromFormat(
            'Y-m-d',
            $huidig_Jaar . '-' . $inschrijvings_Maand . '-' . $inschrijvingsMaandEerste_Dag);

        return $turnoverDate;
    }

     /**
     * @param DateTimeInterface $start
     * @param DateTimeInterface $end
     * @return int
     */
    protected function calcVerschilInDagen(DateTimeInterface $start, DateTimeInterface $end): int
    {
        $diff = $end->diff($start);
        $days = (int)$diff->format('%a');
        if ((int)$diff->format('%h') > 0 || (int)$diff->format('%i') > 0 || (int)$diff->format('%s') > 0) {
            ++$days;
        }

        return $days + 1;
    }

    /**
     * @param DateTimeInterface $to
     * @param VoordeelWagenInputTot2020 $input
     * @return float
     */
    protected function calcOuderdomsPercentage(DateTimeInterface $to, VoordeelWagenInputTot2020 $input): float
    {
        $months = $this->calcOuderdomInMaanden($to, $input);
        return $this->auxiliaries->getOuderdomsPercentagePercentage($months);
    }

    /**
     * @param DateTimeInterface $to
     * @param VoordeelWagenInputTot2020 $input
     * @return int
     */
    protected function calcOuderdomInMaanden(DateTimeInterface $to, VoordeelWagenInputTot2020 $input): int
    {
        $start = $input->getEersteInschrijvingDIV();
        $diff = $to->diff($start);
        // $diff->y returns amount of years between the dates
        // diff-> m returns amount of months between the dates
        $maanden = $diff->y * 12 + $diff->m;

        //checks if there are days, hours,minutes or seconds between the dates. If so , add 1 month
        // foo > bar ? return value if true : return value if false
        $maanden += ((int)$diff->format('%d') > 0 || (int)$diff->format('%h') > 0 || (int)$diff->format('%i') > 0 || (int)$diff->format('%s') > 0) ?
            1 : 0;

        return $maanden;
    }

    /**pot
     * @param int $year
     * @param VoordeelWagenInputTot2020 $input
     * @return CO2InfoResult
     */
    protected function calcCO2Info(int $year, VoordeelWagenInputTot2020 $input): CO2InfoResult
    {
        $CO2Ref = $this->calcCO2Ref($year, $input);
        return new CO2InfoResult($CO2Ref, $this->calcCO2Percentage($year, $input));
    }

    /**
     * @param int $year
     * @param VoordeelWagenInputTot2020 $input
     * @return float
     */
    protected function calcCO2Ref(int $year, VoordeelWagenInputTot2020 $input): float
    {
        $brandstof = $input->getBrandstof();

        if ($brandstof === BrandstofEnumTot2020::ELEKTRISCH()) {
            return 0.0;
        }

        if ($brandstof === BrandstofEnumTot2020::BENZINE()) {
            if ($input->getco2uitstoot() === 0.0) {
                return $this->getBenzineNoRef($year);
            }
            return $this->getBenzRef($year);
        }
        if ($input->getco2uitstoot() === 0.0) {
            return $this->getDieselNoRef($year);
        }
        return $this->getDieselRef($year);
    }

    /**
     * @param int $year
     * @return float
     */
    protected function getBenzineNoRef(int $year): float
    {
        return $this->auxiliaries->indexedValue('wagen_gebrek_uitstootgegevens_benzine',
            date_create_from_format('Y-m-d', $year . '-01-01'));
    }

    /**
     * @param int $year
     * @return float
     */
    protected function getBenzRef(int $year): float
    {
        return $this->auxiliaries->indexedValue('wagen_referentie_benzine',
            date_create_from_format('Y-m-d', $year . '-01-01'));
    }

    /**
     * @param int $year
     * @return float
     */
    protected function getDieselNoRef(int $year): float
    {
        return $this->auxiliaries->indexedValue('wagen_gebrek_uitstootgegevens_diesel',
            date_create_from_format('Y-m-d', $year . '-01-01'));
    }

    /**
     * @param int $year
     * @return float
     */
    protected function getDieselRef(int $year): float
    {
        return $this->auxiliaries->indexedValue('wagen_referentie_diesel',
            date_create_from_format('Y-m-d', $year . '-01-01'));
    }

    /**
     * @param int $year
     * @param VoordeelWagenInputTot2020 $input
     * @return float
     */
    protected function calcCO2Percentage(int $year, VoordeelWagenInputTot2020 $input): float
    {
        $CO2Ref = $this->calcCO2Ref($year, $input);
        if ($CO2Ref === 0.0) {
            return $this->getMinPercentage($year);
        }

        $uitstoot = $input->getco2uitstoot();
        $berekening = $this->getCO2Percentage($year) + ($uitstoot - $CO2Ref) * $this->getAanpassingPerGram($year);

        if ($berekening < $this->getMinPercentage($year)) {
            $berekening = $this->getMinPercentage($year);
        } elseif ($berekening > $this->getMaxPercentage($year)) {
            $berekening = $this->getMaxPercentage($year);
        }
        return $berekening;
    }

    /**
     * @param int $year
     * @return float
     */
    protected function getMinPercentage(int $year): float
    {
        return $this->auxiliaries->indexedValue('wagen_minimum_percentage',
            date_create_from_format('Y-m-d', $year . '-01-01'));
    }

    /**
     * @param int $year
     * @return float
     */
    protected function getCO2Percentage(int $year): float
    {
        return $this->auxiliaries->indexedValue('CO2Percentage', date_create_from_format('Y-m-d', $year . '-01-01'));
    }

    /**
     * @param int $year
     * @return float
     */
    protected function getAanpassingPerGram(int $year): float
    {
        return $this->auxiliaries->indexedValue('wagen_aanpassing_per_gram',
            date_create_from_format('Y-m-d', $year . '-01-01'));
    }

    /**
     * @param int $year
     * @return float
     */
    protected function getMaxPercentage(int $year): float
    {
        return $this->auxiliaries->indexedValue('wagen_maximum_percentage',
            date_create_from_format('Y-m-d', $year . '-01-01'));
    }

    /**
     * @param VoordeelWagenInputTot2020 $input
     * @param OuderdomResult $ouderdomResult
     * @param $year
     * @param CO2InfoResult $CO2InfoResult
     * @return StagesResult
     * @throws Exception
     *
     * this calculates the 2 stages in a year. If there is no turnover there will be no stage 2
     */
    protected function stagesResult(
        VoordeelWagenInputTot2020 $input,
        OuderdomResult $ouderdomResult,
        $year,
        CO2InfoResult $CO2InfoResult
    ): StagesResult {
        $calculationStage2 = '';
        $calculationResultStage2 = 0;

        $calculationStage1 = $this->calcCatalogusWaarde($input) . ' &times; ' . $ouderdomResult->getStage1()->getPercentage()
            . ' &times; ' . $ouderdomResult->getStage1()->getDaysBetween() . '/' . $this->calcDagenInJaar($year) . ' &times; 6/7 &times; ' .
            number_format($CO2InfoResult->getPercentage()*100,2,',','.') . '% =';

        $calculationResultStage1 = $this->calcCatalogusWaarde($input) * $ouderdomResult->getStage1()->getPercentage()
            * ($ouderdomResult->getStage1()->getDaysBetween() / $this->calcDagenInJaar($year)) * (6 / 7) * $this->calcCO2Info($year,
                $input)->getPercentage();

        if ($ouderdomResult->getStage2() !== null)
        {
            $calculationStage2 = $this->calcCatalogusWaarde($input) . ' &times; ' . $ouderdomResult->getStage2()->getPercentage()
                . ' &times; ' . $ouderdomResult->getStage2()->getDaysBetween() . '/' . $this->calcDagenInJaar($year) . ' &times; 6/7 &times; ' . number_format($this->calcCO2Info($year,
                        $input)->getPercentage()*100,2,',','.') . '% =';
            $calculationResultStage2 = $this->calcCatalogusWaarde($input) * $ouderdomResult->getStage2()->getPercentage()
                * ($ouderdomResult->getStage2()->getDaysBetween() / $this->calcDagenInJaar($year)) * (6 / 7) * $this->calcCO2Info($year,
                    $input)->getPercentage();
        }
        return new StagesResult($calculationStage1, $calculationResultStage1, $calculationStage2,
            $calculationResultStage2);
    }

    /**
     * @param VoordeelWagenInputTot2020 $wagen
     * @return float
     */
    protected function calcCatalogusWaarde(VoordeelWagenInputTot2020 $wagen): float
    {
        return $wagen->getCatalogusPrijsVoorKorting()
            + $wagen->getOpties()
            + $wagen->getWerkelijkBetaaldeBTW()
            - $wagen->getKortingConstructeur();
    }

    /**
     * @param int $year
     * @return int
     * @throws Exception
     */
    protected function calcDagenInJaar(int $year): int
    {
        $begin = simple_date($year, 1, 1);
        $end = simple_date($year + 1, 1, 1);

        return (int)$end->diff($begin)->format('%a');
    }

    /**
     * @param StagesResult $stages
     * @param $year
     * @param OuderdomResult $ouderdomResult
     * @param VoordeelWagenInputTot2020 $input
     * @return VoordelenAlleAardResult
     * @throws Exception
     *
     * this method calculates the total calculation of a year
     */
    protected function calcVoordelenAlleAard(
        StagesResult $stages,
        $year,
        OuderdomResult $ouderdomResult,
        VoordeelWagenInputTot2020 $input
    ): VoordelenAlleAardResult {
        $daysInStages = $ouderdomResult->getStage1()->getDaysBetween();
        if ($ouderdomResult->getStage2() !== null){
            $daysInStages = $ouderdomResult->getStage1()->getDaysBetween() + $ouderdomResult->getStage2()->getDaysBetween();
        }

        $minAmountString = 'Minimaal ' . $this->getMinVoordeel($year) . ' &times; ' . $daysInStages . '/'
            . $this->calcDagenInJaar($year);

        $minAmount = $this->getMinVoordeel($year) * ($daysInStages / $this->calcDagenInJaar($year));
        $forfaitairVoordeel = max($minAmount, $stages->getTotal());
        $eigenBijdrage = $input->getEigenBijdrage($year);
        $voordeelAlleAard = max( $forfaitairVoordeel - $eigenBijdrage, 0);

        if ($year < 2017){
            $grondslag1206 = $voordeelAlleAard;
        }
        else {
            $grondslag1206 = max($stages->getTotal(), $minAmount);
        }

        return new VoordelenAlleAardResult(
            $stages, $minAmountString, $minAmount, $forfaitairVoordeel,
            $eigenBijdrage, $voordeelAlleAard, $grondslag1206);

    }

    /**
     * @param int $year
     * @return float
     */
    protected function getMinVoordeel(int $year): float
    {
        return $this->auxiliaries->indexedValue('wagen_mimimum_voordeel',
            date_create_from_format('Y-m-d', $year . '-01-01'));
    }

    /**
     * @param $year
     * @param VoordeelWagenInputTot2020 $input
     * @param VoordelenAlleAardResult $alleAardResult
     * @return VenootschapInfoResult
     */
    protected function calcVennootschapInfo($year, VoordeelWagenInputTot2020 $input,VoordelenAlleAardResult $alleAardResult): VenootschapInfoResult
    {
        $grondslag = $alleAardResult->getGrondslag1206();
        $tarief = $this->calcVennootSchapTarief($year, $input);
        $info1206 = $this->calc1206($grondslag, $tarief);
        return new VenootschapInfoResult($grondslag, $tarief, $info1206);
    }

    /**
     * @param int $year
     * @param VoordeelWagenInputTot2020 $input
     * @return float
     */
    protected function calcVennootSchapTarief(int $year, VoordeelWagenInputTot2020 $input): float
    {
        if ($year >= 2017 && $input->isWagenMetTankKaart($year)) {
            return 0.4;
        }

        return 0.17;
    }

    /**
     * @param float $grondslag
     * @param float $tarief
     * @return float
     */
    protected function calc1206(float $grondslag, float $tarief): float
    {
        return round($grondslag * $tarief, 2);
    }
}

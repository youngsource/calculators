<?php
/** @noinspection InterfacesAsConstructorDependenciesInspection */
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 05/08/19
 * Time: 16:01
 */

namespace Laudis\Calculators\VoordeelWagen\Input;


use DateTime;
use Laudis\Calculators\BrandstofEnumVanaf2020;
use Laudis\Calculators\VoordeelWagen\BrandstofEnumTot2020;

/**
 * Class VoordeelWagenInput
 * @package Laudis\Calculators\VoordeelWagen\Input
 */
class VoordeelWagenInputTot2020
{
    /** @var string */
    private $typeVoertuig;
    /** @var string */
    private $nummerplaat;
    /** @var BrandstofEnumVanaf2020 */
    private $brandstof;
    /** @var float */
    private $co2uitstoot;
    /** @var DateTime */
    private $eersteInschrijvingDIV;
    /** @var DateTime */
    private $beginDatumVAA;
    /** @var DateTime */
    private $eindDatumVAA;
    /** @var float */
    private $catalogusPrijsVoorKorting;
    /** @var float */
    private $opties;
    /** @var float */
    private $werkelijkBetaaldeBTW;
    /** @var float */
    private $kortingConstructeur;
    /** @var array */
    private $eigenBijdragen;
    /** @var array */
    private $wagenMetTankKaart;

    /**
     * VoordeelWagenInput constructor.
     * @param string $typeVoertuig
     * @param string $nummerplaat
     * @param BrandstofEnumTot2020 $brandstof
     * @param float $co2uitstoot
     * @param DateTime $eersteInschrijvingDIV
     * @param DateTime $beginDatumVAA
     * @param DateTime $eindDatumVAA
     * @param float $catalogusPrijsVoorKorting
     * @param float $opties
     * @param float $werkelijkBetaaldeBTW
     * @param float $kortingConstructeur
     * @param array $eigenBijdrage
     * @param array $wagenMetTankKaart
     */
    public function __construct(
        string $typeVoertuig,
        string $nummerplaat,
        BrandstofEnumVanaf2020 $brandstof,
        float $co2uitstoot,
        DateTime $eersteInschrijvingDIV,
        DateTime $beginDatumVAA,
        DateTime $eindDatumVAA,
        float $catalogusPrijsVoorKorting,
        float $opties,
        float $werkelijkBetaaldeBTW,
        float $kortingConstructeur,
        array $eigenBijdrage,
        array $wagenMetTankKaart)
    {
        $this->typeVoertuig = $typeVoertuig;
        $this->nummerplaat = $nummerplaat;
        $this->brandstof = $brandstof;
        $this->co2uitstoot = $co2uitstoot;
        $this->eersteInschrijvingDIV = $eersteInschrijvingDIV;
        $this->beginDatumVAA = $beginDatumVAA;
        $this->eindDatumVAA = $eindDatumVAA;
        $this->catalogusPrijsVoorKorting = $catalogusPrijsVoorKorting;
        $this->opties = $opties;
        $this->werkelijkBetaaldeBTW = $werkelijkBetaaldeBTW;
        $this->kortingConstructeur = $kortingConstructeur;
        $this->eigenBijdragen = $eigenBijdrage;
        $this->wagenMetTankKaart = $wagenMetTankKaart;
    }

    /**
     * @return string
     */
    public function getTypeVoertuig(): string
    {
        return $this->typeVoertuig;
    }

    /**
     * @return string
     */
    public function getNummerplaat(): string
    {
        return $this->nummerplaat;
    }

    /**
     * @return BrandstofEnumVanaf2020
     */
    public function getBrandstof(): BrandstofEnumVanaf2020
    {
        return $this->brandstof;
    }

    /**
     * @return float
     */
    public function getco2uitstoot(): float
    {
        return $this->co2uitstoot;
    }

    /**
     * @return DateTime
     */
    public function getEersteInschrijvingDIV(): DateTime
    {
        return $this->eersteInschrijvingDIV;
    }

    /**
     * @return DateTime
     */
    public function getBeginDatumVAA(): DateTime
    {
        return $this->beginDatumVAA;
    }

    /**
     * @return DateTime
     */
    public function getEindDatumVAA(): DateTime
    {
        return $this->eindDatumVAA;
    }

    /**
     * @return float
     */
    public function getCatalogusPrijsVoorKorting(): float
    {
        return $this->catalogusPrijsVoorKorting;
    }

    /**
     * @return float
     */
    public function getOpties(): float
    {
        return $this->opties;
    }

    /**
     * @return float
     */
    public function getWerkelijkBetaaldeBTW(): float
    {
        return $this->werkelijkBetaaldeBTW;
    }

    /**
     * @return float
     */
    public function getKortingConstructeur(): float
    {
        return $this->kortingConstructeur;
    }

    /**
     * @param int $year
     * @return bool
     */
    public function isWagenMetTankKaart(int $year): bool
    {
        return $this->wagenMetTankKaart[$year] ?? false;
    }

    /**
     * @param int $year
     * @return float
     */
    public function getEigenBijdrage(int $year): float
    {
        return $this->eigenBijdragen[$year] ?? 0;
    }
}

<?php


namespace Laudis\Calculators\VoordeelWagen\Input;


use DateTime;
use DateTimeInterface;
use Laudis\Calculators\VoordeelWagen\BrandstofEnumTot2020;

class VoordeelWagenInputVanaf2020 extends VoordeelWagenInputTot2020
{
    /**
     * @var int
     */
    private $jaarAankoopLeaseOfRenting;
    /**
     * @var bool
     */
    private $valseHybride;
    /**
     * @var float|null
     */
    private $effectieveCO2ValseHybride;
    /**
     * @var DateTimeInterface
     */
    private $aanvangsdatumBoekjaar;

    public function __construct(
        string $typeVoertuig,
        string $nummerplaat,
        BrandstofEnumTot2020 $brandstof,
        float $co2uitstoot,
        DateTime $eersteInschrijvingDIV,
        DateTime $beginDatumVAA,
        DateTime $eindDatumVAA,
        float $catalogusPrijsVoorKorting,
        float $opties,
        float $werkelijkBetaaldeBTW,
        float $kortingConstructeur,
        array $eigenBijdrage,
        array $wagenMetTankKaart,
        int $jaarAankoopLeaseOfRenting,
        bool $valseHybride,
        ?float $effectieveCO2ValseHybride,
        DateTimeInterface $aanvangsdatumBoekjaar
    ) {
        parent::__construct(
            $typeVoertuig,
            $nummerplaat,
            $brandstof,
            $co2uitstoot,
            $eersteInschrijvingDIV,
            $beginDatumVAA,
            $eindDatumVAA,
            $catalogusPrijsVoorKorting,
            $opties,
            $werkelijkBetaaldeBTW,
            $kortingConstructeur,
            $eigenBijdrage,
            $wagenMetTankKaart
        );
        $this->jaarAankoopLeaseOfRenting = $jaarAankoopLeaseOfRenting;
        $this->valseHybride = $valseHybride;
        $this->effectieveCO2ValseHybride = $effectieveCO2ValseHybride;
        $this->aanvangsdatumBoekjaar = $aanvangsdatumBoekjaar;
    }

    /**
     * @return int
     */
    public function getJaarAankoopLeaseOfRenting(): int
    {
        return $this->jaarAankoopLeaseOfRenting;
    }

    /**
     * @return bool
     */
    public function isValseHybride(): bool
    {
        return $this->valseHybride;
    }

    /**
     * @return float
     */
    public function getEffectieveCO2ValseHybride(): ?float
    {
        return $this->effectieveCO2ValseHybride;
    }

    /**
     * @return DateTimeInterface
     */
    public function getAanvangsdatumBoekjaar(): DateTimeInterface
    {
        return $this->aanvangsdatumBoekjaar;
    }
}
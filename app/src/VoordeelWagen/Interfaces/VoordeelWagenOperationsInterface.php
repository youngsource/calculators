<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 05/08/19
 * Time: 16:28
 */

namespace Laudis\Calculators\VoordeelWagen\Interfaces;


use Laudis\Calculators\Venb\BasicCalculationResult;
use Laudis\Calculators\VoordeelWagen\Input\VoordeelWagenInputTot2020;

/**
 * Interface VoordeelWagenOperationsInterface
 * @package Laudis\Calculators\VoordeelWagen\Interfaces
 */
interface VoordeelWagenOperationsInterface
{
    /**
     * @param VoordeelWagenInputTot2020 $input
     * @return BasicCalculationResult
     */
    public function calculate(VoordeelWagenInputTot2020 $input) : BasicCalculationResult;
}

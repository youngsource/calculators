<?php

namespace Laudis\Calculators\VoordeelWagen\Interfaces;

interface ArrayOutputInterface
{
    /**
     * Transforms the object to an array filled with human readable output.
     * @return array
     */
    public function output(): array;
}

<?php
declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 05/08/19
 * Time: 18:45
 */

namespace Laudis\Calculators\VoordeelWagen\Auxiliaries;

use DateTime;
use DateTimeInterface;
use Laudis\Calculators\Registers\IndexedValueRegister;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Scale\Contracts\ContextualRepositoryScaleInterface;
use Laudis\Scale\ScalePresenter;

/**
 * Class VoordeelWagenAuxiliaries
 * @package Laudis\Calculators\VoordeelWagen\Auxiliaries
 */
final class VoordeelWagenAuxiliaries
{
    /** @var ScalePresenter */
    private $ouderdomsPercentageBarema;
    /** @var ScalePresenter */
    private $benzinebarema;
    /** @var ScalePresenter */
    private $dieselBarema;
    /** @var IndexedValueRegister  */
    private $indexedValues;

    /**
     * VoordeelWagenAuxiliaries constructor.
     * @param ContextualRepositoryScaleInterface $repositoryScale
     * @param ContextualIndexedValueRepositoryInterface $indexedValueRepository
     */
    public function __construct(
        ContextualRepositoryScaleInterface $repositoryScale,
        ContextualIndexedValueRepositoryInterface $indexedValueRepository
    ) {
        $this->ouderdomsPercentageBarema = $repositoryScale->getFromDate(
            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
            'voordeelWagenOuderDomsPercentage');
        $this->benzinebarema = $repositoryScale->getFromDate(
            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
            'voordeelWagenBenzineCO2');
        $this->dieselBarema = $repositoryScale->getFromDate(
            DateTime::createFromFormat('Y-m-d', '2018-01-01'),
            'voordeelWagenDieselCO2');
        $this->indexedValues = $indexedValueRepository;
    }

    /**
     * @param float $value
     * @return float
     */
    public function getOuderdomsPercentagePercentage(float $value): float
    {
        return $this->ouderdomsPercentageBarema->calculate($value);
    }

    /**
     * @param float $value
     * @return float
     */
    public function getBenzinePercentage(float $value): float
    {
        return $this->benzinebarema->calculate($value);
    }

    /**
     * @param float $value
     * @return float
     */
    public function getDieselPercentage(float $value): float
    {
        return $this->dieselBarema->calculate($value);
    }

    /**
     * @param string $identifier
     * @param DateTimeInterface $context
     * @return float
     */
    public function indexedValue(string $identifier, DateTimeInterface $context): float
    {
        return $this->indexedValues->getFromDate($context, $identifier)->getValue();
    }
}

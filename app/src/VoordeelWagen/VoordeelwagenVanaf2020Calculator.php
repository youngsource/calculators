<?php


namespace Laudis\Calculators\VoordeelWagen;


use Exception;
use Laudis\Calculators\BrandstofEnumVanaf2020;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\GramFormula\GramFormulaCalculator;
use Laudis\Calculators\GramFormula\GramFormulaInput;
use Laudis\Calculators\Venb\BasicCalculationResult;
use Laudis\Calculators\VoordeelWagen\Auxiliaries\VoordeelWagenAuxiliaries;
use Laudis\Calculators\VoordeelWagen\Input\VoordeelWagenInputTot2020;
use Laudis\Calculators\VoordeelWagen\Input\VoordeelWagenInputVanaf2020;
use Laudis\Calculators\VoordeelWagen\Results\AftrekbareAutokostenResult;
use Laudis\Calculators\VoordeelWagen\Results\AftrekbareAutokostenRowResult;

class VoordeelwagenVanaf2020Calculator extends VoordeelWagenTot2020Calculator
{
    private array $voordeelMapping;
    /**
     * @var GramFormulaCalculator
     */
    private $gramFormulaCalculator;

    public function __construct(VoordeelWagenAuxiliaries $auxiliaries, GramFormulaCalculator $gramFormulaCalculator)
    {
        parent::__construct($auxiliaries);
        $this->voordeelMapping = [
            BrandstofEnumVanaf2020::BENZINE()->getValue() => BrandstofEnumTot2020::BENZINE(),
            BrandstofEnumVanaf2020::CNG_MIN_12PK()->getValue() => BrandstofEnumTot2020::BENZINE(),
            BrandstofEnumVanaf2020::CNG_PLUS_12PK()->getValue() => BrandstofEnumTot2020::BENZINE(),
            BrandstofEnumVanaf2020::DIESEL()->getValue() => BrandstofEnumTot2020::DIESEL(),
            BrandstofEnumVanaf2020::LPG()->getValue() => BrandstofEnumTot2020::BENZINE(),
            BrandstofEnumVanaf2020::HYBRIDE_BENZINE()->getValue() => BrandstofEnumTot2020::BENZINE(),
            BrandstofEnumVanaf2020::HYBRIDE_DIESEL()->getValue() => BrandstofEnumTot2020::DIESEL(),
            BrandstofEnumVanaf2020::HYBRIDE_CNG_MIN_12PK()->getValue() => BrandstofEnumTot2020::BENZINE(),
            BrandstofEnumVanaf2020::HYBRIDE_CNG_PLUS_12PK()->getValue() => BrandstofEnumTot2020::BENZINE(),
            BrandstofEnumVanaf2020::HYBRIDE_LPG()->getValue() => BrandstofEnumTot2020::BENZINE(),
            BrandstofEnumVanaf2020::ELEKTRISCH()->getValue() => BrandstofEnumTot2020::ELEKTRISCH(),
        ];
        $this->gramFormulaCalculator = $gramFormulaCalculator;
    }

    protected function getPercentage(BrandstofEnumTot2020 $brandstof, float $co2Uitstoot): float
    {
        return parent::getPercentage($this->voordeelMapping[$brandstof->getValue()], $co2Uitstoot);
    }

    /**
     * @param VoordeelWagenInputTot2020|VoordeelWagenInputVanaf2020 $input
     * @throws Exception
     */
    public function calculate(VoordeelWagenInputTot2020 $input): BasicCalculationResult
    {
        $results = parent::calculate($input)->output();
        $gramFormulaResult = $this->gramFormula($input)->output();
        $results['gramformule'] = $gramFormulaResult;
        $results['aftrekbareAutokosten'] = $this->calculateAftrekbareAutokosten($input)->output();

        return new BasicCalculationResult($results);
    }

    private function calculateAftrekbareAutokosten(VoordeelWagenInputVanaf2020 $input) : AftrekbareAutokostenResult
    {
        return new AftrekbareAutokostenResult(
            $this->addVennootschapTotEnMetAj2020($input),
            $this->addVennootschapVanaf2021($input),
            $this->addEenmansZaakTotEnMetAJ2018(),
            $this->addEenmansZaakInAJ20192020($input),
            $this->addEenmansZaakVanaf2021($input)
        );
    }

    /**
     * @param VoordeelWagenInputTot2020|VoordeelWagenInputVanaf2020 $input
     * @return float
     */
    protected function calculateActualCO2Uitstoot(VoordeelWagenInputTot2020 $input): float
    {
        return $this->gramFormula($input)->output()['actualCo2'];
    }

    private function gramFormula(VoordeelWagenInputVanaf2020 $input): CalculationResultInterface
    {
        return $this->gramFormulaCalculator->calculate($this->adaptGramFormulaInput($input));
    }

    protected function adaptGramFormulaInput(VoordeelWagenInputVanaf2020 $input): GramFormulaInput
    {
        /** @var BrandstofEnumVanaf2020 $brandstofType */
        $brandstofType = $input->getBrandstof();
        return new GramFormulaInput(
            $brandstofType,
            $input->getco2uitstoot(),
            $input->isValseHybride(),
            $input->getEffectieveCO2ValseHybride() ?? 0.0,
            $input->getEffectieveCO2ValseHybride() !== null,
        );
    }

    /**
     * @param VoordeelWagenInputVanaf2020 $input
     */
    private function addVennootschapTotEnMetAj2020(VoordeelWagenInputVanaf2020 $input): AftrekbareAutokostenRowResult
    {
        $percentage = $input->getBrandstof() === BrandstofEnumVanaf2020::ELEKTRISCH() ? 120.0
            : $this->getPercentage($input->getBrandstof(), $this->calculateActualCO2Uitstoot($input));

        return new AftrekbareAutokostenRowResult(
            'vennootschap tot en met AJ 2020',
            75.0,
            $percentage * 100
        );
    }

    private function addVennootschapVanaf2021(VoordeelWagenInputVanaf2020 $input): AftrekbareAutokostenRowResult
    {
        return new AftrekbareAutokostenRowResult(
            'vennootschap vanaf AJ 2021',
            75.0,
            $this->gramFormula($input)->output()['resultPercentage']
        );
    }

    private function addEenmansZaakTotEnMetAJ2018(): AftrekbareAutokostenRowResult
    {
        return new AftrekbareAutokostenRowResult(
            'eenmanszaak tot en met AJ 2018',
            75.0,
            75.0
        );
    }

    private function addEenmansZaakInAJ20192020(VoordeelWagenInputVanaf2020 $input): AftrekbareAutokostenRowResult
    {
        $percentage = 120.0;

        if ($input->getBrandstof() !==  BrandstofEnumVanaf2020::ELEKTRISCH())
        {
            if ($input->getJaarAankoopLeaseOfRenting() < 2018) {
               $percentage = max($this->getPercentage($input->getBrandstof(), $this->calculateActualCO2Uitstoot($input)),75.0);
            }else{
                $this->getPercentage($input->getBrandstof(), $this->calculateActualCO2Uitstoot($input));
            }
        }
        /// IF(fuel_voordeel="ELEKTRISCH")
        ///     120%
        /// ELSE
        ///     IF(aankoop<2018)
        ///         MAX(VLOOKUP(uitstoot_gewoon; (fuel_voordeel="BENZINE") ? BENZINECO2 : DIESELCO2); 3), 75%)
        ///     ELSE
        ///         VLOOKUP(uitstoot_gewoon; (fuel_voordeel="BENZINE") ? BENZINECO2 : DIESELCO2);3)

        return new AftrekbareAutokostenRowResult(
            'eenmanszaak tot en met AJ 2018',
            75.0,
            $percentage
        );
    }

    private function addEenmansZaakVanaf2021(VoordeelWagenInputVanaf2020 $input): AftrekbareAutokostenRowResult
    {
        // =IF(AND(aankoop<2018;uitstoot_weerhouden<200);MAX(75%;gramformule);gramformule)
       $percentage = $this->gramFormula($input)->output()['resultPercentage'];

        if ($input->getJaarAankoopLeaseOfRenting() < 2018 && $input->getEffectieveCO2ValseHybride() < 300)
        {
            $percentage = max ( $this->gramFormula($input)->output()['resultPercentage'], 75.0 );
        }


        return new AftrekbareAutokostenRowResult(
            'eenmanszaak tot en met AJ 2018',
            $percentage,
            $percentage
        );
    }
}
<?php
declare(strict_types=1);

namespace Laudis\Calculators\VoordeelWagen;

use Youngsource\TypedEnum\TypedEnum;

/** @noinspection LowerAccessLevelInspection */

/**
 * Class BrandstofEnum
 * @package Laudis\Calculators\Venb
 *
 * @method static BrandstofEnumTot2020 ELEKTRISCH()
 * @method static BrandstofEnumTot2020 DIESEL()
 * @method static BrandstofEnumTot2020 BENZINE()
 *
 * @method static BrandstofEnumTot2020 resolve($constValue)
 */
class BrandstofEnumTot2020 extends TypedEnum
{
    /** @var string */
    protected const ELEKTRISCH = 'ELEKTRISCH';
    /** @var string */
    protected const DIESEL = 'DIESEL';
    /** @var string */
    protected const BENZINE = 'BENZINE';
}

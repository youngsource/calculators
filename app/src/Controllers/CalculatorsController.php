<?php
declare(strict_types=1);

namespace Laudis\Calculators\Controllers;

use InvalidArgumentException;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;
use Laudis\Calculators\Presenters\CalculatorsPresenter;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\Common\Controllers\BaseController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class CalculatorsController
 * Handles all calculator requests.
 *
 * @package Laudis\Calculators\Controllers
 */
final class CalculatorsController extends BaseController
{
    /** @var CalculatorsPresenter */
    private $presenter;
    /** @var CalculatorFactoriesCollection */
    private $collection;

    /**
     * CalculatorsController constructor.
     * @param CalculatorsPresenter $presenter
     * @param CalculatorFactoriesCollection $collection
     */
    public function __construct(
        ResponseWriterInterface $responseWriter,
        CalculatorsPresenter $presenter,
        CalculatorFactoriesCollection $collection
    ) {
        parent::__construct($responseWriter);
        $this->presenter = $presenter;
        $this->collection = $collection;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @return Response
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function __invoke(Request $request, Response $response): Response
    {
        return $this->writeToResponse($response, $this->presenter->present($this->collection));
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Calculators\Controllers;

use InvalidArgumentException;
use Laudis\Common\Contracts\RequestToCalculationInterface;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\Common\Controllers\BaseController;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class CalculationController
 * Handles all calculation requests.
 *
 * @package Laudis\Calculators\Controllers
 */
final class CalculationController extends BaseController
{
    /** @var RequestToCalculationInterface */
    private $strategy;

    /**
     * CalculationController constructor.
     *
     * @param ResponseWriterInterface $responseWriter
     * @param RequestToCalculationInterface $strategy
     */
    public function __construct(ResponseWriterInterface $responseWriter, RequestToCalculationInterface $strategy)
    {
        parent::__construct($responseWriter);
        $this->strategy = $strategy;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $params
     * @return Response
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function __invoke(Request $request, Response $response, array $params): Response
    {
        return $this->writeToResponse($response, [
            'data' => $this->strategy->run($request, $params['calculator'])->output()
        ]);
    }
}

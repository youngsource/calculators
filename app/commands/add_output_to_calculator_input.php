<?php
declare(strict_types=1);

use Commando\Command;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\TaxShelter\OutputPresenter;
use Laudis\Calculators\TaxShelter\TaxShelterCalculator;
use Laudis\Common\Exceptions\ValidationException;

require_once __DIR__ . '/../vendor/autoload.php';

/** @noinspection PhpUnhandledExceptionInspection */
/** @var CalculatorFactoriesCollection $collection */
$collection = boot_app()->getContainer()[CalculatorFactoriesCollection::class];

$command = new Command;

$command->option('f')
    ->aka('file')
    ->file()
    ->require()
    ->describedAs('The calculators v0.1 input format file.');

$command->option('o')
    ->aka('output')
    ->require()
    ->describedAs('The calculators v0.1 output format file.');

$command->option('c')
    ->aka('calculator')
    ->require()
    ->must(static function (string $id) use ($collection) {
        return in_array($id, $collection->getAllCalculatorNames(), true);
    })
    ->describedAs('The calculator identifier which will be used to get the output from. All values are: ' . implode(', ',
            $collection->getAllCalculatorNames()));

$inputFile = $command['file'];
$outputFile = $command['output'];
$calculatorId = $command['calculator'];

$calculatorFactory = $collection->get($calculatorId);
$inputGroup = json_decode(file_get_contents($inputFile), true, 512, JSON_THROW_ON_ERROR);

$tbr = array_map(static function (array $input) use ($calculatorFactory) {

    /** @var TaxShelterCalculator $calculator */
    $inputValues = $input['input'];
    $validation = $calculatorFactory->validation($inputValues);
    $calculator = $calculatorFactory->calculator($inputValues);
    if ($validation->fails()) {
        throw new ValidationException($validation);
    }
    $result = $calculator->calculate($calculatorFactory->inputFromArray($inputValues));
    $result->setOutputMode(CalculationResultInterface::BASIC);
    $input['output'] = $result->output();
    return $input;

}, $inputGroup);

file_put_contents($outputFile, json_encode($tbr, JSON_THROW_ON_ERROR));


<?php
declare(strict_types=1);

use Commando\Command;

require_once __DIR__ . '/../vendor/autoload.php';

$command = new Command;

$command->option('f')
    ->aka('file')
    ->file()
    ->require()
    ->describedAs('The postman v2.1 file to read the input from');

$command->option('g')
    ->aka('groupname')
    ->require()
    ->describedAs('The name of the input group');

$command->option('o')
    ->aka('output')
    ->default('output.json')
    ->describedAs('The transformed output in calculators v0.1 format');

$file = $command['file'];
$outputFile = $command['output'];
$groupName = $command['groupname'];

$inputFile = json_decode(file_get_contents($file), true, 512, JSON_THROW_ON_ERROR);

/** @var array[] $subgroups */
$subgroups = $inputFile['item'];
$tbr = [];
foreach ($subgroups as $subgroup) {

    /** @var array $subGroupCollection */
    ['name' => $subGroupName, 'item' => $subGroupCollection] = $subgroup;

    foreach ($subGroupCollection as $input) {

        $inputName = $input['name'];
        $body = $input['request']['body'];
        /** @var array $testInput */
        $testInput = $body[$body['mode']];

        $inputSet = [
            'groupName' => $groupName,
            'subGroupName' => $subGroupName,
            'inputName' => $inputName,
            'input' => []
        ];

        foreach ($testInput as $value) {
            $inputSet['input'][$value['key']] = $value['value'];
        }

        $tbr[] = $inputSet;
    }
}

file_put_contents($outputFile, json_encode($tbr, JSON_THROW_ON_ERROR));

return 0;

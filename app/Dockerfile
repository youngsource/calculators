FROM php:7.4-fpm

LABEL maintainer="Ghlen Nagels <ghlen@pm.me>"


# Global configuration & installation fase: configure everything needed to have the programs run effectively

RUN apt-get -yqq update \
    && apt-get install -yqq --no-install-recommends libzip-dev zip unzip libicu-dev libgmp-dev chromium dos2unix \
    && docker-php-ext-install -j$(nproc) pdo_mysql opcache zip intl bcmath gmp calendar \
    && pecl install xdebug \
    && pecl install redis \
    && docker-php-ext-enable xdebug redis

COPY docker/composer-installer.sh .
COPY docker/php.ini /usr/local/etc/php/php.ini
COPY docker/conf.d/* /usr/local/etc/php/conf.d/

RUN chmod +x composer-installer.sh \
    && dos2unix composer-installer.sh \
    && dos2unix /usr/local/etc/php/php.ini \
    && dos2unix /usr/local/etc/php/conf.d/* \
    && bash composer-installer.sh \
    && mv composer.phar /usr/local/bin/composer \
    && chmod +x /usr/local/bin/composer \
    && mkdir -p /backups/mariadb


#apperently the home directory of www-data is not owned by www-data.. only $home/html is owned by www-data
RUN mkdir /var/www/logs $(eval echo "~www-data")/.composer \
    && chown www-data:www-data /var/www/logs $(eval echo "~www-data")/.composer

USER www-data:www-data

RUN mkdir caching api


WORKDIR caching
COPY composer.json composer.loc[k] ./
RUN composer install \
        --no-interaction \
        --prefer-dist \
        --no-progress \
        --no-scripts \
        --profile \
        --no-autoloader

WORKDIR /var/www/html/api/
COPY --chown=www-data:www-data src/ src/
COPY --chown=www-data:www-data public/ public/
COPY --chown=www-data:www-data resources/ resources/
COPY --chown=www-data:www-data composer.json composer.loc[k] .env phinx.php ./
COPY --chown=www-data:www-data submodules/ submodules/
RUN mkdir logs

RUN composer install \
        --no-interaction \
        --prefer-dist \
        --no-progress \
        --no-scripts \
        --profile \
    && composer dumpautoload

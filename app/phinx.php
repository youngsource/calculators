<?php
declare(strict_types=1);

require __DIR__ . '/vendor/autoload.php';

$pdo = boot_app()->getContainer()->get(PDO::class);

return [
    'paths' => [
        'migrations' => __DIR__ . '/resources/migrations',
        'seeds' => __DIR__ . '/resources/seeds'
    ],
    'environments' => [
        'default_database' => 'development',
        'development' => [
            'connection' => $pdo,
            'name' => $pdo->query('SELECT DATABASE()')->fetchColumn()
        ]
    ],
    'version_order' => 'creation'
];

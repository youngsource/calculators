<?php
declare(strict_types=1);
/** @noinspection PhpIllegalPsrClassPathInspection */

use Codeception\Exception\InjectionException;
use Dotenv\Dotenv;
use PHPUnit\Framework\TestCase;

/** @noinspection PhpIllegalPsrClassPathInspection */

/**
 * Class ResetLinkCest
 *
 * @mixin TestCase
 */
final class ResetLinkCest
{


    /**
     * @param AcceptanceTester $I
     * @throws InjectionException
     * @throws Exception
     */
    public function tryToTest(AcceptanceTester $I): void
    {
        Dotenv::create(__DIR__.'/../../', '.env')->load();
        $I->resizeWindow(1280, 720);
        $this->gotoLoginAndForgetPassword($I);
        $this->requestResetLink($I);
        $this->checkIfMailWasSent($I);
        $url = $this->grabTheResetlinkFromMail($I);
        $password = $this->insertNewPassword($I, $url);
        $this->loginWithNewPass($I, $password);
        $this->checkIfIWasRedirected($I);
        $this->changePasswordBack($I, $password);
        $this->tryToReuseResetLink($I, $url);
    }

    /**
     * @param AcceptanceTester $I
     * @throws InjectionException
     */
    private function gotoLoginAndForgetPassword(AcceptanceTester $I): void
    {
        $I->amOnPage('/login');
        $I->clickWithLeftButton(['css' => '#forgot-password']);
    }

    /**
     * @param AcceptanceTester $I
     * @throws InjectionException
     * @throws Exception
     */
    private function requestResetLink(AcceptanceTester $I): void
    {
        $I->canSeeInCurrentUrl('send-reset');
        $I->fillField(['name' => 'email'], 'ghlen@pm.me');
        $I->click(['css' => '#email-reset-btn']);
        $I->waitForElement(['css' => '.success']);
    }

    /**
     * @param AcceptanceTester $I
     * @throws InjectionException
     */
    private function checkIfMailWasSent(AcceptanceTester $I): void
    {
        $I->amOnUrl(getenv('MAIL_POSTBIN_URL'));

        $time = new DateTime;
        $time1 = new DateTime('-1 minute');
        $time2 = new DateTime('-2 minutes');
        $possibleDates = '/(' .
            $time->format('m\\\/d\\\/Y @ h:i') . ')|(' .
            $time1->format('m\\\/d\\\/Y @ h:i') . ')|(' .
            $time2->format('m\\\/d\\\/Y @ h:i') .
            ')/';
        $text = $I->grabTextFrom(['css' => '.rs-main .rs-container .rs-inner .rs-row .span-12 [align$="left"]']);
        TestCase::assertRegExp($possibleDates, $text);
    }

    /**
     * @param AcceptanceTester $I
     * @return string
     * @throws InjectionException
     */
    private function grabTheResetlinkFromMail(AcceptanceTester $I): string
    {
        $email = $I->grabTextFrom(['css' => '.rs-main .rs-container .rs-inner .rs-row .span-12 .standard:nth-of-type(1) tbody tr:nth-of-type(4) div:nth-of-type(1)']);
        $matches = [];
        preg_match('/<a id="reset-link"(\W|\w)*>/U', $email, $matches);
        strpos($email, $matches[0]);
        $url = substr($email, strpos($email, $matches[0]) + strlen($matches[0]));
        $url = substr($url, 0, strpos($url, '<'));
        return $url;
    }

    /**
     * @param AcceptanceTester $I
     * @param string $url
     * @return string
     * @throws InjectionException
     * @throws Exception
     */
    private function insertNewPassword(AcceptanceTester $I, string $url): string
    {
        $I->amOnUrl($url);
        $randomPass = 'nieuw-wachtwoord';
        $I->fillField(['name' => 'password'], $randomPass);
        $I->fillField(['name' => 'passwordConfirmed'], $randomPass);

        $I->click(['css' => '#reset-btn']);
        $I->canSeeElement(['css' => '.success']);
        return $randomPass;
    }

    /**
     * @param AcceptanceTester $I
     * @param string $randomPass
     * @throws InjectionException
     */
    private function loginWithNewPass(AcceptanceTester $I, string $randomPass): void
    {
        $I->amOnPage('/login');
        $I->fillField(['name' => 'email'], 'ghlen@pm.me');
        $I->fillField(['name' => 'password'], $randomPass);
        $I->click(['css' => '#login-btn']);
    }

    /**
     * @param AcceptanceTester $I
     * @throws InjectionException
     */
    private function checkIfIWasRedirected(AcceptanceTester $I): void
    {
        $I->cantSeeInCurrentUrl('login');
    }

    /**
     * @param AcceptanceTester $I
     * @throws InjectionException
     * @throws Exception
     */
    private function changePasswordBack(AcceptanceTester $I, string $oldpass): void
    {
        $I->amOnPage('/update-password');
        $I->fillField(['name' => 'passwordOld'], $oldpass);
        $I->fillField(['name' => 'password'], 'testen');
        $I->fillField(['name' => 'passwordConfirmed'], 'testen');
        $I->click(['css' => '#update-password-btn']);
        $I->waitForElement(['css' => '.success']);
    }

    /**
     * @param AcceptanceTester $I
     * @param string $url
     * @throws InjectionException
     * @throws Exception
     */
    private function tryToReuseResetLink(AcceptanceTester $I, string $url): void
    {
        $I->amOnUrl($url);
        $randomPass = base64_encode(random_bytes(10));
        $I->fillField(['name' => 'password'], $randomPass);
        $I->fillField(['name' => 'passwordConfirmed'], $randomPass);

        $I->click(['css' => '#reset-btn']);
        $I->waitForElement(['css' => '.errors']);
    }
}

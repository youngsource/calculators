<?php
/** @noinspection PhpIllegalPsrClassPathInspection */
declare(strict_types=1);

/** @noinspection PhpIllegalPsrClassPathInspection */

use Codeception\Exception\InjectionException;

/**
 * Class FirstCest
 */
final class TaxShelterCest
{
    /** @var array */
    private const INPUT_FILES = [
        __DIR__ . '/../../resources/testinput/taxshelter/tax_shelter_io_aj2018.json',
        __DIR__ . '/../../resources/testinput/taxshelter/tax_shelter_io_aj2019.json',
        __DIR__ . '/../../resources/testinput/taxshelter/tax_shelter_io_aj2020.json',
        __DIR__ . '/../../resources/testinput/taxshelter/tax_shelter_io_aj2021.json'
    ];

    /**
     * @param AcceptanceTester $I
     * @throws InjectionException
     * @throws Exception
     */
    public function tryToTest(AcceptanceTester $I): void
    {
        $I->amOnPage('/login');
        $I->fillField(['name' => 'email'], 'ghlen@pm.me');
        $I->fillField(['name' => 'password'], 'testen');
        $I->click(['css' => '#login-btn']);
        $I->resizeWindow(1280, 720);
        $I->amOnPage('/taxshelter');
        $I->waitForElement(['css' => '#tax-shelter-form']);
        foreach ($this->ioDataProvider() as $name => $unitData) {
            $this->testIoData($unitData['input'], $unitData['output'], $unitData['name'], $I);
        }
    }

    /**
     * @param array $input
     * @param array $output
     * @param string $name
     * @param AcceptanceTester $tester
     * @throws InjectionException
     * @throws Exception
     */
    private function testIoData(array $input, array $output, string $name,  AcceptanceTester $tester): void
    {
        foreach ($input as $key => $value) {
            if ($value === null || $value === '') {
                continue;
            }
            if (strpos($key, 'is') === 0 || $key === 'maxTaxShelter' || $key === 'maxLiqReserve') {
                if ($value) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    $tester->clickWithLeftButton(['css' => '#' . $key . '-ja']);
                } else {
                    $tester->clickWithLeftButton(['css' => '#' . $key . '-nee']);
                    if ($key === 'maxTaxShelter') {
                        $tester->waitForElement(['css' => "input[name='taxShelterInvestering']"]);
                    }
                    if ($key === 'maxLiqReserve') {
                        $tester->waitForElement(['css' => "input[name='liqReserve']"]);
                    }
                }
            } else {
                $tester->fillField(['css' => 'input[name=' . $key . ']'], $value);
            }
        }

        $filename = str_replace(' ', '_', strtolower($name));

        $tester->clickWithLeftButton(['css' => '#calculate']);
        $tester->waitForElement(['css' => '.tax-shelter-output']);
        try {
            $tester->seeElement(['css' => '.tax-shelter-boeking']);
        } /** @noinspection BadExceptionsProcessingInspection */ catch(\PHPUnit\Framework\Exception $e) {
            $tester->clickWithLeftButton(['css' => '#toggle-tax-shelter-boeking']);
            $tester->waitForElement(['css' => '.tax-shelter-boeking' ]);
        }
        $tester->makeScreenshot($filename);
    }


    /**
     * @return array
     */
    private function ioDataProvider(): array
    {
        $ioData = [];
        foreach (self::INPUT_FILES as $inputFile) {
            $io = json_decode(file_get_contents($inputFile), true, 512, JSON_THROW_ON_ERROR);
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $ioData = array_merge($ioData, $io);
        }
        $tbr = [];
        foreach ($ioData as $io) {
            ['groupName' => $group, 'subGroupName' => $subGroup, 'inputName' => $inputName] = $io;
            $name = 'Group: ' . $group . ', Subgroup: ' . $subGroup . ' Input: ' . $inputName;
            $tbr[$name] = [
                'input' => $io['input'],
                'output' => $io['output'],
                'name' => $name
            ];
        }
        return $tbr;
    }
}

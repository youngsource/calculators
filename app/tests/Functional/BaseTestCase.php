<?php
declare(strict_types=1);

namespace Laudis\Calculators\Tests\Functional;

use Dotenv\Dotenv;
use Laudis\Calculators\Calculators\Enums\TaxShelterColumnEnum as Column;
use Laudis\Calculators\Calculators\Enums\TaxShelterValueEnum as Value;
use Laudis\Calculators\Calculators\Factories\TaxShelterInputFactory;
use Laudis\Calculators\Calculators\Results\BasicResult;
use Laudis\Calculators\Calculators\Results\FormattedResult;
use Laudis\Calculators\Tests\Traits\RunsApplication;
use NumberFormatter;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use function count;
use const JSON_PRETTY_PRINT;

/**
 * Class BaseTestCase
 * @package Tests\Functional
 */
abstract class BaseTestCase extends TestCase
{
    use RunsApplication;

    /** @var TaxShelterInputFactory */
    private $inputFactory;

    /**
     * @param string $name
     * @param string $dataName
     */
    public function __construct(?string $name = null, array $data = [], string $dataName = null)
    {
        parent::__construct($name, $data, $dataName ?? '');
        Dotenv::create(__DIR__ . './../../')->load();
        $this->inputFactory = new TaxShelterInputFactory;
    }

    /**
     * @param string $url
     * @param array $expected
     * @param array $input
     */
    protected function assertCalculationOutput(string $url, array $expected, array $input): void
    {
        $response = $this->runApp('POST', $url, $input);

        if ($response->getStatusCode() !== 200) {
            $response->getBody()->rewind();
            throw new RuntimeException(json_encode(
                json_decode($response->getBody()->getContents()),
                JSON_PRETTY_PRINT
            ));
        }

        static::assertEquals(200, $response->getStatusCode());
        $response->getBody()->rewind();
        $data = json_decode($response->getBody()->getContents(), true, JSON_THROW_ON_ERROR)['data'];

        $expected = $this->formatResult($expected);

        $mappingExpected = [];
        $values = $this->values();
        for ($it = 0, $iMax = count($values); $it < $iMax; ++$it) {
            $mappingExpected[$values[array_keys($values)[$it]]->getValue()] = [
                Column::AANSLAG_BEREKENING_EN_BELASTINGEN()->getValue() => $expected[$it][0],
                Column::C1080PN()->getValue() => $expected[$it][1],
                Column::C9905()->getValue() => $expected[$it][2]
            ];
        }

        static::assertEqualsWithDelta($mappingExpected, $data, 0.01);
    }

    /**
     * @param array $expected
     * @return array
     */
    protected function formatResult(array $expected): array
    {
        $expected = (new FormattedResult(
            new BasicResult($expected),
            $this->app->getContainer()->get('currency_formatter')
        ))->output();
        return $expected;
    }

    /**
     * @return Value[]
     */
    private function values(): array
    {
        return Value::getAllInstances();
    }

    /**
     * @return TaxShelterInputFactory
     */
    protected function getInputFactory(): TaxShelterInputFactory
    {
        return $this->inputFactory;
    }
}

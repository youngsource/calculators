<?php
declare(strict_types=1);

namespace Laudis\Calculators\Tests\Traits;

use Exception;
use Laudis\Calculators\Registers\MainRegister;
use Laudis\Common\Auxiliaries\Application;
use Laudis\Common\Contracts\AppInterface;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Environment;
use Slim\Http\Request;

/**
 * Trait RunsApplication
 * @package Laudis\Calculators\Tests\Traits
 */
trait RunsApplication
{
    /** @var AppInterface */
    protected $app;

    /**
     * Process the application given a request method and URI
     *
     * @param string $requestMethod the request method (e.g. GET, POST, etc.)
     * @param string $requestUri the request URI
     * @param array|object|null $requestData the request data
     * @return ResponseInterface
     */
    protected function runApp(string $requestMethod, string $requestUri, array $requestData = null): ResponseInterface
    {
        $this->registerApp($requestMethod, $requestUri, $requestData);

        try {
            return $this->app->process(
                $this->app->getContainer()->get('request'),
                $this->app->getContainer()->get('response')
            );
        } catch (Exception $exception) {
            return $this->app->getContainer()->get('errorHandler')(
                $this->app->getContainer()->get('request'),
                $this->app->getContainer()->get('response'),
                $exception
            );
        }
    }

    /**
     * @param string|null $requestMethod
     * @param string|null $requestUri
     * @param array|null $requestData
     * @return \Laudis\Common\Contracts\AppInterface
     */
    protected function registerApp(
        string $requestMethod = null,
        string $requestUri = null,
        array $requestData = null
    ): AppInterface {
        $environment = Environment::mock([
            'REQUEST_METHOD' => $requestMethod,
            'REQUEST_URI' => $requestUri
        ]);

        $request = Request::createFromEnvironment($environment);

        if ($requestData !== null) {
            $request = $request->withParsedBody($requestData);
        }

        $this->app = new Application;

        $container = $this->app->getContainer();
        $container->set('request', $request);

        MainRegister::make()->register($this->app);

        return $this->app;
    }
}

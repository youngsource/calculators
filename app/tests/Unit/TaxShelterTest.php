<?php

declare(strict_types=1);

namespace Laudis\Calculators\Tests\Unit;

use DateTime;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;
use Laudis\Calculators\Contracts\CalculationResultInterface;
use Laudis\Calculators\Contracts\CalculatorFactoryInterface;
use Laudis\Calculators\Registers\MainRegister;
use Laudis\Calculators\TaxShelter\TaxShelterCalculator;
use Laudis\Calculators\TaxShelter\TaxShelterCalulatorFactory;
use Laudis\Calculators\TaxShelter\TaxShelterInputFactory;
use Laudis\Calculators\TaxShelter\ValidationFactory;
use Laudis\Calculators\TaxShelter\VersieAj2018\TaxShelterInput2018;
use Laudis\Common\Auxiliaries\Application;
use Laudis\Index\Contracts\ContextualIndexedValueRepositoryInterface;
use Laudis\Scale\Contracts\ContextualRepositoryScaleInterface;
use PHPUnit\Framework\TestCase;
use Pimple\Container;
use Psr\Container\ContainerInterface;
use Rakit\Validation\Validator;

use function array_merge;
use function file_get_contents;
use function json_decode;

use const JSON_THROW_ON_ERROR;

/**
 * Class TaxShelterTest
 * @package Laudis\Calculators\Tests\Unit
 */
final class TaxShelterTest extends TestCase
{
    /** @var array */
    private const INPUT_FILES = [
        __DIR__ . '/../../resources/testinput/taxshelter/tax_shelter_io_aj2018.json',
        __DIR__ . '/../../resources/testinput/taxshelter/tax_shelter_io_aj2019.json',
        __DIR__ . '/../../resources/testinput/taxshelter/tax_shelter_io_aj2020.json',
        __DIR__ . '/../../resources/testinput/taxshelter/tax_shelter_io_aj2021.json'
    ];

    /** @var CalculatorFactoryInterface */
    private $calcFactory;

    /**
     * @dataProvider ioDataProvider
     * @param array $input
     * @param array $output
     */
    public function testIoData(array $input, array $output): void
    {
        /** @var TaxShelterCalculator $calculator */
        $calculator = $this->calcFactory->calculator($input);
        /** @var TaxShelterInput2018 $inputObject */
        $inputObject = $this->calcFactory->inputFromArray($input);

        $taxShelterOutput = $calculator->calculate($inputObject);
        $taxShelterOutput->setOutputMode(CalculationResultInterface::BASIC);
        $result = $taxShelterOutput->output();
        self::assertEquals($output, $result);
    }

    /**
     * @return array
     */
    public function ioDataProvider(): array
    {
        $ioData = [];
        foreach (self::INPUT_FILES as $inputFile) {
            $io = json_decode(file_get_contents($inputFile), true, 512, JSON_THROW_ON_ERROR);
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $ioData = array_merge($ioData, $io);
        }
        $tbr = [];
        foreach ($ioData as $io) {
            ['groupName' => $group, 'subGroupName' => $subGroup, 'inputName' => $inputName] = $io;
            $tbr['Group: ' . $group . ', Subgroup: ' . $subGroup . ' Input: ' . $inputName] = [
                'input' => $io['input'],
                'output' => $io['output']
            ];
        }
        return $tbr;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $app = new Application();
        MainRegister::make()->register($app);
        $container = $app->getContainer();
        assert($container instanceof Container);
        $container->set(
            TaxShelterCalulatorFactory::class,
            static function (ContainerInterface $container) {
                return new TaxShelterCalulatorFactory(
                    new TaxShelterInputFactory(DateTime::createFromFormat('Y-m-d', '2019-07-01')),
                    new ValidationFactory(new Validator()),
                    $container->get(ContextualIndexedValueRepositoryInterface::class),
                    $container->get(ContextualRepositoryScaleInterface::class)
                );
            }
        );
        $this->calcFactory = $container->get(CalculatorFactoriesCollection::class)->get('tax_shelter_boeking');
    }
}

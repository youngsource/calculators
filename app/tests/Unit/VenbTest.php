<?php
/**
 * Created by PhpStorm.
 * User: bavo
 * Date: 01/08/19
 * Time: 15:59
 */

namespace Laudis\Calculators\Tests\Unit;

use Exception;
use Laudis\Calculators\Collections\CalculatorFactoriesCollection;
use Laudis\Calculators\Venb\VenbCalculatorFactory;
use Laudis\Calculators\Venb\VenbInput2018;
use Laudis\Calculators\Venb\VenbOperationInterface;
use PHPUnit\Framework\TestCase;
use function array_merge;
use function file_get_contents;
use function json_decode;
use const JSON_THROW_ON_ERROR;

final class VenbTest extends TestCase
{
    /** @var array */
    private const INPUT_FILES = [
        __DIR__ . '/../../resources/testinput/venb/venb_io.json'
    ];

    /** @var VenbCalculatorFactory */
    private $calcFactory;

    /**
     * @dataProvider ioDataProvider
     * @param array $input
     * @param array $output
     * @throws Exception
     */
    public function testIoData(array $input, array $output) : void
    {
        /** @var VenbOperationInterface $calculator */
        $calculator = $this->calcFactory->calculator($input);
        /** @var VenbInput2018 $inputObject */
        $inputObject = $this->calcFactory->inputFromArray($input);

        $venbOutput2018 = $calculator->calculate($inputObject);
        $result = $venbOutput2018->output();

        self::assertEquals($output, $result);
    }

    /**
     * @return array
     */
    public function ioDataProvider(): array
    {
        $ioData = [];
        foreach (self::INPUT_FILES as $inputFile) {
            $io = json_decode(file_get_contents($inputFile), true, 512, JSON_THROW_ON_ERROR);
            /** @noinspection SlowArrayOperationsInLoopInspection */
            $ioData = array_merge($ioData, $io);
        }
        $tbr = [];
        foreach ($ioData as $io) {
            ['groupName' => $group, 'subGroupName' => $subGroup, 'inputName' => $inputName] = $io;
            $tbr['Group: ' . $group . ', Subgroup: ' . $subGroup . ' Input: ' . $inputName] = [
                'input' => $io['input'],
                'output' => $io['output']
            ];
        }
        return $tbr;
    }

    protected function setUp() : void
    {
        parent::setUp();
        $this->calcFactory = boot_app()->getContainer()->get(CalculatorFactoriesCollection::class)->get('venb');
    }
}

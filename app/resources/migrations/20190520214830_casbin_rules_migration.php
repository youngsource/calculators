<?php
/** @noinspection AutoloadingIssuesInspection */
declare(strict_types=1);

use CasbinAdapter\Database\Adapter;
use Phinx\Migration\AbstractMigration;

/**
 * Class CasbinRulesMigration
 */
final class CasbinRulesMigration extends AbstractMigration
{
    /**
     * @throws Throwable
     */
    public function change(): void
    {
        Adapter::newAdapter(settings()['casbin'])->initTable();
    }
}

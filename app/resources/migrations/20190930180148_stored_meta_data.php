<?php

use Phinx\Migration\AbstractMigration;

class StoredMetaData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    //TODO add version
    public function change()
    {
//        $this->table('stored_meta_data')
//            ->addColumn('userID','integer')
//            ->addColumn('calculator','string')
//            ->addColumn('calculator_version','string')
//            ->addColumn('error','bool')
//            ->addColumn('error_message','string')
//            ->addColumn('input','string')
//            ->addColumn('calculation_timestamp','string')
//            ->addColumn('output','string')
//            ->addColumn('calculated_timestamp','string')
//            //weet niet zeker wat je bedoelde dus heb ze beide erin gezet
//            ->addTimestamps()
//            ->create();
    }

}

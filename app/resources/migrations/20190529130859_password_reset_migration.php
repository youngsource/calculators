<?php
/** @noinspection AutoloadingIssuesInspection */
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

/**
 * Class PasswordResetMigration
 */
final class PasswordResetMigration extends AbstractMigration
{
    public function change(): void
    {
        $this->table('password_resets')
            ->addColumn('token', 'string', ['length' => 1000])
            ->addColumn('user_id', 'integer')
            ->addColumn('used', 'boolean', ['default' => 0])
            ->addForeignKey(['user_id'], 'users')
            ->addIndex(['token'])
            ->create();
    }
}

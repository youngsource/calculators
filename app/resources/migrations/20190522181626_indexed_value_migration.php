<?php
declare(strict_types=1);

use Phinx\Migration\AbstractMigration;

/** @noinspection AutoloadingIssuesInspection */

/**
 * Class IndexedValueMigration
 */
final class IndexedValueMigration extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change(): void
    {
        $this->table('indexed_values')
            ->addTimestamps()

            ->addColumn('name', 'string')
            ->addColumn('contextual_id', 'integer')
            ->addColumn('value', 'float')
            ->addColumn('added_since', 'date')
            ->addColumn('removed_since', 'date', ['null' => true])
            ->addColumn('type', 'string')

            ->addIndex(['contextual_id', 'added_since'], ['unique' =>  true])
            ->addIndex('added_since')
            ->addIndex('removed_since')
            ->addIndex('contextual_id')

            ->create();
    }
}

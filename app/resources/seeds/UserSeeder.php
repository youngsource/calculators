<?php
declare(strict_types=1);

use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Phinx\Seed\AbstractSeed;

/**
 * Class UserSeeder
 */
final class UserSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run(): void
    {
        parent::run();
        $faker = Faker\Factory::create();
        $container = boot_app()->getContainer();
        $repository = $container->get(UserRepositoryInterface::class);
        $hasher = $container->get(PasswordHasherInterface::class);

        try {
            if ($repository->findUser('ghlen@pm.me') === null) {
                $repository->insertUser('ghlen@pm.me', 'ghlen', $hasher->hash('test'));
            }
            if ($repository->findUser('tim.galloo@gmail.com') === null) {
                $repository->insertUser('tim.galloo@gmail.com', 'tim', $hasher->hash('galloo'));
            }
            if ($repository->findUser('anja.berlanger@minfin.fed.be') === null) {
                $repository->insertUser('anja.berlanger@minfin.fed.be', 'anja', $hasher->hash('berlanger'));
            }
            if ($repository->findUser('Annik.cardoen@galloptaxshelter.be') === null) {
                $repository->insertUser('Annik.cardoen@galloptaxshelter.be', 'annik', $hasher->hash('annik_cardoen1'));
            }
            if ($repository->findUser('Kristel.romulus@galloptaxshelter.be') === null) {
                $repository->insertUser('Kristel.romulus@galloptaxshelter.be', 'kristel', $hasher->hash('kristel_romulus1'));
            }
            if ($repository->findUser('Marcel.zandstra@galloptaxshelter.be') === null) {
                $repository->insertUser('Marcel.zandstra@galloptaxshelter.be', 'marcel', $hasher->hash('marcel_zandstra1'));
            }
            if ($repository->findUser('Lieve.decoster@demensen.be') === null) {
                $repository->insertUser('Lieve.decoster@demensen.be', 'lieve', $hasher->hash('lieve_decoster1'));
            }
            if ($repository->findUser('Maurits.lemmens@demensen.be') === null) {
                $repository->insertUser('Maurits.lemmens@demensen.be', 'maurits', $hasher->hash('maurits_lemmens1'));
            }
            if ($repository->findUser('Guy.torrekens@demensen.be') === null) {
                $repository->insertUser('Guy.torrekens@demensen.be', 'guy', $hasher->hash('guy_torrekens1'));
            }
            if ($repository->findUser('Mike.lammers@demensen.be') === null) {
                $repository->insertUser('Mike.lammers@demensen.be', 'mike', $hasher->hash('mike_lammers1'));
            }
            if ($repository->findUser('Karen.feys@demensen.be') === null) {
                $repository->insertUser('Karen.feys@demensen.be', 'karen', $hasher->hash('karen_feys1'));
            }
            if ($repository->findUser('guest@galloptaxshelter.be') === null) {
                $repository->insertUser('guest@galloptaxshelter.be', 'guest', $hasher->hash('gastlogin'));
            }
            if ($repository->findUser('guest@laudis.tech') === null) {
                $repository->insertUser('guest@laudis.tech', 'guest', $hasher->hash('gastlogin'));
            }
            if ($repository->getUserById(1) === null) {
                for ($i = 0; $i < 100; $i++) {
                    $repository->insertUser(
                        $faker->email,
                        $faker->userName,
                        $hasher->hash($faker->password)
                    );
                }
            }
        } catch (Error | Exception | Throwable $e) {
            echo $e->getMessage();
        }
    }
}

<?php
declare(strict_types=1);

use Phinx\Seed\AbstractSeed;

/**
 * Class CabinRulesSeeder
 */
final class CabinRulesSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run(): void
    {
        parent::run();
        $cnt = boot_app()->getContainer()->get(PDO::class)->query('SELECT COUNT(*) FROM casbin_rule')->fetchColumn();
        if ((int)$cnt === 0) {
            $this->table('casbin_rule')->insert([
                [
                    'ptype' => 'p',
                    'v0' => 'gallop_client',
                    'v1' => 'calculate',
                    'v2' => 'tax_shelter'
                ],
                [
                    'ptype' => 'p',
                    'v0' => 'practinet_user',
                    'v1' => 'calculate',
                    'v2' => 'tax_shelter'
                ],
                [
                    'ptype' => 'p',
                    'v0' => 'gallop_client',
                    'v1' => 'calculate',
                    'v2' => 'tax_shelter_raming'
                ],
                [
                    'ptype' => 'p',
                    'v0' => 'gallop_client',
                    'v1' => 'calculate',
                    'v2' => 'tax_shelter_boeking'
                ],
                [
                    'ptype' => 'p',
                    'v0' => 'gallop_client',
                    'v1' => 'calculate',
                    'v2' => 'venb'
                ]
            ])->save();
        }
    }
}

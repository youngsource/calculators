<?php
declare(strict_types=1);

use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Phinx\Seed\AbstractSeed;

/**
 * Class RolesSeeder
 */
final class RolesSeeder extends AbstractSeed
{
    /**
     * @return array
     */
    public function getDependencies(): array
    {
        return array_merge(parent::getDependencies(), [
            UserSeeder::class
        ]);
    }

    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run(): void
    {
        parent::run();
        /** @var UserRepositoryInterface $repo */
        $repo = boot_app()->getContainer()->get(UserRepositoryInterface::class);
        foreach ($repo->getAllUsers() as $user) {
            if (count($repo->getRoles($user)) === 0) {
                $this->table('roles')->insert(['user_id' => $user->getId(), 'role' => 'gallop_client'])->save();
            }
        }
    }
}

#!/usr/bin/env bash

php ././../../commands/transform_postman_input.php \
    -f ././../testinput/taxshelter/tax_shelter_test_aj2021.postman_collection.json \
    -o ././../testinput/taxshelter/tax_shelter_aj2021.json \
    -g "tax shelter aj 2021"

php ././../../commands/transform_postman_input.php \
    -f ././../testinput/taxshelter/tax_shelter_test_aj2020.postman_collection.json \
    -o ././../testinput/taxshelter/tax_shelter_aj2020.json \
    -g "tax shelter aj 2020"

php ././../../commands/transform_postman_input.php \
    -f ././../testinput/taxshelter/tax_shelter_test_aj2019.postman_collection.json \
    -o ././../testinput/taxshelter/tax_shelter_aj2019.json \
    -g "tax shelter aj 2019"

php ././../../commands/transform_postman_input.php \
    -f ././../testinput/taxshelter/tax_shelter_test_aj2018.postman_collection.json \
    -o ././../testinput/taxshelter/tax_shelter_aj2018.json \
    -g "tax shelter aj 2018"

php ././../../commands/add_output_to_calculator_input.php \
    -f ././../testinput/taxshelter/tax_shelter_aj2021.json \
    -o ././../testinput/taxshelter/tax_shelter_io_aj2021.json \
    -c tax_shelter

php ././../../commands/add_output_to_calculator_input.php \
    -f ././../testinput/taxshelter/tax_shelter_aj2020.json \
    -o ././../testinput/taxshelter/tax_shelter_io_aj2020.json \
    -c tax_shelter

php ././../../commands/add_output_to_calculator_input.php \
    -f ././../testinput/taxshelter/tax_shelter_aj2019.json \
    -o ././../testinput/taxshelter/tax_shelter_io_aj2019.json \
    -c tax_shelter

php ././../../commands/add_output_to_calculator_input.php \
    -f ././../testinput/taxshelter/tax_shelter_aj2018.json \
    -o ././../testinput/taxshelter/tax_shelter_io_aj2018.json \
    -c tax_shelter

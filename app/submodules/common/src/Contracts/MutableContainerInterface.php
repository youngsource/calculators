<?php
declare(strict_types=1);

namespace Laudis\Common\Contracts;

use Psr\Container\ContainerInterface;

/**
 * Interface MutableContainerInterface
 * @package Laudis\Calculators\Contracts
 */
interface MutableContainerInterface extends ContainerInterface
{
    /**
     * @param string $identifier
     * @param $value
     */
    public function set(string $identifier, $value): void;

    /**
     * @param string $identifier
     * @return bool
     */
    public function has($identifier): bool;

}

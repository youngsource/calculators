<?php
declare(strict_types=1);

namespace Laudis\Common\Contracts;

use Iterator;

/**
 * Interface CsvReaderInterface
 * @package Laudis\Calculators\Contracts
 */
interface CsvReaderInterface
{
    /**
     * @return Iterator
     */
    public function getRecords(array $header = null): Iterator;
}

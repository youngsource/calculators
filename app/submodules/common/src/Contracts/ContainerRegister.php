<?php
declare(strict_types=1);

namespace Laudis\Common\Contracts;

/**
 * Interface ContainerRegister
 * @package Laudis\Calculators\Contracts
 */
interface ContainerRegister
{
    /**
     * @param AppInterface $app
     */
    public function register(AppInterface $app): void;
}

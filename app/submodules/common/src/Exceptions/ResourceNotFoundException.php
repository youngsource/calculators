<?php
declare(strict_types=1);

namespace Laudis\Common\Exceptions;

use RuntimeException;

/**
 * Class ResourceNotFoundException
 * @package Laudis\Calculators\Exceptions
 */
class ResourceNotFoundException extends RuntimeException
{

}

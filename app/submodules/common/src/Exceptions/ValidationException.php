<?php
declare(strict_types=1);

namespace Laudis\Common\Exceptions;

use Rakit\Validation\Validation;
use RuntimeException;

/**
 * Class ValidationException
 * Encapsulates the behaviour of a validation failing in the current state of the application.
 *
 * @package Laudis\Calculators\Exceptions
 */
final class ValidationException extends RuntimeException
{
    /** @var Validation|null */
    private $validation;

    /**
     * ValidationException constructor.
     * @param Validation $validation
     */
    public function __construct(?Validation $validation, string $message = 'De opgegeven data is niet correct')
    {
        parent::__construct($message);
        $this->validation = $validation;
    }

    /**
     * @return Validation
     */
    public function getValidation(): ?Validation
    {
        return $this->validation;
    }
}

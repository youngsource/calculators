<?php
declare(strict_types=1);

namespace Laudis\Common\Auxiliaries;

use Iterator;
use Laudis\Common\Contracts\CsvReaderInterface;
use League\Csv\Exception;
use League\Csv\Reader;

/**
 * Class CsvReader
 * @package Laudis\Calculators\Auxiliaries
 */
final class CsvReader implements CsvReaderInterface
{
    /** @var Reader */
    private $reader;

    /**
     * CsvReader constructor.
     *
     * @param string $path
     * @param string|null $openMode
     * @param resource|null $context
     * @throws Exception
     * @throws Exception
     */
    public function __construct(string $path, ?string $openMode = null, $context = null)
    {
        $this->reader = Reader::createFromPath($path, $openMode ?? 'r', $context);
    }

    /**
     * @param array $header
     * @return Iterator
     * @throws Exception
     * @throws Exception
     */
    public function getRecords(array $header = null): Iterator
    {
        return $this->reader->getRecords($header ?? []);
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Common\Rules;

use Rakit\Validation\Rule;
use function is_bool;

/**
 * Class BooleanRule
 * @package Laudis\Calculators\Rules
 */
final class BooleanRule extends Rule
{
    public function __construct()
    {
        $this->setMessage(':attribute moet een booleaanse entiteit zijn.');
    }

    /**
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        return $value === 'true' || $value === 'false' || is_bool($value);
    }
}

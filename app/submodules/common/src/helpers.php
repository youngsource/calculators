<?php
declare(strict_types=1);

use Dotenv\Dotenv;
use Laudis\Common\Auxiliaries\Application;
use Laudis\Common\Contracts\AppInterface;
use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;

if (!function_exists('lazy_get')) {
    /**
     * Gets the item which might be encapsulated in a callable for lazy evaluation.
     *
     * @param mixed $object
     * @param mixed ...$params
     * @return object|mixed
     */
    function lazy_get($object, ...$params): object
    {
        return is_callable($object) ? $object(...$params) : $object;
    }
}

if (!function_exists('array_last')) {
    /**
     * @param array $array
     * @return mixed
     */
    function array_last(array $array)
    {
        return $array[array_key_last($array)];
    }
}

if (!function_exists('array_first')) {
    /**
     * @param array $array
     * @return mixed
     */
    function array_first(array $array)
    {
        return $array[array_key_first($array)];
    }
}

if (!function_exists('object_to_array')) {
    /**
     * @param object $object
     * @return array
     * @throws ReflectionException
     */
    function object_to_array(object $object): array
    {
        $reflection = new ReflectionClass($object);

        $tbr = [];
        foreach ($reflection->getProperties() as $property) {
            $property->setAccessible(true);
            $tbr[$property->getName()] = $property->getValue($object);
        }
        return $tbr;
    }
}

if (!function_exists('run_app')) {
    /**
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     */
    function run_app(): void
    {
        boot_app()->run();
    }
}

if (!function_exists('date_to_string')) {
    /**
     * Returns the string representation of a datetime or sets it to null in query friendly format.
     *
     * @param DateTimeInterface|null $dateTime
     * @return string
     */
    function date_to_string_query(?DateTimeInterface $dateTime): string
    {
        return $dateTime === null ? 'NULL' : "'" . $dateTime->format('Y-m-d') . "'";
    }
}

if (!function_exists('boot_app')) {
    /**
     * @return AppInterface
     */
    function boot_app(): AppInterface
    {
        static $app;
        if ($app === null) {
            $app = new Application();
            Laudis\Calculators\Registers\MainRegister::make()->register($app);
        }
        return $app;
    }
}

if (!function_exists('app')) {
    /**
     * @return AppInterface
     */
    function app(): AppInterface
    {
        static $app;
        if ($app === null) {
            $app = boot_app();
        }
        return $app;
    }
}

if (!function_exists('uuid')) {
    /**
     * @return string
     * @throws Exception
     */
    function uuid(): string
    {
        return bin2hex(random_bytes(32));
    }
}

if (!function_exists('max_date')) {
    /**
     * @return DateTimeInterface
     * @throws Exception
     */
    function max_date(): DateTimeInterface
    {
        /** We need to do this instead of using PHP_INT_MAX because the bolt driver does not work correctly and compares
         * against this expression as seen in the source code:
         * @see \Graphaware\Bolt\Packstream\Packer::packInteger
         */
        /** @noinspection UnnecessaryCastingInspection */
        return DateTime::createFromFormat('Y-m-d', '9999-01-01');
    }
}
if (!function_exists('min_date')) {
    /**
     * @return DateTimeInterface
     * @throws Exception
     */
    function min_date(): DateTimeInterface
    {
        /** We need to do this instead of using PHP_INT_MIN because the bolt driver does not work correctly and compares
         * against this expression as seen in the source code:
         * @see \Graphaware\Bolt\Packstream\Packer::packInteger
         */
        return DateTime::createFromFormat('Y-m-d', '1-01-01');
    }
}
if (!function_exists('simple_date')) {
    /**
     * @return DateTimeInterface
     * @throws Exception
     */
    function simple_date(int $year, int $month, int $day): DateTimeInterface
    {
        return DateTime::createFromFormat('Y-m-d', "$year-$month-$day");
    }
}

if (!function_exists('to_float')) {
    /**
     * Casts the value to a float.
     * @param $value
     * @return float
     */
    function to_float($value): float
    {
        return (float)$value;
    }
}

if (!function_exists('to_int')) {
    /**
     * Casts the value to an int.
     * @param $value
     * @return int
     */
    function to_int($value): int
    {
        return (int)$value;
    }
}

if (!function_exists('to_bool')) {
    /**
     * Asserts the value whether it represents a true value, false otherwise.
     * @param $value
     * @return bool
     */
    function to_bool($value): bool
    {
        return $value === 'true' || $value === true;
    }
}

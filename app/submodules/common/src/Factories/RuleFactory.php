<?php /** @noinspection PhpStrictTypeCheckingInspection */
/** @noinspection PhpDocSignatureInspection */
declare(strict_types=1);

namespace Laudis\Common\Factories;

use Laudis\Common\Rules\BooleanRule;
use Laudis\Common\Rules\EmptyIfRule;
use Laudis\Common\Rules\IfValueIsRule;
use Rakit\Validation\Rules\Integer;

/**
 * Class RuleFactory
 * @package Laudis\Calculators\Factories
 */
final class RuleFactory
{
    /**
     * @param string $otherValue
     * @param $value
     * @param $operand
     * @return IfValueIsRule
     */
    public function ifValueIs(string $otherValue, $value, $operand): IfValueIsRule
    {
        return new IfValueIsRule($otherValue, $value, $operand);
    }

    /**
     * @return BooleanRule
     */
    public function boolean(): BooleanRule
    {
        return new BooleanRule;
    }

    /**
     * @param string $otherValue
     * @return EmptyIfRule
     */
    public function emptyIfRule(string $otherValue): EmptyIfRule
    {
        return new EmptyIfRule($otherValue);
    }

    /**
     * @return Integer
     */
    public function integer(): Integer
    {
        return new Integer;
    }
}

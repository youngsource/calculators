<?php
declare(strict_types=1);

namespace Laudis\Common\Factories;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\StreamInterface as Stream;
use Slim\Http\Body;
use Slim\Http\Headers as SlimHeaders;
use Slim\Http\Response as SlimResponse;
use Slim\Interfaces\Http\HeadersInterface as Headers;

/**
 * Class ResponseFactory
 * @package Laudis\Calculators\Factories
 */
final class ResponseFactory
{
    /** @var int */
    private $stdStatus;
    /** @var Headers */
    private $stdHeaders;
    /** @var Stream */
    private $stdBody;

    /**
     * ResponseFactory constructor.
     * @param int|null $stdStatus
     * @param Headers|null $stdHeaders
     * @param Stream|null $stdBody
     */
    public function __construct(?int $stdStatus = null, ?Headers $stdHeaders = null, ?Stream $stdBody = null)
    {
        $this->stdStatus = $stdStatus ?? 200;
        $this->stdHeaders = $stdHeaders ?? new SlimHeaders([
            'Content-Type' => 'application/json;charset=utf-8'
        ]);
        $this->stdBody = $stdBody ?? new Body(fopen('php://temp', 'rb+'));
    }

    /**
     * @param int|null $status
     * @param Headers|null $headers
     * @param Stream|null $body
     * @return Response
     */
    public function make(?int $status = null, ?Headers $headers = null, ?Stream $body = null): Response
    {
        return new SlimResponse(
            $status ?? $this->stdStatus,
            $this->mergeHeaders($headers),
            $body ?? $this->stdBody
        );
    }

    /**
     * @param Headers|null $headers
     * @return Headers
     */
    private function mergeHeaders(?Headers $headers): Headers
    {
        if ($headers === null) {
            return $this->stdHeaders;
        }
        /** @noinspection ForeachSourceInspection */
        foreach ($this->stdHeaders as $name => $value) {
            if (!$headers->has($name)) {
                $headers->add($name, $value);
            }
        }
        return $headers;
    }
}

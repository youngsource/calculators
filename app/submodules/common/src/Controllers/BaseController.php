<?php
declare(strict_types=1);

namespace Laudis\Common\Controllers;

use Laudis\Common\Contracts\ResponseWriterInterface;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class BaseController
 * Common controller behaviour able to write to the response.
 *
 * @package Laudis\Calculators\Controllers
 */
abstract class BaseController
{
    /** @var ResponseWriterInterface */
    private $responseWriter;

    /**
     * BaseController constructor.
     *
     * @param ResponseWriterInterface $responseWriter
     */
    public function __construct(ResponseWriterInterface $responseWriter)
    {
        $this->responseWriter = $responseWriter;
    }

    /**
     * Writes the given data to the given response and overrides the status code if provided.
     *
     * @param Response $response
     * @param mixed $data
     * @param int|null $statusCode
     * @return Response
     */
    final protected function writeToResponse(Response $response, $data, ?int $statusCode = null): Response
    {
        return $this->responseWriter->write($response, $data, $statusCode);
    }
}

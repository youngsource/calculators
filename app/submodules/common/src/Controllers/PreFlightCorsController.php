<?php
/** @noinspection PhpUnusedParameterInspection */
declare(strict_types=1);

namespace Laudis\Common\Controllers;

use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class PreflightCorsController
 * Handles all preflight requests.
 *
 * @package Laudis\Calculators\Controllers
 */
final class PreFlightCorsController extends BaseController
{
    /**
     * Allow all option requests in the case of a preflight.
     *
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws InvalidArgumentException
     */
    public function allowOptions(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $this->writeToResponse($response, [
            'message' => 'Option OK.'
        ]);
    }

    /**
     * Falls back to catch all other routes so they don't trigger a 405 status.
     *
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws InvalidArgumentException
     */
    public function fallback(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $this->writeToResponse($response, [
            'message' => 'Resource not found.'
        ], 404);
    }
}

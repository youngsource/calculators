<?php
declare(strict_types=1);

if (!function_exists('settings')) {
    /**
     * @return array
     */
    function settings(array $override = []): array
    {
        return array_merge([
            'displayErrorDetails' => getenv('APP_DEBUG'),
            'debug' => getenv('APP_DEBUG'),
            'locale' => getenv('APP_LOCALE'),
            'addContentLengthHeader' => true,
            'httpVersion' => getenv('HTTP_VERSION'),
            'responseChunkSize' => 4096,
            'outputBuffering' => 'append',
            'determineRouteBeforeAppMiddleware' => true,
            'routerCacheFile' => false,

            'logger' => [
                'name' => 'calculators',
                'level' => Monolog\Logger::INFO,
                'path' => __DIR__ . '/../logs/app.log'
            ],
            'casbin' => [
                'type' => 'mysql',
                'hostname' => getenv('DB_HOST'),
                'database' => getenv('DB_NAME'),
                'username' => getenv('DB_USERNAME'),
                'password' => getenv('DB_PASSWORD'),
                'hostport' => getenv('DB_PORT')
            ]
        ], $override);
    }
}


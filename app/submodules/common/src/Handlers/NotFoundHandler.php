<?php
/** @noinspection PhpUndefinedClassInspection */
declare(strict_types=1);

namespace Laudis\Common\Handlers;

use InvalidArgumentException;
use Laudis\Common\Exceptions\ResourceNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Class CalculatorNotFoundHandler
 * Handles the exception in the case a calculator is not found.
 *
 * @package Laudis\Calculators\Handlers
 */
final class NotFoundHandler extends BaseHandler
{
    /**
     * Handles the CalculatorNotFound exception.
     *
     * @param ResponseInterface $response
     * @param Throwable $exception
     * @return ResponseInterface|null
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function handleException(ResponseInterface $response, Throwable $exception): ?ResponseInterface
    {
        if ($exception instanceof ResourceNotFoundException) {
            return $this->writeJson($response, [
                'message' => $exception->getMessage()
            ], 404);
        }
        return null;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\Common\Strategies;

use InvalidArgumentException;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class WriteJsonToResponse
 * Implements the response writer by writing json to the response.
 *
 * @package Laudis\Calculators\Strategies
 */
final class WriteJsonToResponse implements ResponseWriterInterface
{
    /**
     * @param ResponseInterface $response
     * @param mixed $data
     * @param int|null $statusCode
     * @return ResponseInterface
     * @throws InvalidArgumentException
     * @throws InvalidArgumentException
     */
    public function write(ResponseInterface $response, $data, ?int $statusCode = null): ResponseInterface
    {
        $response->getBody()->write(json_encode($data, JSON_THROW_ON_ERROR));
        if ($statusCode !== null) {
            return $response->withStatus($statusCode);
        }
        return $response;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use Casbin\Enforcer;
use Casbin\Persist\Adapter as AdapterContract;
use CasbinAdapter\Database\Adapter;
use Jose\Component\Checker\AlgorithmChecker;
use Jose\Component\Checker\HeaderCheckerManager;
use Jose\Component\Core\AlgorithmManager;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\Algorithm\HS256;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\JWSTokenSupport;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\CompactSerializer;
use Jose\Component\Signature\Serializer\JWSSerializer;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use Laudis\Common\Contracts\MutableContainerInterface;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\Common\Rules\ExistsRule;
use Laudis\Common\Rules\PasswordRule;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Controllers\AuthTokenIssueController;
use Laudis\UserManagement\Controllers\ChangePasswordWithLinkController;
use Laudis\UserManagement\Controllers\ResetPasswordLinkController;
use Laudis\UserManagement\Controllers\UpdatePasswordController;
use Laudis\UserManagement\Validation\ResetPasswordRule;
use Mailgun\HttpClientConfigurator;
use Mailgun\Mailgun;
use PDO;
use Psr\Container\ContainerInterface;
use Rakit\Validation\Rules\Required;
use Rakit\Validation\Validator;
use function getenv;

/**
 * Class UserManagerRegister
 * @package Laudis\Calculators\UserManagement
 */
final class UserManagerRegister
{
    /**
     * @param MutableContainerInterface $container
     */
    public function register(MutableContainerInterface $container): void
    {
        $this->registerUserRespository($container);
        $this->registerPasswordManager($container);
        $this->registerAlgorithmManager($container);
        $this->registerAuthTokenIssueController($container);
        $this->registerPossibleUserMiddleware($container);
        $this->registerServerToken($container);
        $this->registerUserManager($container);
        $this->registerAuthenticateUser($container);
        $this->registerAuthorization($container);
        $this->registerTokenBuilder($container);
        $this->registerJWSBuilder($container);
        $this->registerJWSVerfifier($container);
        $this->registerJWSSerializer($container);
        $this->registerResetPasswordRule($container);
        $this->registerChangePasswordWithLinkController($container);
        $this->registerResetPasswordLinkController($container);
        $this->registerMailgun($container);
        $this->registerPasswordRule($container);
        $this->registerUpdatePasswordController($container);
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerUserRespository(MutableContainerInterface $container): void
    {
        $container->set(UserRepositoryInterface::class, static function (ContainerInterface $container) {
            return new UserRepository(
                $container->get(PDO::class),
                new UserFactory(),
                $container->get(TokenBuilder::class)
            );
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerPasswordManager(MutableContainerInterface $container): void
    {
        $container->set(PasswordHasherInterface::class, new PasswordHasher());
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerAlgorithmManager(MutableContainerInterface $container): void
    {
        $container->set(AlgorithmManager::class, static function () {
            return AlgorithmManager::create([new HS256]);
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerAuthTokenIssueController(MutableContainerInterface $container): void
    {
        $container->set(AuthTokenIssueController::class, function (ContainerInterface $container) {
            $existsRule = new ExistsRule($container->get(PDO::class), 'users', 'email',
                'Het mailadres werd niet gevonden in onze databank.');
            $existsRule->setMessage('Het mailadres is niet geregistreerd bij ons.');
            $password = $container->get(PasswordRule::class);
            return new AuthTokenIssueController(
                $container->get(ResponseWriterInterface::class),
                new IssueTokenStrategy(
                    $container->get(PasswordHasherInterface::class),
                    $container->get(UserRepositoryInterface::class),
                    $this->validator()->make([], [
                        'email' => [new Required, $existsRule],
                        'password' => [new Required, $password]
                    ]),
                    $container->get(JWSBuilder::class),
                    $container->get('server_token')
                ),
                $container->get(JWSSerializer::class)
            );
        });
    }

    /**
     * @return Validator
     */
    private function validator(): Validator
    {
        return new Validator;
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerPossibleUserMiddleware(MutableContainerInterface $container): void
    {
        $container->set(AddPossibleUserMiddleware::class, static function (ContainerInterface $container) {
            $algorithms = $container->get(AlgorithmManager::class);
            return new AddPossibleUserMiddleware(
                HeaderCheckerManager::create([new AlgorithmChecker($algorithms->list())], [new JWSTokenSupport]),
                $container->get(UserManager::class),
                $container->get(JWSVerifier::class),
                JWSSerializerManager::create([new CompactSerializer]),
                $container->get('server_token')
            );
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerServerToken(MutableContainerInterface $container): void
    {
        $container->set('server_token', static function (): JWK {
            return new JWK([
                'kty' => 'oct',
                'k' => getenv('APP_KEY')
            ]);
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerUserManager(MutableContainerInterface $container): void
    {
        $container->set(UserManager::class, static function (ContainerInterface $container): UserManager {
            return new UserManager($container->get(UserRepositoryInterface::class));
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerAuthenticateUser(MutableContainerInterface $container): void
    {
        $container->set(AuthenticateMiddleware::class, static function (ContainerInterface $container) {
            return new AuthenticateMiddleware(
                $container->get(UserManager::class),
                );
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerAuthorization(MutableContainerInterface $container): void
    {
        $container->set(AdapterContract::class, static function () {
            return Adapter::newAdapter(settings()['casbin']);
        });

        $container->set(Enforcer::class, static function (ContainerInterface $container) {
            return new Enforcer(__DIR__ . '/model.conf', $container->get(AdapterContract::class));
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerTokenBuilder(MutableContainerInterface $container): void
    {
        $container->set(TokenBuilder::class, static function (ContainerInterface $container): TokenBuilder {
            return new TokenBuilder(
                $container->get('server_token'),
                $container->get(JWSBuilder::class),
                $container->get(JWSSerializer::class)
            );
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerJWSBuilder(MutableContainerInterface $container): void
    {
        $container->set(JWSBuilder::class, static function (ContainerInterface $container): JWSBuilder {
            return new JWSBuilder(null, $container->get(AlgorithmManager::class));
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerJWSVerfifier(MutableContainerInterface $container): void
    {
        $container->set(JWSVerifier::class, static function (ContainerInterface $container): JWSVerifier {
            return new JWSVerifier($container->get(AlgorithmManager::class));
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerJWSSerializer(MutableContainerInterface $container): void
    {
        $container->set(JWSSerializer::class, static function (): JWSSerializer {
            return new CompactSerializer;
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerResetPasswordRule(MutableContainerInterface $container): void
    {
        $container->set(ResetPasswordRule::class, static function (ContainerInterface $container): ResetPasswordRule {
            return new ResetPasswordRule(
                $container->get(JWSSerializer::class),
                $container->get(JWSVerifier::class),
                $container->get('server_token'),
                $container->get(PDO::class)
            );
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerChangePasswordWithLinkController(MutableContainerInterface $container): void
    {
        $container->set(ChangePasswordWithLinkController::class,
            function (ContainerInterface $container): ChangePasswordWithLinkController {
                $validation = $this->validator()->make([], [
                    'password' => 'required|min:5',
                    'passwordConfirmed' => 'required|same:password',
                    'resetToken' => [
                        'required',
                        $container->get(ResetPasswordRule::class)
                    ]
                ]);

                return new ChangePasswordWithLinkController(
                    $validation,
                    $container->get(PasswordHasherInterface::class),
                    $container->get(UserRepositoryInterface::class),
                    $container->get(ResponseWriterInterface::class)
                );
            });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerResetPasswordLinkController(MutableContainerInterface $container): void
    {
        $container->set(ResetPasswordLinkController::class, function (ContainerInterface $container) {
            $message = 'Het mailadres werd niet gevonden in onze databank.';
            $validation = $this->validator()->make([], [
                'email' => [new ExistsRule($container->get(PDO::class), 'users', 'email', $message), new Required]
            ]);

            return new ResetPasswordLinkController(
                $container->get(ResponseWriterInterface::class),
                $container->get(Mailgun::class),
                $container->get(UserRepositoryInterface::class),
                $validation
            );
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerMailgun(MutableContainerInterface $container): void
    {
        $container->set(Mailgun::class, static function (): Mailgun {
            if (getenv('APP_DEBUG') === 'true') {
                $configurator = new HttpClientConfigurator;
                $configurator->setEndpoint(getenv('MAIL_POSTBIN_URL'));
                $configurator->setDebug(true);
                return Mailgun::configure($configurator, new ModelHydrator);
            }
            return Mailgun::create(getenv('MAILGUN_API_KEY'));
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerPasswordRule(MutableContainerInterface $container): void
    {
        $container->set(PasswordRule::class, static function (ContainerInterface $container): PasswordRule {
            return new PasswordRule(
                $container->get(PasswordHasherInterface::class),
                $container->get(PDO::class),
                $container->get(UserManager::class)
            );
        });
    }

    /**
     * @param MutableContainerInterface $container
     */
    private function registerUpdatePasswordController(MutableContainerInterface $container): void
    {
        $container->set(UpdatePasswordController::class,
            function (ContainerInterface $container): UpdatePasswordController {
                return new UpdatePasswordController(
                    $container->get(UserRepositoryInterface::class),
                    $this->validator()->validate([], [
                        'password' => 'required|min:5',
                        'passwordConfirmed' => 'required|same:password',
                        'passwordOld' => [new Required, $container->get(PasswordRule::class)]
                    ]),
                    $container->get(ResponseWriterInterface::class),
                    $container->get(PasswordHasherInterface::class),
                    $container->get(UserManager::class)
                );
            });
    }
}

<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use Exception;
use Jose\Component\Core\JWK;
use Jose\Component\Core\Util\JsonConverter;
use Jose\Component\Signature\JWS;
use Jose\Component\Signature\JWSBuilder;
use Laudis\Common\Exceptions\ValidationException;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Exceptions\AuthenticationException;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\Validation;
use function getenv;

/**
 * Class IssueTokenStrategy
 * @package Laudis\Calculators
 */
final class IssueTokenStrategy
{
    /** @var PasswordHasherInterface */
    private $hasher;
    /** @var UserRepositoryInterface */
    private $userRepository;
    /** @var Validation */
    private $validator;
    /** @var JWSBuilder */
    private $builder;
    /** @var JWK */
    private $privateKey;

    /**
     * IssueTokenStrategy constructor.
     * @param PasswordHasherInterface $hasher
     * @param UserRepositoryInterface $userRepository
     * @param Validation $validator
     * @param JWSBuilder $builder
     * @param JWK $privateKey
     */
    public function __construct(
        PasswordHasherInterface $hasher,
        UserRepositoryInterface $userRepository,
        Validation $validator,
        JWSBuilder $builder,
        JWK $privateKey
    ) {
        $this->hasher = $hasher;
        $this->userRepository = $userRepository;
        $this->validator = $validator;
        $this->builder = $builder;
        $this->privateKey = $privateKey;
    }

    /**
     * @param ServerRequestInterface $request
     * @return JWS
     * @throws Exception
     */
    public function issueToken(ServerRequestInterface $request): JWS
    {
        $body = $request->getParsedBody();
        $this->validate($body);
        ['email' => $email, 'password' => $password] = $body;

        $user = $this->authenticateUser($email, $password);
        $payload = $this->makePayload($user);

        return $this->builder->create()
            ->withPayload($payload)
            ->addSignature($this->privateKey, ['alg' => 'HS256'])
            ->build();
    }

    /**
     * @param $body
     */
    private function validate($body): void
    {
        $this->validator->validate((array)$body);
        if ($this->validator->fails()) {
            throw new ValidationException($this->validator);
        }
    }

    /**
     * @param $email
     * @param $password
     * @return User
     */
    private function authenticateUser($email, $password): User
    {
        $user = $this->userRepository->findUser($email);
        if ($user === null || !$this->hasher->check($password, $user->getPasswordHash())) {
            throw new AuthenticationException("Could not authenticate user with email: $email");
        }
        return $user;
    }

    /**
     * @param User $user
     * @return string
     */
    private function makePayload(User $user): string
    {
        return JsonConverter::encode([
            'iat' => time(),
            'nbf' => time(),
            'exp' => time() + 3600,
            'iss' => getenv('APP_NAME'),
            'aud' => getenv('APP_AUD'),
            'user_id' => $user->getId()
        ]);
    }
}

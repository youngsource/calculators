<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use function array_first;
use function array_map;
use Casbin\Enforcer;
use Casbin\Exceptions\CasbinException;
use function getenv;
use Laudis\UserManagement\Exceptions\AuthenticationException;
use Laudis\UserManagement\Exceptions\UnauthorizedException;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Slim\Route;

/**
 * Class AuthorizationMiddleware
 * @package Laudis\UserManagement
 */
final class AuthorizationMiddleware implements MiddlewareInterface
{
    /**
     * @var string
     */
    private $object;
    /**
     * @var string
     */
    private $act;
    /**
     * @var Enforcer
     */
    private $enforcer;
    /**
     * @var UserManager
     */
    private $manager;

    /**
     * AuthorizationMiddleware constructor.
     * @param string $object
     * @param string $act
     */
    public function __construct(Enforcer $enforcer, UserManager $manager, ?string $object, string $act)
    {
        $this->object = $object;
        $this->act = $act;
        $this->enforcer = $enforcer;
        $this->manager = $manager;
    }

    /**
     * @param string $object
     * @param string $act
     * @return callable
     */
    public static function make(string $object, string $act): callable
    {
        return function (ContainerInterface $container) use ($object, $act) {
            return new AuthorizationMiddleware(
                $container->get(Enforcer::class),
                $container->get(UserManager::class),
                $object,
                $act
            );
        };
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @throws CasbinException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if (getenv('APP_AUTH_ON') === 'false') {
            return $handler->handle($request);
        }

        /** @var Route $attribute */
        if (getenv('APP_AUTH_ON') === 'false') {
            return $handler->handle($request);
        }
        $attribute = $request->getAttribute('route');
        $object = $this->object ?? $attribute->getArguments()['calculator'];
        $loggedInUserRoles = $this->manager->getLoggedInUserRoles();
        $roles = array_map(static function (Role $role) {
            return $role->getName();
        }, $loggedInUserRoles);

        if ($this->enforcer->enforce(array_first($roles), $this->act, $object) !== true) {
            $user = $this->manager->getLoggedInUser();
            if ($user === null) {
                throw new AuthenticationException('Unauthenticated user.');
            }
            throw new UnauthorizedException(
                'User: ' . $user->getEmail() . ' has no permission to ' . $this->act . ' ' . $object
            );
        }
        return $handler->handle($request);
    }
}

<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use Exception;
use InvalidArgumentException;
use Jose\Component\Checker\HeaderCheckerManager;
use Jose\Component\Checker\InvalidHeaderException;
use Jose\Component\Checker\MissingMandatoryHeaderParameterException;
use Jose\Component\Core\JWK;
use Jose\Component\Core\Util\JsonConverter;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\JWSSerializerManager;
use Laudis\UserManagement\Exceptions\AuthenticationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use function env;

/**
 * Class AddPossibleUserMiddleware
 * @package Laudis\UserManagement
 */
final class AddPossibleUserMiddleware implements MiddlewareInterface
{
    /** @var HeaderCheckerManager */
    private $checker;
    /** @var UserManager */
    private $manager;
    /** @var JWSVerifier */
    private $verifier;
    /** @var JWSSerializerManager */
    private $serializer;
    /** @var JWK */
    private $serverToken;

    /**
     * CheckJwtToken constructor.
     * @param HeaderCheckerManager $checker
     * @param UserManager $manager
     * @param JWSVerifier $verifier
     * @param JWSSerializerManager $serializer
     * @param JWK $serverToken
     */
    public function __construct(
        HeaderCheckerManager $checker,
        UserManager $manager,
        JWSVerifier $verifier,
        JWSSerializerManager $serializer,
        JWK $serverToken
    ) {
        $this->checker = $checker;
        $this->manager = $manager;
        $this->verifier = $verifier;
        $this->serializer = $serializer;
        $this->serverToken = $serverToken;
    }

    /**
     * @param string $token
     * @throws InvalidHeaderException
     * @throws MissingMandatoryHeaderParameterException
     * @throws Exception
     */
    private function login(string $token): void
    {
        if ($token === 'null' || $token === 'undefined' || env('APP_AUTH_ON') !== 'true') {
            return;
        }
        try {
            $authorizationToken = $this->serializer->unserialize($token);
        } catch (InvalidArgumentException $exception) {
            throw new AuthenticationException('Could not verify authorization token', 0, $exception);
        }
        $this->checker->check($authorizationToken, 0);
        if (!$this->verifier->verifyWithKey($authorizationToken, $this->serverToken, 0)) {
            throw new AuthenticationException('Could not verify authorization token');
        }
        $this->manager->login(JsonConverter::decode($authorizationToken->getPayload())['user_id']);
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     * @param ServerRequestInterface $request
     * @param RequestHandlerInterface $handler
     * @return ResponseInterface
     * @throws InvalidHeaderException
     * @throws MissingMandatoryHeaderParameterException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $token = $request->getHeaderLine('Authorization');
        if ($token !== '') {
            $this->login($token);
        }
        return $handler->handle($request);
    }
}

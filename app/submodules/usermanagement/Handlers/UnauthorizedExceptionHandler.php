<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Handlers;

use Laudis\Common\Contracts\ExceptionHandlerInterface;
use Laudis\UserManagement\Exceptions\UnauthorizedException;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Class UnauthorizedExceptionHandler
 * @package Laudis\Calculators
 */
final class UnauthorizedExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * Handles the exception by returning the appropriate response if the exception applies to the handler.
     *
     * @param ResponseInterface $response
     * @param Throwable $exception
     * @return ResponseInterface|null
     */
    public function handleException(ResponseInterface $response, Throwable $exception): ?ResponseInterface
    {
        if ($exception instanceof UnauthorizedException) {
            return $response->withStatus(403, $exception->getMessage());
        }
        return null;
    }
}

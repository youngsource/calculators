<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Handlers;

use Laudis\Common\Contracts\ExceptionHandlerInterface;
use Laudis\UserManagement\Exceptions\AuthenticationException;
use Psr\Http\Message\ResponseInterface;
use Throwable;

/**
 * Class UnauthenticatedExceptionHandler
 * @package Laudis\Calculators\UserManagement
 */
final class UnauthenticatedExceptionHandler implements ExceptionHandlerInterface
{
    /**
     * Handles the exception by returning the appropriate response if the exception applies to the handler.
     *
     * @param ResponseInterface $response
     * @param Throwable $exception
     * @return ResponseInterface|null
     */
    public function handleException(ResponseInterface $response, Throwable $exception): ?ResponseInterface
    {
        if ($exception instanceof AuthenticationException) {
            return $response->withStatus(401, $exception->getMessage());
        }
        return null;
    }
}

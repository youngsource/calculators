<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\Exceptions\AuthenticationException;

/**
 * Class UserManager
 * @package Laudis\Calculators\UserManagement
 */
final class UserManager
{
    /** @var UserRepositoryInterface */
    private $userRepository;
    /** @var User|null */
    private $user;

    /**
     * UserManager constructor.
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @return User|null
     */
    public function getLoggedInUser(): ?User
    {
        return $this->user;
    }

    /**
     * @return array
     */
    public function getLoggedInUserRoles(): array
    {
        if ($this->user === null) {
            return [];
        }
        return $this->userRepository->getRoles($this->user);
    }

    /**
     * @param int $id
     * @throws AuthenticationException
     * @internal
     */
    public function login(int $id): void
    {
        $user = $this->userRepository->getUserById($id);
        if ($user === null) {
            throw new AuthenticationException('Could not login user with id: ' . $id);
        }
        $this->user = $user;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use DateTime;
use Exception;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use PDO;
use PDOException;
use function array_map;

/**
 * Class UserRepository
 * @package Laudis\Calculators
 */
final class UserRepository implements UserRepositoryInterface
{
    /** @var PDO */
    private $pdo;
    /** @var UserFactory */
    private $userFactory;
    /**
     * @var TokenBuilder
     */
    private $builder;

    /**
     * UserRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo, UserFactory $userFactory, TokenBuilder $builder)
    {
        $this->pdo = $pdo;
        $this->userFactory = $userFactory;
        $this->builder = $builder;
    }

    /**
     * @param string $email
     * @param string $username
     * @param string $passwordHash
     *
     * @throws PDOException
     */
    public function insertUser(string $email, string $username, string $passwordHash): void
    {
        $this->pdo->exec("
            INSERT INTO users (email, username, password_hash) 
            VALUES (\"$email\", \"$username\", \"$passwordHash\")
        ");
    }

    /**
     * @param string $email
     * @return User|null
     *
     * @throws PDOException
     * @throws Exception
     */
    public function findUser(string $email): ?User
    {
        $result = $this->pdo->query("SELECT * FROM users WHERE email = \"$email\"")->fetch(PDO::FETCH_ASSOC);
        if ($result === false) {
            return null;
        }
        return $this->parseResult($result);
    }

    /**
     * @param array $result
     * @return User
     * @throws Exception
     */
    private function parseResult(array $result): User
    {
        return $this->userFactory->userFromAssociativeArray($result);
    }

    /**
     * @param User $user
     *
     */
    public function updateUser(User $user): void
    {
        $email = $user->getEmail();
        $name = $user->getUserName();
        $hash = $user->getPasswordHash();
        $id = $user->getId();
        $this->pdo->exec("
            UPDATE users 
            SET email = \"$email\", username = \"$name\", password_hash = \"$hash\", updated_at = current_timestamp() 
            WHERE id = $id
        ");
    }

    /**
     * @return User[]
     */
    public function getAllUsers(): array
    {
        $result = $this->pdo->query('SELECT * FROM users')->fetchAll(PDO::FETCH_ASSOC);
        return array_map(function (array $values) {
            return $this->userFactory->userFromAssociativeArray($values);
        }, $result);
    }

    /**
     * @param User $user
     */
    public function deleteUser(User $user): void
    {
        $id = $user->getId();
        $this->pdo->exec("DELETE FROM users WHERE id = \"$id\"");
    }

    /**
     * @param int $id
     * @return User|null
     * @throws Exception
     */
    public function getUserById(int $id): ?User
    {
        $result = $this->pdo->query("SELECT * FROM users WHERE id = \"$id\"")->fetch(PDO::FETCH_ASSOC);
        if ($result === false) {
            return null;
        }
        return $this->parseResult($result);
    }

    /**
     * @param User $user
     * @return Role[]
     */
    public function getRoles(User $user): array
    {
        $result = $this->pdo->query('SELECT * FROM roles WHERE user_id = ' . $user->getId())->fetchAll(PDO::FETCH_ASSOC);
        return array_map(function (array $values) {
            return $this->userFactory->roleFromAssociativeArray($values);
        }, $result);
    }

    /**
     * @param string $email
     * @return string
     * @throws Exception
     */
    public function requestPasswordReset(string $email): string
    {
        $token = $this->builder->serializedSignedToken([
            'email' => $email,
            'exp' => (new DateTime('+1 day'))->getTimestamp(),
            'type' => 'reset'
        ]);

        $this->pdo->exec("INSERT INTO password_resets (token, user_id) VALUES ('$token', (
            SELECT id FROM users WHERE email = '$email'    
        ))");

        return $token;
    }

    /**
     * @param string $passwordHash
     * @param string $resetToken
     * @throws Exception
     */
    public function updatePassword(string $passwordHash, string $resetToken): void
    {
        $this->pdo->exec("
            UPDATE users SET `password_hash` = '$passwordHash' WHERE id IN (
                SELECT user_id FROM password_resets WHERE token = '$resetToken'
            );
            UPDATE password_resets SET `used` = 1 WHERE `token` = '$resetToken';
        ");
    }
}

<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Contracts;

use Exception;
use Laudis\UserManagement\Role;
use Laudis\UserManagement\User;
use PDOException;

/**
 * Interface UserRepository
 * @package Laudis\Calculators\Contracts
 */
interface UserRepositoryInterface
{
    /**
     * @param string $email
     * @param string $username
     * @param string $passwordHash
     *
     * @throws PDOException
     */
    public function insertUser(string $email, string $username, string $passwordHash): void;

    /**
     * @param string $email
     * @return User|null
     *
     * @throws PDOException
     */
    public function findUser(string $email): ?User;

    /**
     * @param int $id
     * @return User|null
     *
     * @throws PDOException
     */
    public function getUserById(int $id): ?User;

    /**
     * @param User $user
     */
    public function updateUser(User $user): void;

    /**
     * @param User $user
     *
     * @throws PDOException
     */
    public function deleteUser(User $user): void;

    /**
     * @param User $user
     * @return Role[]
     */
    public function getRoles(User $user): array;

    /**
     * @return User[]
     */
    public function getAllUsers(): array;

    /**
     * @param string $email
     * @return string
     */
    public function requestPasswordReset(string $email): string;

    /**
     * @param string $passwordHash
     * @param string $resetToken
     * @throws Exception
     */
    public function updatePassword(string $passwordHash, string $resetToken): void;
}

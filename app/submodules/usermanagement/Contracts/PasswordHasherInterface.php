<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Contracts;

/**
 * Interface PasswordHasherInterface
 * @package Laudis\Calculators\UserManagement\Contracts
 */
interface PasswordHasherInterface
{
    /**
     * Hashes the password.
     *
     * @param string $password
     * @return string
     */
    public function hash(string $password): string;

    /**
     * Checks the given password against the given hash.
     *
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public function check(string $password, string $hash): bool;
}

<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Controllers;

use Exception;
use Laudis\Common\Calculators\ValidatesRequest;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Mailgun\Mailgun;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\Validation;
use function getenv;
use function reset_password_email;

/**
 * \class ResetPasswordController
 * controller dealing with all requests about resetting a password
 *
 * @package App\Http\Controllers\Auth
 */
final class ResetPasswordLinkController
{
    use ValidatesRequest;

    /** @var ResponseWriterInterface */
    private $writer;
    /** @var Mailgun */
    private $mailgun;
    /** @var UserRepositoryInterface */
    private $repository;
    /** @var Validation */
    private $validation;

    /**
     * ResetPasswordController constructor.
     * @param ResponseWriterInterface $writer
     * @param Mailgun $mailgun
     * @param UserRepositoryInterface $repository
     * @param Validation $validation
     */
    public function __construct(
        ResponseWriterInterface $writer,
        Mailgun $mailgun,
        UserRepositoryInterface $repository,
        Validation $validation
    ) {
        $this->writer = $writer;
        $this->mailgun = $mailgun;
        $this->repository = $repository;
        $this->validation = $validation;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @throws Exception
     * @throws \Http\Client\Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $email = $this->validateRequest($request)['email'];
        $token = $this->repository->requestPasswordReset($email);

        $this->sendEmail($token, $email);

        return $this->writer->write($response, [
            'message' => 'De link werd succesvol verzonden.'
        ]);
    }

    /**
     * @param string $token
     * @param $email
     * @throws \Http\Client\Exception
     */
    private function sendEmail(string $token, $email): void
    {
        $link = getenv('APP_URL') . '/reset-password/' . $token;
        $html = reset_password_email($link);

        $this->mailgun->messages()->send(getenv('MAILGUN_DOMAIN'), [
            'from' => getenv('MAIL_SENDER'),
            'to' => $email,
            'subject' => 'Uw passwoord reset link van: ' . getenv('APP_NAME'),
            'html' => $html
        ]);
    }

    /** @noinspection LowerAccessLevelInspection */
    /**
     * @return Validation
     */
    protected function getValidation(): Validation
    {
        return $this->validation;
    }
}

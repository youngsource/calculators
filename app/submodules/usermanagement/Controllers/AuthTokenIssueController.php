<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Controllers;

use Exception;
use Jose\Component\Signature\Serializer\JWSSerializer;
use Laudis\Common\Contracts\ActionController;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\UserManagement\IssueTokenStrategy;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class AuthTokenIssueController
 * @package Laudis\Calculators
 */
final class AuthTokenIssueController implements ActionController
{
    /** @var ResponseWriterInterface */
    private $responseWriter;
    /** @var IssueTokenStrategy */
    private $tokenStrategy;
    /** @var JWSSerializer */
    private $serializer;

    /**
     * AuthTokenIssueController constructor.
     * @param ResponseWriterInterface $responseWriter
     * @param IssueTokenStrategy $tokenStrategy
     */
    public function __construct(
        ResponseWriterInterface $responseWriter,
        IssueTokenStrategy $tokenStrategy,
        JWSSerializer $serializer
    ) {
        $this->responseWriter = $responseWriter;
        $this->tokenStrategy = $tokenStrategy;
        $this->serializer = $serializer;
    }

    /**
     * Invokes the action of the controller.
     *
     * @param Request $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws Exception
     */
    public function __invoke(Request $request, ResponseInterface $response): ResponseInterface
    {
        return $this->responseWriter->write($response, [
            'token' => $this->serializer->serialize($this->tokenStrategy->issueToken($request), 0)
        ], 200);
    }
}

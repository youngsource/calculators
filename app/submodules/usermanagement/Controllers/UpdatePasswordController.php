<?php
/** @noinspection LowerAccessLevelInspection */
declare(strict_types=1);

namespace Laudis\UserManagement\Controllers;

use Laudis\Common\Calculators\ValidatesRequest;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Laudis\UserManagement\User;
use Laudis\UserManagement\UserManager;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\Validation;

/**
 * Class UpdateUserController
 * @package Laudis\UserManagement
 */
final class UpdatePasswordController
{
    use ValidatesRequest;

    /**
     * @var UserRepositoryInterface
     */
    private $repository;
    /**
     * @var Validation
     */
    private $validation;
    /**
     * @var ResponseWriterInterface
     */
    private $responseWriter;
    /**
     * @var PasswordHasherInterface
     */
    private $hasher;
    /**
     * @var UserManager
     */
    private $manager;

    /**
     * UpdateUserController constructor.
     * @param UserRepositoryInterface $repository
     * @param Validation $validation
     */
    public function __construct(
        UserRepositoryInterface $repository,
        Validation $validation,
        ResponseWriterInterface $responseWriter,
        PasswordHasherInterface $hasher,
        UserManager $manager
    ) {
        $this->repository = $repository;
        $this->validation = $validation;
        $this->responseWriter = $responseWriter;
        $this->hasher = $hasher;
        $this->manager = $manager;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $this->validateRequest($request);

        /** @var User $user */
        $user = $this->manager->getLoggedInUser();
        $user->setPasswordHash($this->hasher->hash($body['password']));
        $this->repository->updateUser($user);

        return $this->responseWriter->write($response, [
            'message' => 'Het wachtwoord werd succesvol aangepast'
        ]);
    }

    /**
     * @return Validation
     */
    protected function getValidation(): Validation
    {
        return $this->validation;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Controllers;

use Exception;
use Laudis\Common\Calculators\ValidatesRequest;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use Laudis\UserManagement\Contracts\UserRepositoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\Validation;

/**
 * Class ChangePasswordWithLinkController
 * @package Laudis\UserManagement\Controllers
 */
final class ChangePasswordWithLinkController
{
    use ValidatesRequest;

    /**
     * @var Validation
     */
    private $validation;
    /**
     * @var PasswordHasherInterface
     */
    private $hasher;
    /**
     * @var UserRepositoryInterface
     */
    private $repository;
    /**
     * @var ResponseWriterInterface
     */
    private $responseWriter;

    /**
     * ChangePasswordWithLinkController constructor.
     * @param Validation $validation
     */
    public function __construct(
        Validation $validation,
        PasswordHasherInterface $hasher,
        UserRepositoryInterface $repository,
        ResponseWriterInterface $responseWriter
    ) {
        $this->validation = $validation;
        $this->hasher = $hasher;
        $this->repository = $repository;
        $this->responseWriter = $responseWriter;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        ['password' => $password, 'resetToken' => $resetToken] = $this->validateRequest($request);
        $this->repository->updatePassword($this->hasher->hash($password), $resetToken);

        return $this->responseWriter->write($response, [
            'message' => 'Het wachtwoord werd succesvol geupdate.'
        ]);
    }

    /** @noinspection LowerAccessLevelInspection */
    /**
     * @return Validation
     */
    protected function getValidation(): Validation
    {
        return $this->validation;
    }
}

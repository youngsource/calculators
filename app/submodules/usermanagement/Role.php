<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

/**
 * Class Role
 * @package Laudis\UserManagement
 */
final class Role
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $userId;

    /**
     * Role constructor.
     * @param string $name
     * @param int $id
     * @param int $userId
     */
    public function __construct(string $name, int $id, int $userId)
    {
        $this->name = $name;
        $this->id = $id;
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }
}

<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use Laudis\UserManagement\Contracts\PasswordHasherInterface;
use function password_verify;
use const PASSWORD_ARGON2ID;

/**
 * Class PasswordHasher
 * @package Laudis\Calculators
 */
final class PasswordHasher implements PasswordHasherInterface
{
    /**
     * @param string $password
     * @return string
     */
    public function hash(string $password): string
    {
        return password_hash($password, PASSWORD_ARGON2ID);
    }

    /**
     * @param string $password
     * @param string $hash
     * @return bool
     */
    public function check(string $password, string $hash): bool
    {
        return password_verify($password, $hash);
    }
}

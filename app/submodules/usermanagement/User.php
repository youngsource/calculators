<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use DateTimeInterface;

/**
 * Class User
 * @package Laudis\Calculators
 */
final class User
{
    /** @var int $id */
    private $id;
    /** @var string */
    private $email;
    /** @var  DateTimeInterface */
    private $createdAt;
    /** @var DateTimeInterface|null */
    private $updatedAt;
    /** @var  string */
    private $userName;
    /** @var string */
    private $passwordHash;

    /**
     * User constructor.
     * @param int $id
     * @param string $email
     * @param DateTimeInterface $createdAt
     * @param DateTimeInterface|null $updatedAt
     * @param string $userName
     * @param string $passwordHash
     */
    public function __construct(
        int $id,
        string $email,
        DateTimeInterface $createdAt,
        ?DateTimeInterface $updatedAt,
        string $userName,
        string $passwordHash
    ) {
        $this->id = $id;
        $this->email = $email;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->userName = $userName;
        $this->passwordHash = $passwordHash;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @return string
     */
    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }

    /**
     * @param string $passwordHash
     */
    public function setPasswordHash(string $passwordHash): void
    {
        $this->passwordHash = $passwordHash;
    }
}

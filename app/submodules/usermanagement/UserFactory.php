<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use DateTime;
use Exception;

/**
 * Class UserFactory
 * @package Laudis\Calculators
 */
final class UserFactory
{
    /**
     * @param array $array
     * @return User
     * @throws Exception
     */
    public function userFromAssociativeArray(array $array): User
    {
        return new User(
            (int) $array['id'],
            (string) $array['email'],
            new DateTime($array['created_at']),
            $array['updated_at'] === null ? null : new DateTime($array['updated_at']),
            $array['username'],
            $array['password_hash']
        );
    }

    /**
     * @param array $array
     * @return Role
     */
    public function roleFromAssociativeArray(array $array): Role
    {
        return new Role(
            (string) $array['role'],
            (int) $array['id'],
            (int) $array['user_id']
        );
    }
}

<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use Mailgun\Hydrator\Hydrator;
use Mailgun\Model\ApiResponse;
use Psr\Http\Message\ResponseInterface;
use function call_user_func;
use function is_subclass_of;
use function json_decode;
use const JSON_THROW_ON_ERROR;

/**
 * Serialize an HTTP response to domain object.
 *
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
final class ModelHydrator implements Hydrator
{
    /** @noinspection ReturnTypeCanBeDeclaredInspection */
    /**
     * @param ResponseInterface $response
     * @param string $class
     *
     * @return ResponseInterface
     */
    public function hydrate(ResponseInterface $response, $class)
    {
        $body = (string)$response->getBody();

        $data = json_decode($body, true, JSON_THROW_ON_ERROR);

        if (is_subclass_of($class, ApiResponse::class)) {
            $object = call_user_func($class . '::create', $data);
        } else {
            $object = new $class($data);
        }

        return $object;
    }
}

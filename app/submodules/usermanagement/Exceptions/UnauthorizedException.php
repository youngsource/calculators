<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Exceptions;

use RuntimeException;

/**
 * Class UnauthorizedException
 * @package Laudis\Calculators\UserManagement
 */
final class UnauthorizedException extends RuntimeException
{

}

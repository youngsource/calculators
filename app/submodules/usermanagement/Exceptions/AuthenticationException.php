<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Exceptions;

use RuntimeException;

/**
 * Class UnauthenticatedException
 * @package Laudis\Calculators
 */
final class AuthenticationException extends RuntimeException
{

}

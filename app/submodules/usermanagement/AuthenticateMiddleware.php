<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use function getenv;
use Laudis\UserManagement\Exceptions\AuthenticationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;

/**
 * Class CheckJwtToken
 * @package Laudis\Calculators\UserManagement
 */
final class AuthenticateMiddleware implements MiddlewareInterface
{
    /** @var UserManager */
    private $manager;

    /**
     * AuthenticateMiddleware constructor.
     * @param UserManager $manager
     */
    public function __construct(UserManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * Process an incoming server request.
     *
     * Processes an incoming server request in order to produce a response.
     * If unable to produce the response itself, it may delegate to the provided
     * request handler to do so.
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        if ($this->manager->getLoggedInUser() === null && getenv('APP_AUTH_ON') === 'true') {
            throw new AuthenticationException('User is not authenticated');
        }
        return $handler->handle($request);
    }
}

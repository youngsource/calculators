<?php
declare(strict_types=1);

namespace Laudis\UserManagement\Validation;

use Exception;
use Jose\Component\Core\JWK;
use Jose\Component\Signature\JWSVerifier;
use Jose\Component\Signature\Serializer\JWSSerializer;
use const JSON_THROW_ON_ERROR;
use PDO;
use Rakit\Validation\Rule;
use function time;

/**
 * Class ResetPasswordRule
 * @package Laudis\UserManagement\Validation
 */
final class ResetPasswordRule extends Rule
{
    /**
     * @var JWSSerializer
     */
    private $serializer;
    /**
     * @var JWSVerifier
     */
    private $verifier;
    /**
     * @var JWK
     */
    private $serverToken;
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * ResetPasswordRule constructor.
     * @param JWSSerializer $serializer
     */
    public function __construct(JWSSerializer $serializer, JWSVerifier $verifier, JWK $serverToken, PDO $pdo)
    {
        $this->serializer = $serializer;
        $this->verifier = $verifier;
        $this->serverToken = $serverToken;
        $this->setMessage('De reset link is verlopen');
        $this->pdo = $pdo;
    }

    /**
     * @param $value
     * @return bool
     * @throws Exception
     */
    public function check($value): bool
    {
        $value = (string) $value;
        $token = $this->serializer->unserialize($value);

        if (!$this->verifier->verifyWithKey($token, $this->serverToken, 0)) {
            $this->setMessage('De reset link is ongeldig');
            return false;
        }
        $payload = json_decode($token->getPayload(), true, JSON_THROW_ON_ERROR);
        if ( !isset($payload['type']) || !($payload['type'] === 'reset')) {
            $this->setMessage('Dit is geen reset token.');
            return false;
        }
        if ($this->pdo->query("SELECT used FROM password_resets WHERE token = '$value'")->fetchColumn() !== '0') {
            $this->setMessage('Deze reset token is al gebruikt');
            return false;
        }
        return $payload['exp'] > time();
    }
}

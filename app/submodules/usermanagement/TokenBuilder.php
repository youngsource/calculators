<?php
declare(strict_types=1);

namespace Laudis\UserManagement;

use DateTime;
use Exception;
use Jose\Component\Core\JWK;
use Jose\Component\Core\Util\JsonConverter;
use Jose\Component\Signature\JWS;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Serializer\JWSSerializer;
use function array_merge;
use function getenv;
use function time;

/**
 * Class TokenBuilder
 * @package Laudis\UserManagement
 */
final class TokenBuilder
{
    /**
     * @var JWK
     */
    private $privateKey;
    /**
     * @var JWSBuilder
     */
    private $builder;
    /**
     * @var array
     */
    private $basicTokenPayload;
    /**
     * @var JWSSerializer
     */
    private $serializer;

    /**
     * TokenBuilder constructor.
     * @param JWK $privateKey
     * @param JWSBuilder $builder
     * @throws Exception
     */
    public function __construct(
        JWK $privateKey,
        JWSBuilder $builder,
        JWSSerializer $serializer,
        array $basicTokenPayload = null
    ) {
        $this->privateKey = $privateKey;
        $this->builder = $builder;
        $this->serializer = $serializer;

        $this->basicTokenPayload = $basicTokenPayload ?? [
            'iat' => time(),
            'nbf' => time(),
            'exp' => (new DateTime('+1 hour'))->getTimestamp(),
            'iss' => getenv('APP_NAME'),
            'aud' => getenv('APP_AUD')
        ];
    }

    /**
     * @param array $payload
     * @throws Exception
     */
    public function signedToken(array $payload): JWS
    {
        return $this->builder->create()
            ->withPayload(JsonConverter::encode(array_merge($this->basicTokenPayload, $payload)))
            ->addSignature($this->privateKey, ['alg' => 'HS256'])
            ->build();
    }

    /**
     * @param array $payload
     * @return string
     * @throws Exception
     */
    public function serializedSignedToken(array $payload): string
    {
        return $this->serializeToken($this->signedToken($payload));
    }

    /**
     * @param JWS $jws
     * @return string
     * @throws Exception
     */
    public function serializeToken(JWS $jws): string
    {
        return $this->serializer->serialize($jws, 0);
    }

    /**
     * @param string $token
     * @return JWS
     * @throws Exception
     */
    public function unserializeToken(string $token): JWS
    {
        return $this->serializer->unserialize($token);
    }
}

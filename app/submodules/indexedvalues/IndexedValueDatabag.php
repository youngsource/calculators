<?php
declare(strict_types=1);

namespace Laudis\IndexApi;

use DateTimeInterface;

/**
 * Class IndexedValueDatabag
 * @package Laudis\IndexApi
 */
final class IndexedValueDatabag
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $contextualId;
    /**
     * @var float
     */
    private $value;
    /**
     * @var string
     */
    private $name;
    /**
     * @var DateTimeInterface
     */
    private $createdAt;
    /**
     * @var DateTimeInterface|null
     */
    private $updatedAt;
    /**
     * @var DateTimeInterface
     */
    private $addedSince;
    /**
     * @var DateTimeInterface|null
     */
    private $removedSince;
    /**
     * @var string
     */
    private $type;

    /**
     * IndexedValueDatabag constructor.
     * @param int $id
     * @param int $contextualId
     * @param float $value
     * @param string $name
     * @param DateTimeInterface $createdAt
     * @param DateTimeInterface|null $updatedAt
     * @param DateTimeInterface $addedSince
     * @param DateTimeInterface|null $removedSince
     */
    public function __construct(
        int $id,
        int $contextualId,
        float $value,
        string $name,
        string $type,
        DateTimeInterface $createdAt,
        ?DateTimeInterface $updatedAt,
        DateTimeInterface $addedSince,
        ?DateTimeInterface $removedSince
    ){
        $this->id = $id;
        $this->contextualId = $contextualId;
        $this->value = $value;
        $this->name = $name;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->addedSince = $addedSince;
        $this->removedSince = $removedSince;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getContextualId(): int
    {
        return $this->contextualId;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return DateTimeInterface
     */
    public function getCreatedAt(): DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @return DateTimeInterface
     */
    public function getAddedSince(): DateTimeInterface
    {
        return $this->addedSince;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getRemovedSince(): ?DateTimeInterface
    {
        return $this->removedSince;
    }
}

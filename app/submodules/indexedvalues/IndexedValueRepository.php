<?php
declare(strict_types=1);

namespace Laudis\IndexApi;

use Closure;
use DateTimeInterface;
use Exception;
use Laudis\Common\Paginator;
use Laudis\IndexApi\Contracts\IndexedValueRepositoryInterface;
use PDO;
use function array_map;
use function date_to_string_query;
use function Laudis\Common\contextualizeFromAssociativeArray;

/**
 * Class IndexedValueRepository
 * @package Laudis\IndexApi
 */
final class IndexedValueRepository implements IndexedValueRepositoryInterface
{
    /** @var PDO */
    private $pdo;
    /**
     * @var IndexedValueFactory
     */
    private $factory;
    /**
     * @var ContextualQueryGenerator
     */
    private $idGenerator;

    /**
     * IndexedValueRepository constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo, IndexedValueFactory $factory, ContextualQueryGenerator $idGenerator)
    {
        $this->pdo = $pdo;
        $this->factory = $factory;
        $this->idGenerator = $idGenerator;
    }

    /**
     * @return Paginator
     */
    public function all(?int $pageCount = 25): Paginator
    {
        $selectStatement = 'SELECT * FROM indexed_values';
        $countStatement = 'SELECT count(*) FROM indexed_values';

        return $this->factory->paginator($selectStatement, $countStatement, $pageCount ?? 25);
    }

    /**
     * @param string $name
     * @param float $value
     * @param DateTimeInterface $addedSince
     * @param DateTimeInterface|null $removedSince
     * @param int|null $contextualId
     * @return int the id of the inserted object.
     */
    public function insert(
        string $name,
        float $value,
        string $type,
        DateTimeInterface $addedSince,
        ?DateTimeInterface $removedSince,
        ?int $contextualId
    ): int {
        $contextualId = $this->generateContextualIdIfNeeded($contextualId);

        $removed = date_to_string_query($removedSince);
        $added = $addedSince->format('Y-m-d');

        $this->pdo->exec("
            INSERT INTO indexed_values
            (name, value, added_since, removed_since, contextual_id, type)
            VALUES ('$name', '$value', '$added', $removed, '$contextualId', '$type')
        ");
        return (int)$this->pdo->lastInsertId();
    }

    /**
     * @param int|null $contextualId
     * @return int
     */
    private function generateContextualIdIfNeeded(?int $contextualId): int
    {
        return $contextualId ?? $this->idGenerator->generate('indexed_values');
    }

    /**
     * @param int $contextualId
     * @param DateTimeInterface $context
     * @return IndexedValueDatabag|null
     * @throws Exception
     */
    public function findByContext(int $contextualId, DateTimeInterface $context): ?IndexedValueDatabag
    {
        $items = $this->pdo->query("SELECT * FROM indexed_values WHERE contextual_id = '$contextualId'")
            ->fetchAll(PDO::FETCH_ASSOC);

        $item = contextualizeFromAssociativeArray($items, $context);

        return ($item === null) ? null : $this->factory->makeFromAssociativeArray($item);
    }

    /**
     * @param int $id
     * @return IndexedValueDatabag|null
     * @throws Exception
     */
    public function find(int $id): ?IndexedValueDatabag
    {
        $item = $this->pdo->query("SELECT * FROM indexed_values WHERE id = '$id'")->fetch(PDO::FETCH_ASSOC);

        return ($item === null) ? null : $this->factory->makeFromAssociativeArray($item);
    }

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void
    {
        $this->pdo->exec("DELETE FROM indexed_values WHERE id = '$id'");
    }

    /**
     * @param int $contextualId
     * @return array
     */
    public function getAllOfContextualId(int $contextualId): array
    {
        $values = $this->pdo->query("SELECT * FROM indexed_values WHERE contextual_id = '$contextualId'")
            ->fetchAll(PDO::FETCH_ASSOC);

        return array_map(function (array $values) {
            return $this->factory->makeFromAssociativeArray($values);
        }, $values);
    }

    /**
     * @param IndexedValueDatabag $databag
     */
    public function update(IndexedValueDatabag $databag): void
    {
        $id = $databag->getId();
        $addedSince = $databag->getAddedSince()->format('Y-m-d');
        $removedSince = date_to_string_query($databag->getRemovedSince());
        $value = $databag->getValue();
        $name = $databag->getName();
        $contextualId = $databag->getContextualId();
        $type = $databag->getType();

        $this->pdo->exec("
            UPDATE indexed_values
            SET     added_since = '$addedSince',
                    removed_since =  $removedSince,
                    value = '$value',
                    name = '$name',
                    contextual_id = '$contextualId',
                    updated_at = CURRENT_TIMESTAMP,
                    type = '$type'
            WHERE id = '$id'
        ");
    }

    /**
     * @param string $query
     * @return Paginator
     */
    public function search(string $query, int $resultsPerPage = 25): Paginator
    {
        $whereClause = "WHERE   `id` LIKE '%$query%' OR
                                `contextual_id` LIKE '%$query%' OR
                                `name` LIKE '%$query%' OR
                                `value` LIKE '%$query%' OR
                                `type` LIKE '%$query%' OR
                                `added_since` LIKE '%$query%' OR
                                `removed_since` LIKE '%$query%' OR
                                `created_at` LIKE '%$query%' OR
                                `updated_at` LIKE '%$query%'
                        ORDER BY    case
                            WHEN `value` LIKE '%$query%' THEN 1
                            WHEN `name` LIKE '%$query%' THEN 2
                            WHEN `type` LIKE '%$query%' THEN 3
                            WHEN `id` LIKE '%$query%' THEN 4
                            WHEN `contextual_id` LIKE '%$query%' THEN 5
                            ELSE 6
                        END
        ";

        return new Paginator(
            $this->pdo,
            "SELECT * FROM indexed_values $whereClause",
            $resultsPerPage,
            "SELECT COUNT(*) FROM indexed_values $whereClause",
            Closure::fromCallable([$this->factory, 'makeFromAssociativeArray'])
        );
    }

}

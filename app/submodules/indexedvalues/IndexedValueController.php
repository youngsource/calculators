<?php /** @noinspection PhpUnusedParameterInspection */
/** @noinspection NullPointerExceptionInspection */
declare(strict_types=1);

namespace Laudis\IndexApi;

use DateTime;
use Exception;
use Laudis\Common\Contracts\ResponseWriterInterface;
use Laudis\Common\Exceptions\ResourceNotFoundException;
use Laudis\Common\Exceptions\ValidationException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Rakit\Validation\Validation;

/**
 * Class IndexedValueController
 * @package Laudis\IndexApi
 */
final class IndexedValueController
{
    /** @var ResponseWriterInterface */
    private $writer;
    /** @var IndexedValueFactory */
    private $factory;

    /**
     * IndexedValueController constructor.
     * @param ResponseWriterInterface $writer
     * @param IndexedValueFactory $factory
     */
    public function __construct(ResponseWriterInterface $writer, IndexedValueFactory $factory)
    {
        $this->writer = $writer;
        $this->factory = $factory;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody() ?: [];

        $validation = $this->factory->indexValidation($body);
        $this->guardValidation($validation);

        $limit = $body['pageSize'] ?? null;
        $current = $body['currentPage'] ?? 1;

        $presenter = $this->factory->paginationPresenter();
        return $this->writer->write(
            $response,
            $presenter->present($this->factory->repository()->all($limit), $current)
        );
    }

    /**
     * @param Validation $validation
     */
    private function guardValidation(Validation $validation): void
    {
        if ($validation->fails()) {
            throw new ValidationException($validation);
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws Exception
     */
    public function update(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();

        $validation = $this->factory->updateValidation($body);
        $this->guardValidation($validation);

        $repository = $this->factory->repository();
        $value = $repository->find((int) $body['id']);
        $this->guard404($value, $body);

        $value = $this->factory->updateFromAssociativeArray($value, $body);
        $repository->update($value);

        return $this->writer->write($response, [
            'message' => 'Succesfully updated index with id: ' . $value->getId()
        ]);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function delete(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $params = $request->getParsedBody();

        $validation = $this->factory->deleteValidation($params);
        $this->guardValidation($validation);

        $id = (int)$params['id'];
        $this->factory->repository()->delete($id);

        return $this->writer->write($response, [
            'message' => "Index with id: $id succesfully deleted."
        ]);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @param array $params
     * @return ResponseInterface
     * @throws Exception
     */
    public function show(ServerRequestInterface $request, ResponseInterface $response, array $params): ResponseInterface
    {
        $validation = $this->factory->showValidation($params);
        $this->guardValidation($validation);

        $databag = $this->factory->repository()->find((int) $params['id']);
        $this->guard404($databag, $params);

        return $this->writer->write($response, $this->factory->presenter()->present($databag));
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function create(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();

        $validation = $this->factory->createValidation($body);
        $this->guardValidation($validation);

        $contextualId = $body['contextualId'];
        $contextualId = $contextualId !== null ? (int) $contextualId : null;
        $id = $this->factory->repository()->insert(
            (string) $body['name'],
            (float) $body['value'],
            (string) $body['type'],
            DateTime::createFromFormat('Y-m-d', $body['addedSince']),
            $body['removedSince'] === null ? null : DateTime::createFromFormat('Y-m-d', $body['removedSince']),
            $contextualId
        );

        return $this->writer->write($response, [
            'message' => "sucessfully created indexed value with id: $id"
        ]);
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function search(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $body = $request->getParsedBody();

        $validation = $this->factory->searchValidation($body);
        $this->guardValidation($validation);

        $query = $body['query'];
        $page = $body['currentPage'] ?? 1;
        $pageSize = $body['pageSize'] ?? 25;

        $values = $this->factory->paginationPresenter()->present(
            $this->factory->repository()->search($query, $pageSize),
            $page
        );

        return $this->writer->write($response, $values);
    }

    /**
     * @param IndexedValueDatabag|null $value
     * @param $body
     */
    private function guard404(?IndexedValueDatabag $value, $body): void
    {
        if ($value === null) {
            throw new ResourceNotFoundException("Cannot find value with id: ${body['id']}");
        }
    }
}

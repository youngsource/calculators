<?php
declare(strict_types=1);

namespace Laudis\IndexApi;

/**
 * Class IndexedValuePresenter
 * @package Laudis\IndexApi
 */
final class IndexedValuePresenter
{
    /**
     * @param IndexedValueDatabag $databag
     * @return array
     */
    public function present(IndexedValueDatabag $databag): array
    {
        return [
            'id' => $databag->getId(),
            'contextualId' => $databag->getContextualId(),
            'value' => $databag->getValue(),
            'name' => $databag->getName(),
            'type' => $databag->getType(),
            'createdAt' => $databag->getCreatedAt(),
            'updatedAt' => $databag->getUpdatedAt(),
            'addedSince' => $databag->getAddedSince(),
            'removedSince' => $databag->getRemovedSince()
        ];
    }
}

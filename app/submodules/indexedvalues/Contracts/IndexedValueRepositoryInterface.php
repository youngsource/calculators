<?php
declare(strict_types=1);

namespace Laudis\IndexApi\Contracts;

use DateTimeInterface;
use Exception;
use Laudis\Common\Paginator;
use Laudis\IndexApi\IndexedValueDatabag;

/**
 * Interface IndexedValueRepositoryInterface
 * @package Laudis\IndexApi\Contracts
 */
interface IndexedValueRepositoryInterface
{
    /**
     * @return Paginator
     */
    public function all(?int $pageCount = 25): Paginator;

    /**
     * @param string $name
     * @param float $value
     * @param DateTimeInterface $addedSince
     * @param DateTimeInterface|null $removedSince
     * @param int|null $contextualId
     * @return int the id of the inserted object.
     */
    public function insert(
        string $name,
        float $value,
        string $type,
        DateTimeInterface $addedSince,
        ?DateTimeInterface $removedSince,
        ?int $contextualId
    ): int;

    /**
     * @param int $contextualId
     * @param DateTimeInterface $context
     * @return IndexedValueDatabag|null
     * @throws Exception
     */
    public function findByContext(int $contextualId, DateTimeInterface $context): ?IndexedValueDatabag;

    /**
     * @param int $id
     * @return IndexedValueDatabag|null
     * @throws Exception
     */
    public function find(int $id): ?IndexedValueDatabag;

    /**
     * @param int $id
     * @return void
     */
    public function delete(int $id): void;

    /**
     * @param int $contextualId
     * @return array
     */
    public function getAllOfContextualId(int $contextualId): array;

    /**
     * @param IndexedValueDatabag $databag
     */
    public function update(IndexedValueDatabag $databag): void;

    /**
     * Searches the indexed value fields.
     *
     * @param string $query
     * @return Paginator
     */
    public function search(string $query, int $resultsPerPage = 25): Paginator;
}

<?php
declare(strict_types=1);

namespace Laudis\IndexApi;

use Closure;
use DateTime;
use Exception;
use Laudis\Common\Paginator;
use Laudis\Common\Rules\ContextualInsertionRule;
use Laudis\Common\Rules\DateHigherThan;
use Laudis\Common\Rules\ExistsRule;
use PDO;
use Rakit\Validation\Rules\Date;
use Rakit\Validation\Rules\Integer;
use Rakit\Validation\Rules\Min;
use Rakit\Validation\Rules\Numeric;
use Rakit\Validation\Rules\Present;
use Rakit\Validation\Rules\Required;
use Rakit\Validation\Validation;
use Rakit\Validation\Validator;

/**
 * Class IndexedValueFactory
 * @package Laudis\IndexApi
 */
final class IndexedValueFactory
{
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * IndexedValueFactory constructor.
     * @param Validator $validator
     * @param PDO $pdo
     */
    public function __construct(Validator $validator, PDO $pdo)
    {
        $this->validator = $validator;
        $this->pdo = $pdo;
    }

    /**
     * @return IndexedValueRepository
     */
    public function repository(): IndexedValueRepository
    {
        return new IndexedValueRepository($this->pdo, $this, new ContextualQueryGenerator($this->pdo));
    }

    /**
     * @param array $values
     * @return Validation
     */
    public function showValidation(array $values): Validation
    {
        return $this->deleteValidation($values);
    }

    /**
     * @param array $values
     * @return Validation
     */
    public function deleteValidation(array $values): Validation
    {
        return $this->validator->validate($values, [
            'id' => [new Required, new ExistsRule($this->pdo, 'indexed_values', 'id', 'Het geindexeerd bedrag werd niet gevonden')]
        ]);
    }

    /**
     * @return IndexedValuePaginationPresenter
     */
    public function paginationPresenter(): IndexedValuePaginationPresenter
    {
        return new IndexedValuePaginationPresenter($this->presenter());
    }

    /**
     * @return IndexedValuePresenter
     */
    public function presenter(): IndexedValuePresenter
    {
        return new IndexedValuePresenter();
    }

    /**
     * @param array $values
     * @return Validation
     */
    public function indexValidation(array $values): Validation
    {
        return $this->validator->validate($values, [
            'pageSize' => [new Integer],
            'currentPage' => [new Integer]
        ]);
    }

    /**
     * @param array $values
     * @return Validation
     */
    public function createValidation(array $values): Validation
    {
        return $this->validator->validate($values, [
            'contextualId' => [new Integer, new ContextualInsertionRule($this->pdo, 'indexed_values')],
            'value' => [new Required, new Numeric],
            'type' => [new Required],
            'name' => [new Present],
            'addedSince' => [new Required, new Date],
            'removedSince' => [new Date, new DateHigherThan('addedSince')]
        ]);
    }

    /**
     * @param array $values
     * @return Validation
     */
    public function updateValidation(array $values): Validation
    {
        return $this->validator->validate($values, [
            'id' => [new Required, new ExistsRule($this->pdo, 'indexed_values', 'id', 'Het geindexeerd bedrag werd niet gevonden')],
            'contextualId' => [new Integer, new ContextualInsertionRule($this->pdo, 'indexed_values')],
            'value' => [new Numeric],
            'name' => [],
            'type' => [],
            'addedSince' => [new Date],
            'removedSince' => [new Date, new DateHigherThan('addedSince')]
        ]);
    }

    /**
     * @param array $value
     * @return IndexedValueDatabag
     * @throws Exception
     */
    public function makeFromAssociativeArray(array $value): IndexedValueDatabag
    {
        return new IndexedValueDatabag(
            (int)$value['id'],
            (int)$value['contextual_id'],
            (float)$value['value'],
            (string)$value['name'],
            (string)$value['type'],
            new DateTime($value['created_at']),
            $value['updated_at'] === null ? null : new DateTime($value['updated_at']),
            DateTime::createFromFormat('Y-m-d', $value['added_since']),
            $value['removed_since'] === null ? null : DateTime::createFromFormat('Y-m-d', $value['removed_since'])
        );
    }

    /**
     * @param array $values
     * @return IndexedValueDatabag
     * @throws Exception
     */
    public function updateFromAssociativeArray(IndexedValueDatabag $databag, array $values): IndexedValueDatabag
    {
        if (isset($values['removed_since'])) {
            $removedSince = $values['removed_since'] === null
                ? null : DateTime::createFromFormat('Y-m-d', $values['removed_since']);
        } else {
            $removedSince = $databag->getRemovedSince();
        }

        return new IndexedValueDatabag(
            $databag->getId(),
            (int)($values['contextualId'] ?? $databag->getContextualId()),
            (float)($values['value'] ?? $databag->getValue()),
            (string)($values['name'] ?? $databag->getName()),
            (string)($values['type'] ?? $databag->getType()),
            $databag->getCreatedAt(),
            new DateTime,
            DateTime::createFromFormat('Y-m-d', $values['addedSince'] ?? $databag->getAddedSince()->format('Y-m-d')),
            $removedSince
        );
    }

    /**
     * @param string $selectQuery
     * @param string $countQuery
     * @return Paginator
     */
    public function paginator(string $selectQuery, string $countQuery, int $resultsPerPage = 25): Paginator
    {
        return new Paginator(
            $this->pdo,
            $selectQuery,
            $resultsPerPage,
            $countQuery,
            Closure::fromCallable([$this, 'makeFromAssociativeArray'])
        );
    }

    /**
     * @param array $body
     * @return Validation
     */
    public function searchValidation(array $body): Validation
    {
        $min = new Min;
        $min->setParameter('min', 1);
        return $this->validator->validate($body, [
            'query' => [new Required, $min],
            'pageSize' => [new Integer, $min],
            'currentPage' => [new Integer, $min]
        ]);
    }
}

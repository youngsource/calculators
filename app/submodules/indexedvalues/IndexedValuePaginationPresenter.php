<?php
declare(strict_types=1);

namespace Laudis\IndexApi;

use Laudis\Common\Paginator;

/**
 * Class IndexedValuePaginationPresenter
 * @package Laudis\IndexApi
 */
final class IndexedValuePaginationPresenter
{
    /**
     * @var IndexedValuePresenter
     */
    private $presenter;

    /**
     * IndexedValuePaginationPresenter constructor.
     * @param IndexedValuePresenter $presenter
     */
    public function __construct(IndexedValuePresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    /**
     * @param Paginator $paginator
     * @param int $page
     * @return array
     */
    public function present(Paginator $paginator, int $page): array
    {
        $data = $paginator->get($page);
        return [
            'data' => array_map(function (IndexedValueDatabag $databag) {
                return $this->presenter->present($databag);
            }, $data),

            'page' => $page,
            'pageCount' => $paginator->pageCount(),
            'totalElement' => $paginator->totalCount(),
            'lowestPageElement' => $paginator->startLimit($page),
            'highestPageElement' => $paginator->startLimit($page)
        ];
    }
}

<?php
declare(strict_types=1);

namespace Laudis\IndexApi;

use PDO;

/**
 * Class ContextualIdGenerator
 * @package Laudis\IndexApi
 */
final class ContextualQueryGenerator
{
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * ContextualIdGenerator constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param string $table
     * @param string $identifier
     * @return int
     */
    public function generate(string $table, string $identifier = 'contextual_id'): int
    {
        return ((int)$this->pdo->query("SELECT MAX($identifier) FROM  $table")->fetchColumn()) + 1;
    }
}
